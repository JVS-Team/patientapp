import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share_plus/share_plus.dart';
import 'package:softcliniccare/Helper/utils.dart';

import '../Helper/Constant.dart';
import '../Helper/Database/sqflite_helper.dart';
import '../Model/Dashboard/Dashboard_API.dart';
import '../Model/Dashboard/Facility_Name_Model.dart';
import '../Model/Invoice/API_PaymentHistory.dart';
import '../Model/Invoice/ApplicationVisitType.dart';
import '../Model/Invoice/Invoice_Details.dart';
import '../Model/Invoice/PaidList.dart';
import 'PDF/FileProcess.dart';
import 'PDF/PDF_File_Display.dart';

class PaidPage extends StatefulWidget {
  @override
  PaidMainPage createState() => PaidMainPage();
}

class PaidMainPage extends State<PaidPage> {
  String checkdevice = '';

  List<billingList> listing_Payment = [];
  List<visitList> lisitng_visitType = [];

  bool needToDisplayCircle = true;
  bool nowDisplayNoRecordFound = true;

  List<Facility_Details> listing_FacilityName = [];

  @override
  void initState() {
    checkdevice = Utility.getDeviceType();
    ApplicationVisitType();
    FacilityName_Details();
    super.initState();
  }

  void FacilityName_Details() async {
    String AccessToken = await Utility.readAccessToken('Token');

    final data = await SQLHelper.getAll_Facility_Response(
        ConstantVar.str_Common_GETALLFACILITYNAME);
    List<Map<String, dynamic>> allFacilityResponse = data;
    String mainFacilityResponse = "";

    if (allFacilityResponse.length > 0) {
      for (int i = 0; i < allFacilityResponse.length; i++) {
        if (allFacilityResponse[i]['FacilityID'] ==
            ConstantVar.str_Common_GETALLFACILITYNAME) {
          mainFacilityResponse = allFacilityResponse[i]['FacilityResponse'];
          break;
        } else {}
      }
      if (mainFacilityResponse == "") {
        API_Dashboard()
            .Dashboard_Facilityname(AccessToken)
            .then((Response) async {
          if (allFacilityResponse.length > 0) {
            AllFacilityUpdate(ConstantVar.str_Common_GETALLFACILITYNAME,
                json.encode(Response));
          } else {
            AllFacilityInsert(ConstantVar.str_Common_GETALLFACILITYNAME,
                json.encode(Response));
          }
          FacilityMainResponse(Response);
        });
      } else {
        FacilityMainResponse(json.decode(mainFacilityResponse));
      }
    } else {
      API_Dashboard()
          .Dashboard_Facilityname(AccessToken)
          .then((Response) async {
        if (allFacilityResponse.length > 0) {
          AllFacilityUpdate(
              ConstantVar.str_Common_GETALLFACILITYNAME, json.encode(Response));
        } else {
          AllFacilityInsert(
              ConstantVar.str_Common_GETALLFACILITYNAME, json.encode(Response));
        }
        FacilityMainResponse(Response);
      });
    }
  }

  void FacilityMainResponse(var Response) {
    log("Main Facility ${Response}");
    if (mounted) {
      setState(() {
        var tempdata = Response['value'];
        for (int i = 0; i < tempdata.length; i++) {
          listing_FacilityName.add(Facility_Details.fromJson(tempdata[i]));
        }
      });
    } else {
      var tempdata = Response['value'];
      for (int i = 0; i < tempdata.length; i++) {
        listing_FacilityName.add(Facility_Details.fromJson(tempdata[i]));
      }
    }
  }

  void AllFacilityInsert(String FacilityID, String FacilityResponse) async {
    await SQLHelper.insertName_Facility(FacilityID, FacilityResponse);
  }

  void AllFacilityUpdate(String FacilityID, String FacilityResponse) async {
    await SQLHelper.update_facility_response(FacilityID, FacilityResponse);
  }

  void ApplicationVisitType() {
    API_PaymentList().AppointmentVisitTypeDetails().then((mainResponse) async {
      setState(() {
        // log('List Response ${mainResponse}');
        lisitng_visitType = [];
        var resBody = json.decode(mainResponse);

        VisitTypeDetails visitListData = new VisitTypeDetails.fromJson(resBody);

        lisitng_visitType = visitListData.visitTypelist;
        API_of_Paid();
      });
    });
  }

  void API_of_Paid() async {
    String MainPatientID = await Utility.readAccessToken('PatientID');
    print('Patient ID ${MainPatientID}');
    // Utility.showLoadingDialog(context, _AppointmentList, true);

    final data = await SQLHelper.getAll_AppointmentList(
        MainPatientID, ConstantVar.str_Common_GETPAID_INVOICE);
    List<Map<String, dynamic>> allAppointmentList = data;
    String mainAppointmentResponse = "";

    if (allAppointmentList.length > 0) {
      for (int i = 0; i < allAppointmentList.length; i++) {
        if (allAppointmentList[i]['PatientID'] == MainPatientID) {
          mainAppointmentResponse =
              allAppointmentList[i]['AppointmentResponse'];
          break;
        } else {}
      }
      if (mainAppointmentResponse == "") {
        API_PaymentList()
            .Payment_List_All(MainPatientID)
            .then((mainResponse) async {
          setState(() {
            if (allAppointmentList.length > 0) {
              AllAppointmentUpdate(MainPatientID,
                  ConstantVar.str_Common_GETPAID_INVOICE, mainResponse);
            } else {
              AllAppointmentInsert(MainPatientID,
                  ConstantVar.str_Common_GETPAID_INVOICE, mainResponse);
            }
            InvoiceResponse(mainResponse);
          });
        });
      } else {
        // log("Main Appointment Response ${mainAppointmentResponse}");
        InvoiceResponse(mainAppointmentResponse);
      }
    } else {
      API_PaymentList()
          .Payment_List_All(MainPatientID)
          .then((mainResponse) async {
        setState(() {
          if (allAppointmentList.length > 0) {
            AllAppointmentUpdate(MainPatientID,
                ConstantVar.str_Common_GETPAID_INVOICE, mainResponse);
          } else {
            AllAppointmentInsert(MainPatientID,
                ConstantVar.str_Common_GETPAID_INVOICE, mainResponse);
          }
          InvoiceResponse(mainResponse);
        });
      });
    }
  }

  void InvoiceResponse(String mainResponse) {
    setState(() {
      if (mainResponse == "Error") {
      } else {
        listing_Payment = [];
        var resBody = json.decode(mainResponse);

        PaymentAllListData paymentListData =
            new PaymentAllListData.fromJson(resBody);

        double dueAmount = 0.0;
        for (int i = 0; i < paymentListData.paymentList.length; i++) {
          dueAmount =
              double.parse(paymentListData.paymentList[i].inoviceDueAmount);

          if (dueAmount == 0.0) {
            print("Due ${dueAmount}");
            listing_Payment.add(paymentListData.paymentList[i]);
          }
        }

        if (listing_Payment.length > 0) {
          needToDisplayCircle = false;
          nowDisplayNoRecordFound = true;
        } else {
          needToDisplayCircle = true;
          nowDisplayNoRecordFound = false;
        }
      }
    });
  }

  void AllAppointmentInsert(String PatientID, String UpComing_Past,
      String AppointmentResponse) async {
    await SQLHelper.insert_Appointment_Details(
        PatientID, AppointmentResponse, UpComing_Past);
  }

  void AllAppointmentUpdate(String PatientID, String UpComing_Past,
      String AppointmentResponse) async {
    await SQLHelper.update_Appointment_response(
        PatientID, AppointmentResponse, UpComing_Past);
  }

  // void API_of_Paid() async {
  //   String PatientID = await Utility.readAccessToken('PatientID');
  //   API_PaymentList().Payment_List_All(PatientID).then((mainResponse) async {
  //     setState(() {
  //       // log('List Response ${mainResponse}');
  //
  //       if (mainResponse == "Error") {
  //       } else {
  //         listing_Payment = [];
  //         var resBody = json.decode(mainResponse);
  //
  //         PaymentAllListData paymentListData =
  //             new PaymentAllListData.fromJson(resBody);
  //
  //         double dueAmount = 0.0;
  //         for (int i = 0; i < paymentListData.paymentList.length; i++) {
  //           dueAmount =
  //               double.parse(paymentListData.paymentList[i].inoviceDueAmount);
  //
  //           if (dueAmount == 0.0) {
  //             print("Due ${dueAmount}");
  //             listing_Payment.add(paymentListData.paymentList[i]);
  //           }
  //         }
  //
  //         if (listing_Payment.length > 0) {
  //           needToDisplayCircle = false;
  //           nowDisplayNoRecordFound = true;
  //         } else {
  //           needToDisplayCircle = true;
  //           nowDisplayNoRecordFound = false;
  //         }
  //       }
  //     });
  //   });
  // }

  void GetNotificationofAPI(String InvoiceID, String view_share) async {
    final data = await SQLHelper.getAll_PDF_Response(InvoiceID, "invoice");
    List<Map<String, dynamic>> allPDFResponse = data;
    String mainPDFResponse = "";

    if (allPDFResponse.length > 0) {
      for (int i = 0; i < allPDFResponse.length; i++) {
        if (allPDFResponse[i]['invoiceID'] == InvoiceID) {
          mainPDFResponse = allPDFResponse[i]['invoiceResponse'];
          break;
        } else {}
      }
      if (mainPDFResponse == "") {
        API_PaymentList()
            .generateBilling_Notification(InvoiceID)
            .then((Response) async {
          if (allPDFResponse.length > 0) {
            AllDataUpdate(InvoiceID, Response.body);
          } else {
            AllDataInsert(InvoiceID, Response.body);
          }
          PDFResponseHandler(Response.body, view_share);
        });
      } else {
        PDFResponseHandler(mainPDFResponse, view_share);
      }
    } else {
      API_PaymentList()
          .generateBilling_Notification(InvoiceID)
          .then((Response) async {
        if (allPDFResponse.length > 0) {
          AllDataUpdate(InvoiceID, Response.body);
        } else {
          AllDataInsert(InvoiceID, Response.body);
        }
        PDFResponseHandler(Response.body, view_share);
      });
    }
  }

  void PDFResponseHandler(String response, String view_share) {
    var resBody = json.decode(response);

    mainInvoiceService mainInvoice = mainInvoiceService.fromJson(resBody);

    if (mainInvoice.invoiceIDDetails.length > 0) {
      // print("Detas ${mainInvoice.invoiceIDDetails[0].base64String}");

      String mainBase64Details = mainInvoice.invoiceIDDetails[0].base64String;
      _createFileFromString(mainBase64Details.split(',').last, view_share);
    }
  }

  void _createFileFromString(String PDFbase64, String View_Share) async {
    bool? result = await requestPermissionForStorageAndManageExtStorage();
    if (result) {
      final encodedStr = PDFbase64;

      if (View_Share == "1") {
        Future<Uint8List> mainPDFBytes = FileProcess.downloadFile(PDFbase64);
        Uint8List PDFContentBytes = await mainPDFBytes;
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  PDFFileDisplay(main8Bytes: PDFContentBytes)),
        );
      } else {
        Uint8List bytes = base64.decode(encodedStr);

        Directory appDocDir = await getApplicationDocumentsDirectory();
        String tempPath = appDocDir.path;
        log("Path ${tempPath}");
        var knockDir = await new Directory('${tempPath}/patientApp')
            .create(recursive: true);
        File file = File("${knockDir.path}/" +
            DateTime.now().millisecondsSinceEpoch.toString() +
            ".pdf");
        await file.writeAsBytes(bytes);
        log("Path ${file.path}");
        Share.shareFiles(['${file.path}'], text: 'Report');
      }
    } else {
      Utility.WholeAppSnackbar(context, 'Please accept permission.');
    }
  }

  // void _ImageDownloadAndSave(Uint8List bytes, String FileName,
  //     BuildContext context, String Share_Download) async {
  //   bool? result = await requestPermissionForStorageAndManageExtStorage();
  //
  //   if (result) {
  //     String dir = (await getApplicationDocumentsDirectory()).path;
  //     String fullPath = '$dir/' + FileName;
  //     print("local file full path ${fullPath}");
  //     File file = File(fullPath);
  //     await file.writeAsBytes(bytes);
  //     print(file.path);
  //
  //     // 1 Means Download
  //     // 2 Means Share
  //     if (Share_Download == "1") {
  //       final resultAll = await ImageGallerySaver.saveImage(bytes);
  //       print(resultAll['isSuccess']);
  //       if (resultAll['isSuccess']) {
  //         Utility.WholeAppSnackbar(context, 'File Download Successfully');
  //       } else {}
  //     } else {
  //       Share.shareFiles(['${fullPath}'], text: 'Great picture');
  //     }
  //   } else {}
  // }

  Future<bool> requestPermissionForStorageAndManageExtStorage() async {
    await [Permission.storage].request();
    final storagePermission = await Permission.storage.status;
    // final extPermission = await Permission.manageExternalStorage.status;
    // _log('Permissions => Microphone: $micPermission, Camera: $camPermission');

    if (storagePermission == PermissionStatus.granted) {
      return true;
    }

    if (storagePermission == PermissionStatus.denied) {
      return requestPermissionForStorageAndManageExtStorage();
    }

    if (storagePermission == PermissionStatus.permanentlyDenied) {
      log('Permissions => Opening App Settings');
      await openAppSettings();
    }

    return false;
  }

  void AllDataInsert(String invoiceid, String invoiceResponse) async {
    await SQLHelper.insert_PDF_Details(invoiceid, invoiceResponse, "invoice");
  }

  void AllDataUpdate(String invoiceid, String invoiceResponse) async {
    await SQLHelper.update_invoice_response(
        invoiceid, invoiceResponse, "invoice");
  }

  Future<void> _pullRefresh() async {
    print("Refresh");
    String MainPatientID = await Utility.readAccessToken('PatientID');
    print('Patient ID ${MainPatientID}');

    final data = await SQLHelper.getAll_AppointmentList(
        MainPatientID, ConstantVar.str_Common_GETPAID_INVOICE);
    List<Map<String, dynamic>> allAppointmentList = data;

    setState(() {
      API_PaymentList()
          .Payment_List_All(MainPatientID)
          .then((mainResponse) async {
        setState(() {
          if (allAppointmentList.length > 0) {
            AllAppointmentUpdate(MainPatientID,
                ConstantVar.str_Common_GETPAID_INVOICE, mainResponse);
          } else {
            AllAppointmentInsert(MainPatientID,
                ConstantVar.str_Common_GETPAID_INVOICE, mainResponse);
          }
          InvoiceResponse(mainResponse);
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: needToDisplayCircle == true
            ? nowDisplayNoRecordFound == true
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : RefreshIndicator(
                    onRefresh: _pullRefresh,
                    child: Container(
                      child: SingleChildScrollView(
                        physics: AlwaysScrollableScrollPhysics(),
                        child: Container(
                          child: Center(
                            child: Text('No Paid Found'),
                          ),
                          height: MediaQuery.of(context).size.height,
                        ),
                      ),
                    ),
                  )
            : RefreshIndicator(
                onRefresh: _pullRefresh,
                child: ListView.builder(
                  itemCount: listing_Payment.length,
                  itemBuilder: (BuildContext context, int pos) {
                    String serviceName = "";
                    for (int i = 0; i < listing_Payment.length; i++) {
                      for (int j = 0; j < lisitng_visitType.length; j++) {
                        if (listing_Payment[i].visitType ==
                            lisitng_visitType[j].id) {
                          serviceName = lisitng_visitType[j].displayName;
                        }
                      }
                    }

                    String FacilityName = "";
                    for (int i = 0; i < listing_FacilityName.length; i++) {
                      if (listing_FacilityName[i].id ==
                          listing_Payment[pos].facilityId) {
                        FacilityName = listing_FacilityName[i].displayName;
                      }
                    }

                    return GestureDetector(
                      child: Card(
                          elevation: 5,
                          margin: EdgeInsets.all(checkdevice == "1" ? 8 : 12),
                          color: Utility.PutBackgroundCardColor(),
                          child: Container(
                            height: checkdevice == "1" ? 160 : 260,
                            child: Column(
                              children: <Widget>[
                                new Expanded(
                                  flex: 3,
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(
                                        width: checkdevice == "1" ? 10 : 20,
                                      ),
                                      new Expanded(
                                          flex: 1,
                                          child: Container(
                                            child: Align(
                                              alignment: Alignment.topRight,
                                              child: IconButton(
                                                  onPressed: () {},
                                                  icon: SvgPicture.asset(
                                                    'Images/pdf_icon.svg',
                                                    color: Utility
                                                        .PutDarkBlueColor(),
                                                    height: checkdevice == "1"
                                                        ? 50
                                                        : 90,
                                                    width: checkdevice == "1"
                                                        ? 50
                                                        : 90,
                                                  ),
                                                  iconSize: checkdevice == "1"
                                                      ? 50
                                                      : 80),
                                            ),
                                          )),
                                      SizedBox(
                                        width: checkdevice == "1" ? 5 : 15,
                                      ),
                                      new Expanded(
                                        flex: 4,
                                        child: Column(
                                          children: <Widget>[
                                            SizedBox(
                                              height:
                                                  checkdevice == "1" ? 5 : 20,
                                            ),
                                            new Expanded(
                                                flex: 1,
                                                child: Align(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Utility
                                                      .MainNormalTextWithFont(
                                                          "Facility : " +
                                                              FacilityName,
                                                          checkdevice == "1"
                                                              ? 14
                                                              : 24,
                                                          Utility
                                                              .PutDarkBlueColor(),
                                                          'Poppins_Regular'),
                                                )),
                                            SizedBox(
                                              height:
                                                  checkdevice == "1" ? 5 : 20,
                                            ),
                                            new Expanded(
                                                flex: 1,
                                                child: Align(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Utility
                                                      .MainNormalTextWithFont(
                                                          "Service : " +
                                                              serviceName,
                                                          checkdevice == "1"
                                                              ? 14
                                                              : 24,
                                                          Utility
                                                              .PutDarkBlueColor(),
                                                          'Poppins_Regular'),
                                                )),
                                            SizedBox(
                                              height:
                                                  checkdevice == "1" ? 5 : 20,
                                            ),
                                            new Expanded(
                                                flex: 1,
                                                child: Align(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Utility
                                                      .MainNormalTextWithFont(
                                                          "Doctor : " +
                                                              listing_Payment[
                                                                      pos]
                                                                  .doctorName,
                                                          checkdevice == "1"
                                                              ? 14
                                                              : 24,
                                                          Utility
                                                              .PutDarkBlueColor(),
                                                          'Poppins_Regular'),
                                                )),
                                            SizedBox(
                                              height:
                                                  checkdevice == "1" ? 5 : 20,
                                            ),
                                            new Expanded(
                                                flex: 1,
                                                child: Align(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Utility
                                                      .MainNormalTextWithFont(
                                                          "Amount :  " +
                                                              '\u{20B9}' +
                                                              listing_Payment[
                                                                      pos]
                                                                  .inovicePaidAmount,
                                                          checkdevice == "1"
                                                              ? 14
                                                              : 24,
                                                          Utility
                                                              .PutDarkBlueColor(),
                                                          'Poppins_Regular'),
                                                )),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: checkdevice == "1" ? 10 : 20,
                                ),
                                Container(
                                  padding: EdgeInsets.fromLTRB(
                                      0,
                                      0,
                                      checkdevice == "1" ? 10 : 20,
                                      checkdevice == "1" ? 10 : 20),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      InkWell(
                                          child: Center(
                                              child: Row(
                                            children: [
                                              Container(
                                                  child: Text(
                                                Utility.translate('download'),
                                                style: TextStyle(
                                                    fontSize: checkdevice == "1"
                                                        ? 16
                                                        : 26,
                                                    fontFamily:
                                                        'Poppins_Regular',
                                                    color: Utility
                                                        .PutLightBlueColor()),
                                              )),
                                              SizedBox(
                                                width:
                                                    checkdevice == "1" ? 8 : 18,
                                              ),
                                              SvgPicture.asset(
                                                  'Images/Download.svg',
                                                  width: checkdevice == "1"
                                                      ? 15
                                                      : 25,
                                                  height: checkdevice == "1"
                                                      ? 15
                                                      : 25)
                                            ],
                                          )),
                                          onTap: () async {
                                            print(
                                                "Value ${listing_Payment[pos].invoiceId}");
                                            GetNotificationofAPI(
                                                listing_Payment[pos].invoiceId,
                                                "1");
                                          }),
                                      SizedBox(
                                        width: checkdevice == "1" ? 10 : 20,
                                      ),
                                      InkWell(
                                          child: Center(
                                              child: Row(
                                            children: [
                                              Container(
                                                  child: Text(
                                                Utility.translate('share'),
                                                style: TextStyle(
                                                    fontSize: checkdevice == "1"
                                                        ? 16
                                                        : 26,
                                                    fontFamily:
                                                        'Poppins_Regular',
                                                    color: Utility
                                                        .PutLightBlueColor()),
                                              )),
                                              SizedBox(
                                                width:
                                                    checkdevice == "1" ? 8 : 18,
                                              ),
                                              SvgPicture.asset(
                                                'Images/share.svg',
                                                width: checkdevice == "1"
                                                    ? 15
                                                    : 25,
                                                height: checkdevice == "1"
                                                    ? 15
                                                    : 25,
                                              )
                                            ],
                                          )),
                                          onTap: () {
                                            setState(() {
                                              GetNotificationofAPI(
                                                  listing_Payment[pos]
                                                      .invoiceId,
                                                  "2");
                                            });
                                          }),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )),
                    );
                  },
                ),
              ),
      ),
    );
  }
}
