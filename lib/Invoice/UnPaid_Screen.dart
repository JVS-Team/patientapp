import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:softcliniccare/Helper/utils.dart';

import '../Helper/Constant.dart';
import '../Helper/Database/sqflite_helper.dart';
import '../Model/Dashboard/Dashboard_API.dart';
import '../Model/Dashboard/Facility_Name_Model.dart';
import '../Model/Invoice/API_PaymentHistory.dart';
import '../Model/Invoice/ApplicationVisitType.dart';
import '../Model/Invoice/PaidList.dart';

class UnPaidPage extends StatefulWidget {
  @override
  UnPaidMainPage createState() => UnPaidMainPage();
}

class UnPaidMainPage extends State<UnPaidPage> {
  String checkdevice = '';

  List<billingList> listing_Payment = [];
  List<visitList> lisitng_visitType = [];
  bool needToDisplayCircle = true;
  bool nowDisplayNoRecordFound = true;

  List<Facility_Details> listing_FacilityName = [];

  @override
  void initState() {
    checkdevice = Utility.getDeviceType();
    FacilityName_Details();
    ApplicationVisitType();
    super.initState();
  }

  void FacilityName_Details() async {
    String AccessToken = await Utility.readAccessToken('Token');

    final data = await SQLHelper.getAll_Facility_Response(
        ConstantVar.str_Common_GETALLFACILITYNAME);
    List<Map<String, dynamic>> allFacilityResponse = data;
    String mainFacilityResponse = "";

    if (allFacilityResponse.length > 0) {
      for (int i = 0; i < allFacilityResponse.length; i++) {
        if (allFacilityResponse[i]['FacilityID'] ==
            ConstantVar.str_Common_GETALLFACILITYNAME) {
          mainFacilityResponse = allFacilityResponse[i]['FacilityResponse'];
          break;
        } else {}
      }
      if (mainFacilityResponse == "") {
        API_Dashboard()
            .Dashboard_Facilityname(AccessToken)
            .then((Response) async {
          if (allFacilityResponse.length > 0) {
            AllFacilityUpdate(ConstantVar.str_Common_GETALLFACILITYNAME,
                json.encode(Response));
          } else {
            AllFacilityInsert(ConstantVar.str_Common_GETALLFACILITYNAME,
                json.encode(Response));
          }
          FacilityMainResponse(Response);
        });
      } else {
        FacilityMainResponse(json.decode(mainFacilityResponse));
      }
    } else {
      API_Dashboard()
          .Dashboard_Facilityname(AccessToken)
          .then((Response) async {
        if (allFacilityResponse.length > 0) {
          AllFacilityUpdate(
              ConstantVar.str_Common_GETALLFACILITYNAME, json.encode(Response));
        } else {
          AllFacilityInsert(
              ConstantVar.str_Common_GETALLFACILITYNAME, json.encode(Response));
        }
        FacilityMainResponse(Response);
      });
    }
  }

  void FacilityMainResponse(var Response) {
    log("Main Facility ${Response}");
    if (mounted) {
      setState(() {
        var tempdata = Response['value'];
        for (int i = 0; i < tempdata.length; i++) {
          listing_FacilityName.add(Facility_Details.fromJson(tempdata[i]));
        }
      });
    } else {
      var tempdata = Response['value'];
      for (int i = 0; i < tempdata.length; i++) {
        listing_FacilityName.add(Facility_Details.fromJson(tempdata[i]));
      }
    }
  }

  void AllFacilityInsert(String FacilityID, String FacilityResponse) async {
    await SQLHelper.insertName_Facility(FacilityID, FacilityResponse);
  }

  void AllFacilityUpdate(String FacilityID, String FacilityResponse) async {
    await SQLHelper.update_facility_response(FacilityID, FacilityResponse);
  }

  void ApplicationVisitType() {
    API_PaymentList().AppointmentVisitTypeDetails().then((mainResponse) async {
      setState(() {
        log('List Response Visit Type ${mainResponse}');
        lisitng_visitType = [];
        var resBody = json.decode(mainResponse);

        VisitTypeDetails visitListData = new VisitTypeDetails.fromJson(resBody);

        lisitng_visitType = visitListData.visitTypelist;
        API_of_Paid();
      });
    });
  }

  void API_of_Paid() async {
    String MainPatientID = await Utility.readAccessToken('PatientID');
    print('Patient ID ${MainPatientID}');
    // Utility.showLoadingDialog(context, _AppointmentList, true);

    final data = await SQLHelper.getAll_AppointmentList(
        MainPatientID, ConstantVar.str_Common_GETUNPAID_INVOICE);
    List<Map<String, dynamic>> allAppointmentList = data;
    String mainAppointmentResponse = "";

    if (allAppointmentList.length > 0) {
      for (int i = 0; i < allAppointmentList.length; i++) {
        if (allAppointmentList[i]['PatientID'] == MainPatientID) {
          mainAppointmentResponse =
              allAppointmentList[i]['AppointmentResponse'];
          break;
        } else {}
      }
      if (mainAppointmentResponse == "") {
        API_PaymentList()
            .Payment_List_All(MainPatientID)
            .then((mainResponse) async {
          setState(() {
            if (allAppointmentList.length > 0) {
              AllAppointmentUpdate(MainPatientID,
                  ConstantVar.str_Common_GETUNPAID_INVOICE, mainResponse);
            } else {
              AllAppointmentInsert(MainPatientID,
                  ConstantVar.str_Common_GETUNPAID_INVOICE, mainResponse);
            }
            InvoiceResponse(mainResponse);
          });
        });
      } else {
        // log("Main Appointment Response ${mainAppointmentResponse}");
        InvoiceResponse(mainAppointmentResponse);
      }
    } else {
      API_PaymentList()
          .Payment_List_All(MainPatientID)
          .then((mainResponse) async {
        setState(() {
          if (allAppointmentList.length > 0) {
            AllAppointmentUpdate(MainPatientID,
                ConstantVar.str_Common_GETUNPAID_INVOICE, mainResponse);
          } else {
            AllAppointmentInsert(MainPatientID,
                ConstantVar.str_Common_GETUNPAID_INVOICE, mainResponse);
          }
          InvoiceResponse(mainResponse);
        });
      });
    }
  }

  void InvoiceResponse(String mainResponse) {
    setState(() {
      log('List Response Paid ${mainResponse}');

      if (mainResponse == "Error") {
      } else {
        listing_Payment = [];
        var resBody = json.decode(mainResponse);

        PaymentAllListData paymentListData =
            new PaymentAllListData.fromJson(resBody);

        double dueAmount = 0.0;
        for (int i = 0; i < paymentListData.paymentList.length; i++) {
          dueAmount =
              double.parse(paymentListData.paymentList[i].inoviceDueAmount);

          if (dueAmount > 0.0) {
            print("Due ${dueAmount}");
            listing_Payment.add(paymentListData.paymentList[i]);
          }
        }

        if (listing_Payment.length > 0) {
          needToDisplayCircle = false;
          nowDisplayNoRecordFound = true;
        } else {
          needToDisplayCircle = true;
          nowDisplayNoRecordFound = false;
        }
      }
    });
  }

  void AllAppointmentInsert(String PatientID, String UpComing_Past,
      String AppointmentResponse) async {
    await SQLHelper.insert_Appointment_Details(
        PatientID, AppointmentResponse, UpComing_Past);
  }

  void AllAppointmentUpdate(String PatientID, String UpComing_Past,
      String AppointmentResponse) async {
    await SQLHelper.update_Appointment_response(
        PatientID, AppointmentResponse, UpComing_Past);
  }

  Future<void> _pullRefresh() async {
    print("Refresh");
    String MainPatientID = await Utility.readAccessToken('PatientID');
    print('Patient ID ${MainPatientID}');

    final data = await SQLHelper.getAll_AppointmentList(
        MainPatientID, ConstantVar.str_Common_GETUNPAID_INVOICE);
    List<Map<String, dynamic>> allAppointmentList = data;

    setState(() {
      API_PaymentList()
          .Payment_List_All(MainPatientID)
          .then((mainResponse) async {
        setState(() {
          if (allAppointmentList.length > 0) {
            AllAppointmentUpdate(MainPatientID,
                ConstantVar.str_Common_GETUNPAID_INVOICE, mainResponse);
          } else {
            AllAppointmentInsert(MainPatientID,
                ConstantVar.str_Common_GETUNPAID_INVOICE, mainResponse);
          }
          InvoiceResponse(mainResponse);
        });
      });
    });
  }

  // void API_of_Paid() async {
  //   String PatientID = await Utility.readAccessToken('PatientID');
  //   API_PaymentList().Payment_List_All(PatientID).then((mainResponse) async {
  //     setState(() {
  //       log('List Response Paid ${mainResponse}');
  //
  //       if (mainResponse == "Error") {
  //       } else {
  //         listing_Payment = [];
  //         var resBody = json.decode(mainResponse);
  //
  //         PaymentAllListData paymentListData =
  //             new PaymentAllListData.fromJson(resBody);
  //
  //         double dueAmount = 0.0;
  //         for (int i = 0; i < paymentListData.paymentList.length; i++) {
  //           dueAmount =
  //               double.parse(paymentListData.paymentList[i].inoviceDueAmount);
  //
  //           if (dueAmount > 0.0) {
  //             print("Due ${dueAmount}");
  //             listing_Payment.add(paymentListData.paymentList[i]);
  //           }
  //         }
  //
  //         if (listing_Payment.length > 0) {
  //           needToDisplayCircle = false;
  //           nowDisplayNoRecordFound = true;
  //         } else {
  //           needToDisplayCircle = true;
  //           nowDisplayNoRecordFound = false;
  //         }
  //       }
  //     });
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: needToDisplayCircle == true
            ? nowDisplayNoRecordFound == true
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : RefreshIndicator(
                    onRefresh: _pullRefresh,
                    child: Container(
                      child: SingleChildScrollView(
                        physics: AlwaysScrollableScrollPhysics(),
                        child: Container(
                          child: Center(
                            child: Text('No Unpaid Found'),
                          ),
                          height: MediaQuery.of(context).size.height,
                        ),
                      ),
                    ),
                  )
            : RefreshIndicator(
                onRefresh: _pullRefresh,
                child: ListView.builder(
                  itemCount: listing_Payment.length,
                  itemBuilder: (BuildContext context, int pos) {
                    String serviceName = "";
                    for (int i = 0; i < listing_Payment.length; i++) {
                      for (int j = 0; j < lisitng_visitType.length; j++) {
                        if (listing_Payment[i].visitType ==
                            lisitng_visitType[j].id) {
                          serviceName = lisitng_visitType[j].displayName;
                        }
                      }
                    }

                    String FacilityName = "";
                    for (int i = 0; i < listing_FacilityName.length; i++) {
                      if (listing_FacilityName[i].id ==
                          listing_Payment[pos].facilityId) {
                        FacilityName = listing_FacilityName[i].displayName;
                      }
                    }

                    return Container(
                      child: Card(
                          elevation: 5,
                          margin: EdgeInsets.all(checkdevice == "1" ? 8 : 12),
                          color: Utility.PutBackgroundCardColor(),
                          child: Container(
                            height: checkdevice == "1" ? 130 : 270,
                            child: Column(
                              children: <Widget>[
                                new Expanded(
                                  flex: 2,
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(
                                        width: checkdevice == "1" ? 10 : 20,
                                      ),
                                      new Expanded(
                                          flex: 1,
                                          child: Container(
                                            child: Align(
                                              alignment: Alignment.topRight,
                                              child: IconButton(
                                                  onPressed: () {},
                                                  icon: SvgPicture.asset(
                                                    'Images/pdf_icon.svg',
                                                    color: Utility
                                                        .PutDarkBlueColor(),
                                                    height: checkdevice == "1"
                                                        ? 50
                                                        : 90,
                                                    width: checkdevice == "1"
                                                        ? 50
                                                        : 90,
                                                  ),
                                                  iconSize: checkdevice == "1"
                                                      ? 50
                                                      : 80),
                                            ),
                                          )),
                                      SizedBox(
                                        width: checkdevice == "1" ? 5 : 15,
                                      ),
                                      new Expanded(
                                        flex: 4,
                                        child: Column(
                                          children: <Widget>[
                                            SizedBox(
                                              height:
                                                  checkdevice == "1" ? 5 : 20,
                                            ),
                                            new Expanded(
                                                flex: 1,
                                                child: Align(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Utility
                                                      .MainNormalTextWithFont(
                                                          "Facility : " +
                                                              FacilityName,
                                                          checkdevice == "1"
                                                              ? 14
                                                              : 24,
                                                          Utility
                                                              .PutDarkBlueColor(),
                                                          'Poppins_Regular'),
                                                )),
                                            SizedBox(
                                              height:
                                                  checkdevice == "1" ? 5 : 20,
                                            ),
                                            new Expanded(
                                                flex: 1,
                                                child: Align(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Utility
                                                      .MainNormalTextWithFont(
                                                          "Service : " +
                                                              serviceName,
                                                          checkdevice == "1"
                                                              ? 14
                                                              : 24,
                                                          Utility
                                                              .PutDarkBlueColor(),
                                                          'Poppins_Regular'),
                                                )),
                                            SizedBox(
                                              height:
                                                  checkdevice == "1" ? 5 : 20,
                                            ),
                                            new Expanded(
                                                flex: 1,
                                                child: Align(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Utility
                                                      .MainNormalTextWithFont(
                                                          "Doctor : " +
                                                              listing_Payment[
                                                                      pos]
                                                                  .doctorName,
                                                          checkdevice == "1"
                                                              ? 14
                                                              : 24,
                                                          Utility
                                                              .PutDarkBlueColor(),
                                                          'Poppins_Regular'),
                                                )),
                                            SizedBox(
                                              height:
                                                  checkdevice == "1" ? 5 : 20,
                                            ),
                                            new Expanded(
                                                flex: 1,
                                                child: Align(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Utility
                                                      .MainNormalTextWithFont(
                                                          "Amount :  " +
                                                              '\u{20B9}' +
                                                              listing_Payment[
                                                                      pos]
                                                                  .inoviceDueAmount,
                                                          checkdevice == "1"
                                                              ? 14
                                                              : 24,
                                                          Utility
                                                              .PutDarkBlueColor(),
                                                          'Poppins_Regular'),
                                                )),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                // SizedBox(
                                //   height: checkdevice == "1" ? 5 : 15,
                                // ),
                                // new Expanded(
                                //   flex: 1,
                                //   child: Container(
                                //     height: checkdevice == "1" ? 80 : 60,
                                //     padding: EdgeInsets.fromLTRB(
                                //         checkdevice == "1" ? 15 : 25,
                                //         0,
                                //         checkdevice == "1" ? 15 : 25,
                                //         0),
                                //     width: Utility.getDeviceType() == '1'
                                //         ? double.infinity
                                //         : 400,
                                //     child: TextButton(
                                //       style: TextButton.styleFrom(
                                //         shape: StadiumBorder(),
                                //         shadowColor: Colors.black,
                                //         textStyle: TextStyle(
                                //             fontSize:
                                //                 checkdevice == "1" ? 20 : 30),
                                //         primary: Colors.white,
                                //         backgroundColor:
                                //             Utility.PutDarkBlueColor(),
                                //       ),
                                //       onPressed: () {},
                                //       child: Text(
                                //         Utility.translate('pay'),
                                //         style: TextStyle(
                                //             color: Colors.white,
                                //             fontFamily: 'Poppins_Regular',
                                //             fontSize:
                                //                 checkdevice == "1" ? 16 : 26),
                                //       ),
                                //     ),
                                //   ),
                                // ),
                                // SizedBox(
                                //   height: checkdevice == "1" ? 15 : 25,
                                // )
                              ],
                            ),
                          )),
                    );
                  },
                ),
              ),
      ),
    );
  }
}
