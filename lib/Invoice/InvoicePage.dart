import 'package:flutter/material.dart';
import 'package:softcliniccare/Appointment/Bookappointment_screen.dart';
import 'package:softcliniccare/Appointment/Current_Appointment.dart';
import 'package:softcliniccare/Appointment/Past_Appointment.dart';
import 'package:softcliniccare/Helper/utils.dart';

import 'Paid_Screen.dart';
import 'UnPaid_Screen.dart';

class invoicemanagment extends StatefulWidget {
  @override
  appointmentmanagementState createState() => appointmentmanagementState();
}

class appointmentmanagementState extends State<invoicemanagment>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  String checkdevice = '';

  @override
  void initState() {
    checkdevice = Utility.getDeviceType();
    super.initState();
    tabController = new TabController(vsync: this, length: 3);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: Utility.TopToolBar(Utility.translate('invoice'),
            checkdevice == "1" ? 20 : 30, Utility.PutDarkBlueColor(), context),
        body: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(checkdevice == "1" ? 90 : 120),
              child: Container(
                height: Utility.getDeviceType() == '1' ? 50 : 70,
                margin: Utility.getDeviceType() == '1'
                    ? EdgeInsets.fromLTRB(10, 25, 10, 20)
                    : EdgeInsets.fromLTRB(20, 20, 20, 10),
                decoration: BoxDecoration(
                    border: Border.all(color: Utility.PutDarkBlueColor())),
                child: new TabBar(
                  // indicatorColor: Colors.blue,
                  unselectedLabelColor: Utility.PutDarkBlueColor(),
                  // labelColor: Colors.blue,
                  // indicatorSize: TabBarIndicatorSize.label,
                  indicator: ShapeDecoration(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(0),
                              topLeft: Radius.circular(0))),
                      color: Utility.PutDarkBlueColor()),
                  tabs: [
                    Tab(
                      child: Text(Utility.translate('unpaid'),
                          style: TextStyle(
                              fontSize: checkdevice == "1" ? 16 : 26)),
                    ),
                    Tab(
                      child: Text(Utility.translate('paid'),
                          style: TextStyle(
                              fontSize: checkdevice == "1" ? 16 : 26)),
                    ),
                  ],
                ),
              ),
            ),
            body: TabBarView(
              children: [
                UnPaidPage(),
                PaidPage(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
