import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'dart:typed_data';
import 'package:path_provider/path_provider.dart';

class FileProcess {
  FileProcess._();

  static bool isFolderCreated = false;
  static Directory? directory;

  static checkDocumentFolder() async {
    try {
      if (!isFolderCreated) {
        directory = await getApplicationDocumentsDirectory();
        await directory!.exists().then((value) {
          if (value) directory!.create();
          isFolderCreated = true;
        });
      }
    } catch (e) {
      print(e.toString());
    }
  }

  static Future<Uint8List> downloadFile(String Base64Value) async {
    String FileName = generateRandomString(8);
    final base64str = Base64Value;
    Uint8List bytes = base64.decode(base64str);
    // await checkDocumentFolder();
    // String dir = directory!.path + "/" + FileName + ".pdf";
    // print("File Path ${dir}");
    // File file = new File(dir);
    // if (!file.existsSync()) file.create();
    // await file.writeAsBytes(bytes);
    return bytes;
  }

  static String generateRandomString(int len) {
    var r = Random();
    return String.fromCharCodes(
        List.generate(len, (index) => r.nextInt(33) + 89));
  }
}
