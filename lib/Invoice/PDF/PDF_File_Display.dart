import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:printing/printing.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

import 'package:http/http.dart' as http;

import '../../Helper/utils.dart';

class PDFFileDisplay extends StatefulWidget {
  PDFFileDisplay({Key? key, required this.main8Bytes}) : super(key: key);
  Uint8List main8Bytes;

  @override
  PDFFileState createState() => PDFFileState();
}

class PDFFileState extends State<PDFFileDisplay> {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();
  Uint8List? _documentBytes;

  @override
  void initState() {
    print("Here Details ");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => Utility.Willpopscope,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            body: SfPdfViewer.memory(widget.main8Bytes, key: _pdfViewerKey,
                onDocumentLoaded: (PdfDocumentLoadedDetails details) async {
              Navigator.pop<dynamic>(context);
              final Uint8List bytes =
                  Uint8List.fromList(await details.document.save());
              await Navigator.push<dynamic>(
                  context,
                  MaterialPageRoute<dynamic>(
                      builder: (BuildContext context) => SafeArea(
                              child: PdfPreview(
                            build: (format) => bytes,
                          ))));
            })));

    // Working code of Network
    // body: SfPdfViewer.network(
    //     'https://app.softclinicgenx.com/dasharedresources/e2866659-6a02-4d9e-9a4c-22fba7dc2f54.pdf',
    //     key: _pdfViewerKey,
    //     onDocumentLoaded: (PdfDocumentLoadedDetails details) async {
    //   Navigator.pop<dynamic>(context);
    //   final Uint8List bytes =
    //       Uint8List.fromList(details.document.save());
    //   await Navigator.push<dynamic>(
    //       context,
    //       MaterialPageRoute<dynamic>(
    //           builder: (BuildContext context) => SafeArea(
    //                   child: PdfPreview(
    //                 build: (format) => bytes,
    //               ))));
    // })));

    // body: SafeArea(
    //   // child: SfPdfViewer.network(
    //   //   'https://app.softclinicgenx.com/dasharedresources/e2866659-6a02-4d9e-9a4c-22fba7dc2f54.pdf',
    //   // ),
    //   child: SfPdfViewer.asset(
    //     'assets/pdf/opd_pdf.pdf',
    //     key: _pdfViewerKey,
    //     onDocumentLoaded: (PdfDocumentLoadedDetails details) async {
    //       final Uint8List bytes =
    //           Uint8List.fromList(details.document.save());
    //       await Navigator.push<dynamic>(
    //           context,
    //           MaterialPageRoute<dynamic>(
    //               builder: (BuildContext context) => SafeArea(
    //                     child: PdfPreview(
    //                       build: (PdfPageFormat format) => bytes,
    //                     ),
    //                   )));
    //     },
    //   ),
    // )));
  }
}
