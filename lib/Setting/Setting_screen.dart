import 'dart:io';
import 'package:adaptive_action_sheet/adaptive_action_sheet.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:softcliniccare/Helper/utils.dart';

class settingscreen extends StatefulWidget {
  settingscreen({Key? key}) : super(key: key);

  // final String title;

  @override
  _settingscreenState createState() => _settingscreenState();
}

class _settingscreenState extends State<settingscreen> {
  String LanguageCode = "", LanguageName = "";
  bool _lights = false;
  String checkdevice = '';

  @override
  void initState() {
    checkdevice = Utility.getDeviceType();
    super.initState();
    Utility.getAllLangData().then((languagecode) async {
      setState(() {
        LanguageCode = languagecode.toString();
        if (LanguageCode == "en") {
          LanguageName = "English";
        } else if (LanguageCode == "hi") {
          LanguageName = "Hindi";
        } else if (LanguageCode == "sp") {
          LanguageName = "Spanish";
        } else if (LanguageCode == "fr") {
          LanguageName = "French";
        }
      });
    });

    Utility.getAllLangData().then((languagecode) async {
      setState(() {
        if (languagecode == "") {
          _lights = false;
        } else {
          _lights = true;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: Utility.getDeviceType() == '1' ? 50 : 70,
          centerTitle: true,
          backgroundColor: Colors.white,
          leading: IconButton(
            padding: EdgeInsets.fromLTRB(
                Utility.getDeviceType() == '1' ? 5 : 20,
                Utility.getDeviceType() == '1' ? 5 : 10,
                0,
                0),
            icon: SvgPicture.asset('Images/menu.svg',
                color: Colors.black,
                width: Utility.getDeviceType() == "1" ? 15 : 50,
                height: Utility.getDeviceType() == "1" ? 15 : 50),
            onPressed: () => {Scaffold.of(context).openDrawer()},
          ),
          actions: [
            Container(
                padding: Utility.getDeviceType() == '1'
                    ? EdgeInsets.fromLTRB(0, 0, 10, 0)
                    : EdgeInsets.fromLTRB(0, 5, 5, 0),
                height: Utility.getDeviceType() == '1' ? 50 : 70,
                width: Utility.getDeviceType() == '1' ? 50 : 70,
                child: CircleAvatar(
                  radius: Utility.getDeviceType() == '1' ? 55 : 70,
                  backgroundColor: Colors.transparent,
                  child: CircleAvatar(
                    radius: Utility.getDeviceType() == '1' ? 50 : 70,
                    backgroundColor: Colors.transparent,
                    backgroundImage: NetworkImage(
                        'https://vinusimages.co/wp-content/uploads/2018/10/EG7A2390.djpgA_.jpg'),
                  ),
                ))
          ],
          title: Image.asset(
            'Images/softclinic_logo.png',
            height: checkdevice == "1" ? 30 : 40,
            alignment: Alignment.centerRight,
          ),
        ),
        body: Container(
          margin: EdgeInsets.fromLTRB(
              checkdevice == "1" ? 5 : 15, checkdevice == "1" ? 20 : 30, 0, 0),
          child: Column(children: <Widget>[
            Card(
              margin: EdgeInsets.fromLTRB(0, checkdevice == "1" ? 10 : 20,
                  checkdevice == "1" ? 10 : 20, 0),
              color: Colors.white,
              elevation: 10,
              child: ListTile(
                title: Text(Utility.translate('select_language'),
                    style: TextStyle(fontSize: checkdevice == "1" ? 12 : 22)),
                subtitle: Text(LanguageName,
                    style: TextStyle(fontSize: checkdevice == "1" ? 10 : 18)),
                trailing: Icon(Icons.keyboard_arrow_right),
                dense: true,
                onTap: () {
                  showAdaptiveActionSheet(
                    context: context,
                    // title: const Text('Select your Language'),
                    actions: <BottomSheetAction>[
                      BottomSheetAction(
                          title: Text('English',
                              style: TextStyle(
                                  fontSize: checkdevice == "1" ? 18 : 28)),
                          onPressed: (context) {
                            Navigator.pop(context);
                            setState(() {
                              Utility.SaveAllLangData('LangName', 'en');
                              Utility.load();
                              LanguageName = "English";
                            });
                          }),
                      // BottomSheetAction(
                      //     title: Text('Hindi'),
                      //     onPressed: () {
                      //       Navigator.pop(context);
                      //       setState(() {
                      //         Utility.SaveAllLangData('LangName','hi');
                      //         Utility.load();
                      //         LanguageName = "Hindi";
                      //       });
                      //     }),
                      BottomSheetAction(
                          title: Text('Spanish',
                              style: TextStyle(
                                  fontSize: checkdevice == "1" ? 18 : 28)),
                          onPressed: (context) {
                            Navigator.pop(context);
                            setState(() {
                              Utility.SaveAllLangData('LangName', 'sp');
                              Utility.load();
                              LanguageName = "Spanish";
                            });
                          }),
                      BottomSheetAction(
                          title: Text(
                            'French',
                            style: TextStyle(
                                fontSize: checkdevice == "1" ? 18 : 28),
                          ),
                          onPressed: (context) {
                            Navigator.pop(context);
                            setState(() {
                              Utility.SaveAllLangData('LangName', 'fr');
                              Utility.load();
                              LanguageName = "French";
                            });
                          }),
                      // BottomSheetAction(title: 'Spanish', onPressed: () {
                      //   Utility.SaveAllLangData('sp');
                      // }),
                    ],
                    cancelAction: CancelAction(
                        title: Text('Cancel',
                            style: TextStyle(
                                fontSize: checkdevice == "1"
                                    ? 18
                                    : 28))), // onPressed parameter is optional by default will dismiss the ActionSheet
                  );
                },
              ),
            ),
            SizedBox(
              height: checkdevice == "1" ? 10 : 20,
            ),
            MergeSemantics(
              child: ListTile(
                title: Text(Utility.translate('notification'),
                    style: TextStyle(fontSize: checkdevice == "1" ? 15 : 22)),
                trailing: CupertinoSwitch(
                  value: _lights,
                  onChanged: (bool value) {
                    setState(() {
                      _lights = value;
                      if (value == true) {
                        FirebaseMessaging _firebaseMessaging =
                            FirebaseMessaging.instance;
                        if (Platform.isIOS) {
                          _firebaseMessaging.requestPermission();
                        }

                        _firebaseMessaging.getToken().then((value) {
                          print("FirebaseMessaging token: $value");
                          Utility.SaveAllLangData('Token', value!);
                        });
                      } else {
                        FirebaseMessaging.instance.deleteToken();
                        Utility.SaveAllLangData('Token', '');
                      }
                    });
                  },
                ),
                onTap: () {
                  setState(() {
                    //_lights = !_lights;
                  });
                },
              ),
            )
          ]),
        ),
      ),
    );
  }
}
