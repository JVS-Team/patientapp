import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:developer';
import 'dart:io';
import 'package:softcliniccare/Helper/utils.dart';

import '../Helper/Age_Calculator.dart';
import '../Helper/AllURL.dart';
import '../Helper/Database/sqflite_helper.dart';
import '../Model/Profile/ProfileAPI.dart';

class profilescreen extends StatefulWidget {
  profilescreen({Key? key}) : super(key: key);

  @override
  profilescreenState createState() => profilescreenState();
}

class profilescreenState extends State<profilescreen> {
  String FinalName = '';
  int Age = 0;
  String Gender = '';
  String MobileNo = '';
  String Email = '';
  String Address = '';
  GlobalKey<State> _KeyProfile = new GlobalKey<State>();
  String checkdevice = '';
  Uint8List imageBytes = base64.decode(AllURL.Base64_PlaceHolderImage);

  @override
  void initState() {
    super.initState();
    checkdevice = Utility.getDeviceType();
    ProfilePage();
  }

  void ProfilePage() async {
    String AccessToken = await Utility.readAccessToken('Token');
    String PatientID = await Utility.readAccessToken('PatientID');
    Utility.showLoadingDialog(context, _KeyProfile, true);

    API_Profile().ProfilePageDisplay(AccessToken, PatientID).then((Response) {
      setState(() {
        // str_Email = Response['patregi']['firstName'];

        String EmailID = Response['patientContactDetail']['emailId'] ?? "";
        // String EmailID =  Response['patientContactDetail']['emailId'];

        String FName, Lname;
        FName = Response['patregi']['firstName'] ?? "";
        Lname = Response['patregi']['lastName'] ?? "";
        print('Here Profile ${FName}');
        FinalName = FName + " " + Lname;
        Gender = Response['patregi']['gender'] ?? "";
        String DOBData = Response['patregi']['dob'] ?? "";
        MobileNo = Response['patientContactDetail']['mobileNo'] ?? "";
        if (EmailID == "" || EmailID == "null") {
          Email = "";
        } else {
          Email = EmailID;
        }
        String HouseNo = Response['patientContactDetail']['houseNumber'] ?? "";
        String Street = Response['patientContactDetail']['street'] ?? "";
        String Location = Response['patientContactDetail']['location'] ?? "";
        String Pincode = Response['patientContactDetail']['pincode'] ?? "";

        if (HouseNo != "" && Street != "" && Location != "" && Pincode != "") {
          Address = HouseNo + " ," + Street + ", " + Location + ", " + Pincode;
        } else {
          Address = "";
        }

        _selectDate(DOBData).then((int result) {
          setState(() {
            Age = result;
          });
        });

        String imageFileDetails = Response['patientPhotographDetail']['imagePath'] ?? "";
        log(imageFileDetails);

        if (imageFileDetails == '') {
          imageBytes = base64.decode(AllURL.Base64_PlaceHolderImage);
        } else {
          var parts = imageFileDetails.split(',');
          var mainImages = parts[1].trim();
          imageBytes = base64.decode(mainImages);
        }

        // Age = _selectDate(DOBData);
        // print(str_Name);
        // str_PhoneNo = Response['phone'];
        Navigator.pop(context);
      });
    });
  }

  // Insert a new profile data to database
  Future<void> _addItem(String EmailID) async {
    await SQLHelper.createItem(EmailID);
    // _refreshJournals();
  }

  Future<int> _selectDate(String DateofBirth) async {
    int Age = 0;
    DateTime currentDate = DateTime.now();

    DateTime dobAllDate = DateFormat('yyyy-MM-ddTHH:mm:ss').parse(DateofBirth);

    if (dobAllDate != null && dobAllDate != currentDate)
      setState(() {
        // currentDate = pickedDate;
        String formattedDateDOB = DateFormat('dd/MM/yyyy').format(dobAllDate);

        // Date of Birth Calculation
        int Days = dobAllDate.day;
        int Month = dobAllDate.month;
        int Year = dobAllDate.year;

        print('Date $formattedDateDOB');

        DateTime birthday = DateTime(Year, Month, Days);

        DateDuration duration =
            AgeCalculator.age(birthday); // 2 Years 3 Month 5 Days

        int CalcuDays = duration.days;
        int CalcuYears = duration.years;
        int CalcuMonth = duration.months;
        print(CalcuYears);
        Age = CalcuYears;
      });
    return Age;
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        // appBar: AppBar(
        //   toolbarHeight: Utility.getDeviceType() == '1' ? 50 : 70,
        //   centerTitle: true,
        //   backgroundColor: Colors.white,
        //   leading: IconButton(
        //     padding: EdgeInsets.fromLTRB(
        //         Utility.getDeviceType() == '1' ? 5 : 20,
        //         Utility.getDeviceType() == '1' ? 5 : 10,
        //         0,
        //         0),
        //     icon: SvgPicture.asset('Images/menu.svg',
        //         color: Colors.black,
        //         width: Utility.getDeviceType() == "1" ? 15 : 50,
        //         height: Utility.getDeviceType() == "1" ? 15 : 50),
        //     onPressed: () => {Scaffold.of(context).openDrawer()},
        //     //_scaffoldKey.currentState!.openDrawer()},
        //   ),
        //   title: Image.asset(
        //     'Images/softclinic_logo.png',
        //     height: Utility.getDeviceType() == '1' ? 30 : 40,
        //     alignment: Alignment.centerRight,
        //   ),
        // ),
        appBar: Utility.TopToolBar('Profile',
            checkdevice == "1" ? 20 : 30, Utility.PutDarkBlueColor(), context),
        body: SingleChildScrollView(
          child: Container(
            padding: Utility.getDeviceType() == '1'
                ? EdgeInsets.fromLTRB(15, 15, 10, 5)
                : EdgeInsets.fromLTRB(25, 25, 25, 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Row(
                    children: [
                      Container(
                          padding: EdgeInsets.fromLTRB(0, 0,
                              Utility.getDeviceType() == '1' ? 15 : 25, 0),
                          height: Utility.getDeviceType() == '1' ? 90 : 150,
                          width: Utility.getDeviceType() == '1' ? 90 : 150,
                          child: CircleAvatar(
                            backgroundColor: Colors.transparent,
                            child: CircleAvatar(
                              radius: Utility.getDeviceType() == '1' ? 50 : 80,
                              backgroundColor: Colors.transparent,
                              child: Image.memory(imageBytes,
                                  width: 100, height: 100, fit: BoxFit.contain),
                              // child: FadeInImage.assetNetwork(
                              //     image: "Images/loading.png",
                              //     placeholder: "Images/loading.png"),
                              // backgroundImage: NetworkImage(
                              //     'https://vinusimages.co/wp-content/uploads/2018/10/EG7A2390.jpgA_.jpg'),
                            ),
                          )),
                      Text(
                        FinalName,
                        style: TextStyle(
                            fontSize: Utility.getDeviceType() == '1' ? 20 : 30,
                            fontFamily: 'Poppins_SemiBold',
                            color: Utility.PutDarkBlueColor()),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: Utility.getDeviceType() == '1' ? 20 : 30,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            color: Colors.white,
                            child: Text(
                              'Age (Year)',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize:
                                    Utility.getDeviceType() == '1' ? 17 : 27,
                                fontFamily: 'Poppins_SemiBold',
                                color: Utility.PutDarkBlueColor(),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            color: Colors.white,
                            child: Text(
                              'Gender',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize:
                                    Utility.getDeviceType() == '1' ? 17 : 27,
                                fontFamily: 'Poppins_SemiBold',
                                color: Utility.PutDarkBlueColor(),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            color: Colors.white,
                            child: Text(
                              Age.toString(),
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize:
                                    Utility.getDeviceType() == '1' ? 16 : 26,
                                fontFamily: 'Poppins_Regular',
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            color: Colors.white,
                            child: Text(
                              Gender,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize:
                                    Utility.getDeviceType() == '1' ? 16 : 26,
                                fontFamily: 'Poppins_Regular',
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: Utility.getDeviceType() == '1' ? 15 : 25,
                ),
                Text(
                  'Contact number',
                  style: TextStyle(
                      fontSize: Utility.getDeviceType() == '1' ? 17 : 27,
                      fontFamily: 'Poppins_SemiBold',
                      color: Utility.PutDarkBlueColor()),
                ),
                Text(
                  MobileNo,
                  style: TextStyle(
                      fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                      fontFamily: 'Poppins_Regular',
                      color: Colors.black),
                ),
                SizedBox(
                  height: Utility.getDeviceType() == '1' ? 20 : 30,
                ),
                Email != ""
                    ? Text(
                        'Email',
                        style: TextStyle(
                            fontSize: Utility.getDeviceType() == '1' ? 17 : 27,
                            fontFamily: 'Poppins_SemiBold',
                            color: Utility.PutDarkBlueColor()),
                      )
                    : Container(),
                Email != ""
                    ? Text(
                        Email,
                        style: TextStyle(
                            fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                            fontFamily: 'Poppins_Regular',
                            color: Colors.black),
                      )
                    : Container(),
                SizedBox(
                  height: Utility.getDeviceType() == '1' ? 20 : 30,
                ),
                Address != ""
                    ? Text(
                        'Address',
                        style: TextStyle(
                            fontSize: Utility.getDeviceType() == '1' ? 17 : 27,
                            fontFamily: 'Poppins_SemiBold',
                            color: Utility.PutDarkBlueColor()),
                      )
                    : Container(),
                Address != ""
                    ? Text(
                        Address,
                        style: TextStyle(
                            fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                            fontFamily: 'Poppins_Regular',
                            color: Colors.black),
                      )
                    : Container()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
