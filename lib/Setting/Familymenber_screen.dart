import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:softcliniccare/Helper/Constant_Data.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'package:softcliniccare/Home/main_screen.dart';
import 'package:softcliniccare/Setting/Hospitallist_screen.dart';

import '../Helper/API_CALL.dart';
import '../Helper/Database/sqflite_helper.dart';
import '../Model/Login/FamilyMember_List.dart';
import '../Model/Login/Login_API.dart';
import '../Model/Login/OTP_Verify_Model.dart';

class FamilymemberPage extends StatefulWidget {
  FamilymemberPage(
      {Key? key,
      required this.value,
      required this.mobileno,
      required this.familyDetails})
      : super(key: key);
  final int value;
  OTP_Verify familyDetails;
  String mobileno;

  @override
  FamilymemberPageState createState() => FamilymemberPageState();
}

class FamilymemberPageState extends State<FamilymemberPage> {
  String checkdevice = '';
  GlobalKey<State> _KeyHospitalList = new GlobalKey<State>();

  List<familyMemberListing> listing_Family_Store_First = [];
  List<familyMemberListing> listing_SearchFamilyMember = [];

  TextEditingController txt_searchtext = TextEditingController();

  Timer? _debounce;

  @override
  void initState() {
    super.initState();
    checkdevice = Utility.getDeviceType();
    HospitalNameDetails();
  }

  void HospitalNameDetails() {
    listing_Family_Store_First = [];
    for (int i = 0; i < widget.familyDetails.designAll.length; i++) {
      for (int j = 0;
          j < widget.familyDetails.designAll[i].hospital_List.length;
          j++) {
        // print("Name Patient ${widget.familyDetails.designAll[i].name}");
        // print("Name Code ${widget.familyDetails.designAll[i].code}");
        // print(
        //     "Name Patient ID ${widget.familyDetails.designAll[i].hospital_List[j].patientId}");
        // print(
        //     "Name Tenant ID ${widget.familyDetails.designAll[i].hospital_List[j].tenantId}");

        getAllFacilityDetails(
            widget.familyDetails.designAll[i].hospital_List[j].tenantId,
            widget.familyDetails.designAll[i].hospital_List[j].patientId,
            widget.familyDetails.designAll[i].code,
            widget.familyDetails.designAll[i].name,
            widget.familyDetails.designAll[i].hospital_List[j].crNumber);
      }
    }
  }

  void getAllFacilityDetails(String mainFacilityID, String PatientID,
      String PatientCode, String PatientName, String CRNO) async {
    final data = await SQLHelper.getAll_Facility_Response(mainFacilityID);
    List<Map<String, dynamic>> allFacilityResponse = data;
    String mainFacilityResponse = "";

    if (allFacilityResponse.length > 0) {
      for (int i = 0; i < allFacilityResponse.length; i++) {
        if (allFacilityResponse[i]['FacilityID'] == mainFacilityID) {
          mainFacilityResponse = allFacilityResponse[i]['FacilityResponse'];
          break;
        } else {}
      }
      if (mainFacilityResponse == "") {
        API_LoginAPI()
            .MainHospitalName(mainFacilityID)
            .then((mainResponse) async {
          setState(() {
            log("Hospital Details ${mainResponse}");
            if (allFacilityResponse.length > 0) {
              AllDataUpdate(mainFacilityID, mainResponse);
            } else {
              AllDataInsert(mainFacilityID, mainResponse);
            }
            GetAllFacilityDetails(mainResponse, mainFacilityID, PatientID,
                PatientCode, PatientName, CRNO);
          });
        });
      } else {
        GetAllFacilityDetails(mainFacilityResponse, mainFacilityID, PatientID,
            PatientCode, PatientName, CRNO);
      }
    } else {
      API_LoginAPI()
          .MainHospitalName(mainFacilityID)
          .then((mainResponse) async {
        setState(() {
          // String FinalHospitalName = Response['name'];
          // log("Hospital Details ${Response}");

          if (allFacilityResponse.length > 0) {
            AllDataUpdate(mainFacilityID, mainResponse);
          } else {
            AllDataInsert(mainFacilityID, mainResponse);
          }
          GetAllFacilityDetails(mainResponse, mainFacilityID, PatientID,
              PatientCode, PatientName, CRNO);
        });
      });
    }
  }

  void GetAllFacilityDetails(String mainResponse, String mainFacilityID,
      String PatientID, String PatientCode, String PatientName, String CRNO) {
    // log("Name Hospital Details  ${mainResponse}");
    var resBody = json.decode(mainResponse);

    var tempdata = resBody;

    String FinalHospitalName = resBody['name'];

    // print("Name Hospital Name  ${FinalHospitalName}");
    setState(() {
      listing_Family_Store_First.add(familyMemberListing.fromJson(
          mainFacilityID,
          FinalHospitalName,
          PatientName,
          PatientID,
          PatientCode,
          CRNO));

      // print("Length ${listing_Family_Store.length}");
    });
  }

  void FinalAccessToken(
      String PatientID, String TenantID, String MobileNo) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("PatientID", PatientID);
    prefs.setString("HospitalTenantID", TenantID);
    print(TenantID);

    API_CALL()
        .VerifyMobileOTPCall_FinalToken(MobileNo, PatientID, TenantID)
        .then((Response) async {
      Map FinalResponse = json.decode(Response);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("Token", FinalResponse['Accesstoken']);

      // Navigator.push(
      //   context,
      //   MaterialPageRoute(builder: (context) => MainScreen()),
      // );
      Navigator.pop(context);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => MainScreen()),
          (Route<dynamic> route) => false);
    });
  }

  void AllDataInsert(String FacilityID, String FacilityResponse) async {
    await SQLHelper.insertName_Facility(FacilityID, FacilityResponse);
  }

  void AllDataUpdate(String FacilityID, String FacilityResponse) async {
    await SQLHelper.update_facility_response(FacilityID, FacilityResponse);
  }

  _onSearchChanged(String inputValue) {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(const Duration(milliseconds: 50), () {
      // do something with query

      // print("Here que ${inputValue}");
      print("New value inserted in textField $inputValue");

      if (inputValue.length >= 3) {
        print("Here Search");
        // setState(() {
        //   bottomPortion = false;
        //   Height_of_Search = MediaQuery.of(context).size.height * 0.27;
        // });
        SearchAPICall(inputValue);
      } else {
        print("Not Search");
        setState(() {
          listing_SearchFamilyMember.clear();
        });
        // lst_Search_Medication_Drug = [];
        // setState(() {
        //   lst_Search_Medication_Drug = Temp_lst_SearchMedication_Drug;
        //   searchIndicator = false;
        //   print(
        //       "New value inserted in Search Fav ${Temp_lst_SearchMedication_Drug.length}");
        //   print(
        //       "New value inserted in Search Now New ${lst_Search_Medication_Drug.length}");
        // });
      }
    });
  }

  void SearchAPICall(String searchText) {
    listing_SearchFamilyMember.clear();
    if (searchText.isEmpty) {
      setState(() {});
      return;
    }

    listing_Family_Store_First.forEach((listing_Family_Store) {
      if (listing_Family_Store.facilityName
              .toLowerCase()
              .contains(searchText) ||
          listing_Family_Store.patientName.toLowerCase().contains(searchText) ||
          listing_Family_Store.crNo.toLowerCase().contains(searchText)) {
        listing_SearchFamilyMember.add(listing_Family_Store);
      }
    });

    setState(() {
      print("Search Length ${listing_SearchFamilyMember.length}");
    });
  }

  Widget _buildSearchResults(List<familyMemberListing> listing_Family_Store) {
    return Container(
      padding: EdgeInsets.all(checkdevice == '1' ? 10 : 20),
      child: ListView.separated(
        physics: AlwaysScrollableScrollPhysics(),
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(
            height: checkdevice == '1' ? 8 : 16,
          );
        },
        shrinkWrap: true,
        itemCount: listing_Family_Store.length,
        itemBuilder: (context, pos) {
          return GestureDetector(
            onTap: () {
              setState(() {
                Utility.showLoadingDialog(context, _KeyHospitalList, true);
                print(
                    "Facility Name ${listing_Family_Store[pos].facilityName}");
                ConstantDetails.vFacilityName =
                    listing_Family_Store[pos].facilityName;
                FinalAccessToken(listing_Family_Store[pos].patientID,
                    listing_Family_Store[pos].facilityID, widget.mobileno);

                // List<HospotalList> hospital_List =
                //     widget.familyDetails.designAll[pos].hospital_List;
                //
                // print(hospital_List.length);
                // Utility.showLoadingDialog(context, _KeyHospitalList, true);
                //
                // Future.delayed(const Duration(milliseconds: 500), () {
                //   setState(() {
                //     String MobileNoMain = widget.mobileno;
                //     Navigator.pop(context);
                //     Navigator.push(
                //       context,
                //       MaterialPageRoute(
                //           builder: (context) => HospitallistPage(
                //               value: widget.value,
                //               All_hospital_List: hospital_List,
                //               mobileno: MobileNoMain)),
                //     );
                //   });
                // });
              });
            },
            child: Card(
              color: Colors.white,
              elevation: 5,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(width: checkdevice == '1' ? 8 : 16),
                  Flexible(
                      flex: checkdevice == '1' ? 3 : 0,
                      child: Center(
                        child: Container(
                          width: checkdevice == '1' ? 80 : 130,
                          height: checkdevice == '1' ? 80 : 130,
                          child: Center(
                            child: Text(
                              Utility.getTextDisplay(
                                      listing_Family_Store[pos].patientName)
                                  .toUpperCase(),
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25,
                                  letterSpacing: 2.0,
                                  color: Colors.white),
                            ),
                          ),
                          decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Utility.PutLightBlueColor(),
                            // image: new DecorationImage(
                            //     fit: BoxFit.fill,
                            //     image: new NetworkImage(
                            //         "https://image.shutterstock.com/image-photo/modern-hospital-style-building-260nw-212251981.jpg")),
                          ),
                        ),
                      )),
                  Container(height: checkdevice == '1' ? 110 : 160),
                  SizedBox(
                    width: checkdevice == '1' ? 15 : 25,
                  ),
                  Flexible(
                    flex: checkdevice == '1' ? 5 : 8,
                    child: Column(
                      children: [
                        Center(
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                                listing_Family_Store[pos].patientName +
                                    " ( " +
                                    listing_Family_Store[pos].crNo +
                                    " )",
                                style: TextStyle(
                                    fontFamily: 'Poppins_SemiBold',
                                    fontSize: checkdevice == '1' ? 13 : 25,
                                    color: Utility.PutDarkBlueColor())),
                          ),
                        ),
                        Center(
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(listing_Family_Store[pos].facilityName,
                                style: TextStyle(
                                    fontFamily: 'Poppins_SemiBold',
                                    fontSize: checkdevice == '1' ? 12 : 25,
                                    color: Utility.PutLightBlueColor())),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Flexible(
                      flex: checkdevice == '1' ? 2 : 1,
                      child: Center(
                          child: Container(
                        width: checkdevice == '1' ? 30 : 50,
                        height: checkdevice == '1' ? 30 : 50,
                        child: SvgPicture.asset('Images/GroupBack.svg'),
                      ))),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Future<void> _pullRefresh() async {
    print("Refresh");
    listing_Family_Store_First = [];
    for (int i = 0; i < widget.familyDetails.designAll.length; i++) {
      for (int j = 0;
          j < widget.familyDetails.designAll[i].hospital_List.length;
          j++) {
        getAllPullRefreshDetails(
            widget.familyDetails.designAll[i].hospital_List[j].tenantId,
            widget.familyDetails.designAll[i].hospital_List[j].patientId,
            widget.familyDetails.designAll[i].code,
            widget.familyDetails.designAll[i].name,
            widget.familyDetails.designAll[i].hospital_List[j].crNumber);
      }
    }
  }

  void getAllPullRefreshDetails(String mainFacilityID, String PatientID,
      String PatientCode, String PatientName, String CRNO) async {
    final data = await SQLHelper.getAll_Facility_Response(mainFacilityID);
    List<Map<String, dynamic>> allFacilityResponse = data;
    setState(() {
      API_LoginAPI()
          .MainHospitalName(mainFacilityID)
          .then((mainResponse) async {
        setState(() {
          log("Hospital Details ${mainResponse}");
          if (allFacilityResponse.length > 0) {
            AllDataUpdate(mainFacilityID, mainResponse);
          } else {
            AllDataInsert(mainFacilityID, mainResponse);
          }
          GetAllFacilityDetails(mainResponse, mainFacilityID, PatientID,
              PatientCode, PatientName, CRNO);
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => Utility.Willpopscope,
        child: Scaffold(
            backgroundColor: Colors.white,
            resizeToAvoidBottomInset: false,
            appBar: widget.value == 0
                ? new AppBar(
                    toolbarHeight: Utility.getDeviceType() == "1" ? 50 : 70,
                    backgroundColor: Colors.white,
                    automaticallyImplyLeading: false,
                    centerTitle: false,
                    title: new Padding(
                      padding: const EdgeInsets.only(left: 5),
                      child: new Text(
                        Utility.translate('family_title'),
                        style: TextStyle(
                            fontSize: checkdevice == '1' ? 20 : 30,
                            fontFamily: 'Poppins_SemiBold',
                            color: Utility.PutDarkBlueColor()),
                      ),
                    ),
                  )
                : Utility.TopToolBar(
                    Utility.translate('family_title'),
                    checkdevice == '1' ? 20 : 30,
                    Utility.PutDarkBlueColor(),
                    context),
            body: Column(
              children: [
                SizedBox(
                  height: 15,
                ),
                Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(15, 2, 15, 0),
                      child: TextField(
                        controller: txt_searchtext,
                        onChanged: _onSearchChanged,
                        style: TextStyle(fontSize: 15.0, color: Colors.black),
                        decoration: new InputDecoration(
                          isDense: true,
                          contentPadding: EdgeInsets.all(11),
                          hintText: Utility.translate('search_family_member'),
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                              borderSide:
                                  BorderSide(width: 1, color: Colors.grey)),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            borderSide:
                                BorderSide(width: 1, color: Colors.grey),
                          ),
                        ),
                      ),
                    )),
                Expanded(
                    flex: 15,
                    child: RefreshIndicator(
                        onRefresh: _pullRefresh,
                        child: listing_SearchFamilyMember.length > 0 ||
                                txt_searchtext.text.length > 2
                            ? _buildSearchResults(listing_SearchFamilyMember)
                            : _buildSearchResults(listing_Family_Store_First)))
              ],
            )));
  }

  void HospitalName_To_AllID(
      String HospitalID, List<HospotalList> hospital_List) async {
    API_LoginAPI().MainHospitalName(HospitalID).then((Response) async {
      setState(() {
        String FinalHospitalName = Response['name'];
        log("Hospital Details ${Response}");

        for (int i = 0; i < hospital_List.length; i++) {
          if (hospital_List[i].tenantId == HospitalID) {
            // print(Response['name']);
            hospital_List[i].HospitalName = FinalHospitalName;
            print('Hostpital Name ${hospital_List[i].HospitalName}');
          }
        }
      });
    });
  }
}
