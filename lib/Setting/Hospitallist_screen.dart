import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'package:softcliniccare/Home/main_screen.dart';

import '../Helper/API_CALL.dart';
import '../Model/Login/Login_API.dart';
import '../Model/Login/OTP_Verify_Model.dart';

class HospitallistPage extends StatefulWidget {
  HospitallistPage(
      {Key? key,
      required this.value,
      required this.All_hospital_List,
      required this.mobileno})
      : super(key: key);
  final int value;
  List<HospotalList> All_hospital_List;
  String mobileno;

  @override
  HospitallistPageState createState() => HospitallistPageState();
}

class HospitallistPageState extends State<HospitallistPage> {
  String checkdevice = '';
  GlobalKey<State> _KeyHospitalList = new GlobalKey<State>();

  @override
  void initState() {
    super.initState();
    checkdevice = Utility.getDeviceType();
  }

  void FinalAccessToken(
      String PatientID, String TenantID, String MobileNo) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("PatientID", PatientID);
    prefs.setString("HospitalTenantID", TenantID);
    print(TenantID);

    API_CALL()
        .VerifyMobileOTPCall_FinalToken(MobileNo, PatientID, TenantID)
        .then((Response) async {
      Map FinalResponse = json.decode(Response);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("Token", FinalResponse['Accesstoken']);

      // Navigator.push(
      //   context,
      //   MaterialPageRoute(builder: (context) => MainScreen()),
      // );
      Navigator.pop(context);
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
          MainScreen()), (Route<dynamic> route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: widget.value == 0
            ? new AppBar(
                toolbarHeight: Utility.getDeviceType() == "1" ? 50 : 70,
                backgroundColor: Colors.white,
                automaticallyImplyLeading: false,
                centerTitle: false,
                title: new Padding(
                  padding: const EdgeInsets.only(left: 5),
                  child: new Text(
                    Utility.translate('hospital_title'),
                    style: TextStyle(
                        fontSize: checkdevice == '1' ? 20 : 30,
                        fontFamily: 'Poppins_SemiBold',
                        color: Utility.PutDarkBlueColor()),
                  ),
                ),
              )
            : Utility.TopToolBar(
                Utility.translate('family_title'),
                checkdevice == '1' ? 20 : 30,
                Utility.PutDarkBlueColor(),
                context),
        body: Container(
          padding: EdgeInsets.all(checkdevice == '1' ? 10 : 20),
          child: ListView.separated(
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                height: checkdevice == '1' ? 8 : 16,
              );
            },
            itemCount: widget.All_hospital_List.length,
            shrinkWrap: true,
            itemBuilder: (context, pos) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    Utility.showLoadingDialog(context, _KeyHospitalList, true);
                    FinalAccessToken(
                        widget.All_hospital_List[pos].patientId,
                        widget.All_hospital_List[pos].tenantId,
                        widget.mobileno);
                  });
                },
                child: Card(
                  color: Colors.white,
                  elevation: 5,
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: checkdevice == '1' ? 8 : 16),
                      Flexible(
                          flex: checkdevice == '1' ? 3 : 0,
                          child: Center(
                            child: Container(
                              width: checkdevice == '1' ? 80 : 130,
                              height: checkdevice == '1' ? 80 : 130,
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.pinkAccent,
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage(
                                        "https://image.shutterstock.com/image-photo/modern-hospital-style-building-260nw-212251981.jpg")),
                              ),
                            ),
                          )),
                      Container(height: checkdevice == '1' ? 110 : 160),
                      SizedBox(
                        width: checkdevice == '1' ? 15 : 25,
                      ),
                      Flexible(
                        flex: checkdevice == '1' ? 5 : 8,
                        child: Center(
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                                widget.All_hospital_List[pos].HospitalName,
                                style: TextStyle(
                                    fontFamily: 'Poppins_SemiBold',
                                    fontSize: checkdevice == '1' ? 15 : 25,
                                    color: Utility.PutDarkBlueColor())),
                          ),
                        ),
                      ),
                      Flexible(
                          flex: checkdevice == '1' ? 2 : 1,
                          child: Center(
                              child: Container(
                            width: checkdevice == '1' ? 30 : 50,
                            height: checkdevice == '1' ? 30 : 50,
                            child: SvgPicture.asset('Images/GroupBack.svg'),
                          ))),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
