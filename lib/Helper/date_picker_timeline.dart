library date_picker_timeline;
import 'package:flutter/material.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'date_widget.dart';
import 'dimen.dart';
import 'color.dart';
import 'dimen.dart';
import 'tap.dart';
class DatePickerTimeline extends StatefulWidget {
  final double dateSize, daySize, monthSize;
  final Color dateColor, monthColor, dayColor;
  DateTime currentDate;
  DateTime selectedDate;
  final DateChangeListener onDateChange;
  DatePickerTimeline(
      {
        required Key key,
        required this.currentDate,
        required this.selectedDate,
        this.dateSize = Dimen.dateTextSize,
        this.daySize = Dimen.dayTextSize,
        this.monthSize = Dimen.monthTextSize,
        this.dateColor = AppColors.defaultDateColor,
        this.monthColor = AppColors.defaultMonthColor,
        this.dayColor = AppColors.defaultDayColor,
        required this.onDateChange,
      }) : super(key: key);
  @override
  State<StatefulWidget> createState() => new _DatePickerState();
}
class _DatePickerState extends State<DatePickerTimeline> {
  final _controller = ScrollController();
  @override
  void initState() {
    // DateTime _date = DateTime.now();
    // widget.currentDate = new DateTime(_date.year, _date.month, _date.day);
    // ;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
        height: Utility.getDeviceType() == '1' ? 80 : 120,
        child: ListView.builder(
            itemCount: 50000,
            controller: _controller,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              DateTime _date = widget.selectedDate.add(Duration(days: index));
              DateTime date = new DateTime(_date.year, _date.month, _date.day);
              bool isSelected = compareDate(date, widget.currentDate);
              return DateWidget(
                  date: date,
                  dateColor: isSelected ? Colors.white :widget.dateColor,
                  dateSize: Utility.getDeviceType() == '1' ? widget.dateSize : 30,
                  dayColor: isSelected ? Colors.white :widget.dateColor,
                  daySize: Utility.getDeviceType() == '1' ? widget.daySize : 20,
                  monthColor: isSelected ? Colors.white :widget.dateColor,
                  monthSize: Utility.getDeviceType() == '1' ? widget.monthSize : 20,
                  selectionColor: isSelected
                      ? Utility.PutDarkBlueColor()
                      : Colors.transparent,
                  onDateSelected: (selectedDate) {
                    // A date is selected
                    if (widget.onDateChange != null) {
                      widget.onDateChange(selectedDate);
                    }
                    setState(() {
                      widget.currentDate = selectedDate;
                    });
                  });
            }));
  }
  bool compareDate(DateTime date1, DateTime date2) {
    return date1.day == date2.day &&
        date1.month == date2.month &&
        date1.year == date2.year;
  }
}