class Dimen {
  Dimen._();
  static const double dateTextSize = 23;
  static const double dayTextSize = 11;
  static const double monthTextSize = 11;
}