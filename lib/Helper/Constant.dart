class ConstantVar {
  ConstantVar._();

  // Common Data
  static String str_Common_GetAllAppointmentStatus = 'GetAll_AppointmentStatus';

  // Apointment Listing
  static String str_Common_GETUPCOMINGAPPOINTMENT = 'UPCOMING';
  static String str_Common_GETPASTAPPOINTMENT = 'PAST';

  static String str_Common_GETUNPAID_INVOICE = 'UNPAID_INVOICE';
  static String str_Common_GETPAID_INVOICE = 'PAID_INVOICE';

  // All Facility name
  static String str_Common_GETALLFACILITYNAME = 'ALLFACILITYNAME';

}