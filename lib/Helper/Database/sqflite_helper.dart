import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart' as sql;

class SQLHelper {
  static Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE DateTime_Added(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        DateDetails TEXT,
        Date TEXT,
        Time TEXT
      )
      """);

    await database.execute("""CREATE TABLE PatientAllDetails(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        PatientID TEXT,
        PatientMethod TEXT,
        PatientResponse TEXT
      )
      """);

    await database.execute("""CREATE TABLE ProfileData(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        profile_id TEXT,
        profile_response TEXT
      )
      """);

    await database.execute("""CREATE TABLE FacilityDetails(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        FacilityID TEXT,
        FacilityResponse TEXT
      )
      """);

    await database.execute("""CREATE TABLE PDFSave_FullPayment(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        invoiceID TEXT,
        invoiceResponse TEXT,
        categoryDownload TEXT
      )
      """);

    await database.execute("""CREATE TABLE APPOINTMENT_LIST(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        PatientID TEXT,
        AppointmentResponse TEXT,
        Upcoming_Past TEXT
      )
      """);

    await database.execute("""CREATE TABLE NotChangeData_Common(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        APIName TEXT,
        APIResponse TEXT,
        Date TEXT,
        Time TEXT
      )
      """);
  }

// id: the id of a item
// title, description: name and description of your activity
// created_at: the time that the item was created. It will be automatically handled by SQLite

  static Future<sql.Database> db() async {
    return sql.openDatabase(
      'genx_care_app_V7.db',
      version: 7,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

  // Create new item (journal)
  static Future<int> createItem(String email) async {
    final db = await SQLHelper.db();

    final data = {'Login_Email': email};
    final id = await db.insert('LoginData', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  // Read all items (journals)
  static Future<List<Map<String, dynamic>>> getAllLoginData() async {
    final db = await SQLHelper.db();
    return db.query('LoginData', orderBy: "id");
  }

  // Update an item by id
  static Future<int> updateItem(
      int id, String title, String? descrption) async {
    final db = await SQLHelper.db();

    final data = {
      'title': title,
      'description': descrption,
      'createdAt': DateTime.now().toString()
    };

    final result =
        await db.update('items', data, where: "id = ?", whereArgs: [id]);
    return result;
  }

  // Delete
  static Future<void> deleteItem(int id) async {
    final db = await SQLHelper.db();
    try {
      await db.delete("items", where: "id = ?", whereArgs: [id]);
    } catch (err) {
      debugPrint("Something went wrong when deleting an item: $err");
    }
  }

  // Insert Name of the data
  static Future<int> insertName_Facility(
      String facilityID, String facilityResponse) async {
    final db = await SQLHelper.db();

    final data = {
      'FacilityID': facilityID,
      'FacilityResponse': facilityResponse
    };
    final id = await db.insert('FacilityDetails', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  static Future<int> update_facility_response(
      String facilityID, String facilityResponse) async {
    final db = await SQLHelper.db();
    return await db.rawUpdate(
        "UPDATE FacilityDetails SET FacilityResponse = \'$facilityResponse\' WHERE FacilityID = '$facilityID'");
  }

  // Read all items (journals)
  static Future<List<Map<String, dynamic>>> getAll_Facility_Response(
      String facilityID) async {
    final db = await SQLHelper.db();
    var result = await db.rawQuery(
        "SELECT * FROM FacilityDetails WHERE FacilityID = '$facilityID'");
    // print("DB Length ${result.length}");
    return result;
  }

  // Insert PDF of the data
  static Future<int> insert_PDF_Details(
      String invoiceID, String invoiceResponse, String CategoryName) async {
    final db = await SQLHelper.db();

    final data = {
      'invoiceID': invoiceID,
      'invoiceResponse': invoiceResponse,
      'categoryDownload': CategoryName
    };
    final id = await db.insert('PDFSave_FullPayment', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  static Future<int> update_invoice_response(
      String invoiceID, String invoiceResponse, String Category) async {
    final db = await SQLHelper.db();
    return await db.rawUpdate(
        "UPDATE PDFSave_FullPayment SET invoiceResponse = \'$invoiceResponse\' WHERE invoiceID = '$invoiceID' AND "
        "categoryDownload = '$Category'");
  }

  // Read all items (journals)
  static Future<List<Map<String, dynamic>>> getAll_PDF_Response(
      String invoiceID, String Category) async {
    final db = await SQLHelper.db();
    var result = await db.rawQuery(
        "SELECT * FROM PDFSave_FullPayment WHERE invoiceID = '$invoiceID' AND " +
            "categoryDownload = '$Category'");
    // print("DB Length ${result.length}");
    return result;
  }

  // Insert Appointment List
  static Future<int> insert_Appointment_Details(String PatientID,
      String AppointmentResponse, String Upcoming_Past) async {
    final db = await SQLHelper.db();

    final data = {
      'PatientID': PatientID,
      'AppointmentResponse': AppointmentResponse,
      'Upcoming_Past': Upcoming_Past
    };
    final id = await db.insert('APPOINTMENT_LIST', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  static Future<int> update_Appointment_response(String PatientID,
      String AppointmentResponse, String Upcoming_Past) async {
    final db = await SQLHelper.db();
    return await db.rawUpdate(
        "UPDATE APPOINTMENT_LIST SET AppointmentResponse = \'$AppointmentResponse\' WHERE PatientID = '$PatientID' AND "
        "Upcoming_Past = '$Upcoming_Past'");
  }

  static Future<List<Map<String, dynamic>>> getAll_AppointmentList(
      String PatientID, String Upcoming_Past) async {
    final db = await SQLHelper.db();
    var result = await db.rawQuery(
        "SELECT * FROM APPOINTMENT_LIST WHERE PatientID = '$PatientID' AND " +
            "Upcoming_Past = '$Upcoming_Past'");
    // print("DB Length ${result.length}");
    return result;
  }

  // Insert Profile List
  static Future<int> insert_Profile_Details(
      String PatientID, String ProfileResponse) async {
    final db = await SQLHelper.db();

    final data = {'profile_id': PatientID, 'profile_response': ProfileResponse};
    final id = await db.insert('ProfileData', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  static Future<int> update_Profile_response(
      String PatientID, String ProfileResponse) async {
    final db = await SQLHelper.db();
    return await db.rawUpdate(
        "UPDATE ProfileData SET profile_response = \'$ProfileResponse\' WHERE profile_id = '$PatientID'");
  }

  static Future<List<Map<String, dynamic>>> getAll_ProfileDetails(
      String PatientID) async {
    final db = await SQLHelper.db();
    var result = await db.rawQuery(
        "SELECT * FROM ProfileData WHERE " + "profile_id = '$PatientID'");
    // print("DB Length ${result.length}");
    return result;
  }

  // Date time added which need to check all details
  static Future<int> insertMainDateTime(
      String Date, String Time, String DateDetails) async {
    final db = await SQLHelper.db();

    final data = {'DateDetails': DateDetails, 'Date': Date, 'Time': Time};
    final id = await db.insert('DateTime_Added', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  // Read all items (journals)
  static Future<List<Map<String, dynamic>>> getDateTimeDetails() async {
    final db = await SQLHelper.db();
    return db.query('DateTime_Added', orderBy: "id");
  }

  // All Common Data will be Save
  static Future<int> insertMainCommonData(
      String APINAME, String APIResponse, String Date, String Time) async {
    final db = await SQLHelper.db();

    final data = {
      'APIName': APINAME,
      'APIResponse': APIResponse,
      'Date': Date,
      'Time': Time
    };
    final id = await db.insert('NotChangeData_Common', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  static Future<int> updateNoChangeDetails(
      String APIName, String APIResponse) async {
    final db = await SQLHelper.db();
    return await db.rawUpdate(
        "UPDATE NotChangeData_Common SET APIResponse = \'$APIResponse\' WHERE APIName = '$APIName'");
  }

  // Read all items (journals)
  static Future<List<Map<String, dynamic>>> getNoChangeDetails(
      String APIName) async {
    final db = await SQLHelper.db();
    return db.query('NotChangeData_Common',
        where: "APIName = ?", whereArgs: [APIName]);
  }

  // Insert Patient Add
  static Future<int> insert_PatientAdd(String patientID, String PatientMethod,
      String PatientMethodResponse) async {
    final db = await SQLHelper.db();

    final data = {
      'PatientID': patientID,
      'PatientMethod': PatientMethod,
      'PatientResponse': PatientMethodResponse
    };
    final id = await db.insert('PatientAllDetails', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  // Read all Visit No Data
  static Future<List> getAllPatientDetails(
      String patientID, String PatientMethod) async {
    final db = await SQLHelper.db();
    var result = await db.rawQuery(
        "SELECT * FROM PatientAllDetails WHERE PatientID = '$patientID' AND "
        "PatientMethod = '$PatientMethod'");
    return result;
  }

  static Future<int> UpdateAllPatientDetails(String patientID,
      String PatientMethod, String PatientMethodResponse) async {
    final db = await SQLHelper.db();
    return await db.rawUpdate(
        "UPDATE PatientAllDetails SET PatientResponse = '$PatientMethodResponse' WHERE PatientID = '$patientID' AND "
        "PatientMethod = '$PatientMethod'");
  }

  // Delete All Record from Table
  static deleteAll() async {
    final db = await SQLHelper.db();
    await db.rawDelete("Delete from DateTime_Added");
    await db.rawDelete("Delete from ProfileData");
    await db.rawDelete("Delete from FacilityDetails");
    await db.rawDelete("Delete from PDFSave_FullPayment");
    await db.rawDelete("Delete from APPOINTMENT_LIST");
    await db.rawDelete("Delete from PatientAllDetails");
  }

  static delete_Logout_TableDetails() async {
    final db = await SQLHelper.db();
    await db.rawDelete("Delete from ProfileData");
    await db.rawDelete("Delete from FacilityDetails");
    await db.rawDelete("Delete from PDFSave_FullPayment");
    await db.rawDelete("Delete from APPOINTMENT_LIST");
    await db.rawDelete("Delete from PatientAllDetails");
  }
}
