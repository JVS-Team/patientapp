import 'dart:typed_data';

import 'package:flutter/cupertino.dart';

class ConstData {
  static const Color FONT_COLOR = Color(0xFF333333);
  static const Color BLUE_COLOR = Color(0xFF4499cb);
  static const Color ORANGE_COLOR = Color(0xFFe84a02);
  static const Color WHITE_COLOR = Color(0xFFFFFFFF);
  static const Color WHITE_LIGHT_COLOR = Color(0xFFF5F5F5);
  static const Color BLACK = Color(0xFF000000);

  static const String IS_LOGIN = "isLogin";
  static const String TOKEN = "token";
  static const String USER_ID = "user_id";
  static const String LOGIN_RESPONSE = "LoginResponse";
  static const String BUYER_CATEGORIES = "buyer_categories";
  static const String BUYER_LOCATION = "buyer_location";
  static final String baseURL = "https://www.my-business.com/ApiBuyer";
}

class Categories {
  String? id;
  String? parentId;
  String? name;
  String? extraParam;
  String? DrDegree;
  String? DrImages;
  Uint8List? ByteList;

  Categories({
    required this.id,
    required this.parentId,
    required this.name,
    required this.extraParam,
    required this.DrDegree,
    required this.DrImages,
    required this.ByteList,
  });

  Categories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    parentId = json['name'];
    name = json['displayName'];
    extraParam = json['extraParam'];
    DrDegree = '';
    DrImages = '';
    ByteList = Uint8List.fromList([]);
  }
}

class HospitalData {
  String? fId;
  String? locationId;
  String? facilityShortName;
  String? website;
  String? parentLocationId;
  String? licenceNumber;
  String? weeklyOffDay;
  String? specilitesTypeId;
  String? facilityLogo;
  String? name;
  String? createdBy;
  String? createdAt;
  String? lastModifiedBy;
  String? lastModifiedAt;
  String? status;
  String? displayName;
  String? code;
  String? id;

  HospitalData({
    required this.id,
    required this.fId,
    required this.locationId,
    required this.facilityShortName,
    required this.website,
    required this.parentLocationId,
    required this.licenceNumber,
    required this.weeklyOffDay,
    required this.specilitesTypeId,
    required this.facilityLogo,
    required this.name,
    required this.createdBy,
    required this.createdAt,
    required this.lastModifiedBy,
    required this.lastModifiedAt,
    required this.status,
    required this.displayName,
    required this.code,
  });

  HospitalData.fromJson(Map<String, dynamic> json) {
    fId = json['facilityTypeId'];
    locationId = json['locationId'];
    //if (json['facilityShortName']  == null)
    // {
    facilityShortName = "";
    // }else
    //{
    facilityShortName = json['facilityShortName'];
    //}

    website = ''; //json['website'];
    parentLocationId = json['parentLocationId'];
    licenceNumber = ''; //json['licenceNumber'];
    weeklyOffDay = ''; //json['weeklyOffDay'];
    specilitesTypeId = json['specilitesTypeId'];
    facilityLogo = json['facilityLogo'];
    name = json['name'];
    createdBy = json['createdBy'];
    createdAt = json['createdAt'];
    lastModifiedBy = json['lastModifiedBy'];
    lastModifiedAt = json['lastModifiedAt'];
    status = json['status'];
    displayName = json['displayName'];
    code = json['code'];
    id = json['id'];
  }
}

class Department {
  String? facilityId;
  String? locationId;
  String? type;
  String? head;
  String? name;
  String? createdBy;
  String? createdAt;
  String? lastModifiedBy;
  String? lastModifiedAt;
  String? status;
  String? displayName;
  String? code;
  String? id;

  Department({
    required this.facilityId,
    required this.locationId,
    required this.type,
    required this.head,
    required this.name,
    required this.createdBy,
    required this.createdAt,
    required this.lastModifiedBy,
    required this.lastModifiedAt,
    required this.status,
    required this.displayName,
    required this.code,
    required this.id,
  });

  Department.fromJson(Map<String, dynamic> json) {
    facilityId = json['facilityId'];
    locationId = json['locationId'];
    type = json['type'];
    head = json['head'];
    name = json['name'];
    createdBy = json['createdBy'];
    createdAt = json['createdAt'];
    lastModifiedBy = json['lastModifiedBy'];
    lastModifiedAt = json['lastModifiedAt'];
    status = json['status'];
    displayName = json['displayName'];
    code = json['code'];
    id = json['id'];
  }
}

class Clinic {
  String? locationTypeId;
  String? name;
  String? createdBy;
  String? createdAt;
  String? lastModifiedBy;
  String? lastModifiedAt;
  String? status;
  String? displayName;
  String? code;
  String? id;

  Clinic({
    required this.locationTypeId,
    required this.name,
    required this.createdBy,
    required this.createdAt,
    required this.lastModifiedBy,
    required this.lastModifiedAt,
    required this.status,
    required this.displayName,
    required this.code,
    required this.id,
  });

  Clinic.fromJson(Map<String, dynamic> json) {
    locationTypeId = json['locationTypeId'];
    name = json['name'];
    createdBy = json['createdBy'];
    createdAt = json['createdAt'];
    lastModifiedBy = json['lastModifiedBy'];
    lastModifiedAt = json['lastModifiedAt'];
    status = json['status'];
    displayName = json['displayName'];
    code = json['code'];
    id = json['id'];
  }
}

class AppointmentCategory {
  // bool? isDefault;
  // String? applicableGender;
  // bool? extraCharge;
  // String? serviceGroupId;
  // String? serviceMasterId;
  // bool? isAgeSpecificInCategory;
  // String? fromAge;
  // int? toAge;
  // int? name;
  // String? createdBy;
  // String? createdAt;
  String? lastModifiedBy;
  String? lastModifiedAt;
  String? status;
  String? displayName;
  String? code;
  String? id;

  AppointmentCategory({
    // required this.isDefault,
    //  required this.applicableGender,
    //  required this.extraCharge,
    //  required this.serviceGroupId,
    //  required this.serviceMasterId,
    //  required this.isAgeSpecificInCategory,
    //  required this.fromAge,
    //  required this.toAge,
    //  required this.name,
    //  required this.createdBy,
    //  required this.createdAt,
    required this.lastModifiedBy,
    required this.lastModifiedAt,
    required this.status,
    required this.displayName,
    required this.code,
    required this.id,
  });

  AppointmentCategory.fromJson(Map<String, dynamic> json) {
    // isDefault = json['isDefault'];
    // applicableGender = json['applicableGender'];
    // extraCharge = json['extraCharge'];
    // serviceGroupId = json['serviceGroupId'];
    // serviceMasterId = json['serviceMasterId'];
    // isAgeSpecificInCategory = json['isAgeSpecificInCategory'];
    // fromAge = json['fromAge'];
    // toAge = json['toAge'];
    // name = json['name'];
    // createdBy = json['createdBy'];
    // createdAt = json['createdAt'];
    lastModifiedBy = json['lastModifiedBy'];
    lastModifiedAt = json['lastModifiedAt'];
    status = json['status'];
    displayName = json['displayName'];
    code = json['code'];
    id = json['id'];
  }
}

class Appointmentstatus {
  String? displayName;
  String? id;

  Appointmentstatus({
    required this.displayName,
    required this.id,
  });

  Appointmentstatus.fromJson(Map<String, dynamic> json) {
    displayName = json['displayName'];
    id = json['id'];
  }
}


class Documents {
  String? patientId;
  String? group;
  String? fileName;
  String? description;
  String? fileType;
  String? fileImage;
  String? filePath;
  String? filebase64;
  String? fileList;
  String? createdAt;
  String? id;
  String? name;
  String? status;
  String? displayName;
  String? code;
  String? tenantId;

  Documents({
    required this.patientId,
    required this.group,
    required this.fileName,
    required this.description,
    required this.fileType,
    required this.fileImage,
    required this.filePath,
    required this.filebase64,
    required this.fileList,
    required this.createdAt,
    required this.id,
    required this.name,
    required this.status,
    required this.displayName,
    required this.code,
    required this.tenantId,
  });

  Documents.fromJson(Map<String, dynamic> json) {
    patientId = json['patientId'];
    group = json['group'];
    fileName = json['fileName'];
    description = json['description'];
    fileType = json['fileType'];
    fileImage = json['fileImage'];
    filePath = json['filePath'];
    filebase64 = json['filebase64'];
    fileList = json['fileList'];
    createdAt = json['createdAt'];
    id = json['id'];
    name = json['name'];
    status = json['status'];
    displayName = json['displayName'];
    code = json['code'];
    tenantId = json['tenantId'];
  }
}
