import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'tap.dart';
class DateWidget extends StatelessWidget {
  final DateTime date;
  final double dateSize, daySize, monthSize;
  final Color dateColor, monthColor, dayColor;
  final Color selectionColor;
  final DateSelectionCallback onDateSelected;
  DateWidget(
      {required this.date,
        required this.dateSize,
        required this.daySize,
        required this.monthSize,
        required this.dateColor,
        required this.monthColor,
        required this.dayColor,
        required this.selectionColor,
        required this.onDateSelected});
  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Container(
            margin: EdgeInsets.all(3.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              color: selectionColor,
            ),
            child: Padding(
                padding: Utility.getDeviceType() == '1' ? EdgeInsets.only(
                    top: 8.0, bottom: 8.0, left: 15, right: 15) : EdgeInsets.only(
                    top: 8.0, bottom: 8.0, left: 22, right: 22),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                          new DateFormat("MMM")
                              .format(date)
                              .toUpperCase(), // Month
                          style: TextStyle(
                            color: monthColor,
                            fontSize: monthSize,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w500,
                          )),
                      SizedBox(height: Utility.getDeviceType() == '1' ? 0 : 5,),
                      Text(date.day.toString(), // Date
                          style: TextStyle(
                            color: dateColor,
                            fontSize: dateSize,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w700,
                          )),
                      SizedBox(height: Utility.getDeviceType() == '1' ? 0 : 5,),
                      Text(
                          new DateFormat("E")
                              .format(date)
                              .toUpperCase(), // WeekDay
                          style: TextStyle(
                            color: dayColor,
                            fontSize: daySize,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.w500,
                          ))
                    ]))),
        onTap: () {
          // Check if onDateSelected is not null
          if (onDateSelected != null) {
            // Call the onDateSelected Function
            onDateSelected(this.date);
          }
        });
  }
}