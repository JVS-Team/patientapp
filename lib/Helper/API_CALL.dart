import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:softcliniccare/Helper/AllURL.dart';

class API_CALL {
  //Login Api Call
  Future<Map> loginCall(String Username, String Password) async {
    var response =
        await http.post(Uri.parse(AllURL.MainURL + AllURL.LoginURL), body: {
      'Username': Username,
      'password': Password,
      'grant_type': 'password',
      'client_id': 'a665fe6b-e7b4-45b5-95ef-23343544dcfa',
      'client_secret': '37a7c5bb-dd64-47c9-a6b0-0322d710fb89'
    });
    var resBody = json.decode(response.body);
    log("Login Response ${resBody}");
    return resBody;
  }

  //Mobile OTP Api Call
  Future<String> MobileOTPCall(String MobileNo) async {
    var map = new Map<String, dynamic>();
    var resBody = "";
    map['cellnumber'] = MobileNo;

    var response = await http
        .post(Uri.parse(AllURL.MainURL + AllURL.MobileOTPSendURL), body: map);
    if (response.statusCode == 200) {
      // resBody = json.decode(response.body);
      resBody = response.body;
    } else {
      log("Error Mobile Response -  ${response.body}");
      resBody = "Error";
    }
    log("Mobile Response -  ${response.statusCode}");
    log("Mobile OTP Response ${resBody}");
    return resBody;
  }

  //Mobile OTP Verify Api Call - Temporary Token
  Future<String> VerifyMobileOTPCall_TemporaryToken(
      String MobileNo, String OTP) async {
    var map = new Map<String, dynamic>();
    var resBody = "";
    map['cellnumber'] = MobileNo;
    map['otp'] = OTP;
    map['client_secret'] = '02a0f748-fa1b-4c3a-83ae-de30ee23d764';
    map['client_id'] = '128a789b-3dcd-46e2-84ca-f21763fbd596';
    var response = await http.post(
        Uri.parse(AllURL.MainURL + AllURL.VerifyMobileOTP_Temporary_Token),
        body: map);
    log("Mobile OTP Verify Response ${response.body}");
    if (response.statusCode == 200) {
      // resBody = json.decode(response.body);
      resBody = response.body;
    } else {
      resBody = "Error";
    }
    // log("Mobile Response -  ${response.statusCode}");
    // log("Mobile OTP Verify Response ${resBody}");
    return resBody;
  }

  Future<String> VerifyMobileOTPCall(String MobileNo, String OTP) async {
    var map = new Map<String, dynamic>();
    var resBody = "";
    map['cellnumber'] = MobileNo;
    map['otp'] = OTP;
    var response = await http
        .post(Uri.parse(AllURL.MainURL + AllURL.VerifyMobileOTP), body: map);
    if (response.statusCode == 200) {
      // resBody = json.decode(response.body);
      resBody = response.body;
    } else {
      resBody = "Error";
    }
    // log("Mobile Response -  ${response.statusCode}");
    // log("Mobile OTP Verify Response ${resBody}");
    return resBody;
  }

  //Mobile OTP Verify Api Call - Final Token
  Future<String> VerifyMobileOTPCall_FinalToken(
      String MobileNo, String PatientID, String TenantID) async {
    var map = new Map<String, dynamic>();
    var resBody = "";

    map['cellnumber'] = MobileNo;
    map['patientid'] = PatientID;
    map['tenantid'] = TenantID;
    map['client_secret'] = '37a7c5bb-dd64-47c9-a6b0-0322d710fb89';
    map['client_id'] = 'a665fe6b-e7b4-45b5-95ef-23343544dcfa';

    var response = await http.post(
        Uri.parse(AllURL.MainURL + AllURL.FinalToken_Generator),
        body: map);
    // log("Mobile Token Response ${response.body}");
    if (response.statusCode == 200) {
      // resBody = json.decode(response.body);
      resBody = response.body;
    } else {
      resBody = "Error";
    }
    log("Mobile Response -  ${response.statusCode}");
    log("Mobile Token Response ${resBody}");
    return resBody;
  }
}
