class ConstantDetails {
  ConstantDetails._();

  // Package Info Details All
  static String vAppName = "";
  static String vAppPackageName = "";
  static String vAppVersionNo = "";
  static String vAppBuilderNo = "";

  // Facility Name
  static String vFacilityName = "";
}
