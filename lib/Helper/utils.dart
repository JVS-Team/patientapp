import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'dart:io' show Platform;

class Utility {
  // swipe to navigate all screen
  static bool Willpopscope = true;

  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key, bool visible) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          // bool manuallyClosed = false;
          // Future.delayed(Duration(seconds: 10)).then((_) {
          //   if (!manuallyClosed) {
          //     print(manuallyClosed);
          //     print('Close');
          //     Navigator.of(context).pop();
          //   }
          // }).catchError((err) {
          //   print('err ${err}');
          // });
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.white,
                  children: <Widget>[
                    Container(
                      width: 5,
                      child: Center(
                        child: Column(children: [
                          CircularProgressIndicator(),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Please Wait....",
                            style: TextStyle(color: Colors.lightBlue),
                          )
                        ]),
                      ),
                    )
                  ]));
        });
  }

  static void showAlert(
      BuildContext context, String contentDesc, String ContentTitle) {
    var alert = new AlertDialog(
      title: Text(ContentTitle),
      content: Text(contentDesc),
      actions: <Widget>[
        new TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(
              "OK",
              style: TextStyle(color: Colors.black),
            ))
      ],
    );

    showDialog(
        context: context,
        builder: (_) {
          return alert;
        });
  }

  boldText(String text, double fontSize, var color) {
    return Container(
        padding: EdgeInsets.all(0),
        child: Text(text,
            style: TextStyle(
                fontFamily: 'Bold',
                fontSize: fontSize,
                color: color,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.bold)));
  }

  italicText(String text, double fontSize, var color) {
    return Container(
        padding: EdgeInsets.all(0),
        child: Text(text,
            style: TextStyle(
              fontFamily: 'Light',
              fontSize: fontSize,
              color: color,
              fontStyle: FontStyle.italic,
            )));
  }

  normalText(String text, double fontSize, var color) {
    //Colors.lightBlue
    return Container(
      padding: EdgeInsets.all(0),
      child: new Text(text,
          style: TextStyle(
            fontFamily: 'Light',
            fontSize: fontSize,
            color: color,
            fontStyle: FontStyle.normal,
          )),
    );
  }

  static Color PutDarkBlueColor() {
    const color = const Color(0xff1D2284);
    return color;
  }

  static const Color gradiant = Color(0xff0F1144);
  static const Color gradiant1 = Color(0xff1D2284);

  static Color PutLightBlueColor() {
    const color = const Color(0xff00AADE);
    return color;
  }

  static Color PutBackgroundCardColor() {
    const color = const Color(0xffEBF8FD);
    return color;
  }

  static TopToolBar(
      String text, double fontSize, var allcolor, BuildContext context) {
    //Colors.lightBlue
    return AppBar(
      toolbarHeight: Utility.getDeviceType() == "1" ? 50 : 80,
      titleSpacing: Utility.getDeviceType() == "1" ? -5 : 15,
      backgroundColor: Colors.white,
      leading: IconButton(
        padding: new EdgeInsets.fromLTRB(
            Utility.getDeviceType() == "1" ? 10 : 20, 0, 0, 0),
        icon: Icon(
          Icons.arrow_back_ios,
          color: allcolor,
          size: Utility.getDeviceType() == "1" ? 30 : 45,
        ),
        onPressed: () => Navigator.of(context).pop(),
      ),
      // actions: [
      //   Container(
      //       padding: Utility.getDeviceType() == '1'
      //           ? EdgeInsets.fromLTRB(0, 0, 10, 0)
      //           : EdgeInsets.fromLTRB(0, 5, 5, 0),
      //       height: Utility.getDeviceType() == '1' ? 50 : 70,
      //       width: Utility.getDeviceType() == '1' ? 50 : 70,
      //       child: CircleAvatar(
      //         radius: Utility.getDeviceType() == '1' ? 55 : 70,
      //         backgroundColor: Colors.transparent,
      //         child: CircleAvatar(
      //           radius: Utility.getDeviceType() == '1' ? 50 : 70,
      //           backgroundColor: Colors.transparent,
      //           backgroundImage: NetworkImage(
      //               'https://vinusimages.co/wp-content/uploads/2018/10/EG7A2390.djpgA_.jpg'),
      //         ),
      //       ))
      // ],
      title: Text(text,
          style: TextStyle(
              fontSize: fontSize,
              fontFamily: 'Poppins_SemiBold',
              color: allcolor)),
    );
  }

  static MainNormalTextWithFont(
      String text, double fontSize, var color, String FontName) {
    return Container(
      child: new Text(text,
          softWrap: true,
          style: TextStyle(
            fontFamily: FontName,
            fontSize: fontSize,
            color: color,
            fontStyle: FontStyle.normal,
          )),
    );
  }

  static WholeAppSnackbar(BuildContext context, String Msg) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      action: SnackBarAction(
        label: '',
        onPressed: () {
          // Code to execute.
        },
      ),
      content: Text(Msg),
      width: 320.0,
      // Width of the SnackBar.
      padding: const EdgeInsets.symmetric(
        horizontal: 15.0, // Inner padding for SnackBar content.
      ),
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
    ));
  }

  static SaveAllLangData(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static Future<String> getgetAlllocalData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? stringValue = prefs.getString('LangName');
    // print('Value : ${stringValue}');
    if (stringValue == null) {
      return '';
    } else {
      return stringValue;
    }
  }

  static Future<String> getAllLangData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? stringValue = prefs.getString('LangName');
    // print('Value : ${stringValue}');
    if (stringValue == null) {
      return 'en';
    } else {
      return stringValue;
    }
  }

  static List CountryCode = []; //edited line

  static Map<String, dynamic> jsonLanguageMap = {};

  static void load() async {
    // String languagename = getAllLangData().toString();
    getAllLangData().then((languagecode) async {
      String jsonString =
          await rootBundle.loadString('lang/${languagecode}.json');
      jsonLanguageMap = json.decode(jsonString);
      print(jsonLanguageMap);
    });
  }

  static String translate(String jsonkey) {
    String? value = jsonLanguageMap[jsonkey].toString();
    return value;
  }

  static String getDeviceType() {
    final data = MediaQueryData.fromWindow(WidgetsBinding.instance.window);
    if (Platform.isAndroid) {
      // print(data);
      //  print(data.size.shortestSide < 600 ? '1' :'2'); // 1 == phone and 2 == tablet
      return data.size.shortestSide < 600 ? '1' : '2';
    } else {
      //print(data);
      // print(data.size.shortestSide < 700 ? '1' :'2');
      return data.size.shortestSide < 700 ? '1' : '2';
    }
  }

  static ToastMessage(String Message) {
    Fluttertoast.showToast(
        msg: Message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static Future<String> readAccessToken(String key) async {
    String value = '';
    final SharedPreferences pref = await SharedPreferences.getInstance();
    value = pref.getString(key).toString();
    return value;
  }

  static String getTextDisplay(String Name) {
    String finalValue = '';

    finalValue = Name.isNotEmpty
        ? Name.trim().split(RegExp(' +')).map((s) => s[0]).take(2).join()
        : '';

    return finalValue;
  }
}
