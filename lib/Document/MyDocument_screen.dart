import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:mime/mime.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:typed_data';
import 'package:flutter_svg/svg.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'package:share_plus/share_plus.dart';

import '../Model/Document/API_Document.dart';
import '../Model/Document/Document_List.dart';

class mydocument extends StatefulWidget {
  @override
  mydocumentState createState() => mydocumentState();
}

class mydocumentState extends State<mydocument> {
  GlobalKey<State> _DocumentList = new GlobalKey<State>();
  List iamges = [];
  int selectedCard = -1;
  String checkdevice = '';
  late DocumentAllListData lst_AllDocuments;
  List<Document_GetAllList> lst_DocumentList = [];

  Future<Directory?>? _downloadsDirectory;

  @override
  void initState() {
    checkdevice = Utility.getDeviceType();
    super.initState();
    DocumentAllList();
    CreateDirectory();
  }

  void CreateDirectory() async {
    final folderName = "Genx_Care";
    final path = Directory("/sdcard/download/$folderName");
    if ((await path.exists())) {
      print("exist");
    } else {
      print("not exist");
      path.create();
    }
  }

  void DocumentBasicInfo(String UserID) async {
    String AccessToken = await Utility.readAccessToken('Token');

    API_Documents()
        .Basic_Info_Details(AccessToken, UserID)
        .then((Response) async {
      setState(() {});
    });
  }

  void DocumentAllList() async {
    String AccessToken = await Utility.readAccessToken('Token');
    String PatientID = await Utility.readAccessToken('PatientID');
    Utility.showLoadingDialog(context, _DocumentList, true);

    API_Documents()
        .Document_List_All(PatientID, AccessToken)
        .then((Response) async {
      setState(() {
        log('List Response ${Response}');

        if (Response == "Error") {
        } else {
          lst_AllDocuments = new DocumentAllListData.fromJson(Response);

          lst_DocumentList = lst_AllDocuments.designAll;

          for (int i = 0; i < lst_DocumentList.length; i++) {
            API_Documents()
                .Basic_Info_Details(AccessToken, lst_DocumentList[i].createdBy)
                .then((Response) async {
              setState(() {
                // print(Response['name']);

                lst_DocumentList[i].createdByName = Response['name'];
              });
            });
          }
        }
        Navigator.pop(context);
      });
    });
  }

  Future<bool> requestPermissionForStorageAndManageExtStorage() async {
    await [Permission.storage].request();
    final storagePermission = await Permission.storage.status;
    // final extPermission = await Permission.manageExternalStorage.status;
    // _log('Permissions => Microphone: $micPermission, Camera: $camPermission');

    if (storagePermission == PermissionStatus.granted) {
      return true;
    }

    if (storagePermission == PermissionStatus.denied) {
      return requestPermissionForStorageAndManageExtStorage();
    }

    if (storagePermission == PermissionStatus.permanentlyDenied) {
      log('Permissions => Opening App Settings');
      await openAppSettings();
    }

    return false;
  }

  void _ImageDownloadAndSave(Uint8List bytes, String FileName,
      BuildContext context, String Share_Download) async {
    bool? result = await requestPermissionForStorageAndManageExtStorage();

    if (result) {
      String dir = (await getApplicationDocumentsDirectory()).path;
      String fullPath = '$dir/' + FileName;
      print("local file full path ${fullPath}");
      File file = File(fullPath);
      await file.writeAsBytes(bytes);
      print(file.path);

      // 1 Means Download
      // 2 Means Share
      if (Share_Download == "1") {
        FileMIMEType(file).then((mainDetails) {
          setState(() {
            String mimeType = mainDetails;
            DownloadFile(mimeType, bytes, file, FileName);
          });
        });
      } else {
        Share.shareFiles(['${fullPath}'], text: 'Great picture');
      }
    } else {}
  }

  Future<String> FileMIMEType(File mainFile) async {
    String mainCheckFile = "Default";
    final data = await mainFile.readAsBytes();
    final mime = lookupMimeType('', headerBytes: data);
    print("File Type checkkkkkk ${mime.toString()}");

    if (mime.toString().startsWith("application/")) {
      mainCheckFile = "PDF";
    } else if (mime.toString().startsWith("image/")) {
      mainCheckFile = "Image";
    } else if (mime.toString().startsWith("video/")) {
      mainCheckFile = "Video";
    } else if (mime.toString().startsWith("text/")) {
      mainCheckFile = "text";
    }

    return mainCheckFile;
  }

  void DownloadFile(
      String mimeType, Uint8List bytes, File file, String FileName) async {
    if (mimeType == "Image") {
      final resultAll = await ImageGallerySaver.saveImage(bytes);
      print(resultAll['isSuccess']);
      if (resultAll['isSuccess']) {
        Utility.WholeAppSnackbar(context, 'File Download Successfully');
      } else {}
    } else {
      Utility.WholeAppSnackbar(context, 'File Download Successfully');
      writeToFile(bytes.buffer.asByteData(), FileName);
    }
  }

  Future<File> writeToFile(ByteData data, String FileName) async {
    final buffer = data.buffer;

    String dirloc = "";
    if (Platform.isAndroid) {
      final folderName = "Genx_Care";
      final path = Directory("/sdcard/download/$folderName");
      if ((await path.exists())) {
        print("exist");
        dirloc = "/sdcard/download/$folderName";
      } else {
        print("not exist");
        path.create();
        dirloc = "/sdcard/download/$folderName";
      }
    } else {
      dirloc = (await getApplicationDocumentsDirectory()).path;
    }
    print("Final Dow ${dirloc}");

    var filePath =
        dirloc + '/' + FileName; // file_01.tmp is dump file, can be anything
    return new File(filePath).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: Utility.TopToolBar(Utility.translate('document'),
            checkdevice == "1" ? 20 : 30, Utility.PutDarkBlueColor(), context),
        body: Container(
          padding: EdgeInsets.all(checkdevice == "1" ? 10 : 20),
          child: lst_DocumentList.length == 0
              ? Container(
                  child: Center(
                    child: Text('No Document Found'),
                  ),
                )
              : ListView.separated(
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(
                      height: checkdevice == "1" ? 8 : 18,
                    );
                  },
                  itemCount: lst_DocumentList.length,
                  shrinkWrap: true,
                  itemBuilder: (context, pos) {
                    DateTime docCreateAt = DateFormat('yyyy-MM-ddTHH:mm:ss')
                        .parse(lst_DocumentList[pos].createdAt);

                    String FinalDate =
                        DateFormat('dd MMM yyyy').format(docCreateAt);
                    String FinalTime =
                        DateFormat('hh:mm a').format(docCreateAt);

                    String GroupName = lst_DocumentList[pos].group;

                    if (GroupName == "null") {
                      GroupName = "";
                    } else {}

                    return GestureDetector(
                      onTap: () {
                        setState(() {});
                      },
                      child: Card(
                        color: Colors.white,
                        elevation: checkdevice == "1" ? 5 : 7,
                        child: Column(
                          children: [
                            Row(
                              children: <Widget>[
                                SizedBox(
                                  width: checkdevice == "1" ? 8 : 18,
                                ),
                                Flexible(
                                    flex: 10,
                                    child: Column(
                                      children: [
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Text(
                                            lst_DocumentList[pos].name,
                                            style: TextStyle(
                                                fontSize: checkdevice == "1"
                                                    ? 19
                                                    : 32,
                                                fontFamily: 'Poppins_SemiBold',
                                                color:
                                                    Utility.PutDarkBlueColor()),
                                          ),
                                        ),
                                        SizedBox(
                                          height: checkdevice == "1" ? 3 : 13,
                                        ),
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Text(
                                            'DISCARGE SUMMARY',
                                            maxLines: 1,
                                            style: TextStyle(
                                                fontSize: checkdevice == "1"
                                                    ? 16
                                                    : 26,
                                                fontFamily: 'Poppins_Regular',
                                                color:
                                                    Utility.PutDarkBlueColor()),
                                          ),
                                        ),
                                        SizedBox(
                                          height: checkdevice == "1" ? 3 : 13,
                                        ),
                                        Row(
                                          children: [
                                            Flexible(
                                              flex: 10,
                                              child: Column(
                                                children: [
                                                  Row(
                                                    children: [
                                                      Text(
                                                        FinalDate,
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                            fontSize:
                                                                checkdevice ==
                                                                        "1"
                                                                    ? 16
                                                                    : 26,
                                                            fontFamily:
                                                                'Poppins_Regular',
                                                            color: Utility
                                                                .PutDarkBlueColor()),
                                                      ),
                                                      SizedBox(
                                                        width:
                                                            checkdevice == "1"
                                                                ? 30
                                                                : 40,
                                                      ),
                                                      Text(
                                                        FinalTime,
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                            fontSize:
                                                                checkdevice ==
                                                                        "1"
                                                                    ? 16
                                                                    : 26,
                                                            fontFamily:
                                                                'Poppins_Regular',
                                                            color: Utility
                                                                .PutDarkBlueColor()),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: checkdevice == "1"
                                                        ? 3
                                                        : 13,
                                                  ),
                                                  Row(
                                                    children: [
                                                      Text(
                                                        'Uploaded By : ',
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                            fontSize:
                                                                checkdevice ==
                                                                        "1"
                                                                    ? 16
                                                                    : 26,
                                                            fontFamily:
                                                                'Poppins_Regular',
                                                            color: Utility
                                                                .PutDarkBlueColor()),
                                                      ),
                                                      SizedBox(
                                                        width:
                                                            checkdevice == "1"
                                                                ? 2
                                                                : 12,
                                                      ),
                                                      Text(
                                                        lst_DocumentList[pos]
                                                            .createdByName,
                                                        maxLines: 1,
                                                        style: TextStyle(
                                                            fontSize:
                                                                checkdevice ==
                                                                        "1"
                                                                    ? 16
                                                                    : 26,
                                                            fontFamily:
                                                                'Poppins_Regular',
                                                            color: Utility
                                                                .PutDarkBlueColor()),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Flexible(
                                                flex: 2,
                                                child: Center(
                                                    child: Container(
                                                  width: checkdevice == "1"
                                                      ? 40
                                                      : 60,
                                                  height: checkdevice == "1"
                                                      ? 40
                                                      : 60,
                                                  child: SvgPicture.asset(
                                                      'Images/GroupBack.svg'),
                                                ))),
                                          ],
                                        ),
                                        SizedBox(
                                          height: checkdevice == "1" ? 3 : 13,
                                        ),
                                        GroupName == ""
                                            ? Container()
                                            : Row(
                                                children: [
                                                  Text(
                                                    'From : ',
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                        fontSize:
                                                            checkdevice == "1"
                                                                ? 16
                                                                : 26,
                                                        fontFamily:
                                                            'Poppins_Regular',
                                                        color: Utility
                                                            .PutDarkBlueColor()),
                                                  ),
                                                  SizedBox(
                                                    width: checkdevice == "1"
                                                        ? 2
                                                        : 12,
                                                  ),
                                                  Text(
                                                    GroupName,
                                                    maxLines: 1,
                                                    style: TextStyle(
                                                        fontSize:
                                                            checkdevice == "1"
                                                                ? 16
                                                                : 26,
                                                        fontFamily:
                                                            'Poppins_Regular',
                                                        color: Utility
                                                            .PutDarkBlueColor()),
                                                  ),
                                                ],
                                              ),
                                      ],
                                    )),
                              ],
                            ),
                            Container(
                              padding: EdgeInsets.fromLTRB(
                                  0,
                                  0,
                                  checkdevice == "1" ? 10 : 20,
                                  checkdevice == "1" ? 10 : 20),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  InkWell(
                                      child: Center(
                                          child: Row(
                                        children: [
                                          Container(
                                              child: Text(
                                            Utility.translate('download'),
                                            style: TextStyle(
                                                fontSize: checkdevice == "1"
                                                    ? 16
                                                    : 26,
                                                fontFamily: 'Poppins_Regular',
                                                color: Utility
                                                    .PutLightBlueColor()),
                                          )),
                                          SizedBox(
                                            width: checkdevice == "1" ? 8 : 18,
                                          ),
                                          SvgPicture.asset(
                                              'Images/Download.svg',
                                              width:
                                                  checkdevice == "1" ? 15 : 25,
                                              height:
                                                  checkdevice == "1" ? 15 : 25)
                                        ],
                                      )),
                                      onTap: () async {
                                        String Base64 =
                                            lst_DocumentList[pos].filePath;
                                        Uint8List _bytes = base64
                                            .decode(Base64.split(',').last);
                                        _ImageDownloadAndSave(
                                            _bytes,
                                            lst_DocumentList[pos].name,
                                            context,
                                            "1");
                                      }),
                                  SizedBox(
                                    width: checkdevice == "1" ? 10 : 20,
                                  ),
                                  InkWell(
                                      child: Center(
                                          child: Row(
                                        children: [
                                          Container(
                                              child: Text(
                                            Utility.translate('share'),
                                            style: TextStyle(
                                                fontSize: checkdevice == "1"
                                                    ? 16
                                                    : 26,
                                                fontFamily: 'Poppins_Regular',
                                                color: Utility
                                                    .PutLightBlueColor()),
                                          )),
                                          SizedBox(
                                            width: checkdevice == "1" ? 8 : 18,
                                          ),
                                          SvgPicture.asset(
                                            'Images/share.svg',
                                            width: checkdevice == "1" ? 15 : 25,
                                            height:
                                                checkdevice == "1" ? 15 : 25,
                                          )
                                        ],
                                      )),
                                      onTap: () {
                                        setState(() {
                                          String Base64 =
                                              lst_DocumentList[pos].filePath;
                                          Uint8List _bytes = base64
                                              .decode(Base64.split(',').last);
                                          _ImageDownloadAndSave(
                                              _bytes,
                                              lst_DocumentList[pos].name,
                                              context,
                                              "2");
                                        });
                                      }),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
        ),
      ),
    );
  }
}
