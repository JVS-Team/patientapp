import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:share_plus/share_plus.dart';
import 'package:launch_review/launch_review.dart';
import 'package:softcliniccare/Appointment/Appointment_screen.dart';
import 'package:softcliniccare/Document/MyDocument_screen.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'package:softcliniccare/Home/Home_screen.dart';
import 'package:softcliniccare/Invoice/InvoicePage.dart';
import 'package:softcliniccare/Labreports/labreports.dart';
import 'package:softcliniccare/Medication/Medications.dart';
import 'package:softcliniccare/Notiifcations/Notifications.dart';
import 'package:softcliniccare/Setting/Familymenber_screen.dart';
import 'package:softcliniccare/Setting/Profile_screen.dart';
import 'package:softcliniccare/Setting/Setting_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:softcliniccare/vitals/vitals.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:async';

import '../Appointment/Bookappointment_screen.dart';
import '../Helper/API_CALL.dart';
import '../Login/Login_screen.dart';
import '../Model/Login/OTP_Verify_Model.dart';

class MainScreen extends StatefulWidget {
  @override
  MainScreenState createState() => MainScreenState();
}

class MainScreenState extends State<MainScreen>
    with SingleTickerProviderStateMixin {
  late TabController controller;
  String screenName = "";
  String page1 = "";
  String page2 = "";
  String page3 = "";
  String page4 = "";
  String title = "BNB Demo";
  late List<Widget> _tabs;
  int _currentIndex = 0;
  int? initialPage;
  late Widget _currentPage;
  final GlobalKey<ScaffoldState> _scaffoldKey3 = new GlobalKey<ScaffoldState>();
  GlobalKey<State> _SwitchState = new GlobalKey<State>();

  @override
  void initState() {
    super.initState();
    controller = TabController(length: 4, vsync: this);
    _currentIndex = 0;
    _currentPage = homescreen();
  }

  void changeTab(int index) {
    setState(() {
      _currentIndex = index;
      if (index == 0) {
        _currentPage = homescreen();
      } else if (index == 1) {
        _currentPage = appointmentmanagement(TabCheck: true);
      } else if (index == 2) {
        _currentPage = notification();
      } else if (index == 3) {
        _currentPage = settingscreen();
      }
    });
  }

  void MobileOTPVerifySwitchMember() async {
    String MainMobileNo = await Utility.readAccessToken('MobileNo');
    String MainPin = await Utility.readAccessToken('PinNo');

    print(MainMobileNo);
    print(MainPin);

    API_CALL()
        .VerifyMobileOTPCall_TemporaryToken(MainMobileNo, MainPin)
        .then((Response) async {
      if (Response == "Error") {
        Navigator.pop(context);
        Utility.WholeAppSnackbar(context, 'Error coming from API Side');
        // Fluttertoast.showToast(
        //     msg: "Incorrect OTP",
        //     toastLength: Toast.LENGTH_SHORT,
        //     gravity: ToastGravity.CENTER,
        //     timeInSecForIosWeb: 1,
        //     backgroundColor: Colors.red,
        //     textColor: Colors.white,
        //     fontSize: 16.0);
      } else {
        Map FinalResponse = json.decode(Response);

        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("TokenTemporary", FinalResponse['Accesstoken']);

        API_CALL().VerifyMobileOTPCall(MainMobileNo, MainPin).then((Response) {
          log('OTP Verify Response Main =========  ${Response}');
          Navigator.pop(context);
          if (Response == "Error") {
            Navigator.pop(context);
            // Fluttertoast.showToast(
            //     msg: "Incorrect OTP",
            //     toastLength: Toast.LENGTH_SHORT,
            //     gravity: ToastGravity.CENTER,
            //     timeInSecForIosWeb: 1,
            //     backgroundColor: Colors.red,
            //     textColor: Colors.white,
            //     fontSize: 16.0);
          } else {
            Navigator.pop(context);

            OTP_Verify familyDetails =
                new OTP_Verify.fromJson(json.decode(Response));

            // SaveHospitalList(Response);

            // Map Data = json.decode(Response);
            // Utility.SaveAllLangData(
            //     'Accesstoken', Data['Accesstoken']);

            print(familyDetails.designAll.length);
            // print(familyDetails.designAll);

            // if (familyDetails.designAll.length > 1) {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => FamilymemberPage(
                      value: 0,
                      mobileno: MainMobileNo,
                      familyDetails: familyDetails)),
            );
            // } else if (familyDetails.designAll.length == 1) {
            //   print(
            //       'Hospital Length ${familyDetails.designAll[0].hospital_List.length}');
            //   List<HospotalList> hospital_List =
            //       familyDetails.designAll[0].hospital_List;
            //
            //   for (int i = 0; i < hospital_List.length; i++) {
            //     HospitalName_To_AllID(
            //         hospital_List[i].tenantId, hospital_List);
            //   }
            //
            //   Future.delayed(const Duration(milliseconds: 500),
            //           () {
            //         setState(() {
            //           if (hospital_List.length == 1) {
            //             print('All Lengtjh');
            //             FinalAccessToken(
            //                 hospital_List[0].patientId,
            //                 hospital_List[0].tenantId,
            //                 widget.mobileno);
            //           } else if (hospital_List.length > 1) {
            //             Future.delayed(
            //                 const Duration(milliseconds: 1000), () {
            //               setState(() {
            //                 print('Navigation');
            //                 String MobileNoMain = widget.mobileno;
            //                 Navigator.push(
            //                   context,
            //                   MaterialPageRoute(
            //                       builder: (context) =>
            //                           HospitallistPage(
            //                               value: 0,
            //                               All_hospital_List:
            //                               hospital_List,
            //                               mobileno: MobileNoMain)),
            //                 );
            //               });
            //             });
            //           }
            //         });
            //       });
            // }
          }
        });
      }
    });
  }

  @override
  void dispose() {
    // _scaffoldKey3.currentState!.dispose();
    controller.dispose();
    super.dispose();
  }

  void changeTitle(String title) {
    setState(() {
      screenName = title;
    });
  }

  navigateToPage(BuildContext context, String page) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil(page, (Route<dynamic> route) => false);
  }

  Widget expandableCommercial() {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        key: _scaffoldKey3,
        drawer: Container(
          width: Utility.getDeviceType() == '1'
              ? MediaQuery.of(context).size.width - 150
              : 400,
          child: Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                Container(
                  height: Utility.getDeviceType() == '1' ? 120 : 200,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Image.asset(
                      "Images/softclinic_logo.png",
                      height: Utility.getDeviceType() == '1' ? 60 : 100,
                    ),
                  ),
                ),
                //createDrawerHeader(),
                SizedBox(
                  height: 10,
                ),
                ListTile(
                  contentPadding: Utility.getDeviceType() == '1'
                      ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                      : EdgeInsets.fromLTRB(20, 20, 10, 10),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                    ),
                    child:
                        Image.asset('Images/dashboard.png', fit: BoxFit.cover),
                  ),
                  title: Text(
                    Utility.translate('dashboard'),
                    style: TextStyle(
                        fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                        fontFamily: 'Poppins_Regular',
                        color: Utility.PutDarkBlueColor()),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    _currentIndex = 0;
                    changeTab(0);
                  },
                ),
                ListTile(
                  contentPadding: Utility.getDeviceType() == '1'
                      ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                      : EdgeInsets.fromLTRB(20, 20, 10, 10),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                    ),
                    child: SvgPicture.asset('Images/charity.svg',
                        fit: BoxFit.cover),
                  ),
                  title: Text(
                    Utility.translate('vitals'),
                    style: TextStyle(
                        fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                        fontFamily: 'Poppins_Regular',
                        color: Utility.PutDarkBlueColor()),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => vitals()),
                    );
                  },
                ),
                ListTile(
                  contentPadding: Utility.getDeviceType() == '1'
                      ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                      : EdgeInsets.fromLTRB(20, 20, 10, 10),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                    ),
                    child: SvgPicture.asset('Images/hospital.svg',
                        fit: BoxFit.cover),
                  ),
                  title: Text(
                    Utility.translate('medications'),
                    style: TextStyle(
                        fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                        fontFamily: 'Poppins_Regular',
                        color: Utility.PutDarkBlueColor()),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Medications()),
                    );
                  },
                ),
                ListTile(
                  contentPadding: Utility.getDeviceType() == '1'
                      ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                      : EdgeInsets.fromLTRB(20, 20, 10, 10),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                    ),
                    child:
                        SvgPicture.asset('Images/file.svg', fit: BoxFit.cover),
                  ),
                  title: Text(
                    Utility.translate('documents'),
                    style: TextStyle(
                        fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                        fontFamily: 'Poppins_Regular',
                        color: Utility.PutDarkBlueColor()),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => mydocument()),
                    );
                  },
                ),
                ListTile(
                  contentPadding: Utility.getDeviceType() == '1'
                      ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                      : EdgeInsets.fromLTRB(20, 20, 10, 10),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                    ),
                    child: SvgPicture.asset('Images/calendar.svg',
                        fit: BoxFit.cover),
                  ),
                  title: Text(
                    Utility.translate('appointment'),
                    style: TextStyle(
                        fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                        fontFamily: 'Poppins_Regular',
                        color: Utility.PutDarkBlueColor()),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //       builder: (context) => bookappointment(
                    //         doctorvalue: doctorvalue,
                    //         hospitalvalue: hospitalvalue,
                    //       )),
                    // );
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => appointmentmanagement(TabCheck: false)),
                    );
                  },
                ),
                ListTile(
                  contentPadding: Utility.getDeviceType() == '1'
                      ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                      : EdgeInsets.fromLTRB(20, 20, 10, 10),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                    ),
                    child: SvgPicture.asset('Images/invoice.svg'),
                  ),
                  title: Text(
                    Utility.translate('invoice'),
                    style: TextStyle(
                        fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                        fontFamily: 'Poppins_Regular',
                        color: Utility.PutDarkBlueColor()),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => invoicemanagment()),
                    );
                  },
                ),
                ListTile(
                  contentPadding: Utility.getDeviceType() == '1'
                      ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                      : EdgeInsets.fromLTRB(20, 20, 10, 10),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                    ),
                    child: SvgPicture.asset('Images/report.svg',
                        fit: BoxFit.cover),
                  ),
                  title: Text(
                    Utility.translate('lab_report'),
                    style: TextStyle(
                        fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                        fontFamily: 'Poppins_Regular',
                        color: Utility.PutDarkBlueColor()),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => labreports()),
                    );
                  },
                ),
                ListTile(
                  contentPadding: Utility.getDeviceType() == '1'
                      ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                      : EdgeInsets.fromLTRB(20, 20, 10, 10),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                    ),
                    child: Image.asset(
                      'Images/notification.png',
                      fit: BoxFit.cover,
                      color: Utility.PutDarkBlueColor(),
                    ),
                  ),
                  title: Text(
                    Utility.translate('notification'),
                    style: TextStyle(
                        fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                        fontFamily: 'Poppins_Regular',
                        color: Utility.PutDarkBlueColor()),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    _currentIndex = 2;
                    changeTab(2);
                  },
                ),
                ListTile(
                  contentPadding: Utility.getDeviceType() == '1'
                      ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                      : EdgeInsets.fromLTRB(20, 20, 10, 10),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                    ),
                    child: Image.asset('Images/switch_mem.png',
                        fit: BoxFit.cover, color: Utility.PutDarkBlueColor()),
                  ),
                  title: Text(
                    Utility.translate('swt_mem'),
                    style: TextStyle(
                        fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                        fontFamily: 'Poppins_Regular',
                        color: Utility.PutDarkBlueColor()),
                  ),
                  onTap: () {
                    // Navigator.pop(context);
                    Utility.showLoadingDialog(context, _SwitchState, true);
                    MobileOTPVerifySwitchMember();
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //       builder: (context) => FamilymemberPage(value: 1)),
                    // );
                  },
                ),
                // ListTile(
                //   contentPadding: Utility.getDeviceType() == '1'
                //       ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                //       : EdgeInsets.fromLTRB(20, 20, 10, 10),
                //   leading: ConstrainedBox(
                //     constraints: BoxConstraints(
                //       minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                //       minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                //       maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                //       maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                //     ),
                //     child: Image.asset('Images/faq.png',
                //         fit: BoxFit.cover, color: Utility.PutDarkBlueColor()),
                //   ),
                //   title: Text(
                //     Utility.translate('faq'),
                //     style: TextStyle(
                //         fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                //         fontFamily: 'Poppins_Regular',
                //         color: Utility.PutDarkBlueColor()),
                //   ),
                //   onTap: () {
                //     Navigator.pop(context);
                //     _launchURL();
                //   },
                // ),
                // ListTile(
                //   contentPadding: Utility.getDeviceType() == '1'
                //       ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                //       : EdgeInsets.fromLTRB(20, 20, 10, 10),
                //   leading: ConstrainedBox(
                //     constraints: BoxConstraints(
                //       minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                //       minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                //       maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                //       maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                //     ),
                //     child: Image.asset('Images/aboutus.png',
                //         fit: BoxFit.cover, color: Utility.PutDarkBlueColor()),
                //   ),
                //   title: Text(
                //     Utility.translate('about_us'),
                //     style: TextStyle(
                //         fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                //         fontFamily: 'Poppins_Regular',
                //         color: Utility.PutDarkBlueColor()),
                //   ),
                //   onTap: () {
                //     Navigator.pop(context);
                //     _launchURL();
                //   },
                // ),
                ListTile(
                  contentPadding: Utility.getDeviceType() == '1'
                      ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                      : EdgeInsets.fromLTRB(20, 20, 10, 10),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                    ),
                    child: SvgPicture.asset(
                      'Images/settings.svg',
                      width: Utility.getDeviceType() == '1' ? 23 : 40,
                      height: Utility.getDeviceType() == '1' ? 23 : 40,
                      color: Utility.PutDarkBlueColor(),
                    ),
                  ),
                  title: Text(
                    "Settings",
                    style: TextStyle(
                        fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                        fontFamily: 'Poppins_Regular',
                        color: Utility.PutDarkBlueColor()),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    // _launchURL();
                    _currentIndex = 3;
                    changeTab(3);
                  },
                ),
                ListTile(
                  contentPadding: Utility.getDeviceType() == '1'
                      ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                      : EdgeInsets.fromLTRB(20, 20, 10, 10),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                    ),
                    child: Image.asset('Images/rate.png',
                        fit: BoxFit.cover, color: Utility.PutDarkBlueColor()),
                  ),
                  title: Text(
                    Utility.translate('rate_app'),
                    style: TextStyle(
                        fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                        fontFamily: 'Poppins_Regular',
                        color: Utility.PutDarkBlueColor()),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    LaunchReview.launch(
                        androidAppId: "com.softclinicgenxcare.patient",
                        iOSAppId: "284882215");
                  },
                ),
                ListTile(
                  contentPadding: Utility.getDeviceType() == '1'
                      ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                      : EdgeInsets.fromLTRB(20, 20, 10, 10),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                    ),
                    child: Image.asset('Images/share.png',
                        fit: BoxFit.cover, color: Utility.PutDarkBlueColor()),
                  ),
                  title: Text(
                    Utility.translate('share_app'),
                    style: TextStyle(
                        fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                        fontFamily: 'Poppins_Regular',
                        color: Utility.PutDarkBlueColor()),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    if (Platform.isAndroid) {
                      Share.share(
                          'https://play.google.com/store/apps/developer?id=com.softclinicgenxcare.patient');
                    } else if (Platform.isIOS) {
                      Share.share(
                          'https://apps.apple.com/ca/app/softcliniclive-care/id12177099');
                    }
                  },
                ),
                ListTile(
                  contentPadding: Utility.getDeviceType() == '1'
                      ? EdgeInsets.fromLTRB(10, 0, 0, 0)
                      : EdgeInsets.fromLTRB(20, 20, 10, 10),
                  leading: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      minHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxWidth: Utility.getDeviceType() == '1' ? 30 : 45,
                      maxHeight: Utility.getDeviceType() == '1' ? 30 : 45,
                    ),
                    child: Image.asset('Images/logout.png',
                        fit: BoxFit.cover, color: Utility.PutDarkBlueColor()),
                  ),
                  title: Text(
                    Utility.translate('logout'),
                    style: TextStyle(
                        fontSize: Utility.getDeviceType() == '1' ? 16 : 26,
                        fontFamily: 'Poppins_Regular',
                        color: Utility.PutDarkBlueColor()),
                  ),
                  onTap: () {
                    Logout();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => LoginPage(
                            isLogoutClick: true
                        )),
                        (Route<dynamic> route) => false);
                  },
                ),
                Container(
                  height: 30,
                ),
              ],
            ),
          ),
        ),
        body: _currentPage,
        bottomNavigationBar: BottomNavigationBar(
          onTap: changeTab,
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.white,
          selectedItemColor: Colors.black,
          unselectedItemColor: Colors.black,
          currentIndex: _currentIndex,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          elevation: 5,
          items: [
            BottomNavigationBarItem(
              label: '',
              icon: Container(
                // height: 100,
                padding: Utility.getDeviceType() == '1'
                    ? EdgeInsets.fromLTRB(15, 8, 15, 8)
                    : EdgeInsets.fromLTRB(35, 18, 35, 18),
                decoration: BoxDecoration(
                  color: _currentIndex == 0
                      ? Utility.PutDarkBlueColor()
                      : Colors.transparent,
                  borderRadius: new BorderRadius.all(Radius.elliptical(35, 40)),
                ),
                child: SvgPicture.asset(
                  'Images/home_icon.svg',
                  width: Utility.getDeviceType() == '1' ? 23 : 40,
                  height: Utility.getDeviceType() == '1' ? 23 : 40,
                  color: _currentIndex == 0 ? Colors.white : Colors.black26,
                ),
              ),
            ),
            BottomNavigationBarItem(
              label: '',
              icon: Container(
                // height: 100,
                padding: Utility.getDeviceType() == '1'
                    ? EdgeInsets.fromLTRB(15, 8, 15, 8)
                    : EdgeInsets.fromLTRB(35, 18, 35, 18),
                decoration: BoxDecoration(
                  color: _currentIndex == 1
                      ? Utility.PutDarkBlueColor()
                      : Colors.transparent,
                  borderRadius: new BorderRadius.all(Radius.elliptical(35, 40)),
                ),
                child: SvgPicture.asset(
                  'Images/calendar.svg',
                  width: Utility.getDeviceType() == '1' ? 23 : 40,
                  height: Utility.getDeviceType() == '1' ? 23 : 40,
                  color: _currentIndex == 1 ? Colors.white : Colors.black26,
                ),
              ),
            ),
            BottomNavigationBarItem(
              label: '',
              icon: Container(
                // height: 100,
                padding: Utility.getDeviceType() == '1'
                    ? EdgeInsets.fromLTRB(15, 8, 15, 8)
                    : EdgeInsets.fromLTRB(35, 18, 35, 18),
                decoration: BoxDecoration(
                  color: _currentIndex == 2
                      ? Utility.PutDarkBlueColor()
                      : Colors.transparent,
                  borderRadius: new BorderRadius.all(Radius.elliptical(35, 40)),
                ),
                child: SvgPicture.asset(
                  'Images/message.svg',
                  width: Utility.getDeviceType() == '1' ? 23 : 40,
                  height: Utility.getDeviceType() == '1' ? 23 : 40,
                  color: _currentIndex == 2 ? Colors.white : Colors.black26,
                ),
              ),
            ),
            BottomNavigationBarItem(
              label: '',
              icon: Container(
                // height: 100,
                padding: Utility.getDeviceType() == '1'
                    ? EdgeInsets.fromLTRB(15, 8, 15, 8)
                    : EdgeInsets.fromLTRB(35, 18, 35, 18),
                decoration: BoxDecoration(
                  color: _currentIndex == 3
                      ? Utility.PutDarkBlueColor()
                      : Colors.transparent,
                  borderRadius: new BorderRadius.all(Radius.elliptical(35, 40)),
                ),
                child: SvgPicture.asset(
                  'Images/settings.svg',
                  width: Utility.getDeviceType() == '1' ? 23 : 40,
                  height: Utility.getDeviceType() == '1' ? 23 : 40,
                  color: _currentIndex == 3 ? Colors.white : Colors.black26,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget createDrawerHeader() {
    return Container(
      height: 120,
      child: DrawerHeader(
          margin: EdgeInsets.all(35),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('Images/softclinic_logo.png'))),
          child: Container()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return expandableCommercial();
  }

  _launchURL() async {
    const url = 'https://androidride.com';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void Logout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove('Token');
    await preferences.remove('TokenTemporary');
    // await preferences.clear();
  }
}
