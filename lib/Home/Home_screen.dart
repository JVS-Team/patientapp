import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:softcliniccare/Appointment/Appointment_Selection.dart';
import 'package:softcliniccare/Appointment/Appointment_screen.dart';
import 'package:softcliniccare/Helper/MapUtils.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'package:softcliniccare/Document/MyDocument_screen.dart';
import 'package:softcliniccare/Invoice/InvoicePage.dart';
import 'package:softcliniccare/Labreports/labreports.dart';
import 'package:softcliniccare/Login/Login_screen.dart';
import 'package:softcliniccare/Medication/Medications.dart';
import 'package:softcliniccare/Setting/Profile_screen.dart';
import 'package:softcliniccare/VideoCall/Videocalling.dart';
import 'package:softcliniccare/vitals/vitals.dart';
import 'package:url_launcher/url_launcher.dart';

import '../Helper/AllURL.dart';
import '../Helper/Constant.dart';
import '../Helper/Database/sqflite_helper.dart';
import '../Labreports/LabReport_TempPage.dart';
import '../Model/Appointment/Appointment_Status.dart';
import '../Model/Dashboard/AppointmentListModel.dart';
import '../Model/Dashboard/Dashboard_API.dart';
import '../Model/Dashboard/Facility_Name_Model.dart';
import '../Model/Dashboard/PatientDetails_All.dart';
import '../Model/Dashboard/PatientDetails_All.dart';
import '../Model/Profile/ProfileAPI.dart';

class homescreen extends StatefulWidget {
  homescreen({Key? key}) : super(key: key);
  static const String routeName = '/homescreen';

  // final String title;
  @override
  _homescreenState createState() => _homescreenState();
}

class _homescreenState extends State<homescreen> {
  int currentIndex = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey1 = new GlobalKey<ScaffoldState>();

  late PatientDetails mainPatientDetails;

  // List Appointment List Details
  List<AppointmentList_Details> listing_MainAppointment_List = [];
  List<AppointmentList_Details> listing_Appointment_Details = [];

  List<AppointmentStatusDetails> listing_App_Status = [];

  // List Direction Details
  List<Facility_Details> listing_FacilityName = [];
  String AppointmentMonth = '';
  String AppointmentDate = '';
  String AppointmentWeekName = '';
  String FacilityName = '';
  String Time12Hour = '';
  String AppointmentStatus = '';
  String AppointmentID = '';

  String FirstName = '';
  String LastName = '';
  String GreetingText = '';

  // final String Link = 'https://jvsgroup.page.link/H3Ed';
  // String? _linkMessage;
  GlobalKey<State> _KeyList = new GlobalKey<State>();

  Uint8List imageBytes = base64.decode(AllURL.Base64_PlaceHolderImage);
  String imageFileDetails = "";

  @override
  void initState() {
    super.initState();
    getAppointmentStatusAPI();
    MainPatientDetails();
    FacilityName_Details();
    AppointmentList();
  }

  void FacilityName_Details() async {
    String AccessToken = await Utility.readAccessToken('Token');

    API_Dashboard().Dashboard_Facilityname(AccessToken).then((Response) async {
      setState(() {
        var tempdata = Response['value'];
        for (int i = 0; i < tempdata.length; i++) {
          listing_FacilityName.add(Facility_Details.fromJson(tempdata[i]));
        }
      });
    });
  }

  void getAppointmentStatusAPI() async {
    final data = await SQLHelper.getNoChangeDetails(
        ConstantVar.str_Common_GetAllAppointmentStatus);
    List<Map<String, dynamic>> all_AppointmentStatus = data;
    String mainAppointmentStatus = "";

    if (all_AppointmentStatus.length > 0) {
      for (int i = 0; i < all_AppointmentStatus.length; i++) {
        if (all_AppointmentStatus[i]['APIName'] ==
            ConstantVar.str_Common_GetAllAppointmentStatus) {
          mainAppointmentStatus = all_AppointmentStatus[i]['APIResponse'];
          break;
        } else {}
      }
      if (mainAppointmentStatus == "") {
        API_Dashboard().AppointmentStatusDetails().then((Response) {
          setState(() {
            if (all_AppointmentStatus.length > 0) {
              AllDataUpdate(
                  ConstantVar.str_Common_GetAllAppointmentStatus, Response);
            } else {
              AllDataInsert(
                  ConstantVar.str_Common_GetAllAppointmentStatus, Response);
            }
            mainStatusDetails(Response);
          });
        });
      } else {
        mainStatusDetails(mainAppointmentStatus);
      }
    } else {
      API_Dashboard().AppointmentStatusDetails().then((Response) {
        setState(() {
          if (all_AppointmentStatus.length > 0) {
            AllDataUpdate(
                ConstantVar.str_Common_GetAllAppointmentStatus, Response);
          } else {
            AllDataInsert(
                ConstantVar.str_Common_GetAllAppointmentStatus, Response);
          }
          mainStatusDetails(Response);
        });
      });
    }
  }

  void mainStatusDetails(String mainResponse) {
    var resBody = json.decode(mainResponse);

    var tempdata = resBody;
    for (int i = 0; i < tempdata.length; i++) {
      listing_App_Status.add(AppointmentStatusDetails.fromJson(tempdata[i]));
    }
  }

  void MainPatientDetails() async {
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');

    API_Profile()
        .ProfilePageDisplay(AccessToken, MainPatientID)
        .then((Response) async {
      setState(() {
        mainPatientDetails = new PatientDetails.fromJson(Response);

        log(mainPatientDetails.patregi.firstName);

        FirstName = mainPatientDetails.patregi.firstName;
        LastName = mainPatientDetails.patregi.lastName;

        GreetingText = greetingMessage();

        imageFileDetails =
            Response['patientPhotographDetail']['imagePath'] ?? "";
        // log(imageFileDetails);

        if (imageFileDetails == '') {
          imageBytes = base64.decode(AllURL.Base64_PlaceHolderImage);
        } else {
          var parts = imageFileDetails.split(',');
          var mainImages = parts[1].trim();
          imageBytes = base64.decode(mainImages);
        }
      });
    });
  }

  String greetingMessage() {
    var timeNow = DateTime.now().hour;

    if (timeNow <= 12) {
      return 'Good Morning';
    } else if ((timeNow > 12) && (timeNow <= 16)) {
      return 'Good Afternoon';
    } else if ((timeNow > 16) && (timeNow < 20)) {
      return 'Good Evening';
    } else {
      return 'Good Evening';
    }
  }

  void AppointmentList() async {
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');
    // Utility.showLoadingDialog(context, _KeyList, true);

    API_Dashboard()
        .AppointmentALLListAPI(AccessToken, MainPatientID)
        .then((Response) async {
      setState(() {
        if (Response.statusCode == 200) {
          var jsonResponse = json.decode(Response.body);
          var tempdata = jsonResponse;

          if (tempdata.length > 0) {
            for (int i = 0; i < tempdata.length; i++) {
              listing_MainAppointment_List
                  .add(AppointmentList_Details.fromJson(tempdata[i]));
            }
          }

          if (listing_MainAppointment_List.length > 0) {
            listing_MainAppointment_List
                .sort((a, b) => b.appointmentDate.compareTo(a.appointmentDate));

            // for (int i = 0; i < listing_MainAppointment_List.length; i++) {
            //   print(
            //       "Value Display ${listing_MainAppointment_List[i].appointmentDate}");
            // }

            DateTime parseDate = new DateFormat("yyyy-MM-dd")
                .parse(listing_MainAppointment_List[0].appointmentDate);

            final allDate =
                DateTime(parseDate.year, parseDate.month, parseDate.day);

            final date2 = DateTime.now();
            final difference = date2.difference(allDate).inDays;

            if (difference <= 0) {
              // print('Less');
              listing_Appointment_Details
                  .add(AppointmentList_Details.fromJson(tempdata[0]));

              var inputDate = DateTime.parse(parseDate.toString());
              var outputFormatMonth = DateFormat('MMM');
              var outputFormatDay = DateFormat('EEE');
              var outputFormatDate = DateFormat('dd');
              var outputMonth = outputFormatMonth.format(inputDate);
              AppointmentMonth = outputMonth;
              var outputDay = outputFormatDay.format(inputDate);
              AppointmentWeekName = outputDay;
              var outputDate = outputFormatDate.format(inputDate);
              AppointmentDate = outputDate;

              AppointmentID = listing_Appointment_Details[0].id;
              print('Appointment ID $AppointmentID');

              DateTime parseTime = new DateFormat("HH:mm:ss")
                  .parse(listing_Appointment_Details[0].startTime);

              var inputTime = DateTime.parse(parseTime.toString());
              var outputTimeData = DateFormat('jm');
              var outputTime = outputTimeData.format(inputTime);
              // print(outputTime);
              Time12Hour = outputTime;

              // String AppointmentStatus = '';
              String AppointmentStatusID = '';

              for (int i = 0; i < listing_App_Status.length; i++) {
                if (listing_App_Status[i].id ==
                    listing_Appointment_Details[0].appointmentStatus) {
                  AppointmentStatusID = listing_App_Status[i].id;
                  AppointmentStatus = listing_App_Status[i].displayName;
                }
              }

              for (int i = 0; i < listing_FacilityName.length; i++) {
                if (listing_Appointment_Details[0].facilityId ==
                    listing_FacilityName[i].id) {
                  FacilityName = listing_FacilityName[i].name;
                }
              }
            } else {
              // print('Greater');
            }
          }
          // Navigator.pop(context);
        } else {
          print(" Nott ");
          // Navigator.pop(context);
          Logout();
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                  builder: (context) => LoginPage(isLogoutClick: true)),
              (Route<dynamic> route) => false);
        }
      });
    });
  }

  void Logout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove('Token');
    await preferences.remove('TokenTemporary');
    // await preferences.clear();
  }

  void AllDataInsert(String APIName, String APIResponse) async {
    String cdate = DateFormat("yyyy-MM-dd").format(DateTime.now());
    String tTime = DateFormat("HH:mm").format(DateTime.now());
    await SQLHelper.insertMainCommonData(APIName, APIResponse, cdate, tTime);
  }

  void AllDataUpdate(String APIName, String APIResponse) async {
    await SQLHelper.updateNoChangeDetails(APIName, APIResponse);
  }

  Future<void> _pullRefresh() async {
    print("Refresh");
    setState(() {
      listing_MainAppointment_List = [];
      AppointmentList();
    });
  }

  @override
  Widget build(BuildContext context) {
    // Full screen width and height
    double scr_Width = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        key: _scaffoldKey1,
        appBar: AppBar(
          toolbarHeight: Utility.getDeviceType() == '1' ? 50 : 70,
          centerTitle: true,
          backgroundColor: Colors.white,
          leading: IconButton(
            padding: EdgeInsets.fromLTRB(
                Utility.getDeviceType() == '1' ? 5 : 20,
                Utility.getDeviceType() == '1' ? 5 : 10,
                0,
                0),
            icon: SvgPicture.asset('Images/menu.svg',
                color: Colors.black,
                width: Utility.getDeviceType() == "1" ? 15 : 50,
                height: Utility.getDeviceType() == "1" ? 15 : 50),
            onPressed: () => {Scaffold.of(context).openDrawer()},
            //_scaffoldKey.currentState!.openDrawer()},
          ),
          actions: [
            // Container(
            //   child: GestureDetector(
            //     onTap: () {}, // Image tapped
            //     child: SvgPicture.asset(
            //       'assets/user_profile.svg',
            //       fit: BoxFit.cover, // Fixes border issues
            //       width: 50.0,
            //       height: 50.0,
            //     ),
            //   ),
            // )
            Container(
              padding: Utility.getDeviceType() == '1'
                  ? EdgeInsets.fromLTRB(0, 0, 10, 0)
                  : EdgeInsets.fromLTRB(0, 5, 5, 0),
              height: Utility.getDeviceType() == '1' ? 50 : 70,
              width: Utility.getDeviceType() == '1' ? 50 : 70,
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => profilescreen()),
                    );
                  });
                },
                child: Container(
                  child: Center(
                    child: imageFileDetails == ""
                        ? Text(
                            FirstName == ""
                                ? ""
                                : Utility.getTextDisplay(
                                        FirstName + " " + LastName)
                                    .toUpperCase(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 12,
                                letterSpacing: 2.0,
                                color: Colors.white),
                          )
                        : CircleAvatar(
                            radius: Utility.getDeviceType() == '1' ? 55 : 70,
                            backgroundColor: Colors.transparent,
                            child: CircleAvatar(
                              radius: Utility.getDeviceType() == '1' ? 50 : 70,
                              backgroundColor: Colors.transparent,
                              child: Image.memory(imageBytes,
                                  width: 100, height: 100, fit: BoxFit.contain),
                            ),
                          ),
                  ),
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    color: FirstName == ""
                        ? Colors.white
                        : imageFileDetails == ""
                            ? Utility.PutLightBlueColor()
                            : Colors.white,
                    // image: new DecorationImage(
                    //     fit: BoxFit.fill,
                    //     image: new NetworkImage(
                    //         "https://image.shutterstock.com/image-photo/modern-hospital-style-building-260nw-212251981.jpg")),
                  ),
                ),
              ),
            )
          ],
          title: Image.asset(
            'Images/softclinic_logo.png',
            height: Utility.getDeviceType() == '1' ? 30 : 40,
            alignment: Alignment.centerRight,
          ),
        ),
        floatingActionButton: Container(
          padding: EdgeInsets.fromLTRB(
              0, 0, Utility.getDeviceType() == '1' ? 15 : 30, 0),
          height: Utility.getDeviceType() == '1' ? 50 : 70,
          width: Utility.getDeviceType() == '1'
              ? MediaQuery.of(context).size.width - 50
              : MediaQuery.of(context).size.width - 70,
          child: MaterialButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Appointment_Selection()),
              );
            },
            child: Ink(
              width: Utility.getDeviceType() == '1' ? double.infinity : 400,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Utility.gradiant, Utility.gradiant1],
                ),
                borderRadius: BorderRadius.circular(
                    Utility.getDeviceType() == '1' ? 25 : 35),
              ),
              child: Container(
                constraints: BoxConstraints(
                    maxWidth: Utility.getDeviceType() == '1'
                        ? MediaQuery.of(context).size.width - 50
                        : 400,
                    minHeight: Utility.getDeviceType() == '1'
                        ? MediaQuery.of(context).size.width - 50
                        : MediaQuery.of(context).size.width - 70),
                alignment: Alignment.center,
                child: Text(
                  Utility.translate('book_appoint'),
                  style: TextStyle(
                      fontSize: Utility.getDeviceType() == '1' ? 18 : 28,
                      fontFamily: 'Poppins_Medium',
                      color: Colors.white),
                ),
              ),
            ),
            splashColor: Colors.black12,
            padding: EdgeInsets.all(0),
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(25.0),
            ),
          ),
        ),
        body: Container(
          padding: new EdgeInsets.all(10.0),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(10, 7, 0, 7),
                alignment: Alignment.centerLeft,
                child: Utility.MainNormalTextWithFont(
                    GreetingText + ", " + FirstName + " " + LastName,
                    Utility.getDeviceType() == '1' ? 16 : 30,
                    Utility.PutLightBlueColor(),
                    'Poppins_Medium'),
              ),
              Timeslot(),
              Utility.getDeviceType() == '1'
                  ? SizedBox(
                      height: 0,
                    )
                  : SizedBox(height: 25),
              if (listing_Appointment_Details.length > 0)
                Container(
                  padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Utility.MainNormalTextWithFont(
                        Utility.translate('next_appoint'),
                        Utility.getDeviceType() == '1' ? 15 : 30,
                        Utility.PutDarkBlueColor(),
                        'Poppins_SemiBold'),
                  ),
                ),
              SizedBox(height: Utility.getDeviceType() == '1' ? 10 : 20),
              if (listing_Appointment_Details.length > 0)
                RefreshIndicator(
                  onRefresh: _pullRefresh,
                  child: SingleChildScrollView(
                    physics: const AlwaysScrollableScrollPhysics(),
                    child: Container(
                      child: Card(
                          color: Utility.PutBackgroundCardColor(),
                          elevation: 5,
                          child: Container(
                            height: Utility.getDeviceType() == '1' ? 160 : 220,
                            child: Row(
                              children: <Widget>[
                                new Expanded(
                                  flex: 4,
                                  child: new Container(
                                    color: Colors.white,
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.fromLTRB(
                                              Utility.getDeviceType() == '1'
                                                  ? 5
                                                  : 10,
                                              Utility.getDeviceType() == '1'
                                                  ? 9
                                                  : 18,
                                              0,
                                              0),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child:
                                                Utility.MainNormalTextWithFont(
                                                    AppointmentMonth
                                                        .toUpperCase(),
                                                    Utility.getDeviceType() ==
                                                            '1'
                                                        ? 14
                                                        : 24,
                                                    Utility.PutDarkBlueColor(),
                                                    'Poppins_Medium'),
                                          ),
                                        ),
                                        Container(
                                          child: Align(
                                            alignment: Alignment.center,
                                            child:
                                                Utility.MainNormalTextWithFont(
                                                    AppointmentDate
                                                        .toUpperCase(),
                                                    Utility.getDeviceType() ==
                                                            '1'
                                                        ? 24
                                                        : 34,
                                                    Utility.PutLightBlueColor(),
                                                    'Poppins_Medium'),
                                          ),
                                        ),
                                        Container(
                                          child: Align(
                                            alignment: Alignment.center,
                                            child:
                                                Utility.MainNormalTextWithFont(
                                                    AppointmentWeekName
                                                        .toUpperCase(),
                                                    Utility.getDeviceType() ==
                                                            '1'
                                                        ? 18
                                                        : 28,
                                                    Utility.PutLightBlueColor(),
                                                    'Poppins_Medium'),
                                          ),
                                        ),
                                        Container(
                                          margin:
                                              EdgeInsets.fromLTRB(0, 10, 0, 0),
                                          child: Align(
                                            child:
                                                Utility.MainNormalTextWithFont(
                                                    Time12Hour,
                                                    Utility.getDeviceType() ==
                                                            '1'
                                                        ? 20
                                                        : 30,
                                                    Utility.PutDarkBlueColor(),
                                                    'Poppins_Medium'),
                                            alignment: Alignment.bottomCenter,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                    child: Container(
                                  width: 7,
                                  height: double.infinity,
                                  color: Utility.PutLightBlueColor(),
                                )),
                                new Expanded(
                                  flex: 10,
                                  child: new Container(
                                    color: Utility.PutBackgroundCardColor(),
                                    child: Row(
                                      children: <Widget>[
                                        new Expanded(
                                          flex: 7,
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                margin: EdgeInsets.fromLTRB(
                                                    10, 10, 0, 0),
                                                alignment: Alignment.topLeft,
                                                child: Text(
                                                  FirstName + " " + LastName,
                                                  style: TextStyle(
                                                    fontFamily:
                                                        'Poppins_Medium',
                                                    fontSize:
                                                        Utility.getDeviceType() ==
                                                                '1'
                                                            ? 12
                                                            : 22,
                                                    color: Utility
                                                        .PutDarkBlueColor(),
                                                    fontStyle: FontStyle.normal,
                                                  ),
                                                  maxLines: 1,
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.fromLTRB(
                                                    10, 0, 0, 0),
                                                alignment: Alignment.topLeft,
                                                child: Text(
                                                  FacilityName,
                                                  style: TextStyle(
                                                    fontFamily:
                                                        'Poppins_Medium',
                                                    fontSize:
                                                        Utility.getDeviceType() ==
                                                                '1'
                                                            ? 12
                                                            : 22,
                                                    color: Utility
                                                        .PutLightBlueColor(),
                                                    fontStyle: FontStyle.normal,
                                                  ),
                                                  maxLines: 1,
                                                ),
                                              ),
                                              SizedBox(height: 52),
                                              // Container(
                                              //   margin: EdgeInsets.fromLTRB(
                                              //       10, 0, 0, 0),
                                              //   alignment: Alignment.topLeft,
                                              //   child: Text(
                                              //     Utility.translate('for'),
                                              //     style: TextStyle(
                                              //       fontFamily: 'Poppins_Medium',
                                              //       fontSize:
                                              //           Utility.getDeviceType() ==
                                              //                   '1'
                                              //               ? 12
                                              //               : 22,
                                              //       color:
                                              //           Utility.PutDarkBlueColor(),
                                              //       fontStyle: FontStyle.normal,
                                              //     ),
                                              //     maxLines: 1,
                                              //   ),
                                              // ),
                                              // Container(
                                              //   margin: EdgeInsets.fromLTRB(
                                              //       10, 0, 0, 0),
                                              //   alignment: Alignment.topLeft,
                                              //   child: Text(
                                              //     'Follow Up',
                                              //     style: TextStyle(
                                              //       fontFamily: 'Poppins_Medium',
                                              //       fontSize:
                                              //           Utility.getDeviceType() ==
                                              //                   '1'
                                              //               ? 12
                                              //               : 22,
                                              //       color:
                                              //           Utility.PutLightBlueColor(),
                                              //       fontStyle: FontStyle.normal,
                                              //     ),
                                              //     maxLines: 1,
                                              //   ),
                                              // ),
                                            ],
                                          ),
                                        ),
                                        new Expanded(
                                          flex: 4,
                                          child: Container(
                                            child: Column(
                                              children: <Widget>[
                                                new Expanded(
                                                  flex: 1,
                                                  child: new Container(
                                                    child: Row(
                                                      children: <Widget>[
                                                        new Expanded(
                                                          flex: 2,
                                                          child: new Container(
                                                            margin: EdgeInsets
                                                                .fromLTRB(
                                                                    0, 5, 0, 0),
                                                            alignment: Alignment
                                                                .topLeft,
                                                            // child: IconButton(
                                                            //     onPressed: () {
                                                            //       Uri myUri = Uri.parse(
                                                            //           "clock-alarm");
                                                            //       launch(
                                                            //           "clock-alarm");
                                                            //     },
                                                            //     icon: SvgPicture.asset(
                                                            //         'Images/bell.svg',
                                                            //         color: Utility
                                                            //             .PutDarkBlueColor(),
                                                            //         width:
                                                            //             Utility.getDeviceType() ==
                                                            //                     "1"
                                                            //                 ? 30
                                                            //                 : 40,
                                                            //         height:
                                                            //             Utility.getDeviceType() ==
                                                            //                     "1"
                                                            //                 ? 30
                                                            //                 : 40)),
                                                          ),
                                                        ),
                                                        new Expanded(
                                                          flex: 3,
                                                          child: new Container(
                                                            margin: EdgeInsets
                                                                .fromLTRB(
                                                                    0, 5, 0, 0),
                                                            alignment: Alignment
                                                                .topLeft,
                                                            child: IconButton(
                                                                onPressed: () {
                                                                  MapUtils.openMap(
                                                                      -3.823216,
                                                                      -38.481700);
                                                                },
                                                                icon: SvgPicture.asset(
                                                                    'Images/directions.svg',
                                                                    color: Utility
                                                                        .PutDarkBlueColor(),
                                                                    width: Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 30
                                                                        : 40,
                                                                    height: Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 30
                                                                        : 40)),
                                                          ),
                                                        ),
                                                        new Expanded(
                                                          flex: 3,
                                                          child: new Container(
                                                            margin: EdgeInsets
                                                                .fromLTRB(
                                                                    0, 5, 0, 0),
                                                            alignment: Alignment
                                                                .topLeft,
                                                            child: IconButton(
                                                                onPressed: () {
                                                                  // _createDynamicLink(
                                                                  //     true,
                                                                  //     AppointmentID,
                                                                  //     "1");
                                                                  Navigator.of(
                                                                          context)
                                                                      .push(MaterialPageRoute(
                                                                          builder: (context) =>
                                                                              Videocalling(AppointmentID: listing_Appointment_Details[0].id)));
                                                                },
                                                                icon: SvgPicture.asset(
                                                                    'Images/video_call.svg',
                                                                    color: Utility
                                                                        .PutDarkBlueColor(),
                                                                    width: Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 30
                                                                        : 40,
                                                                    height: Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 30
                                                                        : 40)),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                new Expanded(
                                                  flex: 2,
                                                  child: Column(
                                                    children: <Widget>[
                                                      SizedBox(height: 45),
                                                      Container(
                                                        margin:
                                                            EdgeInsets.fromLTRB(
                                                                5, 0, 0, 0),
                                                        alignment:
                                                            Alignment.topLeft,
                                                        child: Text(
                                                          Utility.translate(
                                                              'status'),
                                                          style: TextStyle(
                                                            fontFamily:
                                                                'Poppins_Medium',
                                                            fontSize:
                                                                Utility.getDeviceType() ==
                                                                        '1'
                                                                    ? 12
                                                                    : 22,
                                                            color: Utility
                                                                .PutDarkBlueColor(),
                                                            fontStyle: FontStyle
                                                                .normal,
                                                          ),
                                                          maxLines: 1,
                                                        ),
                                                      ),
                                                      Container(
                                                        margin:
                                                            EdgeInsets.fromLTRB(
                                                                5, 0, 0, 0),
                                                        alignment:
                                                            Alignment.topLeft,
                                                        child: Text(
                                                          AppointmentStatus,
                                                          style: TextStyle(
                                                            fontFamily:
                                                                'Poppins_Medium',
                                                            fontSize:
                                                                Utility.getDeviceType() ==
                                                                        '1'
                                                                    ? 12
                                                                    : 22,
                                                            color: Utility
                                                                .PutLightBlueColor(),
                                                            fontStyle: FontStyle
                                                                .normal,
                                                          ),
                                                          maxLines: 2,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                    ),
                  ),
                ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }

  Widget Timeslot() {
    List<DoctorSpecialization> spec_Choices = const <DoctorSpecialization>[
      const DoctorSpecialization(
          titleName: 'vitals',
          iconName: 'Images/charity.svg',
          ID: 1,
          finalColor: Color(0xffFFE0E3)),
      const DoctorSpecialization(
          titleName: 'medications',
          iconName: 'Images/hospital.svg',
          ID: 2,
          finalColor: Color(0xffDBEFFF)),
      const DoctorSpecialization(
          titleName: 'documents',
          iconName: 'Images/file.svg',
          ID: 3,
          finalColor: Color(0xffFFEAE2)),
      const DoctorSpecialization(
          titleName: 'appointment',
          iconName: 'Images/calendar.svg',
          ID: 4,
          finalColor: Color(0xffDFF1FE)),
      const DoctorSpecialization(
          titleName: 'invoice',
          iconName: 'Images/invoice.svg',
          ID: 5,
          finalColor: Color(0xffF4E0FE)),
      const DoctorSpecialization(
          titleName: 'lab_report',
          iconName: 'Images/report.svg',
          ID: 6,
          finalColor: Color(0xffE1E3E8)),
    ];

    int mainSize = 6;
    return Container(
      //padding: EdgeInsets.all(10),
      child: GridView.builder(
          shrinkWrap: true,
          controller: new ScrollController(keepScrollOffset: false),
          itemCount: mainSize,
          padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            crossAxisSpacing: 5,
            mainAxisSpacing: 5,
            childAspectRatio: Utility.getDeviceType() == '1' ? 1.2 : 1.5,
          ),
          itemBuilder: (BuildContext context, int index) {
            DoctorSpecialization item = spec_Choices[index];
            return GestureDetector(
              onTap: () {
                setState(() {
                  if (index == 0) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => vitals()),
                    );
                  }
                  if (index == 1) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Medications()),
                    );
                  }
                  if (index == 2) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => mydocument()),
                    );
                  }
                  if (index == 3) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              appointmentmanagement(TabCheck: false)),
                    );
                  }
                  if (index == 4) {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //       builder: (context) => invoiceTempPage()),
                    // );
                    print("Invoice Click");
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => invoicemanagment()),
                    );
                  }
                  if (index == 5) {
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //       builder: (context) => LabReportTempPage()),
                    // );
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => labreports()),
                    );
                  }
                });
              },
              child: Container(
                  child: Column(
                children: [
                  Container(
                    //color: item.finalColor,
                    child: AspectRatio(
                      aspectRatio: 2.0,
                      child: Center(
                          child: SvgPicture.asset(item.iconName,
                              color: Utility.PutDarkBlueColor(),
                              height: Utility.getDeviceType() == '1' ? 40 : 80,
                              width: Utility.getDeviceType() == '1' ? 40 : 80)),
                    ),
                  ),
                  Container(
                    // child: Utility.MainNormalTextWithFont(item.titleName, 12,
                    //     Utility.PutDarkBlueColor(), 'Poppins_Medium'),
                    child: Text(Utility.translate(item.titleName),
                        maxLines: 2,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: 'Poppins_Medium',
                            color: Utility.PutDarkBlueColor(),
                            fontSize:
                                Utility.getDeviceType() == '1' ? 11 : 23)),
                  ),
                ],
              )),
            );
          }),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      currentIndex = index;
    });
  }
}
