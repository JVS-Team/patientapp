import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:softcliniccare/Helper/utils.dart';

import '../Helper/Constant.dart';
import '../Helper/Database/sqflite_helper.dart';
import '../Invoice/PDF/FileProcess.dart';
import '../Invoice/PDF/PDF_File_Display.dart';
import '../Model/Appointment/Appointment_Status.dart';
import '../Model/Dashboard/AppointmentListModel.dart';
import '../Model/Dashboard/Dashboard_API.dart';
import '../Model/Dashboard/Facility_Name_Model.dart';
import '../Model/Dashboard/PatientDetails_All.dart';
import '../Model/Profile/ProfileAPI.dart';

class PastAppointment extends StatefulWidget {
  @override
  PastAppointmentState createState() => PastAppointmentState();
}

class PastAppointmentState extends State<PastAppointment> {
  List<AppointmentList_Details> listing_Appointment_Details_Temp = [];
  List<AppointmentList_Details> listing_Appointment_Details = [];

  List<Facility_Details> listing_FacilityName = [];

  String AppointmentMonth = '';
  String AppointmentWeekName = '';
  String AppointmentDate = '';
  String Time12Hour = '';
  String Status = '';
  String FacilityName = '';

  late PatientDetails mainPatientDetails;
  String FirstName = '';
  String LastName = '';
  GlobalKey<State> _AppointmentList = new GlobalKey<State>();
  List<AppointmentStatusDetails> listing_App_Status = [];

  List<Map> Employeetype = [];
  Map Dictemployeetype = {};

  @override
  void initState() {
    super.initState();
    FacilityName_Details();
    AppointmentList();
    MainPatientDetails();
    getAppointmentStatusAPI();
    getDoctorNameList();
  }

  void getDoctorNameList() async {
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');

    final data = await SQLHelper.getAll_Facility_Response(MainPatientID);
    List mainEmployeeDetails = await data;

    print("DB Length Employee List ${mainEmployeeDetails.length}");

    String main_EmployeeDetails = "";

    if (mainEmployeeDetails.length > 0) {
      for (int i = 0; i < mainEmployeeDetails.length; i++) {
        if (mainEmployeeDetails[i]['FacilityID'] == MainPatientID) {
          main_EmployeeDetails = mainEmployeeDetails[i]['FacilityResponse'];
          break;
        } else {}
      }
      // log("Doctor List ${main_EmployeeDetails}");
      if (main_EmployeeDetails == "") {
        API_Dashboard().employeebytype().then((mainResponse) {
          setState(() {
            if (mainEmployeeDetails.length > 0) {
              AllFacilityUpdate(MainPatientID, mainResponse);
            } else {
              AllFacilityInsert(MainPatientID, mainResponse);
            }
            GetEmployeeTypeData(mainResponse);
          });
        });
      } else {
        GetEmployeeTypeData(main_EmployeeDetails);
      }
    } else {
      API_Dashboard().employeebytype().then((mainResponse) {
        setState(() {
          if (mainEmployeeDetails.length > 0) {
            AllFacilityUpdate(MainPatientID, mainResponse);
          } else {
            AllFacilityInsert(MainPatientID, mainResponse);
          }
          GetEmployeeTypeData(mainResponse);
        });
      });
    }
  }

  void GetEmployeeTypeData(String mainResponse) async {
    log("Doctor List ${mainResponse}");
    Employeetype = [];
    var EmployeeData = json.decode(mainResponse);
    List response = EmployeeData;
    for (int i = 0; i < response.length; i++) {
      Employeetype.add(response[i]);
    }
  }

  void MainPatientDetails() async {
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');

    final data = await SQLHelper.getAll_ProfileDetails(MainPatientID);
    List<Map<String, dynamic>> allPatientResponse = data;
    String mainPatientResponse = "";

    if (allPatientResponse.length > 0) {
      for (int i = 0; i < allPatientResponse.length; i++) {
        if (allPatientResponse[i]['profile_id'] == MainPatientID) {
          mainPatientResponse = allPatientResponse[i]['profile_response'];
          break;
        } else {}
      }
      if (mainPatientResponse == "") {
        API_Profile()
            .ProfilePageDisplay(AccessToken, MainPatientID)
            .then((Response) async {
          if (allPatientResponse.length > 0) {
            AllProfileUpdate(MainPatientID, json.encode(Response));
          } else {
            AllProfileInsert(MainPatientID, json.encode(Response));
          }

          ProfileMainResponse(Response);
        });
      } else {
        ProfileMainResponse(json.decode(mainPatientResponse));
      }
    } else {
      API_Profile()
          .ProfilePageDisplay(AccessToken, MainPatientID)
          .then((Response) async {
        if (allPatientResponse.length > 0) {
          AllProfileUpdate(MainPatientID, json.encode(Response));
        } else {
          AllProfileInsert(MainPatientID, json.encode(Response));
        }
        ProfileMainResponse(Response);
      });
    }
  }

  void ProfileMainResponse(var Response) {
    if (mounted) {
      setState(() {
        mainPatientDetails = new PatientDetails.fromJson(Response);

        FirstName = mainPatientDetails.patregi.firstName;
        LastName = mainPatientDetails.patregi.lastName;
      });
    } else {
      mainPatientDetails = new PatientDetails.fromJson(Response);

      FirstName = mainPatientDetails.patregi.firstName;
      LastName = mainPatientDetails.patregi.lastName;
    }
  }

  void AllProfileInsert(String PatientID, String PatientResponse) async {
    await SQLHelper.insert_Profile_Details(PatientID, PatientResponse);
  }

  void AllProfileUpdate(String PatientID, String PatientResponse) async {
    await SQLHelper.update_Profile_response(PatientID, PatientResponse);
  }

  void FacilityName_Details() async {
    String AccessToken = await Utility.readAccessToken('Token');

    final data = await SQLHelper.getAll_Facility_Response(
        ConstantVar.str_Common_GETALLFACILITYNAME);
    List<Map<String, dynamic>> allFacilityResponse = data;
    String mainFacilityResponse = "";

    if (allFacilityResponse.length > 0) {
      for (int i = 0; i < allFacilityResponse.length; i++) {
        if (allFacilityResponse[i]['FacilityID'] ==
            ConstantVar.str_Common_GETALLFACILITYNAME) {
          mainFacilityResponse = allFacilityResponse[i]['FacilityResponse'];
          break;
        } else {}
      }
      if (mainFacilityResponse == "") {
        API_Dashboard()
            .Dashboard_Facilityname(AccessToken)
            .then((Response) async {
          if (allFacilityResponse.length > 0) {
            AllFacilityUpdate(ConstantVar.str_Common_GETALLFACILITYNAME,
                json.encode(Response));
          } else {
            AllFacilityInsert(ConstantVar.str_Common_GETALLFACILITYNAME,
                json.encode(Response));
          }
          FacilityMainResponse(Response);
        });
      } else {
        FacilityMainResponse(json.decode(mainFacilityResponse));
      }
    } else {
      API_Dashboard()
          .Dashboard_Facilityname(AccessToken)
          .then((Response) async {
        if (allFacilityResponse.length > 0) {
          AllFacilityUpdate(
              ConstantVar.str_Common_GETALLFACILITYNAME, json.encode(Response));
        } else {
          AllFacilityInsert(
              ConstantVar.str_Common_GETALLFACILITYNAME, json.encode(Response));
        }
        FacilityMainResponse(Response);
      });
    }
  }

  void FacilityMainResponse(var Response) {
    if (mounted) {
      setState(() {
        var tempdata = Response['value'];
        for (int i = 0; i < tempdata.length; i++) {
          listing_FacilityName.add(Facility_Details.fromJson(tempdata[i]));
        }
      });
    } else {
      var tempdata = Response['value'];
      for (int i = 0; i < tempdata.length; i++) {
        listing_FacilityName.add(Facility_Details.fromJson(tempdata[i]));
      }
    }
  }

  void AppointmentList() async {
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');
    // Utility.showLoadingDialog(context, _AppointmentList, true);

    final data = await SQLHelper.getAll_AppointmentList(
        MainPatientID, ConstantVar.str_Common_GETPASTAPPOINTMENT);
    List<Map<String, dynamic>> allAppointmentList = data;
    String mainAppointmentResponse = "";

    if (allAppointmentList.length > 0) {
      for (int i = 0; i < allAppointmentList.length; i++) {
        if (allAppointmentList[i]['PatientID'] == MainPatientID) {
          mainAppointmentResponse =
              allAppointmentList[i]['AppointmentResponse'];
          break;
        } else {}
      }
      if (mainAppointmentResponse == "") {
        API_Dashboard()
            .AppointmentListAPI(AccessToken, MainPatientID)
            .then((Response) async {
          if (allAppointmentList.length > 0) {
            AllAppointmentUpdate(MainPatientID,
                ConstantVar.str_Common_GETPASTAPPOINTMENT, Response);
          } else {
            AllAppointmentInsert(MainPatientID,
                ConstantVar.str_Common_GETPASTAPPOINTMENT, Response);
          }
          if (mounted) {
            setState(() {
              GetAll_AppList(Response);
            });
          } else {
            GetAll_AppList(Response);
          }
        });
      } else {
        GetAll_AppList(mainAppointmentResponse);
      }
    } else {
      API_Dashboard()
          .AppointmentListAPI(AccessToken, MainPatientID)
          .then((Response) async {
        if (allAppointmentList.length > 0) {
          AllAppointmentUpdate(MainPatientID,
              ConstantVar.str_Common_GETPASTAPPOINTMENT, Response);
        } else {
          AllAppointmentInsert(MainPatientID,
              ConstantVar.str_Common_GETPASTAPPOINTMENT, Response);
        }
        if (mounted) {
          setState(() {
            GetAll_AppList(Response);
          });
        } else {
          GetAll_AppList(Response);
        }
      });
    }
  }

  void GetAll_AppList(String Response) {
    log('Past Appointment Details =========  ${Response}');
    var jsonResponse = json.decode(Response);
    var tempdata = jsonResponse;

    listing_Appointment_Details = [];
    listing_Appointment_Details_Temp = [];

    if (tempdata.length > 0) {
      for (int i = 0; i < tempdata.length; i++) {
        listing_Appointment_Details_Temp
            .add(AppointmentList_Details.fromJson(tempdata[i]));
      }

      for (int i = 0; i < listing_Appointment_Details_Temp.length; i++) {
        DateTime serverDate = new DateFormat("yyyy-MM-dd")
            .parse(listing_Appointment_Details_Temp[i].appointmentDate);

        final birthday =
            DateTime(serverDate.year, serverDate.month, serverDate.day);
        final date2 = DateTime.now();
        final difference = date2.difference(birthday).inDays;

        if (difference <= 0) {
          // print('Less');
        } else {
          // print('Greater');
          listing_Appointment_Details.add(listing_Appointment_Details_Temp[i]);
        }
      }
      print(listing_Appointment_Details.length);
      for (int k = 0; k < listing_Appointment_Details.length; k++) {
        for (int i = 0; i < listing_FacilityName.length; i++) {
          if (listing_Appointment_Details[k].facilityId ==
              listing_FacilityName[i].id) {
            listing_Appointment_Details[k].facilityName =
                listing_FacilityName[i].name;
          }
        }
      }
    }
  }

  void GetPDFDetails(String AppointmentID) async {
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');
    Utility.showLoadingDialog(context, _AppointmentList, true);

    API_Dashboard()
        .getMainAppointmentDetails(AccessToken, MainPatientID, AppointmentID)
        .then((Response) async {
      var resBody = json.decode(Response);
      String mainBase64Details = resBody['base64String'];
      // log("Here ${mainBase64Details}");
      Navigator.of(context).pop();
      _createFileFromString(mainBase64Details.split(',').last);
    });
  }

  void _createFileFromString(String PDFbase64) async {
    Future<Uint8List> mainPDFBytes = FileProcess.downloadFile(PDFbase64);
    Uint8List PDFContentBytes = await mainPDFBytes;
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => PDFFileDisplay(main8Bytes: PDFContentBytes)),
    );
  }

  void AllFacilityInsert(String FacilityID, String FacilityResponse) async {
    await SQLHelper.insertName_Facility(FacilityID, FacilityResponse);
  }

  void AllFacilityUpdate(String FacilityID, String FacilityResponse) async {
    await SQLHelper.update_facility_response(FacilityID, FacilityResponse);
  }

  void AllAppointmentInsert(String PatientID, String UpComing_Past,
      String AppointmentResponse) async {
    await SQLHelper.insert_Appointment_Details(
        PatientID, AppointmentResponse, UpComing_Past);
  }

  void AllAppointmentUpdate(String PatientID, String UpComing_Past,
      String AppointmentResponse) async {
    await SQLHelper.update_Appointment_response(
        PatientID, AppointmentResponse, UpComing_Past);
  }

  void getAppointmentStatusAPI() async {
    final data = await SQLHelper.getNoChangeDetails(
        ConstantVar.str_Common_GetAllAppointmentStatus);
    List<Map<String, dynamic>> all_AppointmentStatus = data;
    String mainAppointmentStatus = "";

    if (all_AppointmentStatus.length > 0) {
      for (int i = 0; i < all_AppointmentStatus.length; i++) {
        if (all_AppointmentStatus[i]['APIName'] ==
            ConstantVar.str_Common_GetAllAppointmentStatus) {
          mainAppointmentStatus = all_AppointmentStatus[i]['APIResponse'];
          break;
        } else {}
      }
      if (mainAppointmentStatus == "") {
        API_Dashboard().AppointmentStatusDetails().then((Response) {
          setState(() {
            if (all_AppointmentStatus.length > 0) {
              AllDataUpdate(
                  ConstantVar.str_Common_GetAllAppointmentStatus, Response);
            } else {
              AllDataInsert(
                  ConstantVar.str_Common_GetAllAppointmentStatus, Response);
            }
            mainStatusDetails(Response);
          });
        });
      } else {
        mainStatusDetails(mainAppointmentStatus);
      }
    } else {
      API_Dashboard().AppointmentStatusDetails().then((Response) {
        setState(() {
          if (all_AppointmentStatus.length > 0) {
            AllDataUpdate(
                ConstantVar.str_Common_GetAllAppointmentStatus, Response);
          } else {
            AllDataInsert(
                ConstantVar.str_Common_GetAllAppointmentStatus, Response);
          }
          mainStatusDetails(Response);
        });
      });
    }
  }

  void mainStatusDetails(String mainResponse) {
    var resBody = json.decode(mainResponse);

    var tempdata = resBody;
    for (int i = 0; i < tempdata.length; i++) {
      listing_App_Status.add(AppointmentStatusDetails.fromJson(tempdata[i]));
    }
  }

  void AllDataInsert(String APIName, String APIResponse) async {
    String cdate = DateFormat("yyyy-MM-dd").format(DateTime.now());
    String tTime = DateFormat("HH:mm").format(DateTime.now());
    await SQLHelper.insertMainCommonData(APIName, APIResponse, cdate, tTime);
  }

  void AllDataUpdate(String APIName, String APIResponse) async {
    await SQLHelper.updateNoChangeDetails(APIName, APIResponse);
  }

  Future<void> _pullRefresh() async {
    print("Refresh");
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');

    final data = await SQLHelper.getAll_AppointmentList(
        MainPatientID, ConstantVar.str_Common_GETPASTAPPOINTMENT);
    List<Map<String, dynamic>> allAppointmentList = data;
    setState(() {
      API_Dashboard()
          .AppointmentListAPI(AccessToken, MainPatientID)
          .then((Response) async {
        if (allAppointmentList.length > 0) {
          AllAppointmentUpdate(MainPatientID,
              ConstantVar.str_Common_GETPASTAPPOINTMENT, Response);
        } else {
          AllAppointmentInsert(MainPatientID,
              ConstantVar.str_Common_GETPASTAPPOINTMENT, Response);
        }
        if (mounted) {
          setState(() {
            GetAll_AppList(Response);
          });
        } else {
          GetAll_AppList(Response);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        //bottomNavigationBar: _myapp,
        body: Container(
          padding: Utility.getDeviceType() == '1'
              ? EdgeInsets.fromLTRB(8, 0, 8, 0)
              : EdgeInsets.fromLTRB(15, 10, 15, 100),
          child: listing_Appointment_Details.length == 0
              ? Container(
                  child: Center(
                    child: Text('No Past Appointment Found'),
                  ),
                )
              : RefreshIndicator(
                  onRefresh: _pullRefresh,
                  child: ListView.builder(
                    itemCount: listing_Appointment_Details.length,
                    itemBuilder: (context, pos) {
                      bool bln_PrescriptionDisplay = false;
                      DateTime parseDate = new DateFormat("yyyy-MM-dd").parse(
                          listing_Appointment_Details[pos].appointmentDate);
                      var inputDate = DateTime.parse(parseDate.toString());
                      var outputFormatMonth = DateFormat('MMM');
                      var outputFormatDay = DateFormat('EEE');
                      var outputFormatDate = DateFormat('dd');
                      var outputMonth = outputFormatMonth.format(inputDate);
                      AppointmentMonth = outputMonth;
                      var outputDay = outputFormatDay.format(inputDate);
                      AppointmentWeekName = outputDay;
                      var outputDate = outputFormatDate.format(inputDate);
                      AppointmentDate = outputDate;

                      DateTime parseTime = new DateFormat("HH:mm:ss")
                          .parse(listing_Appointment_Details[pos].startTime);

                      var inputTime = DateTime.parse(parseTime.toString());
                      var outputTimeData = DateFormat('jm');
                      var outputTime = outputTimeData.format(inputTime);
                      // print(outputTime);
                      Time12Hour = outputTime;
                      // Status = listing_Appointment_Details[pos].appointmentStatus;

                      String AppointmentStatus = '';
                      String AppointmentStatusID = '';

                      String DoctorName = "";

                      for (int i = 0; i < listing_App_Status.length; i++) {
                        if (listing_App_Status[i].id ==
                            listing_Appointment_Details[pos]
                                .appointmentStatus) {
                          AppointmentStatusID = listing_App_Status[i].id;
                          AppointmentStatus = listing_App_Status[i].displayName;
                        }
                      }

                      for (int i = 0; i < Employeetype.length; i++) {
                        if (Employeetype[i]['id'] ==
                            listing_Appointment_Details[pos].doctorId) {
                          DoctorName = Employeetype[i]['displayName'];
                        }
                      }

                      if (Status == "CONSULTATIONCOMPLETED" ||
                          Status == "VISITCLOSED") {
                        bln_PrescriptionDisplay = true;
                      } else {
                        bln_PrescriptionDisplay = false;
                      }

                      return Container(
                        child: Card(
                            margin: EdgeInsets.all(10),
                            color: Utility.PutBackgroundCardColor(),
                            elevation: 5,
                            child: Container(
                              height:
                                  Utility.getDeviceType() == '1' ? 220 : 270,
                              child: Row(
                                children: <Widget>[
                                  new Expanded(
                                    flex: 4,
                                    child: new Container(
                                      color: Colors.white,
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.fromLTRB(
                                                10, 12, 0, 0),
                                            child: Align(
                                              alignment: Alignment.topLeft,
                                              child: Utility
                                                  .MainNormalTextWithFont(
                                                      AppointmentMonth,
                                                      Utility.getDeviceType() ==
                                                              '1'
                                                          ? 14
                                                          : 24,
                                                      Utility
                                                          .PutDarkBlueColor(),
                                                      'Poppins_Medium'),
                                            ),
                                          ),
                                          SizedBox(
                                              height:
                                                  Utility.getDeviceType() == '1'
                                                      ? 25
                                                      : 35),
                                          Container(
                                            height:
                                                Utility.getDeviceType() == '1'
                                                    ? 37
                                                    : 47,
                                            child: Align(
                                              alignment: Alignment.bottomCenter,
                                              child: Utility
                                                  .MainNormalTextWithFont(
                                                      AppointmentDate,
                                                      Utility.getDeviceType() ==
                                                              '1'
                                                          ? 22
                                                          : 32,
                                                      Utility
                                                          .PutLightBlueColor(),
                                                      'Poppins_Medium'),
                                            ),
                                          ),
                                          Container(
                                            child: Align(
                                              alignment: Alignment.topCenter,
                                              child: Utility
                                                  .MainNormalTextWithFont(
                                                      AppointmentWeekName
                                                          .toUpperCase(),
                                                      Utility.getDeviceType() ==
                                                              '1'
                                                          ? 18
                                                          : 28,
                                                      Utility
                                                          .PutLightBlueColor(),
                                                      'Poppins_Medium'),
                                            ),
                                          ),
                                          SizedBox(
                                              height:
                                                  Utility.getDeviceType() == '1'
                                                      ? 15
                                                      : 15),
                                          Container(
                                            child: Align(
                                              child: Utility
                                                  .MainNormalTextWithFont(
                                                      Time12Hour,
                                                      Utility.getDeviceType() ==
                                                              '1'
                                                          ? 20
                                                          : 30,
                                                      Utility
                                                          .PutDarkBlueColor(),
                                                      'Poppins_Medium'),
                                              alignment: Alignment.bottomCenter,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                      child: Container(
                                    width: 7,
                                    height: double.infinity,
                                    color: Utility.PutLightBlueColor(),
                                  )),
                                  new Expanded(
                                      flex: 11,
                                      child: new Container(
                                        color: Utility.PutBackgroundCardColor(),
                                        child: Column(
                                          children: <Widget>[
                                            new Expanded(
                                              child: Row(
                                                children: <Widget>[
                                                  new Expanded(
                                                    flex: 5,
                                                    child: Container(
                                                      margin:
                                                          EdgeInsets.fromLTRB(
                                                              7, 10, 0, 0),
                                                      alignment:
                                                          Alignment.topLeft,
                                                      child: Text(
                                                        FirstName +
                                                            " " +
                                                            LastName,
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'Poppins_Medium',
                                                          fontSize:
                                                              Utility.getDeviceType() ==
                                                                      '1'
                                                                  ? 14
                                                                  : 24,
                                                          color: Utility
                                                              .PutDarkBlueColor(),
                                                          fontStyle:
                                                              FontStyle.normal,
                                                        ),
                                                        maxLines: 1,
                                                      ),
                                                    ),
                                                  ),
                                                  new Expanded(
                                                    flex: 3,
                                                    child: Row(
                                                      children: <Widget>[
                                                        new Expanded(
                                                          flex: 1,
                                                          child: new Container(
                                                            margin: EdgeInsets
                                                                .fromLTRB(
                                                                    0, 5, 0, 0),
                                                            alignment: Alignment
                                                                .topLeft,
                                                            child: IconButton(
                                                                onPressed:
                                                                    () {},
                                                                icon: SvgPicture.asset(
                                                                    'Images/directions.svg',
                                                                    color: Utility
                                                                        .PutDarkBlueColor(),
                                                                    width: Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 30
                                                                        : 40,
                                                                    height: Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 30
                                                                        : 40)),
                                                          ),
                                                        ),
                                                        new Expanded(
                                                          flex: 1,
                                                          child: new Container(
                                                            margin: EdgeInsets
                                                                .fromLTRB(
                                                                    0, 5, 0, 0),
                                                            alignment: Alignment
                                                                .topLeft,
                                                            child: IconButton(
                                                                onPressed:
                                                                    () {},
                                                                icon: SvgPicture.asset(
                                                                    'Images/video_call.svg',
                                                                    color: Utility
                                                                        .PutDarkBlueColor(),
                                                                    width: Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 30
                                                                        : 40,
                                                                    height: Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 30
                                                                        : 40)),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.fromLTRB(
                                                  7, 0, 0, 0),
                                              alignment: Alignment.centerLeft,
                                              child: Text(
                                                DoctorName,
                                                style: TextStyle(
                                                  fontFamily: 'Poppins_Medium',
                                                  fontSize:
                                                      Utility.getDeviceType() ==
                                                              '1'
                                                          ? 12
                                                          : 22,
                                                  color: Utility
                                                      .PutLightBlueColor(),
                                                  fontStyle: FontStyle.normal,
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: Container(
                                                margin: EdgeInsets.fromLTRB(
                                                    7, 0, 0, 0),
                                                alignment: Alignment.topLeft,
                                                child: Text(
                                                  listing_Appointment_Details[
                                                          pos]
                                                      .facilityName,
                                                  style: TextStyle(
                                                    fontFamily:
                                                        'Poppins_Medium',
                                                    fontSize:
                                                        Utility.getDeviceType() ==
                                                                '1'
                                                            ? 12
                                                            : 22,
                                                    color: Utility
                                                        .PutLightBlueColor(),
                                                    fontStyle: FontStyle.normal,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            new Expanded(
                                              child: Row(
                                                children: <Widget>[
                                                  new Expanded(
                                                    flex: 7,
                                                    child: Container(),
                                                    // child: Container(
                                                    //   margin: EdgeInsets.fromLTRB(
                                                    //       7, 20, 0, 0),
                                                    //   alignment:
                                                    //       Alignment.topLeft,
                                                    //   child: Text(
                                                    //     Utility.translate('for'),
                                                    //     style: TextStyle(
                                                    //       fontFamily:
                                                    //           'Poppins_Medium',
                                                    //       fontSize:
                                                    //           Utility.getDeviceType() ==
                                                    //                   '1'
                                                    //               ? 12
                                                    //               : 22,
                                                    //       color: Utility
                                                    //           .PutDarkBlueColor(),
                                                    //       fontStyle:
                                                    //           FontStyle.normal,
                                                    //     ),
                                                    //   ),
                                                    // ),
                                                  ),
                                                  new Expanded(
                                                    flex: 5,
                                                    child: Container(
                                                      margin:
                                                          EdgeInsets.fromLTRB(
                                                              7, 15, 0, 0),
                                                      alignment:
                                                          Alignment.topLeft,
                                                      child: Text(
                                                        Utility.translate(
                                                            'status'),
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'Poppins_Medium',
                                                          fontSize:
                                                              Utility.getDeviceType() ==
                                                                      '1'
                                                                  ? 12
                                                                  : 22,
                                                          color: Utility
                                                              .PutDarkBlueColor(),
                                                          fontStyle:
                                                              FontStyle.normal,
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            new Expanded(
                                              child: Row(
                                                children: <Widget>[
                                                  new Expanded(
                                                    flex: 7,
                                                    child: Container(),
                                                    // child: Container(
                                                    //   margin: EdgeInsets.fromLTRB(
                                                    //       7, 0, 0, 0),
                                                    //   alignment:
                                                    //       Alignment.topLeft,
                                                    //   child: Text(
                                                    //     'Follow Up',
                                                    //     style: TextStyle(
                                                    //       fontFamily:
                                                    //           'Poppins_Medium',
                                                    //       fontSize:
                                                    //           Utility.getDeviceType() ==
                                                    //                   '1'
                                                    //               ? 12
                                                    //               : 22,
                                                    //       color: Utility
                                                    //           .PutLightBlueColor(),
                                                    //       fontStyle:
                                                    //           FontStyle.normal,
                                                    //     ),
                                                    //     maxLines: 1,
                                                    //   ),
                                                    // ),
                                                  ),
                                                  new Expanded(
                                                    flex: 5,
                                                    child: Container(
                                                      margin:
                                                          EdgeInsets.fromLTRB(
                                                              5, 0, 0, 0),
                                                      alignment:
                                                          Alignment.topLeft,
                                                      child: Text(
                                                        AppointmentStatus,
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'Poppins_Medium',
                                                          fontSize:
                                                              Utility.getDeviceType() ==
                                                                      '1'
                                                                  ? 12
                                                                  : 22,
                                                          color: Utility
                                                              .PutLightBlueColor(),
                                                          fontStyle:
                                                              FontStyle.normal,
                                                        ),
                                                        maxLines: 2,
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            bln_PrescriptionDisplay == true
                                                ? new Expanded(
                                                    child: Row(
                                                      children: <Widget>[
                                                        SizedBox(
                                                          width: 3,
                                                        ),
                                                        Container(
                                                          color: Colors
                                                              .transparent,
                                                          child: Row(
                                                            children: <Widget>[
                                                              GestureDetector(
                                                                child: Row(
                                                                  children: [
                                                                    Container(
                                                                      child: SvgPicture.asset(
                                                                          'Images/prescription.svg',
                                                                          color: Utility
                                                                              .PutDarkBlueColor(),
                                                                          width: Utility.getDeviceType() == "1"
                                                                              ? 15
                                                                              : 35,
                                                                          height: Utility.getDeviceType() == "1"
                                                                              ? 15
                                                                              : 35),
                                                                    ),
                                                                    SizedBox(
                                                                      width: Utility.getDeviceType() ==
                                                                              "1"
                                                                          ? 2
                                                                          : 5,
                                                                    ),
                                                                    Container(
                                                                        child:
                                                                            Text(
                                                                      'Prescription',
                                                                      style:
                                                                          TextStyle(
                                                                        fontFamily:
                                                                            'Poppins_Medium',
                                                                        fontSize: Utility.getDeviceType() ==
                                                                                "1"
                                                                            ? 11
                                                                            : 20,
                                                                        color: Utility
                                                                            .PutLightBlueColor(),
                                                                        fontStyle:
                                                                            FontStyle.normal,
                                                                      ),
                                                                      maxLines:
                                                                          1,
                                                                    )),
                                                                  ],
                                                                ),
                                                                onTap: () {
                                                                  setState(() {
                                                                    print(
                                                                        "Main Click ${listing_Appointment_Details[pos].id}");
                                                                    GetPDFDetails(
                                                                        listing_Appointment_Details[pos]
                                                                            .id);
                                                                  });
                                                                },
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        SizedBox(
                                                            width:
                                                                Utility.getDeviceType() ==
                                                                        "1"
                                                                    ? 8
                                                                    : 5),
                                                        Container(
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: <Widget>[
                                                              Container(
                                                                child: SvgPicture.asset(
                                                                    'Images/receipt.svg',
                                                                    color: Utility
                                                                        .PutDarkBlueColor(),
                                                                    width: Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 15
                                                                        : 35,
                                                                    height: Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 15
                                                                        : 35),
                                                              ),
                                                              SizedBox(
                                                                width:
                                                                    Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 2
                                                                        : 5,
                                                              ),
                                                              Container(
                                                                  child: Text(
                                                                'Receipt',
                                                                style:
                                                                    TextStyle(
                                                                  fontFamily:
                                                                      'Poppins_Medium',
                                                                  fontSize:
                                                                      Utility.getDeviceType() ==
                                                                              "1"
                                                                          ? 11
                                                                          : 20,
                                                                  color: Utility
                                                                      .PutLightBlueColor(),
                                                                  fontStyle:
                                                                      FontStyle
                                                                          .normal,
                                                                ),
                                                                maxLines: 1,
                                                              )),
                                                            ],
                                                          ),
                                                        ),
                                                        SizedBox(
                                                            width:
                                                                Utility.getDeviceType() ==
                                                                        "1"
                                                                    ? 8
                                                                    : 5),
                                                        Container(
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: <Widget>[
                                                              Container(
                                                                child: SvgPicture.asset(
                                                                    'Images/summary.svg',
                                                                    color: Utility
                                                                        .PutDarkBlueColor(),
                                                                    width: Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 15
                                                                        : 35,
                                                                    height: Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 15
                                                                        : 35),
                                                              ),
                                                              SizedBox(
                                                                width:
                                                                    Utility.getDeviceType() ==
                                                                            "1"
                                                                        ? 3
                                                                        : 5,
                                                              ),
                                                              Container(
                                                                  child: Text(
                                                                'Summary',
                                                                style:
                                                                    TextStyle(
                                                                  fontFamily:
                                                                      'Poppins_Medium',
                                                                  fontSize:
                                                                      Utility.getDeviceType() ==
                                                                              "1"
                                                                          ? 11
                                                                          : 20,
                                                                  color: Utility
                                                                      .PutLightBlueColor(),
                                                                  fontStyle:
                                                                      FontStyle
                                                                          .normal,
                                                                ),
                                                                maxLines: 1,
                                                              )),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                : Container(),
                                          ],
                                        ),
                                      ))
                                ],
                              ),
                            )),
                      );
                    },
                  ),
                ),
        ),
      ),
    );
  }
}
