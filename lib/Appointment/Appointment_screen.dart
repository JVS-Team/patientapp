import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:softcliniccare/Appointment/Bookappointment_screen.dart';
import 'package:softcliniccare/Appointment/Current_Appointment.dart';
import 'package:softcliniccare/Appointment/Past_Appointment.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'package:softcliniccare/Document/MyDocument_screen.dart';

import '../Model/Dashboard/Dashboard_API.dart';
import '../Model/Dashboard/PatientDetails_All.dart';

class appointmentmanagement extends StatefulWidget {
  appointmentmanagement({Key? key, required this.TabCheck}) : super(key: key);
  bool TabCheck;

  @override
  appointmentmanagementState createState() => appointmentmanagementState();
}

class appointmentmanagementState extends State<appointmentmanagement>
    with SingleTickerProviderStateMixin {
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = new TabController(vsync: this, length: 3);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        // appBar: Utility.TopToolBar(
        //     Utility.translate('my_appointment'),
        //     Utility.getDeviceType() == "1" ? 20 : 30,
        //     Utility.PutDarkBlueColor(),
        //     context),
        appBar: widget.TabCheck == true
            ? AppBar(
                toolbarHeight: Utility.getDeviceType() == '1' ? 50 : 70,
                centerTitle: true,
                backgroundColor: Colors.white,
                leading: IconButton(
                  padding: EdgeInsets.fromLTRB(
                      Utility.getDeviceType() == '1' ? 5 : 20,
                      Utility.getDeviceType() == '1' ? 5 : 10,
                      0,
                      0),
                  icon: SvgPicture.asset('Images/menu.svg',
                      color: Colors.black,
                      width: Utility.getDeviceType() == "1" ? 15 : 50,
                      height: Utility.getDeviceType() == "1" ? 15 : 50),
                  onPressed: () => {Scaffold.of(context).openDrawer()},
                  //_scaffoldKey.currentState!.openDrawer()},
                ),
                actions: [
                  Container(
                      padding: Utility.getDeviceType() == '1'
                          ? EdgeInsets.fromLTRB(0, 0, 10, 0)
                          : EdgeInsets.fromLTRB(0, 5, 5, 0),
                      height: Utility.getDeviceType() == '1' ? 50 : 70,
                      width: Utility.getDeviceType() == '1' ? 50 : 70,
                      child: CircleAvatar(
                        radius: Utility.getDeviceType() == '1' ? 55 : 70,
                        backgroundColor: Colors.transparent,
                        child: CircleAvatar(
                          radius: Utility.getDeviceType() == '1' ? 50 : 70,
                          backgroundColor: Colors.transparent,
                          backgroundImage: NetworkImage(
                              'https://vinusimages.co/wp-content/uploads/2018/10/EG7A2390.djpgA_.jpg'),
                        ),
                      ))
                ],
                title: Image.asset(
                  'Images/softclinic_logo.png',
                  height: Utility.getDeviceType() == '1' ? 30 : 40,
                  alignment: Alignment.centerRight,
                ),
              )
            : Utility.TopToolBar(
                Utility.translate('my_appointment'),
                Utility.getDeviceType() == "1" ? 20 : 30,
                Utility.PutDarkBlueColor(),
                context),
        body: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: PreferredSize(
              preferredSize:
                  Size.fromHeight(Utility.getDeviceType() == '1' ? 90 : 120),
              child: Container(
                height: Utility.getDeviceType() == '1' ? 50 : 70,
                margin: Utility.getDeviceType() == '1'
                    ? EdgeInsets.fromLTRB(10, 25, 10, 20)
                    : EdgeInsets.fromLTRB(20, 20, 20, 10),
                decoration: BoxDecoration(
                    border: Border.all(color: Utility.PutDarkBlueColor())),
                child: new TabBar(
                  // indicatorColor: Colors.blue,
                  unselectedLabelColor: Utility.PutDarkBlueColor(),
                  // labelColor: Colors.blue,
                  // indicatorSize: TabBarIndicatorSize.label,
                  indicator: ShapeDecoration(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(0),
                              topLeft: Radius.circular(0))),
                      color: Utility.PutDarkBlueColor()),
                  tabs: [
                    Tab(
                      child: Text(Utility.translate('upcoming'),
                          style: TextStyle(
                              fontSize:
                                  Utility.getDeviceType() == '1' ? 16.0 : 25)),
                    ),
                    Tab(
                      child: Text(Utility.translate('past'),
                          style: TextStyle(
                              fontSize:
                                  Utility.getDeviceType() == '1' ? 16.0 : 25)),
                    ),
                  ],
                ),
              ),
            ),
            body: TabBarView(
              children: [
                CurrentAppointment(),
                PastAppointment(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
