import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:developer';
import 'package:softcliniccare/Helper/const_data.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:softcliniccare/Helper/date_picker_timeline.dart';
import 'dart:io';
import 'package:softcliniccare/Helper/utils.dart';

//import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:intl/intl.dart';

import '../Helper/AllURL.dart';
import '../Model/Appointment/AppointmentBook_JSON.dart';
import '../Model/Appointment/AppointmentSelection.dart';
import '../Model/Appointment/ClinicData_Model.dart';
import '../Model/Appointment/DepartmentData_Model.dart';
import '../Model/Appointment/ServiceList_Model.dart';
import '../Model/Appointment/TimeSlotModel.dart';
import '../Model/Login/Login_API.dart';
import '../Model/Profile/ProfileAPI.dart';
import 'Appointment_screen.dart';

class bookappointment extends StatefulWidget {
  bookappointment(
      {Key? key,
      required this.doctorvalue,
      required this.FacilityID,
      required this.FacilityName,
      required this.DepartmentName,
      required this.DepartmentID,
      required this.ClinicName,
      required this.ClinicID})
      : super(key: key);
  Categories doctorvalue;
  String FacilityID;
  String FacilityName;
  String DepartmentName;
  String DepartmentID;
  String ClinicName;
  String ClinicID;

  // HospitalData hospitalvalue;

  @override
  bookappointmentState createState() => bookappointmentState();
}

class bookappointmentState extends State<bookappointment> {
  List<DepartmentDetails> listing_Department_List = [];

  List<AllMainClinicDetails> listing_Clinic_List = [];

  List<ServiceAllListing> Servicelist = [
    ServiceAllListing.fromJson({
      "serviceName": "-Select-",
      "serviceTypeId": "00013",
      "serviceGroupId": "0",
      "serviceInformation": {"serviceId": "00"}
    })
  ];
  late ServiceAllListing _selection_service_data = Servicelist[0];

  List<TimeSlotModel> TimeSlotDetails = [];
  int SlotDuration = 15;
  String SelectedEndTime = '';

  List<AppointmentCategory> AppointmentCategorylist = [
    AppointmentCategory.fromJson({
      "displayName": "Category",
      "name": "00013",
      "id": "0",
      "extraParam": "o",
    })
  ];
  List MorningSlot = [];
  String availMorningSlot = "Circle";

  List EveningSlot = [];
  String availEveningSlot = "Circle";

  late AppointmentCategory AppointmentCategoryvalue;
  int selectedCard = -1;
  int month = DateTime.now().month;
  int year = DateTime.now().year;
  String currentmonth = '';
  String SelectedMonth = '';
  String SelectedMonthDisplay = '';
  String DateSlotSelection = '';
  String SelectedTime = '';
  final isSelected = <bool>[false, false, false];
  int selectedslot = 0;

  String mainServiceID = "";

  //DateTime(2021,11,1)
  DateTime startDate = DateTime.now();
  DateTime _selectedValue = DateTime.now();
  String PatientID = '';

  String str_Email = '';
  String str_Name = '';
  String str_PhoneNo = '';

  // Future<String> serviceData() async {
  //   String AccessToken = await Utility.readAccessToken('Token');
  //   // print(AllURL.MainURL +
  //   //     AllURL.appointment_servicelist +
  //   //     'facilityid=${widget.hospitalvalue.id.toString()}&serviceMasterType=Consultations&serviceType=service');
  //   Map<String, String> headersMain = {
  //     "Content-Type": "application/json",
  //     "Accept": "application/json",
  //     "Authorization": "Bearer $AccessToken",
  //   };
  //   var res = await http.get(
  //       Uri.parse(AllURL.MainURL +
  //           AllURL.appointment_servicelist +
  //           'facilityid=${""}&serviceMasterType=Consultations&serviceType=service'),
  //       headers: headersMain);
  //   log('Service condition : ${res.statusCode}');
  //   var resBody = json.decode(res.body);
  //   // log('Service condition : ${res.body}');
  //   setState(() {
  //     AllServiceList doctorList = new AllServiceList.fromJson(resBody);
  //     Servicelist = doctorList.designAll;
  //
  //     _selection_service_data = Servicelist[0];
  //
  //     print('All Doctor ${Servicelist.length}');
  //   });
  //   return "Sucess";
  // }

  Future<String> getAppointmentCategoryata() async {
    String AccessToken = await Utility.readAccessToken('Token');
    Map<String, String> headersMain = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };
    var res = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.appointment_category +
            'Filter=Status%20eq%20%27ACTIVE%27&select=name,id,displayName,isDefault'),
        headers: headersMain);
    var resBody = json.decode(res.body);
    setState(() {
      var tempdata = resBody['value'];
      AppointmentCategorylist = [];
      for (int i = 0; i < tempdata.length; i++) {
        AppointmentCategorylist.add(AppointmentCategory.fromJson(tempdata[i]));
      }

      for (int j = 0; j < AppointmentCategorylist.length; j++) {
        if (AppointmentCategorylist[j].displayName == 'Normal') {
          AppointmentCategoryvalue = AppointmentCategorylist[j];
          return;
        } else {
          AppointmentCategoryvalue = AppointmentCategorylist[0];
        }
      }
    });
    return "Sucess";
  }

  void GetAppointmentService(String FacilityID) async {
    String AccessToken = await Utility.readAccessToken('Token');
    API_AppointmentSelection()
        .getEmployeeService(AccessToken, FacilityID)
        .then((mainResponse) {
      setState(() {
        var resBody = json.decode(mainResponse);

        AllServiceList doctorList = new AllServiceList.fromJson(resBody);
        Servicelist = [];
        Servicelist = [
          ServiceAllListing.fromJson({
            "serviceName": "-Select-",
            "serviceTypeId": "00013",
            "serviceGroupId": "0",
            "serviceInformation": {"serviceId": "00"}
          })
        ];
        Servicelist = doctorList.designAll;

        bool serviceDetails = false;
        for (int i = 0; i < Servicelist.length; i++) {
          if (Servicelist[i].serviceName == "New case (In-Person)") {
            _selection_service_data = Servicelist[i];
            mainServiceID = _selection_service_data.id;
            serviceDetails = true;
            break;
          }
        }
        if (serviceDetails) {
        } else {
          _selection_service_data = Servicelist[0];
          mainServiceID = _selection_service_data.id;
        }

        print('All Doctor mainServiceID ${mainServiceID}');

        AllSlotsDetails(DateSlotSelection, widget.doctorvalue.id.toString(),
            widget.FacilityID, widget.DepartmentID, widget.ClinicID);
      });
    });
  }

  Future<String> BookAppointmwnt(Map Data) async {
    String AccessToken = await Utility.readAccessToken('Token');
    Map<String, String> headersMain = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };
    var res = await http.post(
        Uri.parse(AllURL.MainURL + AllURL.BookAppointment),
        headers: headersMain,
        body: jsonEncode(Data));
    var resBody = json.decode(res.body);
    print(res.statusCode);
    log('Book list : ${res.body}');
    if (res.statusCode == 200) {
      // Utility.showAlert(context, 'Book Appointment Sucessfully', 'Sucess');
      log('Patient list : ${json.decode(res.body)}');
      Utility.WholeAppSnackbar(context, 'Book Appointment Sucessfully');
      Navigator.of(context).pop();
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => appointmentmanagement(TabCheck: false)),
      );
      return "Sucess";
    } else {
      // if (resBody['errors'].length != 0) {
      //   Map validation = resBody['errors'][0];
      //   Utility.showAlert(context, validation['error'], 'Sucess');
      // }
      Utility.WholeAppSnackbar(context, 'Booking slot is not available');
      return "Fail";
    }
  }

  // void DepartmentList() async {
  //   String AccessToken = await Utility.readAccessToken('Token');
  //   String HospitalID = "";
  //   PatientID = await Utility.readAccessToken('PatientID');
  //
  //   API_AppointmentSelection()
  //       .DepartmentList_Appointment(AccessToken, HospitalID)
  //       .then((Response) async {
  //     setState(() {
  //       var tempdata = Response['value'];
  //
  //       if (tempdata.length > 0) {
  //         for (int i = 0; i < tempdata.length; i++) {
  //           listing_Department_List
  //               .add(DepartmentDetails.fromJson(tempdata[i]));
  //         }
  //         DepartmentID = listing_Department_List[0].id;
  //         ClinicLocationList(DepartmentID);
  //       }
  //     });
  //   });
  // }
  //
  // void ClinicLocationList(String DepartmentID) async {
  //   String AccessToken = await Utility.readAccessToken('Token');
  //
  //   API_AppointmentSelection()
  //       .ClinicLocationList(AccessToken, DepartmentID)
  //       .then((Response) async {
  //     setState(() {
  //       var tempdata = Response['value'];
  //
  //       if (tempdata.length > 0) {
  //         for (int i = 0; i < tempdata.length; i++) {
  //           listing_Clinic_List.add(AllMainClinicDetails.fromJson(tempdata[i]));
  //         }
  //         ClinicID = listing_Clinic_List[0].clinicId;
  //
  //         AllSlotsDetails(DateSlotSelection, widget.doctorvalue.id.toString(),
  //             "", DepartmentID, ClinicID);
  //       }
  //     });
  //   });
  // }

  void ProfilePage() async {
    String AccessToken = await Utility.readAccessToken('Token');
    PatientID = await Utility.readAccessToken('PatientID');
    Map<String, dynamic> decodedToken = JwtDecoder.decode(AccessToken);
    String mainPatientID = PatientID;
    API_Profile()
        .ProfilePageDisplay(AccessToken, mainPatientID)
        .then((Response) {
      setState(() {
        // str_Email = Response['patregi']['firstName'];
        String FName, Lname;
        FName = Response['patregi']['firstName'];
        Lname = Response['patregi']['lastName'];
        str_Name = FName + " " + Lname;
        print("Main Name ${str_Name}");
        // str_PhoneNo = Response['phone'];
      });
    });
  }

  void AllSlotsDetails(String AppointmentDate, String DoctorID,
      String FacilityID, String DepartmentID, String clinicID) async {
    String AccessToken = await Utility.readAccessToken('Token');

    API_AppointmentSelection()
        .AppointmentTimeSlot(AccessToken, AppointmentDate, DoctorID,
            DepartmentID, FacilityID, clinicID)
        .then((Response) {
      setState(() {
        log("All Time Slot Here ${Response}");

        if (Response == "Error") {
          availMorningSlot = "No Slot Available";
          availEveningSlot = "No Slot Available";
        } else {
          availMorningSlot = "";
          availEveningSlot = "";
          AllTimeSlotDetails doctorList =
              new AllTimeSlotDetails.fromJson(Response);
          TimeSlotDetails = doctorList.designAll;
          SlotDuration = int.parse(doctorList.slotDuration);
          print("Slot Duration ${SlotDuration}");

          MorningSlot = [];
          EveningSlot = [];

          for (int i = 0; i < TimeSlotDetails.length; i++) {
            if (TimeSlotDetails[i].isMorning) {
              MorningSlot.add(TimeSlotDetails[i].slotimes);
            } else {
              EveningSlot.add(TimeSlotDetails[i].slotimes);
            }
          }
          log("All Morning Here ${MorningSlot.length}");

          if (MorningSlot.length > 0) {
            availMorningSlot = "";
          } else {
            availMorningSlot = "No Slot Available";
          }

          if (EveningSlot.length > 0) {
            availEveningSlot = "";
          } else {
            availEveningSlot = "No Slot Available";
          }
        }

        // if (Response == "Error") {
        //   availMorningSlot = "No Slot Available";
        //   availEveningSlot = "No Slot Available";
        // } else {
        //   availMorningSlot = "";
        //   availEveningSlot = "";
        //   AllTimeSlotDetails doctorList =
        //       new AllTimeSlotDetails.fromJson(Response);
        //   TimeSlotDetails = doctorList.designAll;
        //
        //   print(TimeSlotDetails.length);
        //
        //   MorningSlot = [];
        //   EveningSlot = [];
        //
        //   for (int i = 0; i < TimeSlotDetails.length; i++) {
        //     if (TimeSlotDetails[i].isMorning) {
        //       MorningSlot.add(TimeSlotDetails[i].slotimes);
        //     } else {
        //       EveningSlot.add(TimeSlotDetails[i].slotimes);
        //     }
        //   }
        //   log("All Morning Here ${MorningSlot.length}");
        //
        //   if (MorningSlot.length > 0) {
        //     availMorningSlot = "";
        //   } else {
        //     availMorningSlot = "No Slot Available";
        //   }
        //
        //   if (EveningSlot.length > 0) {
        //     availEveningSlot = "";
        //   } else {
        //     availEveningSlot = "No Slot Available";
        //   }
        // }
      });
    });
  }

  @override
  void initState() {
    super.initState();
    AppointmentCategoryvalue = AppointmentCategorylist[0];
    DateFormat formatter = DateFormat('MMMM yyyy');
    currentmonth = formatter.format(DateTime.now());

    var now = new DateTime.now();
    var dateFormate = new DateFormat('yyyy-MM-dd');
    SelectedMonth = dateFormate.format(now);

    DateFormat monthSelection = DateFormat('dd MMM yyyy');
    SelectedMonthDisplay = monthSelection.format(now);

    DateFormat slotDate = DateFormat('MM-dd-yyyy');
    DateSlotSelection = slotDate.format(now);

    ProfilePage();
    // DepartmentList();
    getAppointmentCategoryata();
    HospitalName_To_AllID();
    GetAppointmentService(widget.FacilityID);
  }

  int daysInMonth(DateTime date) {
    var firstDayThisMonth = new DateTime(date.year, date.month, date.day);
    var firstDayNextMonth = new DateTime(firstDayThisMonth.year,
        firstDayThisMonth.month + 1, firstDayThisMonth.day);
    return firstDayNextMonth.difference(firstDayThisMonth).inDays;
  }

  void BookAppointmentJSON(
      String AppointmentCategoryID,
      String ServiceID,
      String FacilityID,
      String DepartmentID,
      String doctorID,
      String clinicID,
      String patientComplain,
      String DateSelection,
      String TimeSelection,
      String EndTime) async {

    PatientID = await Utility.readAccessToken('PatientID');
    Map finalBodyMap = AppointmentBookJSON.BookAppointmentJSONCreate(
        AppointmentCategoryID,
        ServiceID,
        FacilityID,
        DepartmentID,
        clinicID,
        PatientID,
        doctorID,
        DateSelection,
        TimeSelection,
        patientComplain,
        EndTime);

    log("Main Details ${finalBodyMap}");
    BookAppointmwnt(finalBodyMap);
    // BookAppointmentData(finalBodyMap);
  }

  String AddSomeMinutes(String selectionTime, int TimeDuration) {
    String someTime = "";

    var inputFormat = DateFormat('HH:mm');
    var inputTime = inputFormat.parse(selectionTime);

    var disInputFormat = DateFormat('HH:mm');
    var inputDisplayTime =
        disInputFormat.parse(selectionTime); // <-- dd/MM 24H format

    var outputFormat = DateFormat('hh:mm aa');
    String outputDate = outputFormat.format(inputDisplayTime);
    log("Time Output ${outputDate}");
    // displaySelectedTime = outputDate;

    var addMinutes = inputTime.add(new Duration(minutes: TimeDuration));

    DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");

    DateTime dateTime = dateFormat.parse(addMinutes.toString());

    someTime = DateFormat('HH:mm').format(dateTime);

    print("Main Details Add Time ${someTime}");

    return someTime;
  }

  void HospitalName_To_AllID() async {
    // String HospitalID = await Utility.readAccessToken('TenantID');
    // API_LoginAPI().MainHospitalName(HospitalID).then((Response) async {
    //   setState(() {
    //     String FinalHospitalName = Response['name'];
    //   });
    // });
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: Utility.TopToolBar(
            Utility.translate('book_appoint'),
            Utility.getDeviceType() == "1" ? 20 : 30,
            Utility.PutDarkBlueColor(),
            context),
        //bottomN
        body: SingleChildScrollView(
          child: Container(
            padding: Utility.getDeviceType() == '1'
                ? EdgeInsets.fromLTRB(10, 5, 10, 5)
                : EdgeInsets.fromLTRB(20, 5, 20, 5),
            child: Column(
              children: [
                //Doctor Information
                DoctorInformation(),
                SizedBox(
                  height: Utility.getDeviceType() == '1' ? 10 : 20,
                ),
                //Dropdown
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    new Flexible(
                      child: Textfieldcategory(),
                    ),
                    SizedBox(
                      width: Utility.getDeviceType() == '1' ? 15 : 30,
                    ),
                    new Flexible(
                      child: TextfieldSerice(),
                    ),
                  ],
                ),
                //slots
                SizedBox(
                  height: Utility.getDeviceType() == '1' ? 10 : 20,
                ),
                Row(
                  children: [
                    Expanded(
                      //width: double.infinity,
                      child: Container(
                        child: Text(
                          Utility.translate('available_slots'),
                          style: TextStyle(
                              color: Utility.PutDarkBlueColor(),
                              fontFamily: 'Poppins_Medium',
                              fontSize:
                                  Utility.getDeviceType() == '1' ? 18 : 25),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        child: Row(children: [
                          Material(
                            child: InkWell(
                              onTap: () {
                                if (month == 0 || month == -1) {
                                  month = 1;
                                  year--;
                                } else {
                                  month = month - 1;
                                }
                                int currenmonth = DateTime.now().month;
                                int currentyear = DateTime.now().year;

                                if (currenmonth >= month &&
                                    currentyear == year) {
                                  month = currenmonth;
                                  setState(() {
                                    startDate = DateTime.now();
                                    _selectedValue = DateTime.now();
                                    DateFormat formatter =
                                        DateFormat('MMMM yyyy');
                                    currentmonth = formatter.format(startDate);
                                  });
                                } else {
                                  setState(() {
                                    startDate = DateTime(year, month, 1);
                                    _selectedValue = DateTime(year, month, 1);
                                    DateFormat formatter =
                                        DateFormat('MMMM yyyy');
                                    currentmonth = formatter.format(startDate);
                                  });
                                }
                              },
                              // button pressed
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    Icons.keyboard_arrow_left,
                                    size: Utility.getDeviceType() == '1'
                                        ? 35
                                        : 45,
                                    color: Utility.PutDarkBlueColor(),
                                  ), // icon
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: Utility.getDeviceType() == '1' ? 120 : 200,
                            child: Text(
                              currentmonth,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize:
                                      Utility.getDeviceType() == '1' ? 14 : 22,
                                  fontFamily: 'Poppins_Medium',
                                  color: Utility.PutDarkBlueColor()),
                            ),
                          ),
                          Material(
                            child: InkWell(
                              onTap: () {
                                if (month == 12) {
                                  month = 1;
                                  year++;
                                } else {
                                  month = month + 1;
                                }
                                setState(() {
                                  startDate = DateTime(year, month, 1);
                                  _selectedValue = DateTime(year, month, 1);
                                  DateFormat formatter =
                                      DateFormat('MMMM yyyy');
                                  currentmonth = formatter.format(startDate);
                                });
                              },
                              // button pressed
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    Icons.keyboard_arrow_right,
                                    size: Utility.getDeviceType() == '1'
                                        ? 35
                                        : 45,
                                    color: Utility.PutDarkBlueColor(),
                                  ), // icon
                                ],
                              ),
                            ),
                          ),
                        ]),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: Utility.getDeviceType() == '1' ? 10 : 20,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    DatePickerTimeline(
                        key: _scaffoldKey,
                        currentDate: _selectedValue,
                        selectedDate: startDate,
                        onDateChange: (date) {
                          //  setState(() {
                          _selectedValue = date;
                          DateFormat formatter = DateFormat('dd MMM yyyy');
                          SelectedMonthDisplay =
                              formatter.format(_selectedValue);

                          var dateFormate = new DateFormat('yyyy-MM-dd');
                          SelectedMonth = dateFormate.format(_selectedValue);

                          DateFormat slotDate = DateFormat('MM-dd-yyyy');
                          DateSlotSelection = slotDate.format(_selectedValue);

                          AllSlotsDetails(
                              DateSlotSelection,
                              widget.doctorvalue.id.toString(),
                              widget.FacilityID,
                              widget.DepartmentID,
                              widget.ClinicID);

                          selectedCard = -1;
                          SelectedTime = '';

                          if (mounted) {
                            setState(() {
                              availMorningSlot = "Circle";
                              availEveningSlot = "Circle";

                              MorningSlot = [];
                              EveningSlot = [];
                            });
                          } else {
                            availMorningSlot = "Circle";
                            availEveningSlot = "Circle";

                            MorningSlot = [];
                            EveningSlot = [];
                          }
                          // });
                        }),
                  ],
                ),
                SizedBox(
                  height: Utility.getDeviceType() == '1' ? 10 : 20,
                ),
                Container(
                  height: Utility.getDeviceType() == '1' ? 40 : 60,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    border: Border.all(
                      color: Utility.PutDarkBlueColor(),
                    ),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        width: Utility.getDeviceType() == '1'
                            ? (MediaQuery.of(context).size.width - 22) / 2
                            : (MediaQuery.of(context).size.width - 65) / 2,
                        child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              selectedslot = 0;
                              selectedCard = -1;
                            });
                          },
                          child: Text(Utility.translate('morning'),
                              style: TextStyle(
                                color: selectedslot == 0
                                    ? Colors.white
                                    : Utility.PutDarkBlueColor(),
                              )),
                          style: ElevatedButton.styleFrom(
                            backgroundColor: selectedslot == 0
                                ? Utility.PutDarkBlueColor()
                                : Colors.white,
                          ),
                        ),
                      ),
                      Container(
                        width: (MediaQuery.of(context).size.width - 22) / 2,
                        child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              selectedslot = 1;
                              selectedCard = -1;
                            });
                          },
                          child: Text(Utility.translate('evening'),
                              style: TextStyle(
                                color: selectedslot == 1
                                    ? Colors.white
                                    : Utility.PutDarkBlueColor(),
                              )),
                          style: ElevatedButton.styleFrom(
                            backgroundColor: selectedslot == 1
                                ? Utility.PutDarkBlueColor()
                                : Colors.white,
                          ),
                          // child: Text(Utility.translate('evening')),
                          // style: ButtonStyle(
                          //     backgroundColor: selectedslot == 1
                          //         ? MaterialStateProperty.all(
                          //             Utility.PutDarkBlueColor())
                          //         : MaterialStateProperty.all(Colors.white),
                          //     textStyle: MaterialStateProperty.all(TextStyle(
                          //       fontSize:
                          //           Utility.getDeviceType() == '1' ? 16 : 28,
                          //       fontFamily: 'Poppins_Regular',
                          //       color: Colors.green,
                          //     ))),
                          // color: selectedslot == 1
                          //     ? Utility.PutDarkBlueColor()
                          //     : Colors.white,
                          // child: Padding(
                          //   padding: EdgeInsets.all(0),
                          //   child: Container(
                          //     alignment: Alignment.center,
                          //     child: Text(
                          //       Utility.translate('evening'),
                          //       style: TextStyle(
                          //         fontSize:
                          //             Utility.getDeviceType() == '1' ? 16 : 28,
                          //         fontFamily: 'Poppins_Regular',
                          //         color: selectedslot == 1
                          //             ? Colors.white
                          //             : Utility.PutDarkBlueColor(),
                          //       ),
                          //     ),
                          //   ),
                          // ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: Utility.getDeviceType() == '1' ? 10 : 25,
                ),
                selectedslot == 0 ? Timeslot() : Container(),
                selectedslot == 1 ? Timeslot1() : Container(),
                selectedCard != -1
                    ? Card(
                        elevation: 5,
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: RichText(
                            text: new TextSpan(
                              // Note: Styles for TextSpans must be explicitly defined.
                              // Child text spans will inherit styles from parent
                              style: new TextStyle(
                                fontSize: 14.0,
                                color: Colors.black,
                              ),
                              children: [
                                new TextSpan(
                                  text: Utility.translate('appointment_for'),
                                  style: TextStyle(
                                      fontSize: Utility.getDeviceType() == '1'
                                          ? 12
                                          : 22,
                                      fontFamily: 'Poppins_Medium',
                                      color: Utility.PutDarkBlueColor()),
                                ),
                                new TextSpan(
                                  text: " " + str_Name,
                                  style: TextStyle(
                                      fontSize: Utility.getDeviceType() == '1'
                                          ? 18
                                          : 28,
                                      fontFamily: 'Poppins_SemiBold',
                                      color: Utility.PutDarkBlueColor(),
                                      fontStyle: FontStyle.normal),
                                ),
                                new TextSpan(
                                  text: Utility.translate('on'),
                                  style: TextStyle(
                                      fontSize: Utility.getDeviceType() == '1'
                                          ? 12
                                          : 22,
                                      fontFamily: 'Poppins_Medium',
                                      color: Utility.PutDarkBlueColor(),
                                      fontStyle: FontStyle.normal),
                                ),
                                new TextSpan(
                                  text: SelectedMonthDisplay,
                                  style: TextStyle(
                                      fontSize: Utility.getDeviceType() == '1'
                                          ? 18
                                          : 25,
                                      fontFamily: 'Poppins_SemiBold',
                                      color: Utility.PutLightBlueColor(),
                                      fontStyle: FontStyle.normal),
                                ),
                                new TextSpan(
                                  text: Utility.translate('at'),
                                  style: TextStyle(
                                      fontSize: Utility.getDeviceType() == '1'
                                          ? 13
                                          : 23,
                                      fontFamily: 'Poppins_Medium',
                                      color: Utility.PutDarkBlueColor(),
                                      fontStyle: FontStyle.normal),
                                ),
                                new TextSpan(
                                  text: SelectedTime,
                                  style: TextStyle(
                                      fontSize: Utility.getDeviceType() == '1'
                                          ? 18
                                          : 28,
                                      fontFamily: 'Poppins_SemiBold',
                                      color: Utility.PutLightBlueColor(),
                                      fontStyle: FontStyle.normal),
                                ),
                                new TextSpan(
                                  text: Utility.translate('with'),
                                  style: TextStyle(
                                      fontSize: Utility.getDeviceType() == '1'
                                          ? 15
                                          : 25,
                                      fontFamily: 'Poppins_Medium',
                                      color: Utility.PutDarkBlueColor(),
                                      fontStyle: FontStyle.normal),
                                ),
                                new TextSpan(
                                  text: widget.doctorvalue.name.toString(),
                                  style: TextStyle(
                                      fontSize: Utility.getDeviceType() == '1'
                                          ? 18
                                          : 28,
                                      fontFamily: 'Poppins_SemiBold',
                                      color: Utility.PutDarkBlueColor(),
                                      fontStyle: FontStyle.normal),
                                ),
                                new TextSpan(
                                  text: "\n",
                                  style: TextStyle(
                                      fontSize: Utility.getDeviceType() == '1'
                                          ? 18
                                          : 28,
                                      fontFamily: 'Poppins_SemiBold',
                                      color: Utility.PutDarkBlueColor(),
                                      fontStyle: FontStyle.normal),
                                ),
                                WidgetSpan(
                                  child: Icon(Icons.location_on_sharp,
                                      color: Utility.PutDarkBlueColor(),
                                      size: Utility.getDeviceType() == '1'
                                          ? 25
                                          : 35),
                                ),
                                new TextSpan(
                                  text: "",
                                  style: TextStyle(
                                      fontSize: Utility.getDeviceType() == '1'
                                          ? 15
                                          : 25,
                                      fontFamily: 'Poppins_SemiBold',
                                      color: Utility.PutDarkBlueColor(),
                                      fontStyle: FontStyle.normal),
                                ),
                                new TextSpan(
                                  text: "\n",
                                  style: TextStyle(
                                      fontSize: Utility.getDeviceType() == '1'
                                          ? 18
                                          : 28,
                                      fontFamily: 'Poppins_SemiBold',
                                      color: Utility.PutDarkBlueColor(),
                                      fontStyle: FontStyle.normal),
                                ),
                                // new TextSpan(
                                //   text: "LL",
                                //   style: TextStyle(
                                //       fontSize: Utility.getDeviceType() == '1'
                                //           ? 18
                                //           : 28,
                                //       fontFamily: 'Poppins_SemiBold',
                                //       color: Utility.PutDarkBlueColor(),
                                //       fontStyle: FontStyle.normal),
                                // ),
                              ],
                            ),
                          ),
                        )
                        // child: Container(
                        //   padding: EdgeInsets.all(10),
                        //   height: Utility.getDeviceType() == '1' ? 155 : 200,
                        //   child: Column(
                        //     children: [
                        //       Align(
                        //         alignment: Alignment.topLeft,
                        //         child: Container(
                        //           child: Row(children: [
                        //             Text(
                        //               Utility.translate('appointment_for'),
                        //               style: TextStyle(
                        //                   fontSize:
                        //                       Utility.getDeviceType() == '1'
                        //                           ? 12
                        //                           : 22,
                        //                   fontFamily: 'Poppins_Medium',
                        //                   color: Utility.PutDarkBlueColor()),
                        //               maxLines: 1,
                        //             ),
                        //             Text(
                        //               " " +
                        //                   widget.hospitalvalue.name.toString(),
                        //               style: TextStyle(
                        //                   fontSize:
                        //                       Utility.getDeviceType() == '1'
                        //                           ? 15
                        //                           : 25,
                        //                   fontFamily: 'Poppins_SemiBold',
                        //                   color: Utility.PutDarkBlueColor(),
                        //                   fontStyle: FontStyle.normal),
                        //               maxLines: 1,
                        //             ),
                        //             Text(
                        //               Utility.translate('on'),
                        //               style: TextStyle(
                        //                   fontSize:
                        //                       Utility.getDeviceType() == '1'
                        //                           ? 12
                        //                           : 22,
                        //                   fontFamily: 'Poppins_Medium',
                        //                   color: Utility.PutDarkBlueColor()),
                        //             ),
                        //             Utility.getDeviceType() == '1'
                        //                 ? Container()
                        //                 : Expanded(
                        //                     child: Row(children: [
                        //                       Text(
                        //                         SelectedMonthDisplay,
                        //                         style: TextStyle(
                        //                           fontSize:
                        //                           Utility.getDeviceType() == '1'
                        //                               ? 15
                        //                               : 25,
                        //                           fontFamily: 'Poppins_SemiBold',
                        //                           color:
                        //                           Utility.PutLightBlueColor(),
                        //                         ),
                        //                         maxLines: 1,
                        //                       ),
                        //                       Text(
                        //                         Utility.translate('at'),
                        //                         style: TextStyle(
                        //                             fontSize:
                        //                                 Utility.getDeviceType() ==
                        //                                         '1'
                        //                                     ? 13
                        //                                     : 23,
                        //                             fontFamily:
                        //                                 'Poppins_Medium',
                        //                             color: Utility
                        //                                 .PutDarkBlueColor()),
                        //                       ),
                        //                       Text(
                        //                         SelectedTime,
                        //                         style: TextStyle(
                        //                             fontSize:
                        //                                 Utility.getDeviceType() ==
                        //                                         '1'
                        //                                     ? 15
                        //                                     : 25,
                        //                             fontFamily:
                        //                                 'Poppins_SemiBold',
                        //                             color: Utility
                        //                                 .PutLightBlueColor()),
                        //                       ),
                        //                       Text(
                        //                         Utility.translate('with'),
                        //                         style: TextStyle(
                        //                             fontSize:
                        //                                 Utility.getDeviceType() ==
                        //                                         '1'
                        //                                     ? 13
                        //                                     : 23,
                        //                             fontFamily:
                        //                                 'Poppins_Medium',
                        //                             color: Utility
                        //                                 .PutDarkBlueColor()),
                        //                       ),
                        //                       new Expanded(
                        //                         child: Text(
                        //                           widget.doctorvalue.name
                        //                               .toString(),
                        //                           style: TextStyle(
                        //                             fontSize:
                        //                                 Utility.getDeviceType() ==
                        //                                         '1'
                        //                                     ? 15
                        //                                     : 25,
                        //                             fontFamily:
                        //                                 'Poppins_SemiBold',
                        //                             color: Utility
                        //                                 .PutDarkBlueColor(),
                        //                           ),
                        //                         ),
                        //                       ),
                        //                     ]),
                        //                   ),
                        //           ]),
                        //         ),
                        //       ),
                        //       SizedBox(
                        //         height: 5,
                        //       ),
                        //       Utility.getDeviceType() == '1'
                        //           ? Align(
                        //               alignment: Alignment.topLeft,
                        //               child: Container(
                        //                 child: Row(children: [
                        //                   Text(
                        //                     Utility.translate('at'),
                        //                     style: TextStyle(
                        //                         fontSize:
                        //                             Utility.getDeviceType() ==
                        //                                     '1'
                        //                                 ? 13
                        //                                 : 23,
                        //                         fontFamily: 'Poppins_Medium',
                        //                         color:
                        //                             Utility.PutDarkBlueColor()),
                        //                   ),
                        //                   Text(
                        //                     SelectedTime,
                        //                     style: TextStyle(
                        //                         fontSize:
                        //                             Utility.getDeviceType() ==
                        //                                     '1'
                        //                                 ? 15
                        //                                 : 25,
                        //                         fontFamily: 'Poppins_SemiBold',
                        //                         color: Utility
                        //                             .PutLightBlueColor()),
                        //                   ),
                        //                   Text(
                        //                     Utility.translate('with'),
                        //                     style: TextStyle(
                        //                         fontSize:
                        //                             Utility.getDeviceType() ==
                        //                                     '1'
                        //                                 ? 13
                        //                                 : 23,
                        //                         fontFamily: 'Poppins_Medium',
                        //                         color:
                        //                             Utility.PutDarkBlueColor()),
                        //                   ),
                        //                   new Expanded(
                        //                     child: Text(
                        //                       widget.doctorvalue.name
                        //                           .toString(),
                        //                       style: TextStyle(
                        //                         fontSize:
                        //                             Utility.getDeviceType() ==
                        //                                     '1'
                        //                                 ? 15
                        //                                 : 25,
                        //                         fontFamily: 'Poppins_SemiBold',
                        //                         color:
                        //                             Utility.PutDarkBlueColor(),
                        //                       ),
                        //                     ),
                        //                   ),
                        //                 ]),
                        //               ),
                        //             )
                        //           : Container(),
                        //       SizedBox(
                        //         height: 7,
                        //       ),
                        //       Align(
                        //         alignment: Alignment.topLeft,
                        //         child: Container(
                        //           child: Row(children: [
                        //             Icon(
                        //               Icons.location_on_sharp,
                        //               size: Utility.getDeviceType() == '1'
                        //                   ? 25
                        //                   : 35,
                        //               color: Utility.PutDarkBlueColor(),
                        //             ),
                        //             Text(
                        //               widget.hospitalvalue.name.toString(),
                        //               style: TextStyle(
                        //                   fontSize:
                        //                       Utility.getDeviceType() == '1'
                        //                           ? 17
                        //                           : 27,
                        //                   fontFamily: 'Poppins_Medium',
                        //                   color: Utility.PutDarkBlueColor()),
                        //             ),
                        //           ]),
                        //         ),
                        //       ),
                        //       SizedBox(
                        //         height: 7,
                        //       ),
                        //       Align(
                        //         alignment: Alignment.topLeft,
                        //         child: Container(
                        //           child: Row(children: [
                        //             new Expanded(
                        //               child: Text(
                        //                 'A401 Ganesh plaza navrangpura Ahmedabad',
                        //                 style: TextStyle(
                        //                     fontSize:
                        //                         Utility.getDeviceType() == '1'
                        //                             ? 12
                        //                             : 22,
                        //                     fontFamily: 'Poppins_Medium',
                        //                     color: Utility.PutDarkBlueColor()),
                        //               ),
                        //             ),
                        //           ]),
                        //         ),
                        //       ),
                        //       SizedBox(
                        //         height: 5,
                        //       ),
                        //     ],
                        //   ),
                        // ),
                        )
                    : Container(),
                SizedBox(
                  height: Utility.getDeviceType() == '1' ? 15 : 25,
                ),
                Container(
                  // padding: EdgeInsets.all(15),
                  height: Utility.getDeviceType() == '1' ? 50 : 70,
                  width: MediaQuery.of(context).size.width - 50,
                  child: MaterialButton(
                    onPressed: () {
                      String categoryID =
                          AppointmentCategoryvalue.id.toString();
                      String serviceID = _selection_service_data.id;
                      String FacilityID = "";
                      String DoctorID = widget.doctorvalue.id.toString();
                      print(DoctorID);

                      if (SelectedTime == '') {
                        Utility.WholeAppSnackbar(
                            context, 'Please select Time of slot');
                      } else {
                        DateTime timeData =
                            DateFormat('hh:mm').parse(SelectedTime);
                        String FinalTime = SelectedTime;

                        SelectedEndTime =
                            AddSomeMinutes(SelectedTime, SlotDuration);

                        print(FinalTime);

                        var inputFormat = DateFormat('dd MMM yyyy');
                        var inputDate = inputFormat.parse(SelectedMonthDisplay);

                        var outputFormat = DateFormat('yyyy-MM-dd');
                        var outputDate = outputFormat.format(inputDate);
                        log("Date Output ${outputDate}");

                        BookAppointmentJSON(
                            categoryID,
                            serviceID,
                            widget.FacilityID,
                            widget.DepartmentID,
                            DoctorID,
                            widget.ClinicID,
                            "",
                            outputDate,
                            FinalTime,
                            SelectedEndTime);

                        // AppointmentSaveAPICall(
                        //     categoryID,
                        //     serviceID,
                        //     FinalTime,
                        //     DepartmentID,
                        //     ClinicID,
                        //     FacilityID,
                        //     PatientID,
                        //     SelectedMonth,
                        //     DoctorID);
                      }
                    },
                    child: Ink(
                      width: Utility.getDeviceType() == '1'
                          ? double.infinity
                          : 400,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [Utility.gradiant, Utility.gradiant1],
                        ),
                        borderRadius: BorderRadius.circular(
                            Utility.getDeviceType() == '1' ? 25 : 35),
                      ),
                      child: Container(
                        constraints: BoxConstraints(
                            maxWidth: MediaQuery.of(context).size.width - 50,
                            minHeight: 50.0),
                        alignment: Alignment.center,
                        child: Text(
                          Utility.translate('request_appointentment'),
                          style: TextStyle(
                              fontSize:
                                  Utility.getDeviceType() == '1' ? 18 : 28,
                              fontFamily: 'Poppins_Medium',
                              color: Colors.white),
                        ),
                      ),
                    ),
                    splashColor: Colors.black12,
                    padding: EdgeInsets.all(0),
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // // Appointment Book Save API Call
  // Future<void> AppointmentSaveAPICall(
  //     String CategoryID,
  //     String ServiceID,
  //     String StartTime,
  //     String DepartmentID,
  //     String ClinicID,
  //     String FacilityID,
  //     String PatientID,
  //     String SelectionDate,
  //     String DoctorID) async {
  //   Map mainData = AppointmentBookJSON.OLD_BookAppointmentJSON(
  //       CategoryID,
  //       ServiceID,
  //       StartTime,
  //       DepartmentID,
  //       ClinicID,
  //       FacilityID,
  //       PatientID,
  //       SelectionDate,
  //       DoctorID);
  //
  //   //encode Map to JSON
  //   var body = json.encode(mainData);
  //   log(body);
  //
  //   BookAppointmwnt(mainData);
  // }

  Widget Timeslot() {
    var size = MediaQuery.of(context).size;
    final double itemWidth = size.width / 4;
    print("Here come Slot ${availMorningSlot}");
    return availMorningSlot == "Circle"
        ? CircularProgressIndicator()
        : availMorningSlot == "No Slot Available"
            ? Container(
                height: 50,
                child: Center(
                  child: Text("No Booking Slot Available"),
                ),
              )
            : Container(
                //padding: EdgeInsets.all(10),
                child: GridView.builder(
                    shrinkWrap: true,
                    controller: new ScrollController(keepScrollOffset: false),
                    itemCount: MorningSlot.length,
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 20),
                    gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: Utility.getDeviceType() == '1' ? 4 : 5,
                      crossAxisSpacing: Utility.getDeviceType() == '1' ? 8 : 12,
                      mainAxisSpacing: Utility.getDeviceType() == '1' ? 8 : 12,
                      childAspectRatio:
                          Utility.getDeviceType() == '1' ? 2.5 : 3.0,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            selectedCard = index;
                            SelectedTime = MorningSlot[index];
                          });
                        },
                        child: Container(
                          height: 50,
                          decoration: new BoxDecoration(
                            color: selectedCard == index
                                ? Utility.PutDarkBlueColor()
                                : Utility.PutLightBlueColor(),
                            borderRadius: BorderRadius.circular(
                                Utility.getDeviceType() == '1' ? 25 : 30),
                          ),
                          child: Center(
                            child: Text(
                              MorningSlot[index].toString(),
                              style: TextStyle(
                                  fontSize:
                                      Utility.getDeviceType() == '1' ? 13 : 25,
                                  fontFamily: 'Poppins_Regular',
                                  color: Colors.white),
                            ),
                          ),
                        ),
                      );
                    }),
              );
  }

  Widget Timeslot1() {
    var size = MediaQuery.of(context).size;
    final double itemWidth = size.width / 4;
    return availEveningSlot == "Circle"
        ? CircularProgressIndicator()
        : availEveningSlot == "No Slot Available"
            ? Container(
                height: 50,
                child: Center(
                  child: Text("No Booking Slot Available"),
                ),
              )
            : Container(
                //padding: EdgeInsets.all(10),
                child: GridView.builder(
                    shrinkWrap: true,
                    controller: new ScrollController(keepScrollOffset: false),
                    itemCount: EveningSlot.length,
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 20),
                    gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: Utility.getDeviceType() == '1' ? 4 : 5,
                      crossAxisSpacing: Utility.getDeviceType() == '1' ? 8 : 12,
                      mainAxisSpacing: Utility.getDeviceType() == '1' ? 8 : 12,
                      childAspectRatio:
                          Utility.getDeviceType() == '1' ? 2.5 : 3.0,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            selectedCard = index;
                            SelectedTime = EveningSlot[index];
                          });
                        },
                        child: Container(
                          height: 50,
                          decoration: new BoxDecoration(
                            color: selectedCard == index
                                ? Utility.PutDarkBlueColor()
                                : Utility.PutLightBlueColor(),
                            borderRadius: BorderRadius.circular(
                                Utility.getDeviceType() == '1' ? 25 : 30),
                          ),
                          child: Center(
                            child: Text(
                              EveningSlot[index].toString(),
                              style: TextStyle(
                                  fontSize:
                                      Utility.getDeviceType() == '1' ? 13 : 25,
                                  fontFamily: 'Poppins_Regular',
                                  color: Colors.white),
                            ),
                          ),
                        ),
                      );
                    }),
              );
  }

  Widget DoctorInformation() {
    return Card(
      color: Colors.white,
      elevation: 5,
      child: Container(
        padding: EdgeInsets.all(15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(width: Utility.getDeviceType() == '1' ? 0 : 16),
            Flexible(
              flex: Utility.getDeviceType() == '1' ? 1 : 0,
              child: Container(
                width: Utility.getDeviceType() == '1' ? 80 : 130,
                height: Utility.getDeviceType() == '1' ? 80 : 130,
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.pinkAccent,
                  image: new DecorationImage(
                      fit: BoxFit.fill,
                      image: new NetworkImage(
                          "https://icons-for-free.com/download-icon-boy+man+person+user+woman+icon-1320085967769585303_512.png")),
                ),
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(),
                  Text(
                    '${widget.doctorvalue.name}',
                    style: TextStyle(
                        fontFamily: 'Poppins_Medium',
                        fontSize: Utility.getDeviceType() == '1' ? 15 : 25,
                        color: Utility.PutDarkBlueColor()),
                  ),
                  Text(
                    widget.FacilityName,
                    style: TextStyle(
                        fontFamily: 'Poppins_Regular',
                        fontSize: Utility.getDeviceType() == '1' ? 13 : 23,
                        color: Utility.PutLightBlueColor()),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget Textfieldcategory() {
    return FormField<AppointmentCategory>(
      builder: (FormFieldState<AppointmentCategory> state) {
        return Container(
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              side: BorderSide(width: 1.0, style: BorderStyle.solid),
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
            ),
          ),
          padding: EdgeInsets.only(left: 10),
          child: DropdownButton<AppointmentCategory>(
            value: AppointmentCategoryvalue,
            onChanged: (AppointmentCategory? newValue) {
              setState(() {
                // _selectGender = newValue!;
                // print('Gender ' + _selectGender);
                if (newValue != null) {
                  setState(() {
                    AppointmentCategoryvalue = newValue;
                  });
                }
              });
            },
            isExpanded: true,
            underline: Container(),
            icon: Padding(
              //Icon at tail, arrow bottom is default icon
              padding: EdgeInsets.only(left: 5, right: 5),
              child: Image.asset(
                'Images/down_arrow.png',
                width: 25,
                height: 25,
                color: Color(0XffE0E0E0),
              ),
            ),
            items: AppointmentCategorylist.map<
                    DropdownMenuItem<AppointmentCategory>>(
                (AppointmentCategory value) {
              return DropdownMenuItem<AppointmentCategory>(
                value: value,
                child: Container(
                  child: Text(
                    value.displayName.toString(),
                    style: TextStyle(
                      color: Utility.PutDarkBlueColor(),
                      fontSize: 13,
                    ),
                  ),
                ),
              );
            }).toList(),
          ),
        );
      },
    );
  }

  Widget TextfieldSerice() {
    return FormField<Service>(
      builder: (FormFieldState<Service> state) {
        return Container(
          decoration: ShapeDecoration(
            shape: RoundedRectangleBorder(
              side: BorderSide(width: 1.0, style: BorderStyle.solid),
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
            ),
          ),
          padding: EdgeInsets.only(left: 10),
          child: DropdownButton<ServiceAllListing>(
            value: _selection_service_data,
            onChanged: (ServiceAllListing? newValue) {
              setState(() {
                // _selectGender = newValue!;
                // print('Gender ' + _selectGender);
                if (newValue != null) {
                  setState(() {
                    _selection_service_data = newValue;
                  });
                }
              });
            },
            isExpanded: true,
            underline: Container(),
            icon: Padding(
              //Icon at tail, arrow bottom is default icon
              padding: EdgeInsets.only(left: 5, right: 5),
              child: Image.asset(
                'Images/down_arrow.png',
                width: 25,
                height: 25,
                color: Color(0XffE0E0E0),
              ),
            ),
            items: Servicelist.map<DropdownMenuItem<ServiceAllListing>>(
                (ServiceAllListing value) {
              return DropdownMenuItem<ServiceAllListing>(
                value: value,
                child: Container(
                  child: Text(
                    value.displayName,
                    style: TextStyle(
                        color: Utility.PutDarkBlueColor(), fontSize: 13),
                  ),
                ),
              );
            }).toList(),
          ),
        );
      },
    );
  }
}
