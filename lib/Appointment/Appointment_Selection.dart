import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:softcliniccare/Appointment/Bookappointment_screen.dart';
import 'package:softcliniccare/Helper/const_data.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'package:http/http.dart' as http;

import '../Helper/AllURL.dart';
import '../Helper/Database/sqflite_helper.dart';
import '../Model/Appointment/AppointmentSelection.dart';
import '../Model/Appointment/ClinicData_Model.dart';
import '../Model/Appointment/DepartmentData_Model.dart';
import '../Model/Appointment/HostpitalDetails.dart';

class Appointment_Selection extends StatefulWidget {
  @override
  AppointmentMang createState() => AppointmentMang();
}

class DoctorSpecialization {
  const DoctorSpecialization(
      {required this.titleName,
      required this.iconName,
      required this.ID,
      required this.finalColor});

  final int ID;
  final String titleName;
  final String iconName;
  final Color finalColor;
}

const List<DoctorSpecialization> spec_Choices = const <DoctorSpecialization>[
  const DoctorSpecialization(
      titleName: 'Cardiology',
      iconName: 'Images/heartbeats.png',
      ID: 1,
      finalColor: Color(0xffFFE0E3)),
  const DoctorSpecialization(
      titleName: 'Dentist',
      iconName: 'Images/dentist_icon.png',
      ID: 2,
      finalColor: Color(0xffDBEFFF)),
  const DoctorSpecialization(
      titleName: 'Neuro',
      iconName: 'Images/brain_neuro.png',
      ID: 3,
      finalColor: Color(0xffFFEAE2)),
  const DoctorSpecialization(
      titleName: 'Ayurvedic',
      iconName: 'Images/ayurvedic_icon.png',
      ID: 4,
      finalColor: Color(0xffDFF1FE)),
  const DoctorSpecialization(
      titleName: 'Urology',
      iconName: 'Images/urology_icon.png',
      ID: 5,
      finalColor: Color(0xffF4E0FE)),
  const DoctorSpecialization(
      titleName: 'Psychiatrist',
      iconName: 'Images/psy_icon.png',
      ID: 6,
      finalColor: Color(0xffE1E3E8)),
  const DoctorSpecialization(
      titleName: 'Physiotherapy',
      iconName: 'Images/phy_icon.png',
      ID: 7,
      finalColor: Color(0xffF4F9FC)),
  const DoctorSpecialization(
      titleName: 'Pathology',
      iconName: 'Images/pathology_icon.png',
      ID: 8,
      finalColor: Color(0xffF8F8F8)),
  const DoctorSpecialization(
      titleName: 'Opthalmology',
      iconName: 'Images/opatha_logy_icon.png',
      ID: 9,
      finalColor: Color(0xffC3C0FF)),
  const DoctorSpecialization(
      titleName: 'Oncology',
      iconName: 'Images/oncology_icon.png',
      ID: 10,
      finalColor: Color(0xffFAFCE7)),
  const DoctorSpecialization(
      titleName: 'Obstetrics',
      iconName: 'Images/obstetrical.png',
      ID: 11,
      finalColor: Color(0xffFFE4DA)),
  const DoctorSpecialization(
      titleName: 'Optometry',
      iconName: 'Images/optometry.png',
      ID: 12,
      finalColor: Color(0xffFDE8FC)),
  const DoctorSpecialization(
      titleName: 'Orthopedic',
      iconName: 'Images/orthopedics.png',
      ID: 13,
      finalColor: Color(0xff98DCF1)),
  const DoctorSpecialization(
      titleName: 'Anesthesiology',
      iconName: 'Images/doctor.png',
      ID: 14,
      finalColor: Color(0xffD7F2D7)),
  const DoctorSpecialization(
      titleName: 'Nephrology',
      iconName: 'Images/nephrologist.png',
      ID: 15,
      finalColor: Color(0xff98C1FF)),
  const DoctorSpecialization(
      titleName: 'Hematology',
      iconName: 'Images/hematology.png',
      ID: 16,
      finalColor: Color(0xffF0D0ED)),
  const DoctorSpecialization(
      titleName: 'Pediatric',
      iconName: 'Images/pediatrics.png',
      ID: 17,
      finalColor: Color(0xffDEDCFF)),
  const DoctorSpecialization(
      titleName: 'Cardiac',
      iconName: 'Images/cardiac_arrest.png',
      ID: 18,
      finalColor: Color(0xffFFCDB9)),
  const DoctorSpecialization(
      titleName: 'Anesthesia',
      iconName: 'Images/anethesia.png',
      ID: 19,
      finalColor: Color(0xffFFB6C6)),
  const DoctorSpecialization(
      titleName: 'Allergy',
      iconName: 'Images/allergy.png',
      ID: 20,
      finalColor: Color(0xffD7F3D7)),
  const DoctorSpecialization(
      titleName: 'Infertility',
      iconName: 'Images/interfinity.png',
      ID: 21,
      finalColor: Color(0xffF0D0ED)),
  const DoctorSpecialization(
      titleName: 'Dermatologist',
      iconName: 'Images/determilogy.png',
      ID: 22,
      finalColor: Color(0xffC3C0FF)),
  const DoctorSpecialization(
      titleName: 'Cardiothoracic',
      iconName: 'Images/cardiology.png',
      ID: 23,
      finalColor: Color(0xffFFCCB8)),
  const DoctorSpecialization(
      titleName: 'Audiology',
      iconName: 'Images/audiology.png',
      ID: 24,
      finalColor: Color(0xffF4F9FC)),
  const DoctorSpecialization(
      titleName: 'Internal Medicine',
      iconName: 'Images/internal.png',
      ID: 25,
      finalColor: Color(0xffC3C5DC)),
  const DoctorSpecialization(
      titleName: 'Radiologist',
      iconName: 'Images/radiology.png',
      ID: 26,
      finalColor: Color(0xffE8FCFA)),
  const DoctorSpecialization(
      titleName: 'Endocrinology',
      iconName: 'Images/endorciology.png',
      ID: 27,
      finalColor: Color(0xffBFFFC3)),
  const DoctorSpecialization(
      titleName: 'General Surgery',
      iconName: 'Images/general.png',
      ID: 28,
      finalColor: Color(0xffE1E3E8)),
  const DoctorSpecialization(
      titleName: 'General Medicine',
      iconName: 'Images/general_medicine.png',
      ID: 29,
      finalColor: Color(0xffFFB6C6)),
];

class AppointmentMang extends State<Appointment_Selection>
    with SingleTickerProviderStateMixin {
  // Default Drop Down Item.
  String dropdownValue = 'One';
  var _displayAll = false;
  static const more_color = const Color(0xffFFEEFA);
  String holder = '';

  // late HospitalData hospitalvalue;
  // List<HospitalData> hospitallist = [
  //   HospitalData.fromJson(
  //       {"displayName": "Select Hospital/Clinic", "code": "00013", "id": "0"})
  // ];

  List<HostpitalDetails> hospitalListing = [
    HostpitalDetails.fromJson(
        {"id": "-Select Hospital-", "displayName": "-Select Hospital-"})
  ];
  late HostpitalDetails _selectionHospital = hospitalListing[0];
  String HospitalName = "";
  String HospitalID = "";

  List<DepartmentDetails> departmentListing = [
    DepartmentDetails.fromJson({
      "id": "-Select-",
      "displayName": "-Select Department-",
      "facilityId": "-Select-",
      "name": "-Select-"
    })
  ];
  late DepartmentDetails _selectionDepartment = departmentListing[0];
  String DepartName = "";
  String DepartID = "";

  List<AllMainClinicDetails> clinicListing = [
    AllMainClinicDetails.fromJson({
      "id": "-Select-",
      "displayName": "-Select Clinic-",
      "clinicId": "-Select-",
      "departmentId": "-Select-",
      "name": "-Select-"
    })
  ];
  late AllMainClinicDetails _selectionClinic = clinicListing[0];
  String ClinicName = "";
  String ClinicID = "";

  List<Categories> doctorlist = [
    // Categories.fromJson({
    //   "displayName": "Select Doctor",
    //   "name": "00013",
    //   "id": "0",
    //   "extraParam": "o",
    // })
  ];

  List<String> DrDegree = [];
  List<String> DrImagesDetails = [];

  @override
  void initState() {
    super.initState();
    super.initState();
    getAllHospitalList();
    // getDRData();
  }

  void getAllHospitalList() async {
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');

    Future<List> checkMainHospitalList =
        SQLHelper.getAllPatientDetails(MainPatientID, "AllHospitalList");

    List allHospitalList = await checkMainHospitalList;
    String mainHospitalListing = "";

    if (allHospitalList.length > 0) {
      for (int i = 0; i < allHospitalList.length; i++) {
        if (allHospitalList[i]['PatientID'] == MainPatientID &&
            allHospitalList[i]['PatientMethod'] == "AllHospitalList") {
          mainHospitalListing = allHospitalList[i]['PatientResponse'];
          break;
        } else {}
      }
      if (mainHospitalListing == "") {
        API_AppointmentSelection()
            .HospitalListing(AccessToken)
            .then((mainResponse) async {
          setState(() {
            if (allHospitalList.length > 0) {
              AllPatientUpdate(MainPatientID, "AllHospitalList", mainResponse);
            } else {
              AllPatientNoInsert(
                  MainPatientID, "AllHospitalList", mainResponse);
            }
            AllHospitalDetails(mainResponse);
          });
        });
      } else {
        log("Hospital Details From SQL ${mainHospitalListing}");
        AllHospitalDetails(mainHospitalListing);
      }
    } else {
      API_AppointmentSelection()
          .HospitalListing(AccessToken)
          .then((mainResponse) async {
        setState(() {
          if (allHospitalList.length > 0) {
            AllPatientUpdate(MainPatientID, "AllHospitalList", mainResponse);
          } else {
            AllPatientNoInsert(MainPatientID, "AllHospitalList", mainResponse);
          }
          AllHospitalDetails(mainResponse);
        });
      });
    }
  }

  void AllHospitalDetails(String AllmainResponse) async {
    var mainResponse = jsonDecode(AllmainResponse);

    var tempdata = mainResponse['value'];

    for (int i = 0; i < tempdata.length; i++) {
      hospitalListing.add(HostpitalDetails.fromJson(tempdata[i]));
    }
  }

  void getAllDepartmentList(String FacilityID) async {
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');

    Future<List> checkMainHospitalList =
        SQLHelper.getAllPatientDetails(MainPatientID, FacilityID);

    List allDepartmentList = await checkMainHospitalList;
    String mainDepartmentListing = "";

    if (allDepartmentList.length > 0) {
      for (int i = 0; i < allDepartmentList.length; i++) {
        if (allDepartmentList[i]['PatientID'] == MainPatientID &&
            allDepartmentList[i]['PatientMethod'] == FacilityID) {
          mainDepartmentListing = allDepartmentList[i]['PatientResponse'];
          break;
        } else {}
      }

      if (mainDepartmentListing == "") {
        API_AppointmentSelection()
            .getDepartmentLisitng(AccessToken, FacilityID)
            .then((mainResponse) async {
          setState(() {
            if (allDepartmentList.length > 0) {
              AllPatientUpdate(MainPatientID, FacilityID, mainResponse);
            } else {
              AllPatientNoInsert(MainPatientID, FacilityID, mainResponse);
            }
            AllDepartmentDetails(mainResponse);
          });
        });
      } else {
        // log("Department Details ${mainDepartmentListing}");
        AllDepartmentDetails(mainDepartmentListing);
      }
    } else {
      API_AppointmentSelection()
          .getDepartmentLisitng(AccessToken, FacilityID)
          .then((mainResponse) async {
        setState(() {
          if (allDepartmentList.length > 0) {
            AllPatientUpdate(MainPatientID, FacilityID, mainResponse);
          } else {
            AllPatientNoInsert(MainPatientID, FacilityID, mainResponse);
          }
          AllDepartmentDetails(mainResponse);
        });
      });
    }
  }

  void AllDepartmentDetails(String AllmainResponse) async {
    log("Department Response ${AllmainResponse}");

    setState(() {
      var mainResponse = jsonDecode(AllmainResponse);

      var tempdata = mainResponse['value'];

      if (tempdata.length > 0) {
        for (int i = 0; i < tempdata.length; i++) {
          departmentListing.add(DepartmentDetails.fromJson(tempdata[i]));
        }
      } else {
        Utility.WholeAppSnackbar(context,
            'There is no any department on this Hospital. Please select another hospital');
      }
    });
  }

  void getAllClinicList(String FacilityID, String DepartmentID) async {
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');

    Future<List> checkMainClinicList =
        SQLHelper.getAllPatientDetails(MainPatientID, DepartmentID);

    List allClinicList = await checkMainClinicList;
    String mainClinicResponse = "";

    if (allClinicList.length > 0) {
      for (int i = 0; i < allClinicList.length; i++) {
        if (allClinicList[i]['PatientID'] == MainPatientID &&
            allClinicList[i]['PatientMethod'] == DepartmentID) {
          mainClinicResponse = allClinicList[i]['PatientResponse'];
          break;
        } else {}
      }

      if (mainClinicResponse == "") {
        API_AppointmentSelection()
            .getClinicLocationList(AccessToken, DepartmentID)
            .then((mainResponse) async {
          setState(() {
            if (allClinicList.length > 0) {
              AllPatientUpdate(MainPatientID, DepartmentID, mainResponse);
            } else {
              AllPatientNoInsert(MainPatientID, DepartmentID, mainResponse);
            }
            AllClinicDetails(mainResponse);
          });
        });
      } else {
        // log("Department Details ${mainDepartmentListing}");
        AllClinicDetails(mainClinicResponse);
      }
    } else {
      API_AppointmentSelection()
          .getClinicLocationList(AccessToken, DepartmentID)
          .then((mainResponse) async {
        setState(() {
          if (allClinicList.length > 0) {
            AllPatientUpdate(MainPatientID, DepartmentID, mainResponse);
          } else {
            AllPatientNoInsert(MainPatientID, DepartmentID, mainResponse);
          }
          AllClinicDetails(mainResponse);
        });
      });
    }
  }

  void AllClinicDetails(String AllmainResponse) async {
    log("Clinic Response ${AllmainResponse}");

    setState(() {
      var mainResponse = jsonDecode(AllmainResponse);

      var tempdata = mainResponse['value'];
      if (tempdata.length > 0) {
        for (int i = 0; i < tempdata.length; i++) {
          if (tempdata[i]['displayName'] != null) {
            clinicListing.add(AllMainClinicDetails.fromJson(tempdata[i]));
          }
        }

        // Remove Duplicate Value
        final Map<String, AllMainClinicDetails> profileMap = new Map();
        clinicListing.forEach((item) {
          profileMap[item.name] = item;
        });
        clinicListing = profileMap.values.toList();
      } else {
        Utility.WholeAppSnackbar(context,
            'There is no any Clinic on this Hospital. Please select another Clinic');
      }
    });
  }

  void AllPatientNoInsert(
      String patientID, String PatientMethod, String PatientResponse) async {
    await SQLHelper.insert_PatientAdd(
        patientID, PatientMethod, PatientResponse);
  }

  void AllPatientUpdate(
      String patientID, String PatientMethod, String PatientResponse) async {
    await SQLHelper.UpdateAllPatientDetails(
        patientID, PatientMethod, PatientResponse);
  }

  void GetDoctorWithClinicWise(String ClinicID) async {
    String AccessToken = await Utility.readAccessToken('Token');
    API_AppointmentSelection()
        .getDoctorListFromClinic(AccessToken, ClinicID)
        .then((mainResponse) async {
      setState(() {
        doctorlist = [];
        var resBody = json.decode(mainResponse);
        for (int i = 0; i < resBody.length; i++) {
          doctorlist.add(Categories.fromJson(resBody[i]));
        }

        for (int j = 0; j < doctorlist.length; j++) {
          API_AppointmentSelection()
              .Employeemaster(AccessToken, doctorlist[j].id.toString())
              .then((desResponse) {
            setState(() {
              List EducationDetails = desResponse['employeeEducationDetail'];
              List EmployeeBiometric = desResponse['employeeBiometrics'];
              String DegreeName = '';
              // print(EducationDetails.length);
              if (EducationDetails.length > 0) {
                DrDegree = [];
                for (int i = 0; i < EducationDetails.length; i++) {
                  DrDegree.add(
                      desResponse['employeeEducationDetail'][i]['degree']);
                }

                setState(() {
                  DegreeName = DrDegree.join(', ');
                  doctorlist[j].DrDegree = DegreeName;
                });
              } else {}
            });
          });
        }

        // if (allClinicList.length > 0) {
        //   AllPatientUpdate(MainPatientID, DepartmentID, mainResponse);
        // } else {
        //   AllPatientNoInsert(MainPatientID, DepartmentID, mainResponse);
        // }
        // AllClinicDetails(mainResponse);
      });
    });
  }

  // void getDRData() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   String? token = "Bearer " + prefs.getString("Token").toString();
  //   String AccessToken = await Utility.readAccessToken('Token');
  //   var res =
  //       await http.get(Uri.parse(AllURL.MainURL + AllURL.DRDetails), headers: {
  //     HttpHeaders.contentTypeHeader: "application/json",
  //     HttpHeaders.authorizationHeader: token
  //   });
  //   var resBody = json.decode(res.body);
  //   setState(() {
  //     doctorlist = [];
  //     log('Doctor List ${resBody}');
  //     for (int i = 0; i < resBody.length; i++) {
  //       doctorlist.add(Categories.fromJson(resBody[i]));
  //     }
  //
  //     for (int j = 0; j < doctorlist.length; j++) {
  //       API_AppointmentSelection()
  //           .Employeemaster(AccessToken, doctorlist[j].id.toString())
  //           .then((desResponse) {
  //         setState(() {
  //           List EducationDetails = desResponse['employeeEducationDetail'];
  //           List EmployeeBiometric = desResponse['employeeBiometrics'];
  //           String DegreeName = '';
  //           // print(EducationDetails.length);
  //           if (EducationDetails.length > 0) {
  //             DrDegree = [];
  //             for (int i = 0; i < EducationDetails.length; i++) {
  //               DrDegree.add(
  //                   desResponse['employeeEducationDetail'][i]['degree']);
  //             }
  //
  //             setState(() {
  //               DegreeName = DrDegree.join(', ');
  //               doctorlist[j].DrDegree = DegreeName;
  //             });
  //           } else {}
  //
  //           // if (EmployeeBiometric.length > 0) {
  //           //   DrImagesDetails = [];
  //           //   for (int i = 0; i < EmployeeBiometric.length; i++) {
  //           //     if (desResponse['employeeBiometrics'][i]['biometricsType'] ==
  //           //         "PICTURE") {
  //           //       DrImagesDetails.add(
  //           //           desResponse['employeeBiometrics'][i]['filePath']);
  //           //     }
  //           //   }
  //           //
  //           //   setState(() {
  //           //     if (DrImagesDetails.length > 0) {
  //           //       doctorlist[j].DrImages = DrImagesDetails[0];
  //           //       String Base64 = doctorlist[j].DrImages.toString();
  //           //       doctorlist[j].ByteList = base64.decode(Base64.split(',').last);
  //           //     } else {
  //           //       doctorlist[j].DrImages = "NO";
  //           //     }
  //           //   });
  //           // }
  //         });
  //       });
  //     }
  //   });
  // }

  // Future<String> EmplyeeDetails(String DrEmployeeID) async {
  //   String DegreeName = '';
  //   String AccessToken = await Utility.readAccessToken('Token');
  //   API_AppointmentSelection()
  //       .Employeemaster(AccessToken, DrEmployeeID)
  //       .then((Response) {
  //     // print(Response["employeeProfessionalDetail"]["designation"]);
  //     // print(Response['employeeEducationDetail']);
  //
  //     // DoctorDetail = Response;
  //     // log("Doctor Detail ${DoctorDetail}");
  //     // String designation =
  //     //     Response["employeeProfessionalDetail"]["designation"];
  //     // DRName = Response['firstName'] + " " + Response['lastName'];
  //     List EducationDetails = Response['employeeEducationDetail'];
  //     print(EducationDetails.length);
  //     if (EducationDetails.length > 0) {
  //       DrDegree = [];
  //       for (int i = 0; i < EducationDetails.length; i++) {
  //         DrDegree.add(Response['employeeEducationDetail'][i]['degree']);
  //       }
  //
  //       setState(() {
  //         DegreeName = DrDegree.join(', ');
  //         // print(DegreeName);
  //       });
  //       // API_CALL().Employeedesignation(designation).then((desResponse) {
  //       //   setState(() {
  //       //     Speciality = desResponse['displayName'];
  //       //   });
  //       // });
  //       return DegreeName;
  //     } else {
  //       // print('No Data');
  //       DegreeName = '';
  //       return DegreeName;
  //     }
  //   });
  //   print('Value $DegreeName');
  //   return DegreeName;
  // }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: Utility.TopToolBar(
            Utility.translate('book_appoint'),
            Utility.getDeviceType() == "1" ? 20 : 30,
            Utility.PutDarkBlueColor(),
            context),
        body: SingleChildScrollView(
          child: Container(
            margin:
                new EdgeInsets.all(Utility.getDeviceType() == '1' ? 10 : 20.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                    Widget>[
              InputDecorator(
                decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  enabledBorder: new OutlineInputBorder(
                    borderSide: BorderSide(color: Utility.PutDarkBlueColor()),
                  ),
                ),
                // isEmpty: _selectionHospital == null || _selectionHospital == '',
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<HostpitalDetails>(
                    iconSize: Utility.getDeviceType() == '1' ? 25 : 50,
                    value: _selectionHospital,
                    isDense: true,
                    onChanged: (HostpitalDetails? newValue) {
                      if (newValue != null) {
                        setState(() {
                          _selectionHospital = newValue;
                          print(_selectionHospital.id);

                          departmentListing = [];
                          departmentListing.add(DepartmentDetails.fromJson({
                            "id": "-Select-",
                            "displayName": "-Select Department-",
                            "facilityId": "-Select-",
                            "name": "-Select-"
                          }));
                          _selectionDepartment = departmentListing[0];
                          DepartName = "";
                          DepartID = "";

                          clinicListing = [];
                          clinicListing.add(AllMainClinicDetails.fromJson({
                            "id": "-Select-",
                            "displayName": "-Select Clinic-",
                            "clinicId": "-Select-",
                            "departmentId": "-Select-",
                            "name": "-Select-"
                          }));
                          _selectionClinic = clinicListing[0];
                          ClinicName = "";
                          ClinicID = "";

                          HospitalName = _selectionHospital.displayName;
                          HospitalID = _selectionHospital.id;

                          if (HospitalID == "-Select Hospital-" ||
                              HospitalID == "") {
                          } else {
                            getAllDepartmentList(_selectionHospital.id);
                            // GetAppointmentService(_selectionHospital.id);
                          }
                          doctorlist.clear();
                        });
                      }
                    },
                    items: hospitalListing.map((HostpitalDetails value) {
                      return DropdownMenuItem<HostpitalDetails>(
                        value: value,
                        child: Text(value.displayName.toString(),
                            style: TextStyle(
                                fontFamily: 'Poppins_SemiBold',
                                fontSize:
                                    Utility.getDeviceType() == '1' ? 12 : 22,
                                color: Colors.black)),
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              InputDecorator(
                decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  enabledBorder: new OutlineInputBorder(
                    borderSide: BorderSide(color: Utility.PutDarkBlueColor()),
                  ),
                ),
                isEmpty:
                    _selectionDepartment == null || _selectionDepartment == '',
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<DepartmentDetails>(
                    iconSize: Utility.getDeviceType() == '1' ? 25 : 50,
                    value: _selectionDepartment,
                    isDense: true,
                    onChanged: (DepartmentDetails? newValue) {
                      if (newValue != null) {
                        setState(() {
                          _selectionDepartment = newValue!;

                          clinicListing = [];
                          clinicListing.add(AllMainClinicDetails.fromJson({
                            "id": "-Select-",
                            "displayName": "-Select Clinic-",
                            "clinicId": "-Select-",
                            "departmentId": "-Select-",
                            "name": "-Select-"
                          }));
                          _selectionClinic = clinicListing[0];
                          ClinicName = "";
                          ClinicID = "";

                          if (_selectionDepartment.displayName ==
                              "-Select Department-") {
                            DepartName = "";
                            DepartID = "";
                            doctorlist.clear();
                          } else {
                            DepartName = _selectionDepartment.displayName;
                            DepartID = _selectionDepartment.id;
                            getAllClinicList(_selectionDepartment.facilityId,
                                _selectionDepartment.id);
                          }
                        });
                      }
                    },
                    items: departmentListing.map((DepartmentDetails value) {
                      return DropdownMenuItem<DepartmentDetails>(
                        value: value,
                        child: Text(value.displayName.toString(),
                            style: TextStyle(
                                fontFamily: 'Poppins_SemiBold',
                                fontSize:
                                    Utility.getDeviceType() == '1' ? 12 : 22,
                                color: Colors.black)),
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              InputDecorator(
                decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  enabledBorder: new OutlineInputBorder(
                    borderSide: BorderSide(color: Utility.PutDarkBlueColor()),
                  ),
                ),
                isEmpty: _selectionClinic == null || _selectionClinic == '',
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<AllMainClinicDetails>(
                    iconSize: Utility.getDeviceType() == '1' ? 25 : 50,
                    value: _selectionClinic,
                    isDense: true,
                    onChanged: (AllMainClinicDetails? newValue) {
                      if (newValue != null) {
                        setState(() {
                          _selectionClinic = newValue!;
                          print('ID ' + _selectionClinic.clinicId);

                          if (_selectionClinic.displayName ==
                              "-Select Clinic-") {
                            ClinicName = "";
                            ClinicID = "";
                            doctorlist.clear();
                          } else {
                            ClinicName = _selectionClinic.displayName;
                            ClinicID = _selectionClinic.clinicId;
                            GetDoctorWithClinicWise(ClinicID);
                          }
                        });
                      }
                    },
                    items: clinicListing.map((AllMainClinicDetails value) {
                      return DropdownMenuItem<AllMainClinicDetails>(
                        value: value,
                        child: Text(value.displayName.toString(),
                            style: TextStyle(
                                fontFamily: 'Poppins_SemiBold',
                                fontSize:
                                    Utility.getDeviceType() == '1' ? 12 : 22,
                                color: Colors.black)),
                      );
                    }).toList(),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              // FacilitySlot(),
              // _gridValueAll(),
              Container(
                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: ListView.separated(
                  physics: ScrollPhysics(),
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(
                      height: 6,
                    );
                  },
                  itemCount: doctorlist.length,
                  shrinkWrap: true,
                  itemBuilder: (context, pos) {
                    Categories doctorvalue = doctorlist[pos];
                    return GestureDetector(
                      onTap: () {
                        // setState(() {
                        Categories doctorvalue = doctorlist[pos];
                        // Navigator.of(context).pop();

                        if (HospitalID == "-Select Hospital-" ||
                            HospitalID == "") {
                          Utility.WholeAppSnackbar(
                              context, 'Please Select Hospital Name');
                          return;
                        } else if (DepartName == "-Select Department-" ||
                            DepartName == "") {
                          Utility.WholeAppSnackbar(
                              context, 'Please Select Department Name');
                          return;
                        } else if (ClinicName == "" ||
                            ClinicName == "-Select Clinic-") {
                          Utility.WholeAppSnackbar(
                              context, 'Please Select Clinic Name');
                          return;
                        } else {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => bookappointment(
                                    doctorvalue: doctorvalue,
                                    FacilityID: HospitalID,
                                    FacilityName: HospitalName,
                                    DepartmentName: DepartName,
                                    DepartmentID: DepartID,
                                    ClinicName: ClinicName,
                                    ClinicID: ClinicID)),
                          );
                        }

                        //});
                      },
                      child: Card(
                        color: Colors.white,
                        elevation: 4,
                        child: Row(
                          children: <Widget>[
                            SizedBox(
                              width: Utility.getDeviceType() == '1' ? 8 : 18,
                            ),
                            Flexible(
                                flex: Utility.getDeviceType() == '1' ? 2 : 1,
                                child: Center(
                                  child: Container(
                                    width: Utility.getDeviceType() == '1'
                                        ? 90
                                        : 130,
                                    height: Utility.getDeviceType() == '1'
                                        ? 90
                                        : 130,
                                    // child: Image.memory(Uint8List.fromList(
                                    //     doctorlist[j].ByteList)),
                                    // child: Image(image: AssetImage('Images/default_placeholder.png')),
                                    decoration: BoxDecoration(
                                      color: Utility.PutLightBlueColor(),
                                      image: DecorationImage(
                                        image: AssetImage(
                                            'Images/default_placeholder.png'),
                                        fit: BoxFit.fill,
                                      ),
                                      shape: BoxShape.circle,
                                    ),
                                    // decoration: new BoxDecoration(
                                    //   borderRadius: BorderRadius.circular(10),
                                    //   color: Colors.pinkAccent,
                                    //   image: new DecorationImage(
                                    //       fit: BoxFit.fill,
                                    //       image: new NetworkImage(
                                    //           "https://icons-for-free.com/download-icon-boy+man+person+user+woman+icon-1320085967769585303_512.png")),
                                    // ),
                                  ),
                                )),
                            Container(
                              height:
                                  Utility.getDeviceType() == '1' ? 110 : 170,
                            ),
                            SizedBox(
                              width: Utility.getDeviceType() == '1' ? 10 : 20,
                            ),
                            Flexible(
                              flex: 5,
                              child: Column(
                                children: <Widget>[
                                  Center(
                                    child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Text('${doctorvalue.name}',
                                          style: TextStyle(
                                              fontFamily: 'Poppins_SemiBold',
                                              fontSize:
                                                  Utility.getDeviceType() == '1'
                                                      ? 14
                                                      : 24.0,
                                              color:
                                                  Utility.PutDarkBlueColor())),
                                    ),
                                  ),
                                  Center(
                                    child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Text('${doctorvalue.DrDegree}',
                                          maxLines: 1,
                                          style: TextStyle(
                                              fontFamily: 'Poppins_SemiBold',
                                              fontSize:
                                                  Utility.getDeviceType() == '1'
                                                      ? 12
                                                      : 22.0,
                                              color:
                                                  Utility.PutLightBlueColor())),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }

  Widget FacilitySlot() {
    late DoctorSpecialization item;
    var mainDisplay = false;
    final contactsWidget;
    var size = spec_Choices.length;
    final sizeContacts = spec_Choices.length;
    if (sizeContacts <= 4) {
      size = spec_Choices.length;
      mainDisplay = true;
    } else if (sizeContacts > 4) {
      mainDisplay = false;
      var minusValue = spec_Choices.length - 3;
      size =
          _displayAll ? spec_Choices.length : spec_Choices.length - minusValue;
    }
    int mainSize = 0;
    if (mainDisplay) {
      mainSize = size;
      // contactsWidget = List.generate(
      //     size, (index) => _DoctorSpecial_Details(spec_Choices[index]));
    } else {
      mainSize = size + 1;
      // contactsWidget = List.generate(
      //     size, (index) => _DoctorSpecial_Details(spec_Choices[index]))
      //   ..add(_seeNoSeeMore());
    }

    return Container(
      //padding: EdgeInsets.all(10),
      child: GridView.builder(
          shrinkWrap: true,
          controller: new ScrollController(keepScrollOffset: false),
          itemCount: mainSize,
          padding: EdgeInsets.fromLTRB(
              0,
              Utility.getDeviceType() == '1' ? 5 : 10,
              0,
              Utility.getDeviceType() == '1' ? 5 : 10),
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4,
            crossAxisSpacing: Utility.getDeviceType() == '1' ? 5 : 15,
            mainAxisSpacing: Utility.getDeviceType() == '1' ? 5 : 15,
            childAspectRatio: Utility.getDeviceType() == '1' ? 0.7 : 0.75,
          ),
          itemBuilder: (BuildContext context, int index) {
            if (size != index) {
              item = spec_Choices[index];
            }
            return GestureDetector(
              onTap: () {
                setState(() {
                  //selectedCard = index;
                });
              },
              child: size == index
                  ? _seeNoSeeMore()
                  : Container(
                      child: Column(
                      children: [
                        Card(
                          color: item.finalColor,
                          child: AspectRatio(
                            aspectRatio: 1.1,
                            child: Center(
                                child: Image.asset(
                              item.iconName,
                              width: Utility.getDeviceType() == '1' ? 30 : 60,
                              height: Utility.getDeviceType() == '1' ? 30 : 60,
                              //fit: BoxFit.fitWidth,
                              alignment: Alignment.center,
                              fit: BoxFit.fill,
                            )),
                          ),
                        ),
                        Container(
                          // child: Utility.MainNormalTextWithFont(item.titleName, 12,
                          //     Utility.PutDarkBlueColor(), 'Poppins_Medium'),
                          child: Text(item.titleName,
                              maxLines: 2,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Poppins_Medium',
                                  color: Utility.PutDarkBlueColor(),
                                  fontSize: Utility.getDeviceType() == '1'
                                      ? 12
                                      : 22)),
                        ),
                      ],
                    )),
            );
          }),
    );
  }

  _seeNoSeeMore() {
    return InkWell(
      onTap: () => setState(() => _displayAll = !_displayAll),
      child: Container(
        child: Column(
          children: <Widget>[
            Card(
              color: more_color,
              child: AspectRatio(
                aspectRatio: 1.1,
                child: Center(
                  child: IconButton(
                      onPressed: () {
                        setState(() => _displayAll = !_displayAll);
                      },
                      icon: _displayAll
                          ? SvgPicture.asset(
                              'Images/less_icon.svg',
                              alignment: Alignment.bottomCenter,
                              fit: BoxFit.fill,
                              width: Utility.getDeviceType() == '1' ? 40 : 60,
                              height: Utility.getDeviceType() == '1' ? 40 : 60,
                            )
                          : SvgPicture.asset(
                              'Images/more_icon.svg',
                              fit: BoxFit.fill,
                              width: Utility.getDeviceType() == '1' ? 40 : 60,
                              height: Utility.getDeviceType() == '1' ? 40 : 60,
                            ),
                      iconSize: Utility.getDeviceType() == '1' ? 40 : 60),
                ),
              ),
            ),
            Container(
              child: Text(_displayAll ? "Show Less" : "More",
                  maxLines: 1,
                  style: TextStyle(
                      fontFamily: 'Poppins_Medium',
                      color: Utility.PutDarkBlueColor(),
                      fontSize: Utility.getDeviceType() == '1' ? 12 : 22)),
            ),
          ],
        ),
      ),
    );
  }
}
