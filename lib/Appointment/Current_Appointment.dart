import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:softcliniccare/Helper/MapUtils.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'package:url_launcher/url_launcher.dart';
import '../Helper/Constant.dart';
import '../Helper/Database/sqflite_helper.dart';
import '../Model/Appointment/Appointment_Status.dart';
import '../Model/Dashboard/AppointmentListModel.dart';
import '../Model/Dashboard/Dashboard_API.dart';
import '../Model/Dashboard/Facility_Name_Model.dart';
import '../Model/Dashboard/PatientDetails_All.dart';
import '../Model/Profile/ProfileAPI.dart';
import '../VideoCall/Videocalling.dart';
import 'Appointment_Selection.dart';
import 'Bookappointment_screen.dart';

class CurrentAppointment extends StatefulWidget {
  @override
  CurrentAppointmentState createState() => CurrentAppointmentState();
}

class CurrentAppointmentState extends State<CurrentAppointment> {
  List<AppointmentList_Details> listing_Appointment_Details_Temp = [];
  List<AppointmentList_Details> listing_Appointment_Details = [];

  List<Facility_Details> listing_FacilityName = [];

  late PatientDetails mainPatientDetails;

  String AppointmentMonth = '';
  String AppointmentWeekName = '';
  String AppointmentDate = '';
  String Time12Hour = '';
  String FacilityName = '';

  String FirstName = '';
  String LastName = '';

  List<AppointmentStatusDetails> listing_App_Status = [];

  List<Map> Employeetype = [];
  Map Dictemployeetype = {};

  @override
  void initState() {
    super.initState();
    FacilityName_Details();
    AppointmentList();
    MainPatientDetails();
    getAppointmentStatusAPI();
    getDoctorNameList();
  }

  void getDoctorNameList() async {
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');

    final data = await SQLHelper.getAll_Facility_Response(MainPatientID);
    List mainEmployeeDetails = await data;

    print("DB Length Employee List ${mainEmployeeDetails.length}");

    String main_EmployeeDetails = "";

    if (mainEmployeeDetails.length > 0) {
      for (int i = 0; i < mainEmployeeDetails.length; i++) {
        if (mainEmployeeDetails[i]['FacilityID'] == MainPatientID) {
          main_EmployeeDetails = mainEmployeeDetails[i]['FacilityResponse'];
          break;
        } else {}
      }
      // log("Doctor List ${main_EmployeeDetails}");
      if (main_EmployeeDetails == "") {
        API_Dashboard().employeebytype().then((mainResponse) {
          setState(() {
            if (mainEmployeeDetails.length > 0) {
              AllFacilityUpdate(MainPatientID, mainResponse);
            } else {
              AllFacilityInsert(MainPatientID, mainResponse);
            }
            GetEmployeeTypeData(mainResponse);
          });
        });
      } else {
        GetEmployeeTypeData(main_EmployeeDetails);
      }
    } else {
      API_Dashboard().employeebytype().then((mainResponse) {
        setState(() {
          if (mainEmployeeDetails.length > 0) {
            AllFacilityUpdate(MainPatientID, mainResponse);
          } else {
            AllFacilityInsert(MainPatientID, mainResponse);
          }
          GetEmployeeTypeData(mainResponse);
        });
      });
    }
  }

  void GetEmployeeTypeData(String mainResponse) async {
    log("Doctor List ${mainResponse}");
    Employeetype = [];
    var EmployeeData = json.decode(mainResponse);
    List response = EmployeeData;
    for (int i = 0; i < response.length; i++) {
      Employeetype.add(response[i]);
    }
  }

  void MainPatientDetails() async {
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');

    final data = await SQLHelper.getAll_ProfileDetails(MainPatientID);
    List<Map<String, dynamic>> allPatientResponse = data;
    String mainPatientResponse = "";

    if (allPatientResponse.length > 0) {
      for (int i = 0; i < allPatientResponse.length; i++) {
        if (allPatientResponse[i]['profile_id'] == MainPatientID) {
          mainPatientResponse = allPatientResponse[i]['profile_response'];
          break;
        } else {}
      }
      if (mainPatientResponse == "") {
        API_Profile()
            .ProfilePageDisplay(AccessToken, MainPatientID)
            .then((Response) async {
          if (allPatientResponse.length > 0) {
            AllProfileUpdate(MainPatientID, json.encode(Response));
          } else {
            AllProfileInsert(MainPatientID, json.encode(Response));
          }

          ProfileMainResponse(Response);
        });
      } else {
        ProfileMainResponse(json.decode(mainPatientResponse));
      }
    } else {
      API_Profile()
          .ProfilePageDisplay(AccessToken, MainPatientID)
          .then((Response) async {
        if (allPatientResponse.length > 0) {
          AllProfileUpdate(MainPatientID, json.encode(Response));
        } else {
          AllProfileInsert(MainPatientID, json.encode(Response));
        }
        ProfileMainResponse(Response);
      });
    }
  }

  void ProfileMainResponse(var Response) {
    // log("Main Profile ${Response}");
    if (mounted) {
      setState(() {
        mainPatientDetails = new PatientDetails.fromJson(Response);

        FirstName = mainPatientDetails.patregi.firstName;
        LastName = mainPatientDetails.patregi.lastName;
      });
    } else {
      mainPatientDetails = new PatientDetails.fromJson(Response);

      FirstName = mainPatientDetails.patregi.firstName;
      LastName = mainPatientDetails.patregi.lastName;
    }
  }

  void AllProfileInsert(String PatientID, String PatientResponse) async {
    await SQLHelper.insert_Profile_Details(PatientID, PatientResponse);
  }

  void AllProfileUpdate(String PatientID, String PatientResponse) async {
    await SQLHelper.update_Profile_response(PatientID, PatientResponse);
  }

  void FacilityName_Details() async {
    String AccessToken = await Utility.readAccessToken('Token');

    final data = await SQLHelper.getAll_Facility_Response(
        ConstantVar.str_Common_GETALLFACILITYNAME);
    List<Map<String, dynamic>> allFacilityResponse = data;
    String mainFacilityResponse = "";

    if (allFacilityResponse.length > 0) {
      for (int i = 0; i < allFacilityResponse.length; i++) {
        if (allFacilityResponse[i]['FacilityID'] ==
            ConstantVar.str_Common_GETALLFACILITYNAME) {
          mainFacilityResponse = allFacilityResponse[i]['FacilityResponse'];
          break;
        } else {}
      }
      if (mainFacilityResponse == "") {
        API_Dashboard()
            .Dashboard_Facilityname(AccessToken)
            .then((Response) async {
          if (allFacilityResponse.length > 0) {
            AllFacilityUpdate(ConstantVar.str_Common_GETALLFACILITYNAME,
                json.encode(Response));
          } else {
            AllFacilityInsert(ConstantVar.str_Common_GETALLFACILITYNAME,
                json.encode(Response));
          }
          FacilityMainResponse(Response);
        });
      } else {
        // log("Main Facility ALL ${mainFacilityResponse}");
        FacilityMainResponse(json.decode(mainFacilityResponse));
      }
    } else {
      API_Dashboard()
          .Dashboard_Facilityname(AccessToken)
          .then((Response) async {
        if (allFacilityResponse.length > 0) {
          AllFacilityUpdate(
              ConstantVar.str_Common_GETALLFACILITYNAME, json.encode(Response));
        } else {
          AllFacilityInsert(
              ConstantVar.str_Common_GETALLFACILITYNAME, json.encode(Response));
        }
        FacilityMainResponse(Response);
      });
    }
  }

  void FacilityMainResponse(var Response) {
    // log("Main Facility ${Response}");
    if (mounted) {
      setState(() {
        var tempdata = Response['value'];
        for (int i = 0; i < tempdata.length; i++) {
          listing_FacilityName.add(Facility_Details.fromJson(tempdata[i]));
        }
      });
    } else {
      var tempdata = Response['value'];
      for (int i = 0; i < tempdata.length; i++) {
        listing_FacilityName.add(Facility_Details.fromJson(tempdata[i]));
      }
    }
  }

  void AppointmentList() async {
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');
    print('Patient ID ${MainPatientID}');
    // Utility.showLoadingDialog(context, _AppointmentList, true);

    final data = await SQLHelper.getAll_AppointmentList(
        MainPatientID, ConstantVar.str_Common_GETUPCOMINGAPPOINTMENT);
    List<Map<String, dynamic>> allAppointmentList = data;
    String mainAppointmentResponse = "";

    if (allAppointmentList.length > 0) {
      for (int i = 0; i < allAppointmentList.length; i++) {
        if (allAppointmentList[i]['PatientID'] == MainPatientID) {
          mainAppointmentResponse =
              allAppointmentList[i]['AppointmentResponse'];
          break;
        } else {}
      }
      if (mainAppointmentResponse == "") {
        API_Dashboard()
            .AppointmentListAPI(AccessToken, MainPatientID)
            .then((mainResponse) async {
          setState(() {
            if (allAppointmentList.length > 0) {
              AllAppointmentUpdate(MainPatientID,
                  ConstantVar.str_Common_GETUPCOMINGAPPOINTMENT, mainResponse);
            } else {
              AllAppointmentInsert(MainPatientID,
                  ConstantVar.str_Common_GETUPCOMINGAPPOINTMENT, mainResponse);
            }
            AppointmentResponse(mainResponse);
          });
        });
      } else {
        // log("Main Appointment Response ${mainAppointmentResponse}");
        AppointmentResponse(mainAppointmentResponse);
      }
    } else {
      API_Dashboard()
          .AppointmentListAPI(AccessToken, MainPatientID)
          .then((mainResponse) async {
        setState(() {
          if (allAppointmentList.length > 0) {
            AllAppointmentUpdate(MainPatientID,
                ConstantVar.str_Common_GETUPCOMINGAPPOINTMENT, mainResponse);
          } else {
            AllAppointmentInsert(MainPatientID,
                ConstantVar.str_Common_GETUPCOMINGAPPOINTMENT, mainResponse);
          }
          AppointmentResponse(mainResponse);
        });
      });
    }
  }

  void AppointmentResponse(String mainResponse) {
    var jsonResponse = json.decode(mainResponse);
    var tempdata = jsonResponse;
    listing_Appointment_Details = [];
    listing_Appointment_Details_Temp = [];

    if (tempdata.length > 0) {
      for (int i = 0; i < tempdata.length; i++) {
        listing_Appointment_Details_Temp
            .add(AppointmentList_Details.fromJson(tempdata[i]));
      }

      for (int i = 0; i < listing_Appointment_Details_Temp.length; i++) {
        DateTime serverDate = new DateFormat("yyyy-MM-dd")
            .parse(listing_Appointment_Details_Temp[i].appointmentDate);

        final birthday =
            DateTime(serverDate.year, serverDate.month, serverDate.day);
        final date2 = DateTime.now();
        final difference = date2.difference(birthday).inDays;

        if (difference <= 0) {
          // print('Less');
          listing_Appointment_Details.add(listing_Appointment_Details_Temp[i]);
        } else {
          // print('Greater');
        }
      }
      print(" Main Length ${listing_Appointment_Details.length}");

      for (int k = 0; k < listing_Appointment_Details.length; k++) {
        for (int i = 0; i < listing_FacilityName.length; i++) {
          if (listing_Appointment_Details[k].facilityId ==
              listing_FacilityName[i].id) {
            listing_Appointment_Details[k].facilityName =
                listing_FacilityName[i].name;
          }
        }
      }
    }
  }

  void AllFacilityInsert(String FacilityID, String FacilityResponse) async {
    await SQLHelper.insertName_Facility(FacilityID, FacilityResponse);
  }

  void AllFacilityUpdate(String FacilityID, String FacilityResponse) async {
    await SQLHelper.update_facility_response(FacilityID, FacilityResponse);
  }

  void AllAppointmentInsert(String PatientID, String UpComing_Past,
      String AppointmentResponse) async {
    await SQLHelper.insert_Appointment_Details(
        PatientID, AppointmentResponse, UpComing_Past);
  }

  void AllAppointmentUpdate(String PatientID, String UpComing_Past,
      String AppointmentResponse) async {
    await SQLHelper.update_Appointment_response(
        PatientID, AppointmentResponse, UpComing_Past);
  }

  void getAppointmentStatusAPI() async {
    final data = await SQLHelper.getNoChangeDetails(
        ConstantVar.str_Common_GetAllAppointmentStatus);
    List<Map<String, dynamic>> all_AppointmentStatus = data;
    String mainAppointmentStatus = "";

    if (all_AppointmentStatus.length > 0) {
      for (int i = 0; i < all_AppointmentStatus.length; i++) {
        if (all_AppointmentStatus[i]['APIName'] ==
            ConstantVar.str_Common_GetAllAppointmentStatus) {
          mainAppointmentStatus = all_AppointmentStatus[i]['APIResponse'];
          break;
        } else {}
      }
      if (mainAppointmentStatus == "") {
        API_Dashboard().AppointmentStatusDetails().then((Response) {
          setState(() {
            if (all_AppointmentStatus.length > 0) {
              AllDataUpdate(
                  ConstantVar.str_Common_GetAllAppointmentStatus, Response);
            } else {
              AllDataInsert(
                  ConstantVar.str_Common_GetAllAppointmentStatus, Response);
            }
            mainStatusDetails(Response);
          });
        });
      } else {
        mainStatusDetails(mainAppointmentStatus);
      }
    } else {
      API_Dashboard().AppointmentStatusDetails().then((Response) {
        setState(() {
          if (all_AppointmentStatus.length > 0) {
            AllDataUpdate(
                ConstantVar.str_Common_GetAllAppointmentStatus, Response);
          } else {
            AllDataInsert(
                ConstantVar.str_Common_GetAllAppointmentStatus, Response);
          }
          mainStatusDetails(Response);
        });
      });
    }
  }

  void mainStatusDetails(String mainResponse) {
    var resBody = json.decode(mainResponse);

    var tempdata = resBody;
    for (int i = 0; i < tempdata.length; i++) {
      listing_App_Status.add(AppointmentStatusDetails.fromJson(tempdata[i]));
    }
  }

  void AllDataInsert(String APIName, String APIResponse) async {
    String cdate = DateFormat("yyyy-MM-dd").format(DateTime.now());
    String tTime = DateFormat("HH:mm").format(DateTime.now());
    await SQLHelper.insertMainCommonData(APIName, APIResponse, cdate, tTime);
  }

  void AllDataUpdate(String APIName, String APIResponse) async {
    await SQLHelper.updateNoChangeDetails(APIName, APIResponse);
  }

  Future<void> _pullRefresh() async {
    print("Refresh");
    String AccessToken = await Utility.readAccessToken('Token');
    String MainPatientID = await Utility.readAccessToken('PatientID');
    print('Patient ID ${MainPatientID}');

    final data = await SQLHelper.getAll_AppointmentList(
        MainPatientID, ConstantVar.str_Common_GETUPCOMINGAPPOINTMENT);
    List<Map<String, dynamic>> allAppointmentList = data;
    String mainAppointmentResponse = "";

    setState(() {
      API_Dashboard()
          .AppointmentListAPI(AccessToken, MainPatientID)
          .then((mainResponse) async {
        setState(() {
          if (allAppointmentList.length > 0) {
            AllAppointmentUpdate(MainPatientID,
                ConstantVar.str_Common_GETUPCOMINGAPPOINTMENT, mainResponse);
          } else {
            AllAppointmentInsert(MainPatientID,
                ConstantVar.str_Common_GETUPCOMINGAPPOINTMENT, mainResponse);
          }
          AppointmentResponse(mainResponse);
        });
      });

      // if (allAppointmentList.length > 0) {
      //   for (int i = 0; i < allAppointmentList.length; i++) {
      //     if (allAppointmentList[i]['PatientID'] == MainPatientID) {
      //       mainAppointmentResponse =
      //       allAppointmentList[i]['AppointmentResponse'];
      //       break;
      //     } else {}
      //   }
      //   if (mainAppointmentResponse == "") {
      //     API_Dashboard()
      //         .AppointmentListAPI(AccessToken, MainPatientID)
      //         .then((mainResponse) async {
      //       setState(() {
      //         if (allAppointmentList.length > 0) {
      //           AllAppointmentUpdate(MainPatientID,
      //               ConstantVar.str_Common_GETUPCOMINGAPPOINTMENT, mainResponse);
      //         } else {
      //           AllAppointmentInsert(MainPatientID,
      //               ConstantVar.str_Common_GETUPCOMINGAPPOINTMENT, mainResponse);
      //         }
      //         AppointmentResponse(mainResponse);
      //       });
      //     });
      //   } else {
      //     // log("Main Appointment Response ${mainAppointmentResponse}");
      //     AppointmentResponse(mainAppointmentResponse);
      //   }
      // } else {
      //   API_Dashboard()
      //       .AppointmentListAPI(AccessToken, MainPatientID)
      //       .then((mainResponse) async {
      //     setState(() {
      //       if (allAppointmentList.length > 0) {
      //         AllAppointmentUpdate(MainPatientID,
      //             ConstantVar.str_Common_GETUPCOMINGAPPOINTMENT, mainResponse);
      //       } else {
      //         AllAppointmentInsert(MainPatientID,
      //             ConstantVar.str_Common_GETUPCOMINGAPPOINTMENT, mainResponse);
      //       }
      //       AppointmentResponse(mainResponse);
      //     });
      //   });
      // }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        floatingActionButton: Container(
          padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
          height: Utility.getDeviceType() == '1' ? 50 : 70,
          width: MediaQuery.of(context).size.width - 50,
          child: MaterialButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Appointment_Selection()),
              );
            },
            child: Ink(
              width: Utility.getDeviceType() == '1' ? double.infinity : 400,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Utility.gradiant, Utility.gradiant1],
                ),
                borderRadius: BorderRadius.circular(
                    Utility.getDeviceType() == '1' ? 25 : 35),
              ),
              child: Container(
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width - 50,
                    minHeight: 50.0),
                alignment: Alignment.center,
                child: Text(
                  Utility.translate('book_appointment_new'),
                  style: TextStyle(
                      fontSize: Utility.getDeviceType() == '1' ? 18 : 26,
                      fontFamily: 'Poppins_Medium',
                      color: Colors.white),
                ),
              ),
            ),
            splashColor: Colors.black12,
            padding: EdgeInsets.all(0),
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(25.0),
            ),
          ),
        ),
        //bottomNavigationBar: _myapp,
        body: listing_Appointment_Details.length == 0
            ? RefreshIndicator(
                onRefresh: _pullRefresh,
                child: Container(
                  child: SingleChildScrollView(
                    physics: AlwaysScrollableScrollPhysics(),
                    child: Container(
                      child: Center(
                        child: Text('No Upcoming Appointment Found'),
                      ),
                      height: MediaQuery.of(context).size.height,
                    ),
                  ),
                ),
              )
            : Container(
                padding: Utility.getDeviceType() == '1'
                    ? EdgeInsets.fromLTRB(10, 10, 10, 50)
                    : EdgeInsets.fromLTRB(15, 10, 15, 100),
                child: RefreshIndicator(
                  onRefresh: _pullRefresh,
                  child: ListView.builder(
                    itemCount: listing_Appointment_Details.length,
                    itemBuilder: (context, pos) {
                      DateTime parseDate = new DateFormat("yyyy-MM-dd").parse(
                          listing_Appointment_Details[pos].appointmentDate);
                      var inputDate = DateTime.parse(parseDate.toString());
                      var outputFormatMonth = DateFormat('MMM');
                      var outputFormatDay = DateFormat('EEE');
                      var outputFormatDate = DateFormat('dd');
                      var outputMonth = outputFormatMonth.format(inputDate);
                      AppointmentMonth = outputMonth;
                      var outputDay = outputFormatDay.format(inputDate);
                      AppointmentWeekName = outputDay;
                      var outputDate = outputFormatDate.format(inputDate);
                      AppointmentDate = outputDate;

                      DateTime parseTime = new DateFormat("HH:mm:ss")
                          .parse(listing_Appointment_Details[pos].startTime);

                      var inputTime = DateTime.parse(parseTime.toString());
                      var outputTimeData = DateFormat('jm');
                      var outputTime = outputTimeData.format(inputTime);
                      // print(outputTime);
                      Time12Hour = outputTime;
                      // Status = listing_Appointment_Details[pos].appointmentStatus;

                      String AppointmentStatus = '';
                      String AppointmentStatusID = '';
                      String DoctorName = "";

                      for (int i = 0; i < listing_App_Status.length; i++) {
                        if (listing_App_Status[i].id ==
                            listing_Appointment_Details[pos]
                                .appointmentStatus) {
                          AppointmentStatusID = listing_App_Status[i].id;
                          AppointmentStatus = listing_App_Status[i].displayName;
                        }
                      }

                      for (int i = 0; i < Employeetype.length; i++) {
                        if (Employeetype[i]['id'] ==
                            listing_Appointment_Details[pos].doctorId) {
                          DoctorName = Employeetype[i]['displayName'];
                        }
                      }

                      // AppointmentID = listing_Appointment_Details[0].id;

                      return Container(
                        child: Card(
                            color: Utility.PutBackgroundCardColor(),
                            elevation: 5,
                            child: Container(
                              height:
                                  Utility.getDeviceType() == '1' ? 160 : 220,
                              child: Row(
                                children: <Widget>[
                                  new Expanded(
                                    flex: 4,
                                    child: new Container(
                                      color: Colors.white,
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.fromLTRB(
                                                Utility.getDeviceType() == '1'
                                                    ? 5
                                                    : 10,
                                                Utility.getDeviceType() == '1'
                                                    ? 9
                                                    : 18,
                                                0,
                                                0),
                                            child: Align(
                                              alignment: Alignment.topLeft,
                                              child: Utility
                                                  .MainNormalTextWithFont(
                                                      AppointmentMonth,
                                                      Utility.getDeviceType() ==
                                                              '1'
                                                          ? 14
                                                          : 24,
                                                      Utility
                                                          .PutDarkBlueColor(),
                                                      'Poppins_Medium'),
                                            ),
                                          ),
                                          Container(
                                            child: Align(
                                              alignment: Alignment.center,
                                              child: Utility
                                                  .MainNormalTextWithFont(
                                                      AppointmentDate,
                                                      Utility.getDeviceType() ==
                                                              '1'
                                                          ? 24
                                                          : 34,
                                                      Utility
                                                          .PutLightBlueColor(),
                                                      'Poppins_Medium'),
                                            ),
                                          ),
                                          Container(
                                            child: Align(
                                              alignment: Alignment.center,
                                              child: Utility
                                                  .MainNormalTextWithFont(
                                                      AppointmentWeekName
                                                          .toUpperCase(),
                                                      Utility.getDeviceType() ==
                                                              '1'
                                                          ? 18
                                                          : 28,
                                                      Utility
                                                          .PutLightBlueColor(),
                                                      'Poppins_Medium'),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.fromLTRB(
                                                0, 10, 0, 0),
                                            child: Align(
                                              child: Utility
                                                  .MainNormalTextWithFont(
                                                      Time12Hour,
                                                      Utility.getDeviceType() ==
                                                              '1'
                                                          ? 20
                                                          : 30,
                                                      Utility
                                                          .PutDarkBlueColor(),
                                                      'Poppins_Medium'),
                                              alignment: Alignment.bottomCenter,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                      child: Container(
                                    width: 7,
                                    height: double.infinity,
                                    color: Utility.PutLightBlueColor(),
                                  )),
                                  new Expanded(
                                    flex: 10,
                                    child: new Container(
                                      color: Utility.PutBackgroundCardColor(),
                                      child: Row(
                                        children: <Widget>[
                                          new Expanded(
                                            flex: 6,
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  margin: EdgeInsets.fromLTRB(
                                                      8, 10, 0, 0),
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    FirstName + " " + LastName,
                                                    style: TextStyle(
                                                      fontFamily:
                                                          'Poppins_Medium',
                                                      fontSize:
                                                          Utility.getDeviceType() ==
                                                                  '1'
                                                              ? 12
                                                              : 22,
                                                      color: Utility
                                                          .PutDarkBlueColor(),
                                                      fontStyle:
                                                          FontStyle.normal,
                                                    ),
                                                    maxLines: 2,
                                                  ),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.fromLTRB(
                                                      7, 0, 0, 0),
                                                  alignment: Alignment.centerLeft,
                                                  child: Text(
                                                    DoctorName,
                                                    style: TextStyle(
                                                      fontFamily: 'Poppins_Medium',
                                                      fontSize:
                                                      Utility.getDeviceType() ==
                                                          '1'
                                                          ? 12
                                                          : 22,
                                                      color: Utility
                                                          .PutLightBlueColor(),
                                                      fontStyle: FontStyle.normal,
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  margin: EdgeInsets.fromLTRB(
                                                      8, 0, 0, 0),
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    listing_Appointment_Details[
                                                            pos]
                                                        .facilityName,
                                                    style: TextStyle(
                                                      fontFamily:
                                                          'Poppins_Medium',
                                                      fontSize:
                                                          Utility.getDeviceType() ==
                                                                  '1'
                                                              ? 12
                                                              : 22,
                                                      color: Utility
                                                          .PutLightBlueColor(),
                                                      fontStyle:
                                                          FontStyle.normal,
                                                    ),
                                                    maxLines: 2,
                                                  ),
                                                ),
                                                SizedBox(height: 52),
                                                // Container(
                                                //   margin: EdgeInsets.fromLTRB(
                                                //       10, 0, 0, 0),
                                                //   alignment: Alignment.topLeft,
                                                //   child: Text(
                                                //     Utility.translate('for'),
                                                //     style: TextStyle(
                                                //       fontFamily:
                                                //           'Poppins_Medium',
                                                //       fontSize:
                                                //           Utility.getDeviceType() ==
                                                //                   '1'
                                                //               ? 12
                                                //               : 22,
                                                //       color: Utility
                                                //           .PutDarkBlueColor(),
                                                //       fontStyle: FontStyle.normal,
                                                //     ),
                                                //     maxLines: 1,
                                                //   ),
                                                // ),
                                                // Container(
                                                //   margin: EdgeInsets.fromLTRB(
                                                //       10, 0, 0, 0),
                                                //   alignment: Alignment.topLeft,
                                                //   child: Text(
                                                //     'Follow Up',
                                                //     style: TextStyle(
                                                //       fontFamily:
                                                //           'Poppins_Medium',
                                                //       fontSize:
                                                //           Utility.getDeviceType() ==
                                                //                   '1'
                                                //               ? 12
                                                //               : 22,
                                                //       color: Utility
                                                //           .PutLightBlueColor(),
                                                //       fontStyle: FontStyle.normal,
                                                //     ),
                                                //     maxLines: 1,
                                                //   ),
                                                // ),
                                              ],
                                            ),
                                          ),
                                          new Expanded(
                                            flex: 4,
                                            child: Container(
                                              child: Column(
                                                children: <Widget>[
                                                  new Expanded(
                                                    flex: 1,
                                                    child: new Container(
                                                      child: Row(
                                                        children: <Widget>[
                                                          new Expanded(
                                                            flex: 2,
                                                            child:
                                                                new Container(
                                                              margin: EdgeInsets
                                                                  .fromLTRB(0,
                                                                      5, 0, 0),
                                                              alignment:
                                                                  Alignment
                                                                      .topLeft,
                                                              child: IconButton(
                                                                  onPressed:
                                                                      () {
                                                                    Uri myUri =
                                                                        Uri.parse(
                                                                            "clock-alarm");
                                                                    launch(
                                                                        "clock-alarm");
                                                                  },
                                                                  icon: SvgPicture.asset(
                                                                      'Images/bell.svg',
                                                                      color: Utility
                                                                          .PutDarkBlueColor(),
                                                                      width: Utility.getDeviceType() ==
                                                                              "1"
                                                                          ? 25
                                                                          : 40,
                                                                      height: Utility.getDeviceType() ==
                                                                              "1"
                                                                          ? 25
                                                                          : 40)),
                                                            ),
                                                          ),
                                                          new Expanded(
                                                            flex: 2,
                                                            child:
                                                                new Container(
                                                              margin: EdgeInsets
                                                                  .fromLTRB(0,
                                                                      5, 0, 0),
                                                              alignment:
                                                                  Alignment
                                                                      .topLeft,
                                                              child: IconButton(
                                                                  onPressed:
                                                                      () {
                                                                    MapUtils.openMap(
                                                                        -3.823216,
                                                                        -38.481700);
                                                                  },
                                                                  icon: SvgPicture.asset(
                                                                      'Images/directions.svg',
                                                                      color: Utility
                                                                          .PutDarkBlueColor(),
                                                                      width: Utility.getDeviceType() ==
                                                                              "1"
                                                                          ? 25
                                                                          : 40,
                                                                      height: Utility.getDeviceType() ==
                                                                              "1"
                                                                          ? 25
                                                                          : 40)),
                                                            ),
                                                          ),
                                                          new Expanded(
                                                            flex: 3,
                                                            child:
                                                                new Container(
                                                              margin: EdgeInsets
                                                                  .fromLTRB(0,
                                                                      5, 0, 0),
                                                              alignment:
                                                                  Alignment
                                                                      .topLeft,
                                                              child: IconButton(
                                                                  onPressed:
                                                                      () {
                                                                    // launch(
                                                                    //     "tel:9687276690");
                                                                    print(
                                                                        "ID Appointment ${listing_Appointment_Details[pos].id}");
                                                                    Navigator.of(
                                                                            context)
                                                                        .push(MaterialPageRoute(
                                                                            builder: (context) =>
                                                                                Videocalling(AppointmentID: listing_Appointment_Details[pos].id)));
                                                                  },
                                                                  icon: SvgPicture.asset(
                                                                      'Images/video_call.svg',
                                                                      color: Utility
                                                                          .PutDarkBlueColor(),
                                                                      width: Utility.getDeviceType() ==
                                                                              "1"
                                                                          ? 25
                                                                          : 55,
                                                                      height: Utility.getDeviceType() ==
                                                                              "1"
                                                                          ? 25
                                                                          : 55)),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  new Expanded(
                                                    flex: 2,
                                                    child: Column(
                                                      children: <Widget>[
                                                        SizedBox(height: 45),
                                                        Container(
                                                          margin: EdgeInsets
                                                              .fromLTRB(
                                                                  5, 0, 0, 0),
                                                          alignment:
                                                              Alignment.topLeft,
                                                          child: Text(
                                                            Utility.translate(
                                                                'status'),
                                                            style: TextStyle(
                                                              fontFamily:
                                                                  'Poppins_Medium',
                                                              fontSize:
                                                                  Utility.getDeviceType() ==
                                                                          '1'
                                                                      ? 12
                                                                      : 22,
                                                              color: Utility
                                                                  .PutDarkBlueColor(),
                                                              fontStyle:
                                                                  FontStyle
                                                                      .normal,
                                                            ),
                                                            maxLines: 1,
                                                          ),
                                                        ),
                                                        Container(
                                                          margin: EdgeInsets
                                                              .fromLTRB(
                                                                  5, 0, 0, 0),
                                                          alignment:
                                                              Alignment.topLeft,
                                                          child: Text(
                                                            AppointmentStatus,
                                                            style: TextStyle(
                                                              fontFamily:
                                                                  'Poppins_Medium',
                                                              fontSize:
                                                                  Utility.getDeviceType() ==
                                                                          '1'
                                                                      ? 12
                                                                      : 22,
                                                              color: Utility
                                                                  .PutLightBlueColor(),
                                                              fontStyle:
                                                                  FontStyle
                                                                      .normal,
                                                            ),
                                                            maxLines: 2,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )),
                      );
                    },
                  ),
                ),
              ),
      ),
    );
  }
}
