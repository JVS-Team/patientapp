import 'dart:developer';

import 'package:flutter_svg/svg.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:softcliniccare/Helper/const_data.dart';
import 'package:softcliniccare/Helper/utils.dart';

import '../Model/Vitals/DesignPortlate.dart';
import '../Model/Vitals/SectionPortlate.dart';
import '../Model/Vitals/UOM_Details.dart';
import '../Model/Vitals/VitalSignAPI.dart';
import '../Model/Vitals/Vital_GetDetails.dart';

class vitals extends StatefulWidget {
  @override
  vitalsState createState() => vitalsState();
}

class vitalsState extends State<vitals> {
  late HospitalData hospitalvalue;
  List<HospitalData> hospitallist = [
    HospitalData.fromJson(
        {"displayName": "29 Nov 2021", "code": "00013", "id": "0"})
  ];

  String checkdevice = '';
  String AccessToken = '';
  String PatientID = '';
  GlobalKey<State> _VitalList = new GlobalKey<State>();

  List<Vital_AllList> listing_Date_Dropdown = [
    Vital_AllList.fromJson({
      "vitalSignRecordingDetails": [],
      "name": "-Select-",
      "displayName": "-Select-"
    })
  ];
  late Vital_AllList _selection_Date_List = listing_Date_Dropdown[0];

  List<DesignDetails> vitalDesignDetails = [];

  List chartALLIDDisplayDesign = [];
  List chartAllSectionID = [];
  List<AllVitalSignRecordingDetails> listingVitalSignRecord = [];

  List<TextFieldClass> txtFieldVitallist = [];

  List<SectionDetails> vitalSectionData = [];
  List<UOMMainDetails> listingUOMDetails = [];

  String UOM1 = "";
  String UOM2 = "";
  String heightUOM = "";
  String BloodPressure1 = "";
  String BloodPressure2 = "";
  String BloodPressureUOM = "";

  DesignPortlate designDetails = new DesignPortlate.fromJson([]);

  @override
  void initState() {
    hospitalvalue = hospitallist[0];
    checkdevice = Utility.getDeviceType();
    super.initState();
    VitalAllUOM_Measurement();
    VitalCheck_New_OR_PAST();
  }

  String _GetDetailsFromID(String ID) {
    String Name = '';
    for (int ko = 0; ko < vitalSectionData.length; ko++) {
      if (ID == vitalSectionData[ko].id) {
        Name = vitalSectionData[ko].displayName;
      }
    }
    return Name;
  }

  void VitalCheck_New_OR_PAST() async {
    AccessToken = await Utility.readAccessToken('Token');
    PatientID = await Utility.readAccessToken('PatientID');
    Utility.showLoadingDialog(context, _VitalList, true);

    Vital_API_CALL()
        .VitalSignCheckDetails(AccessToken, PatientID)
        .then((Response) async {
      setState(() {
        VitalMainData vitalAllDetails = new VitalMainData.fromJson(Response);
        // print(vitalAllDetails.AllVitalListDetails.length);

        if (vitalAllDetails.AllVitalListDetails.length > 0) {
          listing_Date_Dropdown = [];

          for (int i = 0; i < vitalAllDetails.AllVitalListDetails.length; i++) {
            listing_Date_Dropdown.add(Vital_AllList.fromJson(Response[i]));
          }

          listing_Date_Dropdown.sort((a, b) => b.vitalSignChartDate
              .compareTo(a.vitalSignChartDate)); // Soorting Data

          _selection_Date_List = listing_Date_Dropdown[0];

          chartALLIDDisplayDesign = [];
          chartAllSectionID = [];

          listingVitalSignRecord = [];
          listingVitalSignRecord = _selection_Date_List.vitalAll;

          listingVitalSignRecord.sort(
              (a, b) => a.sequence.compareTo(b.sequence)); // Soorting Data

          for (int i = 0; i < listingVitalSignRecord.length; i++) {
            // print(listingVitalSignRecord[i].sequence);
            chartALLIDDisplayDesign
                .add(listingVitalSignRecord[i].vitalSignParameterId);
            chartAllSectionID
                .add(listingVitalSignRecord[i].vitalSignChartSectionId);
          }

          if (chartALLIDDisplayDesign.length == 0) {
            CloseDialogBox();
          } else {
            String allIDDisplay = chartALLIDDisplayDesign.join(',');
            String allSectionID = chartAllSectionID.join(',');
            Vital_GroupSectionName(allSectionID, allIDDisplay);
          }
        } else {
          listing_Date_Dropdown = [];
          CloseDialogBox();
        }
      });
    });
  }

  void CloseDialogBox() {
    Navigator.pop(context);
  }

  void Vital_GroupSectionName(String VitalDesignID, String AllID) async {
    AccessToken = await Utility.readAccessToken('Token');

    Vital_API_CALL()
        .VitalAllSections(AccessToken, VitalDesignID)
        .then((Response) async {
      setState(() {
        // log(Response['value']);

        vitalSectionData = [];
        SectionPortlate sectionDetails =
            new SectionPortlate.fromJson(Response['value']);
        vitalSectionData = sectionDetails.designAll;

        // print(vitalSectionData.length);
        VitlSignPortlateDesign(AllID);
      });
    });
  }

  String _GetVitalMeasurementDetails(String UOMID) {
    String UOMName = '';
    for (int l = 0; l < listingUOMDetails.length; l++) {
      // Unit of measurement
      if (UOMID == listingUOMDetails[l].id) {
        UOMName = listingUOMDetails[l].displayName;
      }
    }
    return UOMName;
  }

  void VitalAllUOM_Measurement() async {
    AccessToken = await Utility.readAccessToken('Token');

    Vital_API_CALL()
        .VitalUnitofMeasurement_UOM(AccessToken)
        .then((Response) async {
      setState(() {
        UOM_Details UOMDetails = new UOM_Details.fromJson(Response['value']);
        listingUOMDetails = UOMDetails.designAll;
      });
    });
  }

  void VitlSignPortlateDesign(String VitalDesignID) async {
    AccessToken = await Utility.readAccessToken('Token');

    Vital_API_CALL()
        .VitalDesignData(AccessToken, VitalDesignID)
        .then((Response) async {
      setState(() {
        designDetails =
            new DesignPortlate.fromJson(Response['value']);
        VitalAllDataDisplay();
        // txtFieldVitallist = [];
        // CloseDialogBox();
        // vitalDesignDetails = designDetails.designAll;
        // List<String> DuplicateID = [];

        // for (int k = 0; k < listingVitalSignRecord.length; k++) {
        //   for (int i = 0; i < designDetails.designAll.length; i++) {
        //     for (int i = 0; i < designDetails.designAll.length; i++) {
        //       if (designDetails.designAll[i].id ==
        //           "30734a5d-b510-4603-a32d-a2c52ee4a58e") {
        //         // print(designDetails.designAll[i].unitofMeasurement);
        //         UOM1 = _GetVitalMeasurementDetails(
        //             designDetails.designAll[i].unitofMeasurement);
        //       } else if (designDetails.designAll[i].id ==
        //           "a4d8a6a8-8e8f-4f50-b591-45f96ee89605") {
        //         // print(designDetails.designAll[i].unitofMeasurement);
        //         UOM2 = _GetVitalMeasurementDetails(
        //             designDetails.designAll[i].unitofMeasurement);
        //         // print(UOM2);
        //       } else if (designDetails.designAll[i].id ==
        //           "9d3e12e4-465c-48c5-b560-420e74bb9978") {
        //         var str_Display = designDetails.designAll[i].displayName;
        //         var parts = str_Display.split(' - ');
        //         BloodPressure1 = parts[1].trim();
        //       } else if (designDetails.designAll[i].id ==
        //           "1e701e5c-9d0b-4bea-aa32-6586fb7f7ae6") {
        //         // print(
        //         //     "Display else if ${designDetails.designAll[i].displayName}");
        //         var str_Display = designDetails.designAll[i].displayName;
        //         var parts = str_Display.split(' - ');
        //         BloodPressure2 = parts[1].trim();
        //       }
        //     }
        //     heightUOM = UOM1 + "/" + UOM2;
        //     BloodPressureUOM = BloodPressure1 + "/" + BloodPressure2;
        //
        //     if (listingVitalSignRecord[k].vitalSignParameterId ==
        //         designDetails.designAll[i].id) {
        //       if (listingVitalSignRecord[k].vitalSignChartSectionId ==
        //           "00000000-0000-0000-0000-000000000000") {
        //         String vitalValue = "";
        //         if (listingVitalSignRecord[k].vitalValue == "undefined" ||
        //             listingVitalSignRecord[k].vitalValue == "NaN") {
        //           vitalValue = "";
        //         } else {
        //           vitalValue = listingVitalSignRecord[k].vitalValue;
        //         }
        //
        //         if (vitalValue == "") {
        //         } else {
        //           txtFieldVitallist.add(TextFieldClass(
        //               designDetails.designAll[i].id,
        //               designDetails.designAll[i].displayName,
        //               _GetVitalMeasurementDetails(
        //                   designDetails.designAll[i].unitofMeasurement),
        //               "0",
        //               vitalValue,
        //               ""));
        //         }
        //       } else {
        //         String valueID =
        //             listingVitalSignRecord[k].vitalSignChartSectionId;
        //         var contain = listingVitalSignRecord.where((food) => food
        //             .vitalSignChartSectionId
        //             .toLowerCase()
        //             .contains(valueID));
        //
        //         bool _isILike = true;
        //         // This is how I iterate
        //         if (DuplicateID.contains(valueID)) {
        //           var contain = listingVitalSignRecord.where(
        //               (element) => element.vitalSignChartSectionId == valueID);
        //
        //           if (contain.isEmpty) {
        //             _isILike = true;
        //           } else {
        //             _isILike = false;
        //             DuplicateID.add(valueID);
        //           }
        //         } else {
        //           DuplicateID.add(valueID);
        //
        //           if (contain.length >= 2) {
        //             String value1 = listingVitalSignRecord[k].vitalValue;
        //             String value2 = listingVitalSignRecord[k + 1].vitalValue;
        //             print(
        //                 "Value ID 1 ${listingVitalSignRecord[k].vitalSignChartSectionId}");
        //             print(
        //                 "Value ID 2 ${listingVitalSignRecord[k + 1].vitalSignChartSectionId}");
        //
        //             String vitalValue1_High = "";
        //             String vitalValue1_Low = "";
        //             String FinalValue = "";
        //
        //             if (value1 == "undefined") {
        //               vitalValue1_High = "";
        //             } else {
        //               vitalValue1_High = value1;
        //             }
        //
        //             if (value2 == "undefined") {
        //               vitalValue1_Low = "";
        //             } else {
        //               vitalValue1_Low = value2;
        //             }
        //
        //             if (vitalValue1_High == "" && vitalValue1_Low == "") {
        //               FinalValue = "";
        //             } else if (vitalValue1_High != "" &&
        //                 vitalValue1_Low == "") {
        //               // FinalValue = vitalValue1_High;
        //               if (listingVitalSignRecord[k].vitalSignChartSectionId ==
        //                   "c77a177c-1fed-489a-aa39-dcc753240fbf") {
        //                 FinalValue = vitalValue1_High + " " + UOM1;
        //               } else if (listingVitalSignRecord[k + 1]
        //                       .vitalSignChartSectionId ==
        //                   "5b8d2c5b-ac9b-4182-bba5-f4715fb1c60b") {
        //                 FinalValue = vitalValue1_High + " " + UOM1;
        //               }
        //             } else if (vitalValue1_High == "" &&
        //                 vitalValue1_Low != "") {
        //               // FinalValue = vitalValue1_Low;
        //               if (listingVitalSignRecord[k].vitalSignChartSectionId ==
        //                   "c77a177c-1fed-489a-aa39-dcc753240fbf") {
        //                 FinalValue = vitalValue1_High + " " + UOM2;
        //               } else if (listingVitalSignRecord[k + 1]
        //                       .vitalSignChartSectionId ==
        //                   "5b8d2c5b-ac9b-4182-bba5-f4715fb1c60b") {
        //                 FinalValue = vitalValue1_High + " " + UOM2;
        //               }
        //             } else {
        //               // FinalValue = vitalValue1_High + "/" + vitalValue1_Low;
        //               if (listingVitalSignRecord[k].vitalSignChartSectionId ==
        //                   "c77a177c-1fed-489a-aa39-dcc753240fbf") {
        //                 FinalValue = vitalValue1_High +
        //                     " " +
        //                     UOM1 +
        //                     " / " +
        //                     vitalValue1_Low +
        //                     " " +
        //                     UOM2;
        //               } else if (listingVitalSignRecord[k + 1]
        //                       .vitalSignChartSectionId ==
        //                   "5b8d2c5b-ac9b-4182-bba5-f4715fb1c60b") {
        //                 FinalValue = vitalValue1_High + "/" + vitalValue1_Low;
        //               }
        //             }
        //
        //             print("Value Here ${FinalValue}");
        //
        //             if (txtFieldVitallist
        //                 .contains(designDetails.designAll[i].id)) {
        //             } else {
        //               print("ID of ${designDetails.designAll[i].id}");
        //               txtFieldVitallist.add(TextFieldClass(
        //                   designDetails.designAll[i].id,
        //                   _GetDetailsFromID(listingVitalSignRecord[k]
        //                       .vitalSignChartSectionId),
        //                   _GetVitalMeasurementDetails(
        //                       designDetails.designAll[i].unitofMeasurement),
        //                   "1",
        //                   FinalValue,
        //                   valueID));
        //             }
        //           }
        //         }
        //       }
        //     }
        //   }
        // }
      });
    });
  }

  void VitalAllDataDisplay() {

    txtFieldVitallist = [];
    CloseDialogBox();
    vitalDesignDetails = designDetails.designAll;
    List<String> DuplicateID = [];

    for (int k = 0; k < listingVitalSignRecord.length; k++) {
      for (int i = 0; i < designDetails.designAll.length; i++) {
        for (int i = 0; i < designDetails.designAll.length; i++) {
          if (designDetails.designAll[i].id ==
              "30734a5d-b510-4603-a32d-a2c52ee4a58e") {
            // print(designDetails.designAll[i].unitofMeasurement);
            UOM1 = _GetVitalMeasurementDetails(
                designDetails.designAll[i].unitofMeasurement);
          } else if (designDetails.designAll[i].id ==
              "a4d8a6a8-8e8f-4f50-b591-45f96ee89605") {
            // print(designDetails.designAll[i].unitofMeasurement);
            UOM2 = _GetVitalMeasurementDetails(
                designDetails.designAll[i].unitofMeasurement);
            // print(UOM2);
          } else if (designDetails.designAll[i].id ==
              "9d3e12e4-465c-48c5-b560-420e74bb9978") {
            var str_Display = designDetails.designAll[i].displayName;
            var parts = str_Display.split(' - ');
            BloodPressure1 = parts[1].trim();
          } else if (designDetails.designAll[i].id ==
              "1e701e5c-9d0b-4bea-aa32-6586fb7f7ae6") {
            // print(
            //     "Display else if ${designDetails.designAll[i].displayName}");
            var str_Display = designDetails.designAll[i].displayName;
            var parts = str_Display.split(' - ');
            BloodPressure2 = parts[1].trim();
          }
        }
        heightUOM = UOM1 + "/" + UOM2;
        BloodPressureUOM = BloodPressure1 + "/" + BloodPressure2;

        if (listingVitalSignRecord[k].vitalSignParameterId ==
            designDetails.designAll[i].id) {
          if (listingVitalSignRecord[k].vitalSignChartSectionId ==
              "00000000-0000-0000-0000-000000000000") {
            String vitalValue = "";
            if (listingVitalSignRecord[k].vitalValue == "undefined" ||
                listingVitalSignRecord[k].vitalValue == "NaN") {
              vitalValue = "";
            } else {
              vitalValue = listingVitalSignRecord[k].vitalValue;
            }

            if (vitalValue == "") {
            } else {
              txtFieldVitallist.add(TextFieldClass(
                  designDetails.designAll[i].id,
                  designDetails.designAll[i].displayName,
                  _GetVitalMeasurementDetails(
                      designDetails.designAll[i].unitofMeasurement),
                  "0",
                  vitalValue,
                  ""));
            }
          } else {
            String valueID = listingVitalSignRecord[k].vitalSignChartSectionId;
            var contain = listingVitalSignRecord.where((food) =>
                food.vitalSignChartSectionId.toLowerCase().contains(valueID));

            bool _isILike = true;
            // This is how I iterate
            if (DuplicateID.contains(valueID)) {
              var contain = listingVitalSignRecord.where(
                  (element) => element.vitalSignChartSectionId == valueID);

              if (contain.isEmpty) {
                _isILike = true;
              } else {
                _isILike = false;
                DuplicateID.add(valueID);
              }
            } else {
              DuplicateID.add(valueID);

              if (contain.length >= 2) {
                String value1 = listingVitalSignRecord[k].vitalValue;
                String value2 = listingVitalSignRecord[k + 1].vitalValue;
                print(
                    "Value ID 1 ${listingVitalSignRecord[k].vitalSignChartSectionId}");
                print(
                    "Value ID 2 ${listingVitalSignRecord[k + 1].vitalSignChartSectionId}");

                String vitalValue1_High = "";
                String vitalValue1_Low = "";
                String FinalValue = "";

                if (value1 == "undefined") {
                  vitalValue1_High = "";
                } else {
                  vitalValue1_High = value1;
                }

                if (value2 == "undefined") {
                  vitalValue1_Low = "";
                } else {
                  vitalValue1_Low = value2;
                }

                if (vitalValue1_High == "" && vitalValue1_Low == "") {
                  FinalValue = "";
                } else if (vitalValue1_High != "" && vitalValue1_Low == "") {
                  // FinalValue = vitalValue1_High;
                  if (listingVitalSignRecord[k].vitalSignChartSectionId ==
                      "c77a177c-1fed-489a-aa39-dcc753240fbf") {
                    FinalValue = vitalValue1_High + " " + UOM1;
                  } else if (listingVitalSignRecord[k + 1]
                          .vitalSignChartSectionId ==
                      "5b8d2c5b-ac9b-4182-bba5-f4715fb1c60b") {
                    FinalValue = vitalValue1_High + " " + UOM1;
                  }
                } else if (vitalValue1_High == "" && vitalValue1_Low != "") {
                  // FinalValue = vitalValue1_Low;
                  if (listingVitalSignRecord[k].vitalSignChartSectionId ==
                      "c77a177c-1fed-489a-aa39-dcc753240fbf") {
                    FinalValue = vitalValue1_High + " " + UOM2;
                  } else if (listingVitalSignRecord[k + 1]
                          .vitalSignChartSectionId ==
                      "5b8d2c5b-ac9b-4182-bba5-f4715fb1c60b") {
                    FinalValue = vitalValue1_High + " " + UOM2;
                  }
                } else {
                  // FinalValue = vitalValue1_High + "/" + vitalValue1_Low;
                  if (listingVitalSignRecord[k].vitalSignChartSectionId ==
                      "c77a177c-1fed-489a-aa39-dcc753240fbf") {
                    FinalValue = vitalValue1_High +
                        " " +
                        UOM1 +
                        " / " +
                        vitalValue1_Low +
                        " " +
                        UOM2;
                  } else if (listingVitalSignRecord[k + 1]
                          .vitalSignChartSectionId ==
                      "5b8d2c5b-ac9b-4182-bba5-f4715fb1c60b") {
                    FinalValue = vitalValue1_High + "/" + vitalValue1_Low;
                  }
                }

                print("Value Here ${FinalValue}");

                if (txtFieldVitallist.contains(designDetails.designAll[i].id)) {
                } else {
                  print("ID of ${designDetails.designAll[i].id}");
                  txtFieldVitallist.add(TextFieldClass(
                      designDetails.designAll[i].id,
                      _GetDetailsFromID(
                          listingVitalSignRecord[k].vitalSignChartSectionId),
                      _GetVitalMeasurementDetails(
                          designDetails.designAll[i].unitofMeasurement),
                      "1",
                      FinalValue,
                      valueID));
                }
              }
            }
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: Utility.TopToolBar(Utility.translate('vitals'),
            checkdevice == "1" ? 20 : 30, Utility.PutDarkBlueColor(), context),
        body: SingleChildScrollView(
          child: Container(
            margin: new EdgeInsets.all(checkdevice == "1" ? 10 : 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 41,
                  margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                  decoration: ShapeDecoration(
                    shape: RoundedRectangleBorder(
                      side: BorderSide(width: 0.2, style: BorderStyle.solid),
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    ),
                  ),
                  padding: EdgeInsets.only(left: 10),
                  child: DropdownButton<Vital_AllList>(
                    value: _selection_Date_List,
                    onChanged: (Vital_AllList? newValue) {
                      setState(() {
                        _selection_Date_List = newValue!;

                        chartALLIDDisplayDesign = [];
                        chartAllSectionID = [];

                        listingVitalSignRecord = [];
                        listingVitalSignRecord = _selection_Date_List.vitalAll;
                        Utility.showLoadingDialog(context, _VitalList, true);

                        listingVitalSignRecord.sort((a, b) =>
                            a.sequence.compareTo(b.sequence)); // Soorting Data

                        for (int i = 0;
                            i < listingVitalSignRecord.length;
                            i++) {
                          // print(listingVitalSignRecord[i].sequence);
                          chartALLIDDisplayDesign.add(
                              listingVitalSignRecord[i].vitalSignParameterId);
                          chartAllSectionID.add(listingVitalSignRecord[i]
                              .vitalSignChartSectionId);
                        }

                        if (chartALLIDDisplayDesign.length == 0) {
                        } else {
                          String allIDDisplay =
                              chartALLIDDisplayDesign.join(',');
                          String allSectionID = chartAllSectionID.join(',');
                          // Vital_GroupSectionName(allSectionID, allIDDisplay);
                          // VitlSignPortlateDesign(allIDDisplay);
                          VitalAllDataDisplay();
                        }
                      });
                    },
                    isExpanded: true,
                    underline: Container(),
                    icon: Padding(
                      //Icon at tail, arrow bottom is default icon
                      padding: EdgeInsets.only(left: 20, right: 10),
                      child: Image.asset(
                        'Images/down_arrow.png',
                        width: 25,
                        height: 25,
                        color: Color(0XffE0E0E0),
                      ),
                    ),
                    items: listing_Date_Dropdown
                        .map<DropdownMenuItem<Vital_AllList>>(
                            (Vital_AllList value) {
                      return DropdownMenuItem<Vital_AllList>(
                        value: value,
                        child: Container(
                          child: Text(
                            value.vitalDateDisplay,
                            overflow: TextOverflow.visible,
                            maxLines: 1,
                            style: TextStyle(color: Colors.black, fontSize: 13),
                          ),
                        ),
                      );
                    }).toList(),
                  ),
                ),
                SizedBox(
                  height: checkdevice == "1" ? 10 : 30,
                ),
                Container(
                    height: MediaQuery.of(context).size.height - 50,
                    child: txtFieldVitallist.length == 0
                        ? Container(
                            child: Center(
                              child: Text('No Vital Found'),
                            ),
                          )
                        : ListView.builder(
                            itemCount: txtFieldVitallist.length,
                            itemBuilder: (context, pos) {
                              if (txtFieldVitallist.isEmpty) {
                                return CircularProgressIndicator();
                              } else {
                                // _controllers.add(new TextEditingController());
                                // _controllers[pos].text =
                                //     txtFieldVitallist[pos].VitalValue;
                                return vitalSignList(pos);
                              }
                            },
                          )),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget vitalSignList(int index) {
    TextFieldClass item = txtFieldVitallist[index];

    return Card(
      margin: EdgeInsets.all(3),
      color: Utility.PutBackgroundCardColor(),
      elevation: 3,
      child: Container(
        padding: EdgeInsets.fromLTRB(15, 5, 10, 10),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Row(
          children: [
            Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      txtFieldVitallist[index].TextFieldName,
                      maxLines: 2,
                      style: TextStyle(
                        fontFamily: "Poppins_Medium",
                        fontSize: 16,
                        color: Utility.PutDarkBlueColor(),
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          txtFieldVitallist[index].VitalValue,
                          style: TextStyle(
                            fontFamily: "Poppins_Medium",
                            fontSize: 16,
                            color: Utility.PutDarkBlueColor(),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 3,
                      ),
                      Container(
                        child: Text(
                          txtFieldVitallist[index].id ==
                                  "30734a5d-b510-4603-a32d-a2c52ee4a58e"
                              ? ""
                              : txtFieldVitallist[index].NameMeasurement,
                          style: TextStyle(
                            fontFamily: "Poppins_Medium",
                            fontSize: 16,
                            color: Utility.PutDarkBlueColor(),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 5,
            ),
            Expanded(
              flex: 1,
              child: Container(),
            ),
            // Flexible(
            //     flex: 1,
            //     child: Center(
            //         child: Container(
            //       width: checkdevice == '1' ? 30 : 50,
            //       height: checkdevice == '1' ? 30 : 50,
            //       child: SvgPicture.asset('Images/GroupBack.svg'),
            //     ))),
          ],
        ),
      ),
    );
  }

// Widget Mainlisting() {
//   return ListView.builder(
//     itemCount: 10,
//     itemBuilder: (BuildContext context, int pos) {
//       return Container(
//         child: Card(
//             elevation: 5,
//             margin: EdgeInsets.all(8),
//             color: Utility.PutBackgroundCardColor(),
//             child: Container(
//               height: 160,
//               child: Column(
//                 children: <Widget>[
//                   new Expanded(
//                     flex: 3,
//                     child: Row(
//                       children: <Widget>[
//                         SizedBox(
//                           width: 10,
//                         ),
//                         new Expanded(
//                             flex: 1,
//                             child: Container(
//                               child: Align(
//                                 alignment: Alignment.topRight,
//                                 child: IconButton(
//                                     onPressed: () {},
//                                     icon: SvgPicture.asset(
//                                       'Images/pdf_icon.svg',
//                                       color: Utility.PutDarkBlueColor(),
//                                       height: 50,
//                                       width: 50,
//                                     ),
//                                     iconSize: 50),
//                               ),
//                             )),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         new Expanded(
//                           flex: 4,
//                           child: Column(
//                             children: <Widget>[
//                               new Expanded(
//                                   flex: 1,
//                                   child: Align(
//                                     alignment: Alignment.centerLeft,
//                                     child: Utility.MainNormalTextWithFont(
//                                         "Facility:  Apollo Orthopedic",
//                                         14,
//                                         Utility.PutDarkBlueColor(),
//                                         'Poppins_Regular'),
//                                   )),
//                               new Expanded(
//                                   flex: 1,
//                                   child: Align(
//                                     alignment: Alignment.centerLeft,
//                                     child: Utility.MainNormalTextWithFont(
//                                         "Service:  Laboratory",
//                                         14,
//                                         Utility.PutDarkBlueColor(),
//                                         'Poppins_Regular'),
//                                   )),
//                               new Expanded(
//                                   flex: 1,
//                                   child: Align(
//                                     alignment: Alignment.centerLeft,
//                                     child: Utility.MainNormalTextWithFont(
//                                         "Doctor:  Alex Logan",
//                                         14,
//                                         Utility.PutDarkBlueColor(),
//                                         'Poppins_Regular'),
//                                   )),
//                               new Expanded(
//                                   flex: 1,
//                                   child: Align(
//                                     alignment: Alignment.centerLeft,
//                                     child: Utility.MainNormalTextWithFont(
//                                         "Amount:  " + '\u{20B9}' + "5000000",
//                                         14,
//                                         Utility.PutDarkBlueColor(),
//                                         'Poppins_Regular'),
//                                   )),
//                             ],
//                           ),
//                         )
//                       ],
//                     ),
//                   ),
//                   SizedBox(
//                     height: 10,
//                   )
//                 ],
//               ),
//             )),
//       );
//     },
//   );
// }
}

class TextFieldClass {
  final String id;
  final String TextFieldName;
  final String NameMeasurement;
  final String Check_Multiple_SameID;
  final String VitalValue;
  final String VitalChartSectionID;

  TextFieldClass(this.id, this.TextFieldName, this.NameMeasurement,
      this.Check_Multiple_SameID, this.VitalValue, this.VitalChartSectionID);
}
