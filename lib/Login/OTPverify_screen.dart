import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:softcliniccare/Helper/API_CALL.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'package:flutter/gestures.dart';
import 'package:softcliniccare/Setting/Familymenber_screen.dart';
import 'dart:async';

import '../Home/main_screen.dart';
import '../Model/Login/Login_API.dart';
import '../Model/Login/OTP_Verify_Model.dart';
import '../Setting/Hospitallist_screen.dart';

class OTPVerifyPage extends StatefulWidget {
  OTPVerifyPage({Key? key, required this.mobileno}) : super(key: key);
  String mobileno;

  @override
  OTPVerifyPageState createState() => OTPVerifyPageState();
}

class OTPVerifyPageState extends State<OTPVerifyPage> {
  TextEditingController txtemail = new TextEditingController();
  TextEditingController txtpassword = new TextEditingController();
  GlobalKey<State> _keyLoaderOTPVerify = new GlobalKey<State>();
  String checkdevice = '';
  late Timer _timer;
  int _start = 60;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  @override
  void initState() {
    checkdevice = Utility.getDeviceType();
    startTimer();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _timer.cancel();
    super.dispose();
  }

  // void HospitalName_To_AllID(
  //     String HospitalID, List<HospotalList> hospital_List) async {
  //   API_LoginAPI().MainHospitalName(HospitalID).then((Response) async {
  //     setState(() {
  //       String FinalHospitalName = Response['name'];
  //       // log(FinalHospitalName);
  //
  //       for (int i = 0; i < hospital_List.length; i++) {
  //         if (hospital_List[i].tenantId == HospitalID) {
  //           print(Response['name']);
  //           hospital_List[i].HospitalName = FinalHospitalName;
  //           print('Hostpital Name Main ${hospital_List[i].HospitalName}');
  //         }
  //       }
  //     });
  //   });
  // }

  void FinalAccessToken(
      String PatientID, String TenantID, String MobileNo) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("PatientID", PatientID);
    prefs.setString("HospitalTenantID", TenantID);

    API_CALL()
        .VerifyMobileOTPCall_FinalToken(MobileNo, PatientID, TenantID)
        .then((Response) async {
      Map FinalResponse = json.decode(Response);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("Token", FinalResponse['Accesstoken']);

      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => MainScreen()),
      );
    });
  }

  void SaveHospitalList(String hospital_List) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("OTPVerifyResponse", hospital_List);
  }

  @override
  Widget build(BuildContext context) {
    const color = const Color(0xff0073d2);
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(children: [
            Container(
              child: Column(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height / 3,
                    width: MediaQuery.of(context).size.width - 80,
                    child: Align(
                      alignment: Alignment.center,
                      child: Image(
                          image: AssetImage('Images/softclinic_logo.png')),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.all(checkdevice == "1" ? 7 : 15),
                      child: Center(
                        child: Text(
                            '${Utility.translate('send_otp')} ${widget.mobileno[0]}${widget.mobileno[1]}${widget.mobileno[2]}XXXXX${widget.mobileno[8]}${widget.mobileno[9]}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: 'Poppins_Regular',
                              fontSize: checkdevice == "1" ? 18 : 28,
                              color: Utility.PutDarkBlueColor(),
                              fontStyle: FontStyle.normal,
                            )),
                      )),
                  Container(
                    padding: EdgeInsets.fromLTRB(
                        0, 0, checkdevice == "1" ? 15 : 50, 0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: RichText(
                        text: TextSpan(children: [
                          TextSpan(
                              text: Utility.translate('change_no'),
                              style: TextStyle(
                                color: Utility.PutLightBlueColor(),
                                fontSize: checkdevice == "1" ? 14 : 24,
                                fontFamily: 'Poppins_Regular',
                              ),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  Navigator.pop(context);
                                }),
                        ]),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: checkdevice == "1" ? 30 : 40,
            ),
            Container(
              padding: EdgeInsets.all(checkdevice == "1" ? 15 : 20),
              width: checkdevice == "1"
                  ? MediaQuery.of(context).size.width - 70
                  : MediaQuery.of(context).size.width - 120,
              height: checkdevice == "1" ? 80 : 70,
              decoration: BoxDecoration(
                  color: Color.fromRGBO(242, 242, 242, 1),
                  borderRadius: BorderRadius.all(
                      Radius.circular(checkdevice == "1" ? 25 : 35))),
              // child: TextField(
              //   autofillHints: [AutofillHints.oneTimeCode],
              // ),
              child: PinCodeTextField(
                length: 6,
                autoFocus: true,
                obscureText: false,
                animationType: AnimationType.fade,
                cursorColor: Colors.black,
                keyboardType: TextInputType.number,
                pinTheme: PinTheme(
                    shape: PinCodeFieldShape.underline,
                    activeFillColor: Colors.transparent,
                    inactiveFillColor: Colors.transparent,
                    inactiveColor: Colors.black,
                    activeColor: Colors.black,
                    selectedFillColor: Colors.transparent,
                    selectedColor: Colors.black),
                animationDuration: Duration(milliseconds: 500),
                backgroundColor: Colors.transparent,
                enableActiveFill: true,
                appContext: context,
                onCompleted: (pin) {
                  print("Completed ${pin}");

                  setState(() {
                    _start = 0;
                    _timer.cancel();
                  });
                  Utility.showLoadingDialog(context, _keyLoaderOTPVerify, true);

                  API_CALL()
                      .VerifyMobileOTPCall_TemporaryToken(widget.mobileno, pin)
                      .then((Response) async {
                    if (Response == "Error") {
                      Navigator.pop(context);
                      Fluttertoast.showToast(
                          msg: "Incorrect OTP",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 16.0);
                    } else {
                      Map FinalResponse = json.decode(Response);

                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      prefs.setString(
                          "TokenTemporary", FinalResponse['Accesstoken']);

                      API_CALL()
                          .VerifyMobileOTPCall(widget.mobileno, pin)
                          .then((Response) {
                        log('OTP Verify Response Final =========  ${Response}');
                        if (Response == "Error") {
                          Navigator.pop(context);
                          Fluttertoast.showToast(
                              msg: "Incorrect OTP",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0);
                        } else {
                          Navigator.pop(context);
                          prefs.setString("MobileNo", widget.mobileno);
                          prefs.setString("PinNo", pin);
                          // Map Data = json.decode(Response);
                          // print(Data["item1"]);
                          OTP_Verify familyDetails =
                              new OTP_Verify.fromJson(json.decode(Response));

                          SaveHospitalList(Response);

                          // Map Data = json.decode(Response);
                          // Utility.SaveAllLangData(
                          //     'Accesstoken', Data['Accesstoken']);
                          String MobileNo = widget.mobileno;

                          print(
                              "Main Family ${familyDetails.designAll.length}");
                          // print(familyDetails.designAll);

                          // if (familyDetails.designAll.length > 1) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => FamilymemberPage(
                                    value: 0,
                                    mobileno: MobileNo,
                                    familyDetails: familyDetails)),
                          );
                          // } else if (familyDetails.designAll.length == 1) {
                          //   print(
                          //       'Hospital Length ${familyDetails.designAll[0].hospital_List.length}');
                          //   List<HospotalList> hospital_List =
                          //       familyDetails.designAll[0].hospital_List;
                          //
                          //   for (int i = 0; i < hospital_List.length; i++) {
                          //     HospitalName_To_AllID(
                          //         hospital_List[i].tenantId, hospital_List);
                          //   }
                          //
                          //   Future.delayed(const Duration(milliseconds: 500),
                          //       () {
                          //     setState(() {
                          //       if (hospital_List.length == 1) {
                          //         print('All Lengtjh');
                          //         FinalAccessToken(
                          //             hospital_List[0].patientId,
                          //             hospital_List[0].tenantId,
                          //             widget.mobileno);
                          //       } else if (hospital_List.length > 1) {
                          //         Future.delayed(
                          //             const Duration(milliseconds: 1000), () {
                          //           setState(() {
                          //             print('Navigation');
                          //             String MobileNoMain = widget.mobileno;
                          //             Navigator.push(
                          //               context,
                          //               MaterialPageRoute(
                          //                   builder: (context) =>
                          //                       HospitallistPage(
                          //                           value: 0,
                          //                           All_hospital_List:
                          //                               hospital_List,
                          //                           mobileno: MobileNoMain)),
                          //             );
                          //           });
                          //         });
                          //       }
                          //     });
                          //   });
                          // }
                        }
                      });
                    }
                  });
                },
                onChanged: (value) {
                  print(value);
                  setState(() {
                    // currentText = value;
                  });
                },
                beforeTextPaste: (text) {
                  print("Allowing to paste $text");
                  //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                  //but you can show anything you want here, like your pop up saying wrong paste format or etc
                  return true;
                },
              ),
              // child: OTPTextField(
              //   length: 6,
              //   style: TextStyle(fontSize: checkdevice == "1" ? 17 : 27),
              //   textFieldAlignment: MainAxisAlignment.spaceAround,
              //   fieldStyle: FieldStyle.underline,
              //   onChanged: (pin) {},
              //   onCompleted: (pin) {
              //     print("Completed: " + pin);
              //     setState(() {
              //       _start = 0;
              //       _timer.cancel();
              //     });
              //     Utility.showLoadingDialog(context, _keyLoaderOTPVerify, true);
              //
              //     API_CALL()
              //         .VerifyMobileOTPCall_TemporaryToken(widget.mobileno, pin)
              //         .then((Response) async {
              //       if (Response == "Error") {
              //         Navigator.pop(context);
              //         Fluttertoast.showToast(
              //             msg: "Incorrect OTP",
              //             toastLength: Toast.LENGTH_SHORT,
              //             gravity: ToastGravity.CENTER,
              //             timeInSecForIosWeb: 1,
              //             backgroundColor: Colors.red,
              //             textColor: Colors.white,
              //             fontSize: 16.0);
              //       } else {
              //         Map FinalResponse = json.decode(Response);
              //
              //         SharedPreferences prefs =
              //             await SharedPreferences.getInstance();
              //         prefs.setString(
              //             "TokenTemporary", FinalResponse['Accesstoken']);
              //
              //         API_CALL()
              //             .VerifyMobileOTPCall(widget.mobileno, pin)
              //             .then((Response) {
              //           log('OTP Verify Response Final =========  ${Response}');
              //           if (Response == "Error") {
              //             Navigator.pop(context);
              //             Fluttertoast.showToast(
              //                 msg: "Incorrect OTP",
              //                 toastLength: Toast.LENGTH_SHORT,
              //                 gravity: ToastGravity.CENTER,
              //                 timeInSecForIosWeb: 1,
              //                 backgroundColor: Colors.red,
              //                 textColor: Colors.white,
              //                 fontSize: 16.0);
              //           } else {
              //             Navigator.pop(context);
              //             prefs.setString("MobileNo", widget.mobileno);
              //             prefs.setString("PinNo", pin);
              //             // Map Data = json.decode(Response);
              //             // print(Data["item1"]);
              //             OTP_Verify familyDetails =
              //                 new OTP_Verify.fromJson(json.decode(Response));
              //
              //             SaveHospitalList(Response);
              //
              //             // Map Data = json.decode(Response);
              //             // Utility.SaveAllLangData(
              //             //     'Accesstoken', Data['Accesstoken']);
              //             String MobileNo = widget.mobileno;
              //
              //             print("Main Family ${familyDetails.designAll.length}");
              //             // print(familyDetails.designAll);
              //
              //             // if (familyDetails.designAll.length > 1) {
              //             Navigator.push(
              //               context,
              //               MaterialPageRoute(
              //                   builder: (context) => FamilymemberPage(
              //                       value: 0,
              //                       mobileno: MobileNo,
              //                       familyDetails: familyDetails)),
              //             );
              //             // } else if (familyDetails.designAll.length == 1) {
              //             //   print(
              //             //       'Hospital Length ${familyDetails.designAll[0].hospital_List.length}');
              //             //   List<HospotalList> hospital_List =
              //             //       familyDetails.designAll[0].hospital_List;
              //             //
              //             //   for (int i = 0; i < hospital_List.length; i++) {
              //             //     HospitalName_To_AllID(
              //             //         hospital_List[i].tenantId, hospital_List);
              //             //   }
              //             //
              //             //   Future.delayed(const Duration(milliseconds: 500),
              //             //       () {
              //             //     setState(() {
              //             //       if (hospital_List.length == 1) {
              //             //         print('All Lengtjh');
              //             //         FinalAccessToken(
              //             //             hospital_List[0].patientId,
              //             //             hospital_List[0].tenantId,
              //             //             widget.mobileno);
              //             //       } else if (hospital_List.length > 1) {
              //             //         Future.delayed(
              //             //             const Duration(milliseconds: 1000), () {
              //             //           setState(() {
              //             //             print('Navigation');
              //             //             String MobileNoMain = widget.mobileno;
              //             //             Navigator.push(
              //             //               context,
              //             //               MaterialPageRoute(
              //             //                   builder: (context) =>
              //             //                       HospitallistPage(
              //             //                           value: 0,
              //             //                           All_hospital_List:
              //             //                               hospital_List,
              //             //                           mobileno: MobileNoMain)),
              //             //             );
              //             //           });
              //             //         });
              //             //       }
              //             //     });
              //             //   });
              //             // }
              //           }
              //         });
              //       }
              //     });
              //   },
              // ),
            ),
            SizedBox(
              height: checkdevice == "1" ? 40 : 50,
            ),
            Container(
                child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: _start == 0
                      ? RichText(
                          text: TextSpan(children: [
                            TextSpan(
                              text: Utility.translate('did_not_receice'),
                              style: TextStyle(
                                  color: Colors.black26,
                                  fontSize: checkdevice == "1" ? 16 : 26,
                                  fontFamily: 'Poppins_Medium'),
                            ),
                            TextSpan(
                                text: Utility.translate('req_again'),
                                style: TextStyle(
                                  color: Utility.PutDarkBlueColor(),
                                  fontSize: checkdevice == "1" ? 16 : 26,
                                  fontFamily: 'Poppins_Medium',
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Utility.showLoadingDialog(
                                        context, _keyLoaderOTPVerify, true);
                                    API_CALL()
                                        .MobileOTPCall(widget.mobileno)
                                        .then((Response) {
                                      log('Login Response =========     ${Response}');
                                      if (Response == "Error") {
                                        Utility.ToastMessage('');
                                      } else {
                                        Navigator.pop(context); //pop dialog
                                        setState(() {
                                          _start = 60;
                                          startTimer();
                                        });
                                      }
                                    });
                                  }),
                          ]),
                        )
                      : Container(
                          child: RichText(
                            text: TextSpan(
                              text:
                                  "${Utility.translate('wait_for')} ${_start} ${Utility.translate('second')}",
                              style: TextStyle(
                                  color: Utility.PutDarkBlueColor(),
                                  fontSize: checkdevice == "1" ? 16 : 26,
                                  fontFamily: 'Poppins_Medium'),
                            ),
                          ),
                        ),
                ),
              ],
            )),
          ]),
        ),
      ),
    );
  }
}
