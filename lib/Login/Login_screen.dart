import 'dart:convert';
import 'dart:developer';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:softcliniccare/Helper/API_CALL.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'package:softcliniccare/Login/OTPverify_screen.dart';
import 'package:softcliniccare/Setting/Familymenber_screen.dart';
import 'package:url_launcher/url_launcher.dart';

import '../Helper/Constant_Data.dart';
import '../Model/Login/OTP_Verify_Model.dart';
import '../Website_Display/WebDisplay.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key, required this.isLogoutClick}) : super(key: key);
  bool isLogoutClick;

  @override
  LoginState createState() => LoginState();
}

class LoginState extends State<LoginPage> {
  TextEditingController txtemail = new TextEditingController();
  TextEditingController txt_MobileNo = new TextEditingController();
  GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String checkdevice = '';
  String mySelection = '+91';

  String MainMobileNo = '';
  String MainPin = '';
  bool DirectOTPSend = false;

  final LocalAuthentication auth = LocalAuthentication();

  String _authorized = 'Not Authorized';
  bool _isAuthenticating = false;
  bool checkUserNamePasswordEnter_Not = false;

  // bool _load = false;

  Future<String> getSWData() async {
    String jsonString = await rootBundle.loadString('lang/CountryCodes.json');
    setState(() {
      Utility.CountryCode = json.decode(jsonString);
    });
    print(Utility.CountryCode);

    return "Sucess";
  }

  @override
  void initState() {
    // txt_MobileNo.text = '9998068755';
    checkdevice = Utility.getDeviceType();
    this.getSWData();
    // TODO: implement initState
    MobileAndOTPSave();
  }

  void MobileAndOTPSave() async {
    MainMobileNo = await Utility.readAccessToken('MobileNo');
    MainPin = await Utility.readAccessToken('PinNo');

    if (MainMobileNo == '' || MainMobileNo == "null") {
      DirectOTPSend = false;
      checkUserNamePasswordEnter_Not = true;
    } else {
      txt_MobileNo.text = MainMobileNo;
      checkUserNamePasswordEnter_Not = false;
      DirectOTPSend = true;

      if (widget.isLogoutClick) {
      } else {
        Future.delayed(const Duration(seconds: 1), () {
          setState(() {
            print('Navigation Click');
            _authenticateWithBiometrics();
          });
        });
      }
    }
  }

  Future<void> _authenticateWithBiometrics() async {
    bool authenticated = false;
    try {
      setState(() {
        _isAuthenticating = true;
        _authorized = 'Authenticating';
      });
      authenticated = await auth.authenticate(
        localizedReason:
            'Scan your fingerprint (or face or whatever) to authenticate',
        options: const AuthenticationOptions(
            stickyAuth: false, biometricOnly: true, useErrorDialogs: true),
      );
      setState(() {
        _isAuthenticating = false;
        _authorized = 'Authenticating';
      });
    } on PlatformException catch (e) {
      print(e);
      setState(() {
        Utility.WholeAppSnackbar(
            context, 'Device has not configure any biometrics.');
        _isAuthenticating = false;
        _authorized = 'Error - ${e.message}';
        print("Error of ${e.code}");
      });
      return;
    }
    if (!mounted) {
      return;
    }

    final String message = authenticated ? 'Authorized' : 'Not Authorized';
    setState(() {
      _authorized = message;
      print("Authentication Done - ${_authorized}");

      if (_authorized == "Authorized") {
        // _LoginClick(txt_EmailID.text, txt_Password.text);
        LoginClick();
      }
    });
  }

  void LoginClick() {
    if (txt_MobileNo.text.length == 0) {
      log("Enter Error");
      Utility.WholeAppSnackbar(context, 'Please Enter your Mobile No');
    } else {
      log("Enter Call " + txt_MobileNo.text);
      // Navigator.push(
      //   context,
      //   MaterialPageRoute(
      //       builder: (context) =>
      //           OTPVerifyPage(mobileno: txt_MobileNo.text)),
      // );
      if (txt_MobileNo.text == MainMobileNo) {
        DirectOTPSend = true;
      } else {
        DirectOTPSend = false;
      }
      log("Enter OTP ${DirectOTPSend}");
      if (DirectOTPSend) {
        DirectMobileNoOTPVerify();
      } else {
        Utility.showLoadingDialog(context, _keyLoader, true);
        API_CALL().MobileOTPCall(txt_MobileNo.text).then((Response) {
          log('Login Response =========     ${Response}');
          if (Response == "Error") {
            Navigator.pop(context);
            Utility.ToastMessage('Mobile Number is not Register');
          } else {
            Navigator.pop(context);
            Utility.ToastMessage('OTP Send Sucessfully');
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      OTPVerifyPage(mobileno: txt_MobileNo.text)),
            );
          }
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    const color = const Color(0xff0073d2);
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Column(children: [
          Expanded(
            child: Container(
              height: MediaQuery.of(context).size.height / 3,
              width: MediaQuery.of(context).size.width - 80,
              child: Align(
                alignment: Alignment.center,
                child: Image(image: AssetImage('Images/softclinic_logo.png')),
              ),
            ),
          ),
          Container(
            height: checkdevice == "1" ? 45 : 75,
            padding: EdgeInsets.fromLTRB(checkdevice == "1" ? 15 : 40, 0,
                checkdevice == "1" ? 15 : 40, 0),
            child: Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1.0, color: Colors.black26),
                ),
                color: Colors.transparent,
              ),
              child: _buildPhonefiled(),
            ),
          ),
          SizedBox(
            height: 40,
          ),
          Expanded(
              child: Column(
            children: [
              Container(
                height: checkdevice == "1" ? 50 : 70,
                padding: EdgeInsets.fromLTRB(checkdevice == "1" ? 15 : 40, 0,
                    checkdevice == "1" ? 15 : 40, 0),
                width: double.infinity,
                child: TextButton(
                  style: TextButton.styleFrom(
                    shape: StadiumBorder(),
                    shadowColor: Colors.black,
                    textStyle: TextStyle(fontSize: 20),
                    primary: Colors.white,
                    backgroundColor: Utility.PutDarkBlueColor(),
                    onSurface: color,
                  ),
                  onPressed: () async {
                    LoginClick();

                    // if (txt_MobileNo.text.length == 0) {
                    //   log("Enter Error");
                    //   Utility.WholeAppSnackbar(
                    //       context, 'Please Enter your Mobile No');
                    // } else {
                    //   log("Enter Call " + txt_MobileNo.text);
                    //   // Navigator.push(
                    //   //   context,
                    //   //   MaterialPageRoute(
                    //   //       builder: (context) =>
                    //   //           OTPVerifyPage(mobileno: txt_MobileNo.text)),
                    //   // );
                    //   if (txt_MobileNo.text == MainMobileNo) {
                    //     DirectOTPSend = true;
                    //   } else {
                    //     DirectOTPSend = false;
                    //   }
                    //   log("Enter OTP ${DirectOTPSend}");
                    //   if (DirectOTPSend) {
                    //     DirectMobileNoOTPVerify();
                    //   } else {
                    //     Utility.showLoadingDialog(context, _keyLoader, true);
                    //     API_CALL()
                    //         .MobileOTPCall(txt_MobileNo.text)
                    //         .then((Response) {
                    //       log('Login Response =========     ${Response}');
                    //       if (Response == "Error") {
                    //         Navigator.pop(context);
                    //         Utility.ToastMessage(
                    //             'Mobile Number is not Register');
                    //       } else {
                    //         Navigator.pop(context);
                    //         Utility.ToastMessage('OTP Send Sucessfully');
                    //         Navigator.push(
                    //           context,
                    //           MaterialPageRoute(
                    //               builder: (context) => OTPVerifyPage(
                    //                   mobileno: txt_MobileNo.text)),
                    //         );
                    //       }
                    //     });
                    //   }
                    // }
                  },
                  child: Text(
                    Utility.translate('login'),
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Poppins_Regular',
                        fontSize: checkdevice == "1" ? 18 : 28),
                  ),
                ),
              ),
              SizedBox(
                height: checkdevice == "1" ? 10 : 20,
              ),
              checkUserNamePasswordEnter_Not == false
                  ? Container(
                      child: Text("or"),
                    )
                  : Container(),
              checkUserNamePasswordEnter_Not == false
                  ? SizedBox(
                      height: checkdevice == "1" ? 10 : 20,
                    )
                  : Container(),
              checkUserNamePasswordEnter_Not == false
                  ? Container(
                      child: InkWell(
                        onTap: () {
                          _authenticateWithBiometrics();
                        },
                        child: Center(
                          child: Align(
                            alignment: Alignment.center,
                            child: Image(
                              image: AssetImage('Images/face_id.png'),
                              height: 50,
                              width: 50,
                            ),
                          ),
                        ),
                      ),
                    )
                  : Container(),
              SizedBox(
                height: checkdevice == "1" ? 10 : 20,
              ),
              Container(
                padding: EdgeInsets.fromLTRB(checkdevice == "1" ? 15 : 40, 0,
                    checkdevice == "1" ? 15 : 40, 0),
                child: Center(
                  child: RichText(
                    text: TextSpan(children: [
                      // TextSpan(
                      //     text: Utility.translate('terms1'),
                      //     style: TextStyle(
                      //         color: Utility.PutDarkBlueColor(),
                      //         fontSize: checkdevice == "1" ? 15 : 25,
                      //         fontFamily: 'Poppins_Regular'),
                      //     recognizer: TapGestureRecognizer()
                      //       ..onTap = () async {}),
                      TextSpan(
                          text: Utility.translate('terms2'),
                          style: TextStyle(
                              color: Utility.PutDarkBlueColor(),
                              fontSize: checkdevice == "1" ? 15 : 25,
                              fontFamily: 'Poppins_Regular',
                              decoration: TextDecoration.underline),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () async {
                              // var url =
                              //     'https://www.softclinicsoftware.com/terms-policy/';
                              // if (await canLaunch(url)) {
                              //   await launch(url);
                              // } else {
                              //   throw 'Could not launch $url';
                              // }

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => WebViewContainer(
                                          "https://www.softclinicsoftware.com/terms-policy/")));
                            }),
                      TextSpan(
                          text: Utility.translate('terms_and'),
                          style: TextStyle(
                              color: Utility.PutDarkBlueColor(),
                              fontSize: checkdevice == "1" ? 15 : 25,
                              fontFamily: 'Poppins_Regular'),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () async {}),
                      TextSpan(
                          text: Utility.translate('terms4'),
                          style: TextStyle(
                              color: Utility.PutDarkBlueColor(),
                              fontSize: checkdevice == "1" ? 15 : 25,
                              fontFamily: 'Poppins_Regular',
                              decoration: TextDecoration.underline),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () async {
                              // var url =
                              //     'https://www.softclinicsoftware.com/disclaimer/';
                              // if (await canLaunch(url)) {
                              //   await launch(url);
                              // } else {
                              //   throw 'Could not launch $url';
                              // }
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => WebViewContainer(
                                          "https://www.softclinicsoftware.com/disclaimer/")));
                            }),
                    ]),
                  ),
                ),
              ),
              Container(
                child: Center(
                    child: Text(
                  "Version ${ConstantDetails.vAppVersionNo}",
                  style: TextStyle(
                      color: Utility.PutLightBlueColor(),
                      fontSize: checkdevice == "1" ? 15 : 25,
                      fontFamily: 'Poppins_Regular'),
                )),
              )
            ],
          )),
          // new Align(
          //   child: loadingIndicator,
          //   alignment: FractionalOffset.center,
          // ),
        ]),
      ),
    );
  }

  void DirectMobileNoOTPVerify() {
    print(MainMobileNo);
    print(MainPin);
    Utility.showLoadingDialog(context, _keyLoader, true);
    API_CALL()
        .VerifyMobileOTPCall_TemporaryToken(MainMobileNo, MainPin)
        .then((Response) async {
      log("Response Temp ${Response}");
      if (Response == "Error") {
        Navigator.pop(context);
        Utility.WholeAppSnackbar(context, 'Error coming from API Side');
        DirectOTPSend = false;
        MainMobileNo = "";
        // Fluttertoast.showToast(
        //     msg: "Incorrect OTP",
        //     toastLength: Toast.LENGTH_SHORT,
        //     gravity: ToastGravity.CENTER,
        //     timeInSecForIosWeb: 1,
        //     backgroundColor: Colors.red,
        //     textColor: Colors.white,
        //     fontSize: 16.0);
      } else {
        Map FinalResponse = json.decode(Response);

        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("TokenTemporary", FinalResponse['Accesstoken']);
        API_CALL()
            .VerifyMobileOTPCall(MainMobileNo, MainPin)
            .then((Response) async {
          log('OTP Verify Response Login =========  ${Response}');
          Navigator.pop(context);
          if (Response == "Error") {
            DirectOTPSend = false;
            MainMobileNo = "";
            // Fluttertoast.showToast(
            //     msg: "Incorrect OTP",
            //     toastLength: Toast.LENGTH_SHORT,
            //     gravity: ToastGravity.CENTER,
            //     timeInSecForIosWeb: 1,
            //     backgroundColor: Colors.red,
            //     textColor: Colors.white,
            //     fontSize: 16.0);
          } else {
            OTP_Verify familyDetails =
                new OTP_Verify.fromJson(json.decode(Response));

            // SaveHospitalList(Response);

            // Map Data = json.decode(Response);
            // Utility.SaveAllLangData(
            //     'Accesstoken', Data['Accesstoken']);

            print("Main Familur Men ${familyDetails.designAll.length}");
            // print(familyDetails.designAll);

            // if (familyDetails.designAll.length > 1) {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => FamilymemberPage(
                      value: 0,
                      mobileno: MainMobileNo,
                      familyDetails: familyDetails)),
            );
            // } else if (familyDetails.designAll.length == 1) {
            //   print(
            //       'Hospital Length ${familyDetails.designAll[0].hospital_List.length}');
            //   List<HospotalList> hospital_List =
            //       familyDetails.designAll[0].hospital_List;
            //
            //   for (int i = 0; i < hospital_List.length; i++) {
            //     HospitalName_To_AllID(
            //         hospital_List[i].tenantId, hospital_List);
            //   }
            //
            //   Future.delayed(const Duration(milliseconds: 500),
            //           () {
            //         setState(() {
            //           if (hospital_List.length == 1) {
            //             print('All Lengtjh');
            //             FinalAccessToken(
            //                 hospital_List[0].patientId,
            //                 hospital_List[0].tenantId,
            //                 widget.mobileno);
            //           } else if (hospital_List.length > 1) {
            //             Future.delayed(
            //                 const Duration(milliseconds: 1000), () {
            //               setState(() {
            //                 print('Navigation');
            //                 String MobileNoMain = widget.mobileno;
            //                 Navigator.push(
            //                   context,
            //                   MaterialPageRoute(
            //                       builder: (context) =>
            //                           HospitallistPage(
            //                               value: 0,
            //                               All_hospital_List:
            //                               hospital_List,
            //                               mobileno: MobileNoMain)),
            //                 );
            //               });
            //             });
            //           }
            //         });
            //       });
            // }
          }
        });
      }
    });
  }

  Widget _buildPhonefiled() {
    return Row(
      children: <Widget>[
        Container(
          width: checkdevice == "1" ? 60 : 80,
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              iconSize: 0.0,
              items: Utility.CountryCode.map((item) {
                return new DropdownMenuItem(
                  child: new Text(item['dial_code'],
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: checkdevice == "1" ? 15 : 25,
                          fontFamily: 'Poppins_Regular')),
                  value: item['dial_code'].toString(),
                );
              }).toList(),
              onChanged: (newVal) {
                setState(() {
                  mySelection = newVal.toString();
                });
              },
              value: mySelection,
            ),
          ),
        ),
        SvgPicture.asset(
          'Images/down-arrow.svg',
          color: Colors.black,
          fit: BoxFit.scaleDown,
          width: checkdevice == "1" ? 8 : 16,
          height: checkdevice == "1" ? 8 : 16,
        ),
        SizedBox(
          width: 2,
        ),
        Container(
          height: checkdevice == "1" ? 30 : 50,
          width: checkdevice == "1" ? 1 : 2,
          color: Colors.black26,
          margin: const EdgeInsets.only(left: 10.0, right: 10.0),
        ),
        SizedBox(
          width: 8,
        ),
        new Expanded(
          child: TextField(
            controller: txt_MobileNo,
            style: TextStyle(
                color: Colors.black,
                fontFamily: 'Poppins_Regular',
                fontSize: checkdevice == "1" ? 16 : 26),
            keyboardType: TextInputType.number,
            decoration: InputDecoration.collapsed(
                hintText: Utility.translate('your_mob_hint')),
          ),
          flex: 5,
        ),
      ],
    );
  }
}
