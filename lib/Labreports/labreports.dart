import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter_svg/svg.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share_plus/share_plus.dart';
import 'package:softcliniccare/Helper/utils.dart';

import '../Helper/Constant_Data.dart';
import '../Helper/Database/sqflite_helper.dart';
import '../Invoice/PDF/FileProcess.dart';
import '../Invoice/PDF/PDF_File_Display.dart';
import '../Model/Invoice/API_PaymentHistory.dart';
import '../Model/Invoice/Invoice_Details.dart';
import '../Model/Laboratory/InventoryName.dart';
import '../Model/Laboratory/Lab_API.dart';
import '../Model/Laboratory/Lab_Details.dart';
import '../Model/Laboratory/Lab_Report_Model.dart';

class labreports extends StatefulWidget {
  @override
  labreportsState createState() => labreportsState();
}

class labreportsState extends State<labreports> {
  List iamges = [];
  int selectedCard = -1;
  String checkdevice = '';

  List<LabListDetails> listing_Lab = [];
  List<inventoryName> lst_inventoryName = [];

  @override
  void initState() {
    checkdevice = Utility.getDeviceType();
    ListofReportSections();
    super.initState();
  }

  void ListofReportSections() async {
    String PatientID = await Utility.readAccessToken('PatientID');

    API_LabList().Lab_List_All(PatientID).then((mainResponse) async {
      setState(() {
        log("Lab Report ${mainResponse}");
        listing_Lab = [];
        var resBody = json.decode(mainResponse);

        LabAllListData labListingDetails = new LabAllListData.fromJson(resBody);
        listing_Lab = labListingDetails.labListing;
        // log("Lab Report ${listing_Lab.length}");

        String NameInventory = "";
        List vInvestigationID = [];

        for (int i = 0; i < listing_Lab.length; i++) {
          for (int j = 0; j < listing_Lab[i].inventoryListing.length; j++) {
            print("Name ${listing_Lab[i].inventoryListing[j].investigationId}");
            vInvestigationID
                .add(listing_Lab[i].inventoryListing[j].investigationId);
          }
        }

        String allIDDisplay = vInvestigationID.join(',');

        API_LabList()
            .inventoryListingName(allIDDisplay)
            .then((mainResponse) async {
          setState(() {
            log("Lab Report ${mainResponse}");
            var resBody = json.decode(mainResponse);
            var tempdata = resBody['value'];
            for (int i = 0; i < tempdata.length; i++) {
              lst_inventoryName.add(inventoryName.fromJson(tempdata[i]));
            }

            log("Lab Name ${lst_inventoryName.length}");
          });
        });
      });
    });
  }

  void GetDownloadandShare(String PDFID, String view_share) async {
    final data = await SQLHelper.getAll_PDF_Response(PDFID, "LabReport");
    List<Map<String, dynamic>> allPDFResponse = data;
    String mainPDFResponse = "";

    if (allPDFResponse.length > 0) {
      for (int i = 0; i < allPDFResponse.length; i++) {
        if (allPDFResponse[i]['invoiceID'] == PDFID) {
          mainPDFResponse = allPDFResponse[i]['invoiceResponse'];
          break;
        } else {}
      }
      if (mainPDFResponse == "") {
        API_PaymentList().getPDF_ofLabReport(PDFID).then((Response) async {
          if (allPDFResponse.length > 0) {
            AllDataUpdate(PDFID, Response.body);
          } else {
            AllDataInsert(PDFID, Response.body);
          }
          PDFResponseHandler(Response.body, view_share);
        });
      } else {
        PDFResponseHandler(mainPDFResponse, view_share);
      }
    } else {
      API_PaymentList().getPDF_ofLabReport(PDFID).then((Response) async {
        if (allPDFResponse.length > 0) {
          AllDataUpdate(PDFID, Response.body);
        } else {
          AllDataInsert(PDFID, Response.body);
        }
        PDFResponseHandler(Response.body, view_share);
      });
    }
  }

  void PDFResponseHandler(String response, String view_share) {
    log("Report Handle ${response}");
    var resBody = json.decode(response);

    mainLabService mainInvoice = mainLabService.fromJson(resBody);

    print("Detas ${mainInvoice.base64String}");

    String mainBase64Details = mainInvoice.base64String;
    _createFileFromString(mainBase64Details.split(',').last, view_share);
  }

  void _createFileFromString(String PDFbase64, String View_Share) async {
    bool? result = await requestPermissionForStorageAndManageExtStorage();
    if (result) {
      final encodedStr = PDFbase64;

      if (View_Share == "1") {
        Future<Uint8List> mainPDFBytes = FileProcess.downloadFile(PDFbase64);
        Uint8List PDFContentBytes = await mainPDFBytes;
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  PDFFileDisplay(main8Bytes: PDFContentBytes)),
        );
      } else {
        Uint8List bytes = base64.decode(encodedStr);

        Directory appDocDir = await getApplicationDocumentsDirectory();
        String tempPath = appDocDir.path;
        log("Path ${tempPath}");
        var knockDir = await new Directory('${tempPath}/patientApp')
            .create(recursive: true);
        File file = File("${knockDir.path}/" +
            DateTime.now().millisecondsSinceEpoch.toString() +
            ".pdf");
        await file.writeAsBytes(bytes);
        log("Path ${file.path}");
        Share.shareFiles(['${file.path}'], text: 'Report');
      }
    } else {
      Utility.WholeAppSnackbar(context, 'Please accept permission.');
    }
  }

  Future<bool> requestPermissionForStorageAndManageExtStorage() async {
    await [Permission.storage].request();
    final storagePermission = await Permission.storage.status;
    // final extPermission = await Permission.manageExternalStorage.status;
    // _log('Permissions => Microphone: $micPermission, Camera: $camPermission');

    if (storagePermission == PermissionStatus.granted) {
      return true;
    }

    if (storagePermission == PermissionStatus.denied) {
      return requestPermissionForStorageAndManageExtStorage();
    }

    if (storagePermission == PermissionStatus.permanentlyDenied) {
      log('Permissions => Opening App Settings');
      await openAppSettings();
    }

    return false;
  }

  void AllDataInsert(String invoiceid, String invoiceResponse) async {
    await SQLHelper.insert_PDF_Details(invoiceid, invoiceResponse, "LabReport");
  }

  void AllDataUpdate(String invoiceid, String invoiceResponse) async {
    await SQLHelper.update_invoice_response(
        invoiceid, invoiceResponse, "LabReport");
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: Utility.TopToolBar(Utility.translate('lab_report'),
            checkdevice == "1" ? 20 : 30, Utility.PutDarkBlueColor(), context),
        body: Container(
          padding: EdgeInsets.all(checkdevice == "1" ? 10 : 20),
          child: ListView.separated(
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                height: checkdevice == "1" ? 8 : 18,
              );
            },
            itemCount: listing_Lab.length,
            shrinkWrap: true,
            itemBuilder: (context, pos) {
              String datePattern = "yyyy-MM-ddTHH:mm:ss";
              String FinalDate = "";
              String FinalTime = "";

              DateTime mDate =
                  DateFormat(datePattern).parse(listing_Lab[pos].certifiedOn);

              FinalDate = DateFormat('dd MMM yyyy').format(mDate);
              FinalTime = DateFormat('HH:mm a').format(mDate);

              // print("Length ${listing_Lab[pos].inventoryListing.length}");
              List vInvestigationName = [];

              for (int j = 0;
                  j < listing_Lab[pos].inventoryListing.length;
                  j++) {
                for (int i = 0; i < lst_inventoryName.length; i++) {
                  if (lst_inventoryName[i].id ==
                      listing_Lab[pos].inventoryListing[j].investigationId) {
                    vInvestigationName.add(lst_inventoryName[i].displayName);
                  }
                }
              }
              String allNameDisplay = vInvestigationName.join(',');
              print("Name ${allNameDisplay}");

              return GestureDetector(
                onTap: () {
                  setState(() {});
                },
                child: Card(
                  color: Colors.white,
                  elevation: 5,
                  child: Column(
                    children: [
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: checkdevice == "1" ? 8 : 18,
                          ),
                          Flexible(
                              flex: 10,
                              child: Column(
                                children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      allNameDisplay,
                                      style: TextStyle(
                                          fontSize:
                                              checkdevice == "1" ? 18 : 32,
                                          fontFamily: 'Poppins_SemiBold',
                                          color: Utility.PutDarkBlueColor()),
                                    ),
                                  ),
                                  SizedBox(
                                    height: checkdevice == "1" ? 3 : 13,
                                  ),
                                  // Align(
                                  //   alignment: Alignment.topLeft,
                                  //   child: Text(
                                  //     'DISCARGE SUMMARY',
                                  //     maxLines: 1,
                                  //     style: TextStyle(
                                  //         fontSize:
                                  //             checkdevice == "1" ? 16 : 26,
                                  //         fontFamily: 'Poppins_Regular',
                                  //         color: Utility.PutDarkBlueColor()),
                                  //   ),
                                  // ),
                                  // SizedBox(
                                  //   height: checkdevice == "1" ? 3 : 13,
                                  // ),
                                  Row(
                                    children: [
                                      Flexible(
                                        flex: 10,
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  FinalDate,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      fontSize:
                                                          checkdevice == "1"
                                                              ? 16
                                                              : 26,
                                                      fontFamily:
                                                          'Poppins_Regular',
                                                      color: Utility
                                                          .PutDarkBlueColor()),
                                                ),
                                                SizedBox(
                                                  width: checkdevice == "1"
                                                      ? 30
                                                      : 40,
                                                ),
                                                Text(
                                                  FinalTime,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      fontSize:
                                                          checkdevice == "1"
                                                              ? 16
                                                              : 26,
                                                      fontFamily:
                                                          'Poppins_Regular',
                                                      color: Utility
                                                          .PutDarkBlueColor()),
                                                ),
                                              ],
                                            ),
                                            // SizedBox(
                                            //   height:
                                            //       checkdevice == "1" ? 3 : 13,
                                            // ),
                                            // Row(
                                            //   children: [
                                            //     Text(
                                            //       'Uploaded By : ',
                                            //       maxLines: 1,
                                            //       style: TextStyle(
                                            //           fontSize:
                                            //               checkdevice == "1"
                                            //                   ? 16
                                            //                   : 26,
                                            //           fontFamily:
                                            //               'Poppins_Regular',
                                            //           color: Utility
                                            //               .PutDarkBlueColor()),
                                            //     ),
                                            //     SizedBox(
                                            //       width: checkdevice == "1"
                                            //           ? 2
                                            //           : 12,
                                            //     ),
                                            //     Text(
                                            //       'Alex Logan',
                                            //       maxLines: 1,
                                            //       style: TextStyle(
                                            //           fontSize:
                                            //               checkdevice == "1"
                                            //                   ? 16
                                            //                   : 26,
                                            //           fontFamily:
                                            //               'Poppins_Regular',
                                            //           color: Utility
                                            //               .PutDarkBlueColor()),
                                            //     ),
                                            //   ],
                                            // ),
                                          ],
                                        ),
                                      ),
                                      // Flexible(
                                      //     flex: 2,
                                      //     child: Center(
                                      //         child: Container(
                                      //       width: checkdevice == "1" ? 40 : 60,
                                      //       height:
                                      //           checkdevice == "1" ? 40 : 60,
                                      //       child: SvgPicture.asset(
                                      //           'Images/GroupBack.svg'),
                                      //     ))),
                                    ],
                                  ),
                                  SizedBox(
                                    height: checkdevice == "1" ? 3 : 13,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'From : ',
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize:
                                                checkdevice == "1" ? 16 : 26,
                                            fontFamily: 'Poppins_Regular',
                                            color: Utility.PutDarkBlueColor()),
                                      ),
                                      SizedBox(
                                        width: checkdevice == "1" ? 2 : 12,
                                      ),
                                      Text(
                                        ConstantDetails.vFacilityName,
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize:
                                                checkdevice == "1" ? 16 : 26,
                                            fontFamily: 'Poppins_Regular',
                                            color: Utility.PutDarkBlueColor()),
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(
                            0,
                            0,
                            checkdevice == "1" ? 10 : 20,
                            checkdevice == "1" ? 10 : 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            InkWell(
                                child: Center(
                                    child: Row(
                                  children: [
                                    Container(
                                        child: Text(
                                      Utility.translate('download'),
                                      style: TextStyle(
                                          fontSize:
                                              checkdevice == "1" ? 16 : 26,
                                          fontFamily: 'Poppins_Regular',
                                          color: Utility.PutLightBlueColor()),
                                    )),
                                    SizedBox(
                                      width: checkdevice == "1" ? 8 : 18,
                                    ),
                                    SvgPicture.asset(
                                      'Images/Download.svg',
                                      width: checkdevice == "1" ? 15 : 25,
                                      height: checkdevice == "1" ? 15 : 25,
                                    )
                                  ],
                                )),
                                onTap: () {
                                  print("ID ${listing_Lab[pos].id}");
                                  GetDownloadandShare(listing_Lab[pos].id, "1");
                                }),
                            SizedBox(
                              width: checkdevice == "1" ? 10 : 20,
                            ),
                            InkWell(
                                child: Center(
                                    child: Row(
                                  children: [
                                    Container(
                                        child: Text(
                                      Utility.translate('share'),
                                      style: TextStyle(
                                          fontSize:
                                              checkdevice == "1" ? 16 : 26,
                                          fontFamily: 'Poppins_Regular',
                                          color: Utility.PutLightBlueColor()),
                                    )),
                                    SizedBox(
                                      width: checkdevice == "1" ? 8 : 18,
                                    ),
                                    SvgPicture.asset(
                                      'Images/share.svg',
                                      width: checkdevice == "1" ? 15 : 25,
                                      height: checkdevice == "1" ? 15 : 25,
                                    )
                                  ],
                                )),
                                onTap: () {
                                  GetDownloadandShare(listing_Lab[pos].id, "2");
                                }),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
