import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../Helper/utils.dart';

class LabReportTempPage extends StatefulWidget {
  @override
  labReportState createState() => labReportState();
}

class labReportState extends State<LabReportTempPage> {
  String checkdevice = '';

  @override
  void initState() {
    super.initState();
    checkdevice = Utility.getDeviceType();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: Utility.TopToolBar(Utility.translate('lab_report'),
            checkdevice == "1" ? 20 : 30, Utility.PutDarkBlueColor(), context),
        body: Container(
          // height: 400,
          padding: new EdgeInsets.all(10.0),
          child: Column(
            children: [
              // Container(
              //   margin: EdgeInsets.fromLTRB(10, 7, 0, 7),
              //   alignment: Alignment.centerLeft,
              //   child: Utility.MainNormalTextWithFont(
              //       "Notification",
              //       Utility.getDeviceType() == '1' ? 16 : 30,
              //       Utility.PutLightBlueColor(),
              //       'Poppins_Medium'),
              // ),
              Container(
                height: 450,
                child: Center(
                  child: Text(
                    "No Lab Report Found",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
          // child: Center(
          //   child: Text(
          //     "Comming Soon",
          //     textAlign: TextAlign.center,
          //   ),
          // ),
        ),
      ),
    );
  }
}
