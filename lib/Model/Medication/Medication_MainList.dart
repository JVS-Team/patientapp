class MedicationMainListDrugName {
  final List<MainListData> designAll;

  MedicationMainListDrugName({
    required this.designAll,
  });

  factory MedicationMainListDrugName.fromJson(List<dynamic> parsedJson) {
    List<MainListData> imagesList =
        parsedJson.map((i) => MainListData.fromJson(i)).toList();

    return new MedicationMainListDrugName(designAll: imagesList);
  }
}

class MainListData {
  final String days;
  final String drugName;
  final String date;
  final String Frequency;
  final String ROAName;
  final String specialInstructions;

  MainListData(
      {required this.drugName,
      required this.date,
      required this.days,
      required this.Frequency,
      required this.ROAName,
        required this.specialInstructions});

  factory MainListData.fromJson(Map<String, dynamic> alljson) {
    return new MainListData(
      days: alljson['days'].toString(),
      drugName: alljson['drugName'].toString(),
      date: alljson['date'].toString(),
      Frequency: alljson['Frequency'].toString(),
      ROAName: alljson['ROAName'].toString(),
      specialInstructions: alljson['specialInstructions'].toString(),
    );
  }
}
