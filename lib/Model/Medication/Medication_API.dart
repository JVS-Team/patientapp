import 'dart:convert';
import 'dart:developer';

import '../../Helper/AllURL.dart';
import '../../Helper/utils.dart';
import 'package:http/http.dart' as http;

class API_Medication {
  // List of All Drug
  Future Medication_List_DrugName(String PatientID, String AccessToken) async {
    var jsonResponse;
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(
            AllURL.MainURL + AllURL.API_MedicationList + "?id=" + PatientID),
        headers: headers);

    // log("List Response ${json.decode(response.body)}");

    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
    } else {
      jsonResponse = "Error";
    }
    log("List Response ${jsonResponse}");
    return jsonResponse;
  }

  // Medication Drug Name
  Future<Map> Medication_DrugName(String AccessToken, String AllID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.medication_drugname +
            "%24Filter=id%20in(" +
            AllID +
            ")%20and%20Status%20eq%20%27ACTIVE%27&%24select=id,name,displayName"),
        headers: headers);

    final jsonResponse = json.decode(response.body);
    // print("List Response ${jsonResponse}");
    return jsonResponse;
  }

  // Medication Freq Details
  Future Medication_Freq_MasterList(String AccessToken) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.medication_freq_master_list +
            "%24Filter=Status%20eq%20%27ACTIVE%27&%24select=id,name,displayname,value&%24orderby=displayname%20asc"),
        headers: headers);

    final jsonResponse = json.decode(response.body);

    return jsonResponse;
  }

  // Medication ROA Details
  Future Medication_ROA_MasterList(String AccessToken) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.medication_roa_master_list +
            "%24Filter=Status%20eq%20%27ACTIVE%27&%24select=displayName,name,id"),
        headers: headers);

    final jsonResponse = json.decode(response.body);
    // print("ROA Response ${jsonResponse}");
    print(response.statusCode);
    return jsonResponse;
  }
}
