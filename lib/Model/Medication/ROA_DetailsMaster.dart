class ROA_Details {
    final String id;
    final String name;
    final String displayName;

    ROA_Details(
        {required this.id,
            required this.name,
            required this.displayName});

    factory ROA_Details.fromJson(Map<String, dynamic> json) {
        return new ROA_Details(
            id: json['id'].toString(),
            name: json['name'].toString(),
            displayName: json['displayName'].toString()
        );
    }
}
