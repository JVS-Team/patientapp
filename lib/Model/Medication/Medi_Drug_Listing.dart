import 'dart:convert';

class MedicationGetListing {
  final String patientId;
  final String appointmentId;
  final String doctorId;
  final String orderAmount;
  final String orderDate;
  final String orderTime;
  final String facilityId;
  final String visitTypeId;
  final String visitNumberId;
  final String orderStatus;
  final String statusString;
  final String id;
  final String name;
  final String status;
  final String displayName;
  final String code;
  final String tenantId;
  final List<OrderDetails> drug_Order_Details;

  MedicationGetListing({
    required this.patientId,
    required this.appointmentId,
    required this.doctorId,
    required this.orderAmount,
    required this.orderDate,
    required this.orderTime,
    required this.facilityId,
    required this.visitTypeId,
    required this.visitNumberId,
    required this.orderStatus,
    required this.statusString,
    required this.id,
    required this.name,
    required this.status,
    required this.displayName,
    required this.code,
    required this.tenantId,
    required this.drug_Order_Details,
  });

  factory MedicationGetListing.fromJson(Map<String, dynamic> data) {
    var lst_drug_order_details = data['orderdetails'] as List;
    List<OrderDetails> lst_DrugDetails =
        lst_drug_order_details.map((i) => OrderDetails.fromJson(i)).toList();

    int Value = 0;
    double dValue = 0.0;
    int MainValue = 0;
    var valueHere = data['orderAmount'];

    if (valueHere is int) {
      Value = data['orderAmount'] as int;
    } else if (valueHere is double) {
      dValue = data['orderAmount'] as double;
      Value = dValue.toInt();
    }

    MainValue = Value;

    return MedicationGetListing(
      patientId: data['patientId'] ?? "",
      appointmentId: data['appointmentId'] ?? "",
      doctorId: data['doctorId'] ?? "",
      orderAmount: MainValue.toString(),
      orderDate: data['orderDate'] ?? "",
      orderTime: data['orderTime'] ?? "",
      facilityId: data['facilityId'] ?? "",
      visitTypeId: data['visitTypeId'] ?? "",
      visitNumberId: data['visitNumberId'] ?? "",
      orderStatus: data['orderStatus'] ?? "",
      statusString: data['statusString'] ?? "",
      id: data['id'] ?? "",
      name: data['name'] ?? "",
      status: data['status'] ?? "",
      displayName: data['displayName'] ?? "",
      code: data['code'] ?? "",
      tenantId: data['tenantId'] ?? "",
      drug_Order_Details: lst_DrugDetails,
    );
  }
}

class OrderDetails {
  String orderId;
  String id;
  String moduleName;
  String moduleId;
  OtherDetail_Data orderDetails;
  Map AllOrderDetails;
  String DrugName;

  OrderDetails({
    required this.id,
    required this.orderId,
    required this.moduleName,
    required this.moduleId,
    required this.orderDetails,
    required this.DrugName,
    required this.AllOrderDetails,
  });

  factory OrderDetails.fromJson(Map<String, dynamic> alljson) {
    return OrderDetails(
      id: alljson['id'] ?? "",
      orderId: alljson['orderId'] ?? "",
      moduleName: alljson['moduleName'] ?? "",
      moduleId: alljson['moduleId'] ?? "",
      orderDetails:
          OtherDetail_Data.fromJson(json.decode(alljson['otherDetail'])),
      AllOrderDetails: json.decode(alljson['otherDetail']),
      DrugName: '',
    );
  }
}

class OtherDetail_Data {
  String amount;
  String drugForm;
  String duration;
  String priority;
  String frequency;
  String directions;
  String intakeUnit;
  String freeTextSig;
  String durationDays;
  String totalQuantity;
  String strengthWithuom;
  String startDateAndTime;
  String displayMedication;
  String routeOfAdministration;
  String specialInstructions;

  OtherDetail_Data({
    required this.amount,
    required this.drugForm,
    required this.duration,
    required this.priority,
    required this.frequency,
    required this.directions,
    required this.intakeUnit,
    required this.freeTextSig,
    required this.durationDays,
    required this.totalQuantity,
    required this.strengthWithuom,
    required this.startDateAndTime,
    required this.displayMedication,
    required this.routeOfAdministration,
    required this.specialInstructions,
  });

  factory OtherDetail_Data.fromJson(Map<String, dynamic> json) {
    return new OtherDetail_Data(
      amount: json['amount'].toString(),
      drugForm: json['drugForm'].toString(),
      duration: json['duration'].toString(),
      priority: json['priority'].toString(),
      frequency: json['frequency'].toString(),
      directions: json['directions'].toString(),
      intakeUnit: json['intakeUnit'].toString(),
      freeTextSig: json['freeTextSig'].toString(),
      durationDays: json['durationDays'].toString(),
      totalQuantity: json['totalQuantity'].toString(),
      strengthWithuom: json['strengthWithuom'].toString(),
      startDateAndTime: json['startDateAndTime'].toString(),
      displayMedication: json['displayMedication'].toString(),
      routeOfAdministration: json['routeOfAdministration'].toString(),
      specialInstructions: json['specialInstructions'] ?? "",
    );
  }
}
