class MedicationDrugName {
    final List<DrugName> designAll;

    MedicationDrugName({
        required this.designAll,
    });

    factory MedicationDrugName.fromJson(List<dynamic> parsedJson) {
        List<DrugName> imagesList =
        parsedJson.map((i) => DrugName.fromJson(i)).toList();

        return new MedicationDrugName(designAll: imagesList);
    }
}

class DrugName {
    final String id;
    final String name;
    final String displayName;

    DrugName(
        {required this.id,
            required this.name,
            required this.displayName});

    factory DrugName.fromJson(Map<String, dynamic> json) {
        return new DrugName(
            id: json['id'].toString(),
            name: json['name'].toString(),
            displayName: json['displayName'].toString(),
        );
    }
}
