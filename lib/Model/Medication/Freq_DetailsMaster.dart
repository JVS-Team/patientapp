class Freq_Details {
  final String id;
  final String name;
  final String displayName;
  final String value;

  Freq_Details(
      {required this.id,
        required this.name,
        required this.displayName,
        required this.value});

  factory Freq_Details.fromJson(Map<String, dynamic> json) {
    return new Freq_Details(
      id: json['id'].toString(),
      name: json['name'].toString(),
      displayName: json['displayName'].toString(),
      value: json['value'].toString(),
    );
  }
}
