import 'dart:convert';

import 'dart:developer';

class mainLabService {
  final String visitNumberId;
  final String base64String;


  mainLabService(
      {required this.visitNumberId,
      required this.base64String});

  factory mainLabService.fromJson(Map<String, dynamic> mainData) {
    return new mainLabService(
        visitNumberId: mainData['visitNumberId'].toString(),
        base64String: mainData['base64String'].toString());
  }
}
