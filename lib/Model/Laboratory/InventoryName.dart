class inventoryName {
  final String id;
  final String displayName;

  inventoryName({required this.id, required this.displayName});

  factory inventoryName.fromJson(Map<String, dynamic> json) {
    return new inventoryName(
        id: json['id'].toString(), displayName: json['displayName'].toString());
  }
}
