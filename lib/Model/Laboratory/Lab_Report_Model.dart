class LabAllListData {
  final List<LabListDetails> labListing;

  LabAllListData({
    required this.labListing,
  });

  factory LabAllListData.fromJson(List<dynamic> parsedJson) {
    List<LabListDetails> imagesList =
        parsedJson.map((i) => LabListDetails.fromJson(i)).toList();

    return new LabAllListData(labListing: imagesList);
  }
}

class LabListDetails {
  final String id;
  final String certifiedOn;
  final List<inventoryList> inventoryListing;

  LabListDetails(
      {required this.id,
      required this.inventoryListing,
      required this.certifiedOn});

  factory LabListDetails.fromJson(Map<String, dynamic> json) {
    var lst_InventoryIDS = json['requisitionorderEntryDtl'] as List;
    List<inventoryList> lst_ServiceDetails =
        lst_InventoryIDS.map((i) => inventoryList.fromJson(i)).toList();

    return new LabListDetails(
        id: json['id'].toString(),
        certifiedOn: json['certifiedOn'].toString(),
        inventoryListing: lst_ServiceDetails);
  }
}

class inventoryList {
  final String id;
  final String investigationId;
  final String srNo;
  final String requisitionentryId;

  inventoryList(
      {required this.id,
      required this.investigationId,
      required this.srNo,
      required this.requisitionentryId});

  factory inventoryList.fromJson(Map<String, dynamic> json) {
    return new inventoryList(
        id: json['id'].toString(),
        investigationId: json['investigationId'].toString(),
        srNo: json['srNo'].toString(),
        requisitionentryId: json['requisitionentryId'].toString());
  }
}
