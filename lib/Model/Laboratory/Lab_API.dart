import '../../Helper/AllURL.dart';
import '../../Helper/utils.dart';
import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class API_LabList {
  Future Lab_List_All(String PatientID) async {
    String AccessToken = await Utility.readAccessToken('Token');
    var jsonResponse;
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    String cdate = DateFormat("EEE MMM dd yyyy").format(DateTime.now());
    var cDateEnc = Uri.encodeFull(cdate);
    print("current Date ${cDateEnc}");

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.lab_listing_details +
            "selectedId=&facilityId=&fromdate=Tue%20Jan%2012%202021&todate=" +
            cDateEnc +
            "&reCertified=true&reEntered=&patientId=" +
            PatientID +
            "&selectedDoctor=&page=1&perpage=1000&orderby=createdAt&direction=desc"),
        headers: headers);

    if (response.statusCode == 200) {
      jsonResponse = response.body;
    } else {
      jsonResponse = "Error";
    }

    return jsonResponse;
  }

  Future inventoryListingName(String inventoryID) async {
    String AccessToken = await Utility.readAccessToken('Token');
    var jsonResponse;
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.inventory_listing_name +
            "%24Filter=id%20in(%20" + inventoryID + ")%20and%20Status%20eq%20%27ACTIVE%27&%24top=1000&%24select=displayname,id,name&%24orderby=displayname%20asc"),
        headers: headers);

    if (response.statusCode == 200) {
      jsonResponse = response.body;
    } else {
      jsonResponse = "Error";
    }

    return jsonResponse;
  }

// Future Lab_List_All(String PatientID) async {
//   String AccessToken = await Utility.readAccessToken('Token');
//   var jsonResponse;
//   Map<String, String> headers = {
//     "Content-Type": "application/json",
//     "Accept": "application/json",
//     "Authorization": "Bearer $AccessToken",
//   };
//
//   String cdate = DateFormat("EEE MMM dd yyyy").format(DateTime.now());
//   var cDateEnc = Uri.encodeFull(cdate);
//   print("current Date ${cDateEnc}");
//
//   final response = await http.get(
//       Uri.parse(AllURL.MainURL +
//           AllURL.invoice_listing +
//           "{%22responseField%22:%22%22,%22getDistinctValue%22:false,%22fields%22:[{%22FieldName%22:%22fromdate%22,%22FieldValue%22:%22Tue%20Jan%2012%202021%22},{%22FieldName%22:%22todate%22,%22FieldValue%22:%22" +
//           cDateEnc +
//           "%22},{%22FieldName%22:%22patientid%22,%22FieldValue%22:%22" +
//           PatientID +
//           "%22}]}&page=1&perpage=1000&direction=asc"),
//       headers: headers);
//
//   // print(
//   //     "Main URL ${AllURL.MainURL + AllURL.invoice_listing + "{%22responseField%22:%22%22,%22getDistinctValue%22:false,%22fields%22:[{%22FieldName%22:%22fromdate%22,%22FieldValue%22:%22Tue%20Jan%2012%202021%22},{%22FieldName%22:%22todate%22,%22FieldValue%22:" + cDateEnc + "},{%22FieldName%22:%22facilityid%22,%22FieldValue%22:%22e0749d4f-06a7-43b1-b6db-0ae393532cb3%22},{%22FieldName%22:%22statusType%22,%22FieldValue%22:%22%22},{%22FieldName%22:%22visitType%22,%22FieldValue%22:%22%22}]}&page=1&perpage=10&direction=asc"}");
//   log("List Response ${response.body}");
//
//   if (response.statusCode == 200) {
//     jsonResponse = response.body;
//   } else {
//     jsonResponse = "Error";
//   }
//
//   return jsonResponse;
// }

}
