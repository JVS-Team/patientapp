import 'dart:convert';

class PatientDetails {
  final String id;
  final String code;
  final String tenantId;
  PatregiData patregi;

  PatientDetails({
    required this.id,
    required this.code,
    required this.tenantId,
    required this.patregi,
  });

  factory PatientDetails.fromJson(Map<String, dynamic> data) {
    return new PatientDetails(
      id: data['id'] ?? "",
      code: data['code'] ?? "",
      tenantId: data['tenantId'] ?? "",
      patregi: PatregiData.fromJson(data['patregi']),
    );
  }
}

class PatregiData {
  final String firstName;
  final String lastName;

  PatregiData({
    required this.firstName,
    required this.lastName,
  });

  factory PatregiData.fromJson(Map<String, dynamic> json) {
    return new PatregiData(
      firstName: json['firstName'].toString(),
      lastName: json['lastName'].toString(),
    );
  }
}
