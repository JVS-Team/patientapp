class AppointmentList_Details {
  final String id;
  final String name;
  final String patientId;
  final String facilityId;
  final String clinicId;
  final String doctorId;
  final String serviceId;
  final String departmentId;
  final String appointmentDate;
  final String appointmentStatus;
  final String status;
  final String displayName;
  final String tenantId;
  final String startTime;
  String facilityName;

  AppointmentList_Details(
      {required this.id,
      required this.name,
      required this.patientId,
      required this.facilityId,
      required this.clinicId,
      required this.doctorId,
      required this.serviceId,
      required this.departmentId,
      required this.appointmentDate,
      required this.appointmentStatus,
      required this.status,
      required this.displayName,
      required this.tenantId,
      required this.startTime,
      required this.facilityName});

  factory AppointmentList_Details.fromJson(Map<String, dynamic> json) {
    return new AppointmentList_Details(
      id: json['id'].toString(),
      name: json['name'].toString(),
      patientId: json['patientId'].toString(),
      facilityId: json['facilityId'].toString(),
      clinicId: json['clinicId'].toString(),
      doctorId: json['doctorId'].toString(),
      serviceId: json['serviceId'].toString(),
      departmentId: json['departmentId'].toString(),
      appointmentDate: json['appointmentDate'].toString(),
      appointmentStatus: json['appointmentStatus'].toString(),
      status: json['status'].toString(),
      displayName: json['displayName'].toString(),
      tenantId: json['tenantId'].toString(),
      startTime: json['startTime'].toString(),
      facilityName: '',
    );
  }
}
