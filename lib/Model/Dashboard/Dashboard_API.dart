import 'dart:convert';
import 'dart:developer';

import 'package:intl/intl.dart';

import '../../Helper/AllURL.dart';
import '../../Helper/utils.dart';
import 'package:http/http.dart' as http;

class API_Dashboard {
  // Dashboard API Appointment List
  Future AppointmentListAPI(String AccessToken, String PatientID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };
    final response = await http.get(
        Uri.parse(AllURL.MainURL + AllURL.AppointmentListAPI + PatientID),
        headers: headers);

    final jsonResponse = response.body;
    // log("List App Details ${jsonResponse}");
    return jsonResponse;
  }

  Future<http.Response> AppointmentALLListAPI(
      String AccessToken, String PatientID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    String currentDate = DateFormat("EEE MMM dd yyyy").format(DateTime.now());
    print("Current Date ${currentDate}");

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.AppointmentListDashboardAPI +
            PatientID +
            "&fromdate=" +
            currentDate +
            "&todate=" +
            currentDate +
            "&page=1&perpage=10&orderby=appointmentDate,startTime&direction=desc"),
        headers: headers);
    // log("List App Details ${jsonResponse}");
    return response;
  }

  // Dashboard Facility Name
  Future Dashboard_Facilityname(String AccessToken) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.FacilityNameList +
            "%24Filter=Status%20eq%20%27ACTIVE%27&%24select=displayname,id,name&%24orderby=displayname%20asc"),
        headers: headers);

    final jsonResponse = json.decode(response.body);
    // log("List App Details ${jsonResponse}");
    return jsonResponse;
  }

  Future getMainAppointmentDetails(
      String AccessToken, String PatientID, String AppointmentID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };
    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.getMainAppointmentListAPI +
            "pId=" +
            PatientID +
            "&appointmentId=" +
            AppointmentID),
        headers: headers);

    // log("List App URL ${AllURL.MainURL +
    //     AllURL.getMainAppointmentListAPI +
    //     "pId=" +
    //     PatientID +
    //     "&appointmentId=" +
    //     AppointmentID}");

    final jsonResponse = response.body;
    // log("Details Patient ${jsonResponse}");
    return jsonResponse;
  }

  //Appointment Status API Call
  Future AppointmentStatusDetails() async {
    String AccessToken = await Utility.readAccessToken('Token');
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL + AllURL.AppointmentStatusAPI),
        headers: headers);

    var resBody = response.body;
    // log("Status Response ${response.body}");
    return resBody;
  }

  Future employeebytype() async {
    String AccessToken = await Utility.readAccessToken('Token');
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL + AllURL.AllDoctorList + "?type=DOCTOR"),
        headers: headers);
    log(AllURL.MainURL + AllURL.AllDoctorList);
    final jsonResponse = response.body;
    print("employeebytype ${jsonResponse}");
    return jsonResponse;
  }
}
