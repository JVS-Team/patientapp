class Facility_Details {
  final String id;
  final String name;
  final String displayName;

  Facility_Details(
      {required this.id,
      required this.name,
      required this.displayName});

  factory Facility_Details.fromJson(Map<String, dynamic> json) {
    return new Facility_Details(
      id: json['id'].toString(),
      name: json['name'].toString(),
      displayName: json['displayName'].toString()
    );
  }
}
