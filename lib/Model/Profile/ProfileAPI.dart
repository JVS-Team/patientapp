import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../Helper/AllURL.dart';

class API_Profile {
  // Listing of All Doctor Details
  Future ProfilePageDisplay(String AccessToken, String PatientID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };
    print(' Here Profile API ');

    final response = await http.get(
        Uri.parse(AllURL.MainURL + AllURL.PatientDetailsAPI + PatientID),
        headers: headers);
    log("Profile API Response ${response.body}");
    final jsonResponse = json.decode(response.body);
    return jsonResponse;
  }
}
