import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;

import '../../Helper/AllURL.dart';

class API_AppointmentSelection {
  Future DepartmentList_Appointment(
      String AccessToken, String HospitalID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.appointment_departments +
            "%24Filter=facilityId%20eq%20" +
            HospitalID +
            "%20and%20Status%20eq%20%27ACTIVE%27&%24select=displayname,id,name"),
        headers: headers);
    print(response.body);
    final jsonResponse = json.decode(response.body);

    return jsonResponse;
  }

  Future ClinicLocationList(String AccessToken, String DepartmentID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.appointment_clinic_location +
            "%24Filter=departmentId%20eq%20" +
            DepartmentID +
            "%20and%20Status%20eq%20%27ACTIVE%27&%24select=id,name,displayName,departmentId,clinicId"),
        headers: headers);

    final jsonResponse = json.decode(response.body);

    return jsonResponse;
  }

  Future AppointmentTimeSlot(
      String AccessToken,
      String DateSet,
      String DoctorID,
      String DepartmentID,
      String FacilityID,
      String ClinicID) async {
    var jsonResponse;
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };
// date == MM-dd-yyyy
//     log("All Time Slot ${AllURL.MainURL + AllURL.TimeSlotAPI + "appoitmentdate=" + DateSet + "&doctorid=" + DoctorID}");
    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.TimeSlotAPI +
            "appoitmentdate=" +
            DateSet +
            "&doctorid=" +
            DoctorID +
            "&facilityid=" +
            FacilityID +
            "&departmentid=" +
            DepartmentID +
            "&clinicid=" +
            ClinicID),
        headers: headers);

    if (response.statusCode == 200) {
      log("All Time Status Code ${response.statusCode}");
      jsonResponse = json.decode(response.body);
    } else {
      jsonResponse = "Error";
    }

    return jsonResponse;
  }

  Future<Map> Employeemaster(String AccessToken, String EmployeeID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };
    final response = await http.get(
      Uri.parse(AllURL.MainURL + AllURL.employeemaster + EmployeeID),
      headers: headers,
    );
    // log(response.body);
    // log(response.body);
    var resBody = json.decode(response.body);
    return resBody;
  }

  Future HospitalListing(String AccessToken) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.AllFinalFaciliy +
            "%24Filter=Status%20eq%20%27ACTIVE%27&%24top=1000&%24select=displayname,id,name&%24orderby=displayname%20asc"),
        headers: headers);
    return response.body;
  }

  Future getDepartmentLisitng(String AccessToken, String FacilityID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.AllDepartmentLisitng +
            "%24Filter=facilityId%20eq%20" +
            FacilityID +
            "%20and%20Status%20eq%20%27ACTIVE%27&%24top=1000&%24select=id,name,displayName,facilityId"),
        headers: headers);
    return response.body;
  }

  Future getClinicLocationList(String AccessToken, String DepartmentID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.AllClinicLocationLisitng +
            "%24Filter=departmentId%20eq%20" +
            DepartmentID +
            "%20and%20Status%20eq%20%27ACTIVE%27&%24top=1000&%24select=id,name,displayName,departmentId,clinicId"),
        headers: headers);
    return response.body;
  }

  Future getEmployeeService(String AccessToken, String FacilityID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.appointment_servicelist +
            'facilityid=${FacilityID}&serviceMasterType=Consultations&serviceType=service'),
        headers: headers);
    final jsonResponse = response.body;
    log("Service List ${jsonResponse}");
    return jsonResponse;
  }

  Future getDoctorListFromClinic(String AccessToken, String ClinicID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL + AllURL.DRFromClinicDetails + ClinicID),
        headers: headers);
    // log(AllURL.MainURL + AllURL.getDoctorFromClinicID + ClinicID);
    final jsonResponse = response.body;
    // print("employeebytype DDDD ${jsonResponse}");
    return jsonResponse;
  }
}
