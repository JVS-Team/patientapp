class HostpitalDetails {
  final String displayName;
  final String id;

  HostpitalDetails({
    required this.id,
    required this.displayName,
  });

  factory HostpitalDetails.fromJson(Map<String, dynamic> json) {
    return HostpitalDetails(
        id: json['id'] as String, displayName: json['displayName'] as String);
  }
}