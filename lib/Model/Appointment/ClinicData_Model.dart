//Creating a class user to store the data;
class AllMainClinicDetails {
  final String displayName;
  final String id;
  final String clinicId;
  final String departmentId;
  final String name;

  AllMainClinicDetails({
    required this.id,
    required this.displayName,
    required this.clinicId,
    required this.name,
    required this.departmentId,
  });

  factory AllMainClinicDetails.fromJson(Map<String, dynamic> json) {
    return AllMainClinicDetails(
        id: json['id'] ?? "",
        displayName: json['displayName'] ?? "",
        clinicId: json['clinicId'] ?? "",
        departmentId: json['departmentId'] ?? "",
        name: json['name'] ?? "");
  }
}
