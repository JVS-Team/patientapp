import 'dart:convert';

class AllTimeSlotDetails {
  final List<TimeSlotModel> designAll;
  final String count;
  final String slotDuration;

  AllTimeSlotDetails({
    required this.designAll,
    required this.count,
    required this.slotDuration,
  });

  factory AllTimeSlotDetails.fromJson(Map<String, dynamic> parsedJson) {
    List<TimeSlotModel> lst_TimeSlot = [];
    var lst_ServiceIDS = parsedJson['timeSlotDto'] as List;
    lst_TimeSlot =
        lst_ServiceIDS.map((i) => TimeSlotModel.fromJson(i)).toList();

    return new AllTimeSlotDetails(
        count: parsedJson['count'].toString(),
        slotDuration: parsedJson['slotDuration'].toString(),
        designAll: lst_TimeSlot);
  }
}

class TimeSlotModel {
  final String dateTime;
  final String slotimes;
  final bool isMorning;

  TimeSlotModel({
    required this.dateTime,
    required this.slotimes,
    required this.isMorning,
  });

  factory TimeSlotModel.fromJson(Map<String, dynamic> json) {
    return new TimeSlotModel(
        dateTime: json['dateTime'].toString(),
        slotimes: json['slotimes'].toString(),
        isMorning: json['isMorning']);
  }
}
