class AppointmentStatusDetails {
    final String id;
    final String displayName;

    AppointmentStatusDetails({required this.id, required this.displayName});

    factory AppointmentStatusDetails.fromJson(Map<String, dynamic> json) {
        return new AppointmentStatusDetails(
            id: json['id'].toString(), displayName: json['displayName'].toString());
    }
}
