import 'dart:convert';
import 'dart:developer';

import 'package:intl/intl.dart';

class AppointmentBookJSON {
  static Map OLD_BookAppointmentJSON(
      String CategoryID,
      String ServiceID,
      String StartTime,
      String DepartmentID,
      String ClinicID,
      String FacilityID,
      String PatientID,
      String Date,
      String DoctorID) {
    String vitalValueHere = "";
    // Current Date
    var now = new DateTime.now();
    var dateFormate = new DateFormat('yyyy-MM-dd');
    var timeFormate = new DateFormat('hh:mm');
    String formattedDate = dateFormate.format(now);
    String formattedTime = timeFormate.format(now);
    print(formattedDate); // 2016-01-25
    print(formattedTime); // 10:20
    print(Date);

    Map finalBodyMap = {
      "visitType": "OUTPATIENT",
      "externalPatient": false,
      "isReferralPatient": false,
      "appointmentCategoryId": CategoryID,
      "patientCondition": "",
      "serviceId": ServiceID,
      "appointmentStatus": "BOOKED",
      "actionName": "",
      "statusChange": "",
      "facilityId": FacilityID,
      "doctorId": DoctorID,
      "departmentId": DepartmentID,
      "clinicId": ClinicID,
      "startTime": StartTime,
      "appointmentDate": Date,
      "patientId": PatientID,
      "status": "ACTIVE",
      "code": "Auto generated"
    };

    String jsonTags = jsonEncode(finalBodyMap);
    // log(jsonTags);

    return finalBodyMap;
  }

  static Map BookAppointmentJSONCreate(
      String AppointmentCategoryID,
      String ServiceID,
      String FacilityID,
      String DepartmentID,
      String ClinicID,
      String PatientID,
      String doctorID,
      String DateDetails,
      String TimeDetails,
      String patientComplain,
      String endTime) {
    String currentDate = "";
    String currentTime = "";
    String currentEndTime = "";

    if (DateDetails == "") {
      var now = new DateTime.now();
      var dateFormate = new DateFormat('yyyy-MM-dd');
      currentDate = dateFormate.format(now);

      var timeFormate = new DateFormat('HH:mm');
      currentTime = timeFormate.format(now);

      print(currentDate); // 2016-01-25
      print(currentTime); // 10:20
    } else {
      var now = new DateTime.now();
      currentDate = DateDetails;

      if (TimeDetails == "") {
        var timeFormate = new DateFormat('HH:mm');
        currentTime = timeFormate.format(now);
      } else {
        currentTime = TimeDetails;
      }
    }

    if (endTime == "") {
      currentEndTime = "00:00:00";
    } else {
      currentEndTime = endTime;
    }

    Map finalBodyMap = {
      "visitType": "OUTPATIENT",
      "externalPatient": false,
      "isReferralPatient": false,
      "appointmentCategoryId": AppointmentCategoryID,
      "patientCondition": "",
      "serviceId": ServiceID,
      "appointmentStatus": "BOOKED",
      "actionName": "",
      "statusChange": "",
      "facilityId": FacilityID,
      "doctorId": doctorID,
      "departmentId": DepartmentID,
      "clinicId": ClinicID,
      "startTime": currentTime,
      "endTime": currentEndTime,
      "appointmentDate": currentDate,
      "patientId": PatientID,
      "patientComplaint": patientComplain,
      "status": "ACTIVE",
      "code": "Auto generated"
    };
    // log("Main Details ${finalBodyMap}");

    return finalBodyMap;
  }

}
