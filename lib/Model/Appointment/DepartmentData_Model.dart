//Creating a class user to store the data;
class DepartmentDetails {
  final String displayName;
  final String id;
  final String facilityId;
  final String name;

  DepartmentDetails({
    required this.id,
    required this.displayName,
    required this.facilityId,
    required this.name,
  });

  factory DepartmentDetails.fromJson(Map<String, dynamic> json) {
    return DepartmentDetails(
        id: json['id'] as String,
        displayName: json['displayName'] as String,
        facilityId: json['facilityId'] as String,
        name: json['name'] as String);
  }
}

