import 'dart:convert';

class AllServiceList {
  final List<ServiceAllListing> designAll;

  AllServiceList({
    required this.designAll,
  });

  factory AllServiceList.fromJson(List<dynamic> parsedJson) {
    List<ServiceAllListing> imagesList =
        parsedJson.map((i) => ServiceAllListing.fromJson(i)).toList();

    return new AllServiceList(designAll: imagesList);
  }
}

class ServiceAllListing {
  final String serviceGroupId;
  final String serviceTypeId;
  final String serviceName;
  final String id;
  final String name;
  final String displayName;
  final String code;
  final String tenantId;

  ServiceAllListing({
    required this.serviceGroupId,
    required this.serviceTypeId,
    required this.serviceName,
    required this.id,
    required this.name,
    required this.displayName,
    required this.code,
    required this.tenantId,
  });

  factory ServiceAllListing.fromJson(Map<String, dynamic> json) {
    return new ServiceAllListing(
      serviceGroupId: json['serviceGroupId'].toString(),
      serviceTypeId: json['serviceTypeId'].toString(),
      serviceName: json['serviceName'].toString(),
      id: json['id'].toString(),
      name: json['name'].toString(),
      displayName: json['displayName'].toString(),
      code: json['code'].toString(),
      tenantId: json['tenantId'].toString(),
    );
  }
}
