import 'dart:convert';
import 'dart:developer';

import '../../Helper/AllURL.dart';
import '../../Helper/utils.dart';
import 'package:http/http.dart' as http;

class API_LoginAPI {
  // Hospitalname
  Future MainHospitalName(String HospitalID) async {
    String AccessTokenTemp = await Utility.readAccessToken('TokenTemporary');
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessTokenTemp",
    };
    final response = await http.get(
        Uri.parse(AllURL.MainURL + AllURL.HospitalNameDetails + HospitalID),
        headers: headers);

    final jsonResponse = response.body;
    // log(response.body);

    return jsonResponse;
  }
}
