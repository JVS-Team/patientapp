class familyMemberListing {
  final String facilityID;
  final String facilityName;
  final String patientName;
  final String patientID;
  final String patientcode;
  final String crNo;

  familyMemberListing(
      {required this.facilityID,
      required this.facilityName,
      required this.patientName,
      required this.patientID,
      required this.patientcode,
      required this.crNo});

  factory familyMemberListing.fromJson(String facilityID, String facilityName,
      String patientName, String patientID, String patientcode, String crNo) {
    return new familyMemberListing(
        facilityID: facilityID,
        facilityName: facilityName,
        patientName: patientName,
        patientID: patientID,
        patientcode: patientcode,
        crNo: crNo);
  }
}
