class OTP_Verify {
  final List<FamilyMemberList> designAll;

  OTP_Verify({
    required this.designAll,
  });

  factory OTP_Verify.fromJson(Map<String, dynamic> data) {
    var lst_OTP_Details = data['item1'] as List;
    List<FamilyMemberList> lst_OTPList =
        lst_OTP_Details.map((i) => FamilyMemberList.fromJson(i)).toList();

    return new OTP_Verify(designAll: lst_OTPList);
  }
}

class FamilyMemberList {
  final String name;
  final String code;
  final List<HospotalList> hospital_List;

  FamilyMemberList(
      {required this.name, required this.code, required this.hospital_List});

  factory FamilyMemberList.fromJson(Map<String, dynamic> json) {
    var lst_AllHospital = json['appPatientIdTenants'] as List;
    List<HospotalList> lst_AllList_Hospital = lst_AllHospital
        .map((i) => HospotalList.fromJson(
            i, json['name'].toString(), json['code'].toString()))
        .toList();

    return new FamilyMemberList(
      name: json['name'].toString(),
      code: json['code'].toString(),
      hospital_List: lst_AllList_Hospital,
    );
  }
}

class HospotalList {
  final String patientId;
  final String tenantId;
  final String crNumber;
  String HospitalName;
  String mainName;
  String mainCode;

  HospotalList(
      {required this.patientId,
      required this.tenantId,
      required this.crNumber,
      required this.HospitalName,
      required this.mainName,
      required this.mainCode});

  factory HospotalList.fromJson(
      Map<String, dynamic> json, String Name, String Code) {
    return HospotalList(
        patientId: json['patientId'] ?? "",
        tenantId: json['tenantId'] ?? "",
        crNumber: json['crNumber'] ?? "",
        HospitalName: '',
        mainName: Name,
        mainCode: Code);
  }
}
