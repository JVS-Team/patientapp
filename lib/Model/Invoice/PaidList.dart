class PaymentAllListData {
  final List<billingList> paymentList;

  PaymentAllListData({
    required this.paymentList,
  });

  factory PaymentAllListData.fromJson(List<dynamic> parsedJson) {
    List<billingList> imagesList =
        parsedJson.map((i) => billingList.fromJson(i)).toList();

    return new PaymentAllListData(paymentList: imagesList);
  }
}

class billingList {
  final String appointmentId;
  final String doctorId;
  final String doctorName;
  final String facilityId;
  final String inoviceCode;
  final String inoviceNetAmount;
  final String inovicePaidAmount;
  final String inoviceDueAmount;
  final String inoviceTax;
  final String invoiceId;
  final String inoviceTotal;
  final String patientId;
  final String visitType;
  final String id;

  billingList(
      {required this.appointmentId,
      required this.doctorId,
      required this.doctorName,
      required this.facilityId,
      required this.inoviceCode,
      required this.inoviceNetAmount,
      required this.inovicePaidAmount,
      required this.inoviceTax,
      required this.inoviceTotal,
      required this.patientId,
      required this.inoviceDueAmount,
      required this.visitType,
      required this.invoiceId,
      required this.id});

  factory billingList.fromJson(Map<String, dynamic> json) {
    return new billingList(
        appointmentId: json['appointmentId'].toString(),
        doctorId: json['doctorId'].toString(),
        doctorName: json['doctorName'].toString(),
        facilityId: json['facilityId'].toString(),
        inoviceCode: json['inoviceCode'].toString(),
        inoviceNetAmount: json['inoviceNetAmount'].toString(),
        inovicePaidAmount: json['inovicePaidAmount'].toString(),
        inoviceTax: json['inoviceTax'].toString(),
        inoviceTotal: json['inoviceTotal'].toString(),
        patientId: json['patientId'].toString(),
        inoviceDueAmount: json['inoviceDueAmount'].toString(),
        visitType: json['visitType'].toString(),
        invoiceId: json['invoiceId'].toString(),
        id: json['id'].toString());
  }
}
