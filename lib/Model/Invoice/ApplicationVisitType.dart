class VisitTypeDetails {
  final List<visitList> visitTypelist;

  VisitTypeDetails({
    required this.visitTypelist,
  });

  factory VisitTypeDetails.fromJson(List<dynamic> parsedJson) {
    List<visitList> imagesList =
        parsedJson.map((i) => visitList.fromJson(i)).toList();

    return new VisitTypeDetails(visitTypelist: imagesList);
  }
}

class visitList {
  final String displayName;
  final String id;

  visitList({required this.displayName, required this.id});

  factory visitList.fromJson(Map<String, dynamic> json) {
    return new visitList(
        displayName: json['displayName'].toString(), id: json['id'].toString());
  }
}
