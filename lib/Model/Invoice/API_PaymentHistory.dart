import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import '../../Helper/AllURL.dart';
import '../../Helper/utils.dart';

class API_PaymentList {
  // List of All Payment List
  Future Payment_List_All(String PatientID) async {
    String AccessToken = await Utility.readAccessToken('Token');
    var jsonResponse;
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    String cdate = DateFormat("EEE MMM dd yyyy").format(DateTime.now());
    var cDateEnc = Uri.encodeFull(cdate);
    print("current Date ${cDateEnc}");

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.invoice_listing +
            "{%22responseField%22:%22%22,%22getDistinctValue%22:false,%22fields%22:[{%22FieldName%22:%22fromdate%22,%22FieldValue%22:%22Tue%20Jan%2012%202021%22},{%22FieldName%22:%22todate%22,%22FieldValue%22:%22" +
            cDateEnc +
            "%22},{%22FieldName%22:%22patientid%22,%22FieldValue%22:%22" +
            PatientID +
            "%22}]}&page=1&perpage=1000&direction=asc"),
        headers: headers);

    // print(
    //     "Main URL ${AllURL.MainURL + AllURL.invoice_listing + "{%22responseField%22:%22%22,%22getDistinctValue%22:false,%22fields%22:[{%22FieldName%22:%22fromdate%22,%22FieldValue%22:%22Tue%20Jan%2012%202021%22},{%22FieldName%22:%22todate%22,%22FieldValue%22:" + cDateEnc + "},{%22FieldName%22:%22facilityid%22,%22FieldValue%22:%22e0749d4f-06a7-43b1-b6db-0ae393532cb3%22},{%22FieldName%22:%22statusType%22,%22FieldValue%22:%22%22},{%22FieldName%22:%22visitType%22,%22FieldValue%22:%22%22}]}&page=1&perpage=10&direction=asc"}");
    // log("List Response ${response.body}");

    if (response.statusCode == 200) {
      jsonResponse = response.body;
    } else {
      jsonResponse = "Error";
    }

    return jsonResponse;
  }

  //Appointment Visit Type API Call
  Future AppointmentVisitTypeDetails() async {
    String AccessToken = await Utility.readAccessToken('Token');
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL + AllURL.application_visitType),
        headers: headers);

    var resBody = response.body;
    // log("Status Response ${response.body}");
    return resBody;
  }

  Future<http.Response> generateBilling_Notification(String InvoiceID) async {
    String AccessToken = await Utility.readAccessToken('Token');
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL + AllURL.InvoicePDF_Generate + InvoiceID),
        headers: headers);
    return response;
  }

  Future<http.Response> getPDF_ofLabReport(String PDFID) async {
    String AccessToken = await Utility.readAccessToken('Token');
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    print("URL ${AllURL.MainURL + AllURL.GetPDF_LabReport + PDFID}");

    final response = await http.get(
        Uri.parse(AllURL.MainURL + AllURL.GetPDF_LabReport + PDFID),
        headers: headers);
    return response;
  }
}
