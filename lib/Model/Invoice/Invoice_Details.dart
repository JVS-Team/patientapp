import 'dart:convert';

import 'dart:developer';

class mainInvoiceService {
  final String visitNumberId;
  final List<SeriviceDetails> service_IDDetails;
  final List<InvoiceDetails> invoiceIDDetails;

  mainInvoiceService(
      {required this.visitNumberId,
      required this.service_IDDetails,
      required this.invoiceIDDetails});

  factory mainInvoiceService.fromJson(Map<String, dynamic> mainData) {
    var lst_ServiceIDS = mainData['invoiceServiceDetails'] as List;
    List<SeriviceDetails> lst_ServiceDetails =
        lst_ServiceIDS.map((i) => SeriviceDetails.fromJson(i)).toList();
    var lst_invoiceIDPayment = mainData['invoicePaymentDetails'] as List;
    List<InvoiceDetails> lst_InvoiceIDPayment =
        lst_invoiceIDPayment.map((i) => InvoiceDetails.fromJson(i)).toList();
    return new mainInvoiceService(
        visitNumberId: mainData['visitNumberId'].toString(),
        service_IDDetails: lst_ServiceDetails,
        invoiceIDDetails: lst_InvoiceIDPayment);
  }
}

class InvoiceService {
  final String totalAmount;
  final String serviceGroupName;

  InvoiceService({required this.totalAmount, required this.serviceGroupName});

  factory InvoiceService.fromJson(Map<String, dynamic> mainData) {
    return new InvoiceService(
      totalAmount: mainData['totalAmount'].toString(),
      serviceGroupName: mainData['serviceGroupName'].toString(),
    );
  }
}

class SeriviceDetails {
  final String id;
  final String serviceGroupName;
  final String totalAmount;
  final List<SeriviceItemDetails> service_Item_Details;

  SeriviceDetails(
      {required this.id,
      required this.serviceGroupName,
      required this.totalAmount,
      required this.service_Item_Details});

  factory SeriviceDetails.fromJson(Map<String, dynamic> mainJSON) {
    var lst_ServiceIDS = mainJSON['invoiceServiceItemDetails'] as List;
    List<SeriviceItemDetails> lst_ServiceDetails =
        lst_ServiceIDS.map((i) => SeriviceItemDetails.fromJson(i)).toList();
    return new SeriviceDetails(
        id: mainJSON['id'].toString(),
        serviceGroupName: mainJSON['serviceGroupName'].toString(),
        totalAmount: mainJSON['totalAmount'].toString(),
        service_Item_Details: lst_ServiceDetails);
  }
}

class SeriviceItemDetails {
  final String id;
  final String invoiceServiceDetailId;
  final String amount;
  final String serviceDate;
  final String serviceID;
  final String quantity;

  SeriviceItemDetails(
      {required this.id,
      required this.invoiceServiceDetailId,
      required this.amount,
      required this.serviceDate,
      required this.serviceID,
      required this.quantity});

  factory SeriviceItemDetails.fromJson(Map<String, dynamic> mainJSON) {
    return new SeriviceItemDetails(
        id: mainJSON['id'].toString(),
        invoiceServiceDetailId: mainJSON['invoiceServiceDetailId'].toString(),
        amount: mainJSON['amount'].toString(),
        serviceDate: mainJSON['serviceDate'].toString(),
        serviceID: mainJSON['serviceId'].toString(),
        quantity: mainJSON['quantity'].toString());
  }
}

class InvoiceDetails {
  final String currencyId;
  final String baseCurrencyId;
  final String receivedAmount;
  final String paymentType;
  final String base64String;

  InvoiceDetails(
      {required this.currencyId,
      required this.baseCurrencyId,
      required this.paymentType,
      required this.receivedAmount,
      required this.base64String});

  factory InvoiceDetails.fromJson(Map<String, dynamic> mainJSON) {
    return new InvoiceDetails(
        currencyId: mainJSON['currencyId'].toString(),
        baseCurrencyId: mainJSON['baseCurrencyId'].toString(),
        paymentType: mainJSON['paymentType'].toString(),
        receivedAmount: mainJSON['receivedAmount'].toString(),
        base64String: mainJSON['base64String'].toString());
  }
}
