class SectionPortlate {
  final List<SectionDetails> designAll;

  SectionPortlate({
    required this.designAll,
  });

  factory SectionPortlate.fromJson(List<dynamic> parsedJson) {
    List<SectionDetails> imagesList =
        parsedJson.map((i) => SectionDetails.fromJson(i)).toList();

    return new SectionPortlate(designAll: imagesList);
  }
}

class SectionDetails {
  final String id;
  final String name;
  final String displayName;

  SectionDetails(
      {required this.id,
      required this.name,
      required this.displayName});

  factory SectionDetails.fromJson(Map<String, dynamic> json) {
    return new SectionDetails(
      id: json['id'].toString(),
      name: json['name'].toString(),
      displayName: json['displayName'].toString(),
    );
  }
}
