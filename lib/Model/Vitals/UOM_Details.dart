class UOM_Details {
  final List<UOMMainDetails> designAll;

  UOM_Details({
    required this.designAll,
  });

  factory UOM_Details.fromJson(List<dynamic> parsedJson) {
    List<UOMMainDetails> imagesList =
        parsedJson.map((i) => UOMMainDetails.fromJson(i)).toList();

    return new UOM_Details(designAll: imagesList);
  }
}

class UOMMainDetails {
  final String id;
  final String name;
  final String displayName;

  UOMMainDetails(
      {required this.id,
      required this.name,
      required this.displayName});

  factory UOMMainDetails.fromJson(Map<String, dynamic> json) {
    return new UOMMainDetails(
      id: json['id'].toString(),
      name: json['name'].toString(),
      displayName: json['displayName'].toString(),
    );
  }
}
