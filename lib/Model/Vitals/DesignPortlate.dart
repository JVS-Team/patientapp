class DesignPortlate {
    final List<DesignDetails> designAll;

    DesignPortlate({
        required this.designAll,
    });

    factory DesignPortlate.fromJson(List<dynamic> parsedJson) {
        List<DesignDetails> imagesList =
        parsedJson.map((i) => DesignDetails.fromJson(i)).toList();

        return new DesignPortlate(designAll: imagesList);
    }
}

class DesignDetails {
    final String id;
    final String name;
    final String unitofMeasurement;
    final String displayName;

    DesignDetails(
        {required this.id,
            required this.name,
            required this.unitofMeasurement,
            required this.displayName});

    factory DesignDetails.fromJson(Map<String, dynamic> json) {
        return new DesignDetails(
            id: json['id'].toString(),
            name: json['name'].toString(),
            unitofMeasurement: json['unitofMeasurement'].toString(),
            displayName: json['displayName'].toString(),
        );
    }
}
