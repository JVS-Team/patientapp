import 'dart:convert';

import 'package:intl/intl.dart';

class VitalMainData {
  final List<Vital_AllList> AllVitalListDetails;

  VitalMainData({
    required this.AllVitalListDetails,
  });

  factory VitalMainData.fromJson(List<dynamic> parsedJson) {
    List<Vital_AllList> mainList =
        parsedJson.map((i) => Vital_AllList.fromJson(i)).toList();

    return new VitalMainData(AllVitalListDetails: mainList);
  }
}

class Vital_AllList {
  final String patientId;
  final String visitNumberId;
  final String vitalSignChartId;
  final String vitalSignChartDate;
  final String vitalSignChartTime;
  final String appointmentId;
  final String doctorId;
  final String statusString;
  final String id;
  final String name;
  final String status;
  final String displayName;
  final String code;
  final String tenantId;
  String vitalDateDisplay;
  final List<AllVitalSignRecordingDetails> vitalAll;

  Vital_AllList({
    required this.patientId,
    required this.visitNumberId,
    required this.vitalSignChartId,
    required this.vitalSignChartDate,
    required this.vitalSignChartTime,
    required this.appointmentId,
    required this.doctorId,
    required this.statusString,
    required this.id,
    required this.name,
    required this.status,
    required this.displayName,
    required this.code,
    required this.tenantId,
    required this.vitalAll,
    required this.vitalDateDisplay,
  });

  factory Vital_AllList.fromJson(Map<String, dynamic> json) {
    var lst_drug_order_details = json['vitalSignRecordingDetails'] as List;
    List<AllVitalSignRecordingDetails> vitalList = lst_drug_order_details
        .map((i) => AllVitalSignRecordingDetails.fromJson(i))
        .toList();
    String FinalDate = '';

    if (json['vitalSignChartDate'] != null) {
      DateTime signChartDate =
          DateFormat('yyyy-MM-ddTHH:mm:ss').parse(json['vitalSignChartDate']);

      FinalDate = DateFormat('dd MMM yyyy').format(signChartDate);
    }

    return new Vital_AllList(
        patientId: json['patientId'] ?? "",
        visitNumberId: json['visitNumberId'] ?? "",
        vitalSignChartId: json['vitalSignChartId'] ?? "",
        vitalSignChartDate: json['vitalSignChartDate'] ?? "",
        vitalSignChartTime: json['vitalSignChartTime'] ?? "",
        appointmentId: json['appointmentId'] ?? "",
        doctorId: json['doctorId'] ?? "",
        statusString: json['statusString'] ?? "",
        id: json['id'] ?? "",
        name: json['name'] ?? "",
        status: json['status'] ?? "",
        displayName: json['displayName'] ?? "",
        code: json['code'] ?? "",
        tenantId: json['tenantId'] ?? "",
        vitalAll: vitalList,
        vitalDateDisplay: FinalDate);
  }
}

class AllVitalSignRecordingDetails {
  final String id;
  final String patientVitalSignRecordingAndReviewingId;
  final String vitalSignRecordingId;
  final String vitalSignChartId;
  final String vitalSignChartDate;
  final String vitalSignChartTime;
  final String vitalSignParameterId;
  final String vitalSignParameterName;
  final String vitalValue;
  final String positions;
  final String measurementSite;
  final String source;
  final String rhythm;
  final String volume;
  final String character;
  final String oxygenSource;
  final String oxygenFlowRate;
  final String vitalSignChartSectionId;
  final int sequence;
  final String vitalSignParameter;

  AllVitalSignRecordingDetails({
    required this.id,
    required this.patientVitalSignRecordingAndReviewingId,
    required this.vitalSignRecordingId,
    required this.vitalSignChartId,
    required this.vitalSignChartDate,
    required this.vitalSignChartTime,
    required this.vitalSignParameterId,
    required this.vitalSignParameterName,
    required this.vitalValue,
    required this.positions,
    required this.measurementSite,
    required this.source,
    required this.rhythm,
    required this.volume,
    required this.character,
    required this.oxygenSource,
    required this.oxygenFlowRate,
    required this.vitalSignChartSectionId,
    required this.sequence,
    required this.vitalSignParameter,
  });

  factory AllVitalSignRecordingDetails.fromJson(Map<String, dynamic> data) {
    return AllVitalSignRecordingDetails(
      id: data['id'] ?? "",
      patientVitalSignRecordingAndReviewingId:
          data['patientVitalSignRecordingAndReviewingId'] ?? "",
      vitalSignRecordingId: data['vitalSignRecordingId'] ?? "",
      vitalSignChartId: data['vitalSignChartId'] ?? "",
      vitalSignChartDate: data['vitalSignChartDate'] ?? "",
      vitalSignChartTime: data['vitalSignChartTime'] ?? "",
      vitalSignParameterId: data['vitalSignParameterId'] ?? "",
      vitalSignParameterName: data['vitalSignParameterName'] ?? "",
      vitalValue: data['vitalValue'] ?? "",
      positions: data['positions'] ?? "",
      measurementSite: data['measurementSite'] ?? "",
      source: data['source'] ?? "",
      rhythm: data['rhythm'] ?? "",
      volume: data['volume'] ?? "",
      character: data['character'] ?? "",
      oxygenSource: data['oxygenSource'] ?? "",
      oxygenFlowRate: data['oxygenFlowRate'] ?? "",
      vitalSignChartSectionId: data['vitalSignChartSectionId'] ?? "",
      sequence: data['sequence'] ?? 0,
      vitalSignParameter: data['vitalSignParameter'] ?? "",
    );
  }
}
