import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;

import '../../Helper/AllURL.dart';

class Vital_API_CALL {
  // Vital Sign Check Details
  Future VitalSignCheckDetails(String AccessToken, String PatientID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL + AllURL.VitalCheckDetails + PatientID),
        headers: headers);

    log("Vital Check Details Response ${response.body}");
    var resBody = json.decode(response.body);
    return resBody;
  }

  // Vital Chart Configurations
  Future<Map> VitalAllSections(String AccessToken, String AllID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.VitalDesignSections +
            "%24Filter=id%20in(" +
            AllID +
            ")%20and%20Status%20eq%20%27ACTIVE%27&%24select=*"),
        headers: headers);

    log(AllURL.MainURL +
        AllURL.VitalDesignSections +
        "%24Filter=id%20in(" +
        AllID +
        ")%20and%20Status%20eq%20%27ACTIVE%27&%24select=*");

    // log(response.body);

    final jsonResponse = json.decode(response.body);
    return jsonResponse;
  }

  // Vital Chart Configurations
  Future VitalDesignData(String AccessToken, String AllID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.VitalDesignDetails +
            "%24Filter=id%20in(" +
            AllID +
            ")%20and%20Status%20eq%20%27ACTIVE%27&%24select=*"),
        headers: headers);

    log(AllURL.MainURL +
        AllURL.VitalDesignDetails +
        "%24Filter=id%20in(" +
        AllID +
        ")%20and%20Status%20eq%20%27ACTIVE%27&%24select=*");

    // log("VitalDesignData Response ${response.body}");

    final jsonResponse = json.decode(response.body);

    return jsonResponse;
  }

  // Vital Chart Configurations
  Future VitalUnitofMeasurement_UOM(String AccessToken) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.VitalUOM_Details +
            "%24Filter=uomTypeId%20eq%20%27VITALSIGNS%27%20and%20Status%20eq%20%27ACTIVE%27&%24select=displayname,id,name&%24orderby=displayname%20asc"),
        headers: headers);

    final jsonResponse = json.decode(response.body);

    return jsonResponse;
  }
}
