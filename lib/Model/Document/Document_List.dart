import 'dart:convert';

class DocumentAllListData {
  final List<Document_GetAllList> designAll;

  DocumentAllListData({
    required this.designAll,
  });

  factory DocumentAllListData.fromJson(List<dynamic> parsedJson) {
    List<Document_GetAllList> imagesList =
        parsedJson.map((i) => Document_GetAllList.fromJson(i)).toList();

    return new DocumentAllListData(designAll: imagesList);
  }
}

class Document_GetAllList {
  final String patientId;
  final String group;
  final String fileName;
  final String description;
  final String fileType;
  final String fileImage;
  final String filePath;
  final String filebase64;
  final String fileList;
  final String createdAt;
  final String id;
  final String name;
  final String status;
  final String displayName;
  final String code;
  final String tenantId;
  final String createdBy;
  String createdByName;

  Document_GetAllList({
    required this.patientId,
    required this.group,
    required this.fileName,
    required this.description,
    required this.fileType,
    required this.fileImage,
    required this.filePath,
    required this.filebase64,
    required this.fileList,
    required this.createdAt,
    required this.id,
    required this.name,
    required this.status,
    required this.displayName,
    required this.code,
    required this.tenantId,
    required this.createdBy,
    required this.createdByName,
  });

  factory Document_GetAllList.fromJson(Map<String, dynamic> json) {
    return new Document_GetAllList(
      patientId: json['patientId'].toString(),
      group: json['group'].toString(),
      fileName: json['fileName'].toString(),
      description: json['description'].toString() == "null"
          ? ""
          : json['description'].toString(),
      fileType: json['fileType'].toString(),
      fileImage: json['fileImage'].toString(),
      filePath: json['filePath'].toString(),
      filebase64: json['filebase64'].toString(),
      fileList: json['fileList'].toString(),
      createdAt: json['createdAt'].toString(),
      id: json['id'].toString(),
      name: json['name'].toString(),
      status: json['status'].toString(),
      displayName: json['displayName'].toString(),
      code: json['code'].toString(),
      tenantId: json['tenantId'].toString(),
      createdBy: json['createdBy'].toString(),
      createdByName: '',
    );
  }
}
