import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;

import '../../Helper/AllURL.dart';

class API_Documents {
  // List of All Documents
  Future Document_List_All(String PatientID, String AccessToken) async {
    var jsonResponse;
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL +
            AllURL.document_listing +
            "patientId=" +
            PatientID +
            "&page=1&perpage=50&direction=asc"),
        headers: headers);

    log("List Response ${json.decode(response.body)}");

    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
    } else {
      jsonResponse = "Error";
    }

    return jsonResponse;
  }

  // Document Group Details
  Future<Map> Basic_Info_Details(String AccessToken, String UserID) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Authorization": "Bearer $AccessToken",
    };

    final response = await http.get(
        Uri.parse(AllURL.MainURL + AllURL.basic_info_details + UserID),
        headers: headers);
    // print(AllURL.MainURL + AllURL.basic_info_details + UserID);
    final jsonResponse = json.decode(response.body);
    // log(jsonResponse);
    return jsonResponse;
  }
//
// // Document Save Data
// Future<http.Response> DocumentUploadSave(
//     String AccessToken, String Body) async {
//   Map<String, String> headers = {
//     "Content-Type": "application/json",
//     "Accept": "application/json",
//     "Authorization": "Bearer $AccessToken",
//   };
//   var response = await http.post(
//       Uri.parse(AllURL.MainURL + AllURL.DocumentUploadAPIPOST),
//       headers: headers,
//       body: Body);
//
//   return response;
// }
//
// // Document Delete Data
// Future<http.Response> DocumentDeleteData(
//     String AccessToken, String IDofRow) async {
//   Map<String, String> headers = {
//     "Content-Type": "application/json",
//     "Accept": "application/json",
//     "Authorization": "Bearer $AccessToken",
//   };
//   var response = await http.delete(
//       Uri.parse(
//           AllURL.MainURL + AllURL.DocumentUploadAPIPOST + "/" + IDofRow),
//       headers: headers);
//
//   return response;
// }
//
// // Document Update Data
// Future<http.Response> DocumentUpdateRow(
//     String AccessToken, String IDofRow, String Body) async {
//   Map<String, String> headers = {
//     "Content-Type": "application/json",
//     "Accept": "application/json",
//     "Authorization": "Bearer $AccessToken",
//   };
//   var response = await http.put(
//       Uri.parse(
//           AllURL.MainURL + AllURL.DocumentUploadAPIPOST + "/" + IDofRow),
//       headers: headers,
//       body: Body);
//   print(response);
//
//   return response;
// }
}
