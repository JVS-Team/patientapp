import 'package:flutter_svg/svg.dart';
import 'package:flutter/material.dart';
import 'package:softcliniccare/Helper/utils.dart';

class notification extends StatefulWidget {
  @override
  notificationsState createState() => notificationsState();
}

class notificationsState extends State<notification> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          toolbarHeight: Utility.getDeviceType() == '1' ? 50 : 70,
          centerTitle: true,
          backgroundColor: Colors.white,
          leading: IconButton(
            padding: EdgeInsets.fromLTRB(
                Utility.getDeviceType() == '1' ? 5 : 20,
                Utility.getDeviceType() == '1' ? 5 : 10,
                0,
                0),
            icon: SvgPicture.asset('Images/menu.svg',
                color: Colors.black,
                width: Utility.getDeviceType() == "1" ? 15 : 50,
                height: Utility.getDeviceType() == "1" ? 15 : 50),
            onPressed: () => {Scaffold.of(context).openDrawer()},
            //_scaffoldKey.currentState!.openDrawer()},
          ),
          actions: [
            Container(
                padding: Utility.getDeviceType() == '1'
                    ? EdgeInsets.fromLTRB(0, 0, 10, 0)
                    : EdgeInsets.fromLTRB(0, 5, 5, 0),
                height: Utility.getDeviceType() == '1' ? 50 : 70,
                width: Utility.getDeviceType() == '1' ? 50 : 70,
                child: CircleAvatar(
                  radius: Utility.getDeviceType() == '1' ? 55 : 70,
                  backgroundColor: Colors.transparent,
                  child: CircleAvatar(
                    radius: Utility.getDeviceType() == '1' ? 50 : 70,
                    backgroundColor: Colors.transparent,
                    backgroundImage: NetworkImage(
                        'https://vinusimages.co/wp-content/uploads/2018/10/EG7A2390.djpgA_.jpg'),
                  ),
                ))
          ],
          title: Image.asset(
            'Images/softclinic_logo.png',
            height: Utility.getDeviceType() == '1' ? 30 : 40,
            alignment: Alignment.centerRight,
          ),
        ),
        body: Container(
          // height: 400,
          padding: new EdgeInsets.all(10.0),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(10, 7, 0, 7),
                alignment: Alignment.centerLeft,
                child: Utility.MainNormalTextWithFont(
                    "Notification",
                    Utility.getDeviceType() == '1' ? 16 : 30,
                    Utility.PutLightBlueColor(),
                    'Poppins_Medium'),
              ),
              Container(
                height: 450,
                child: Center(
                  child: Text(
                    "No Notification Found",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
          // child: Center(
          //   child: Text(
          //     "Comming Soon",
          //     textAlign: TextAlign.center,
          //   ),
          // ),
        ),
      ),
    );
  }
}
