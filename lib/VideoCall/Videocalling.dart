import 'dart:async';
import 'package:collection/src/iterable_extensions.dart';
import 'package:flutter/material.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'package:softcliniccare/VideoCall/participant_widget.dart';
import 'package:softcliniccare/VideoCall/twilio_service.dart';
import 'package:twilio_programmable_video/twilio_programmable_video.dart';

import '../Helper/AllURL.dart';

class Videocalling extends StatefulWidget {
  Videocalling({Key? key, required this.AppointmentID}) : super(key: key);

  String AppointmentID;

  @override
  _VideocallingState createState() => _VideocallingState();
}

class _VideocallingState extends State<Videocalling> {
  List<ParticipantWidget> _participants = [];
  bool ConnectionStatus = true;
  var trackId;
  late Room _room;
  var enabled = true;
  var videotrackenabled = true;
  late CameraCapturer cameraCapturer;
  late List<StreamSubscription> _streamSubscriptions = [];
  late String name;
  late String token;
  late String identity;

  ParticipantWidget _buildParticipant({
    required Widget child,
    required String? id,
  }) {
    return ParticipantWidget(
      id: id,
      child: child,
    );
  }

  void initState() {
    super.initState();
    // TwilioFunctionsService().createToken("Patient").then((value) {
    //   name = value['roomName'].toString();
    //   token = value['accessToken'].toString();
    //   identity = value['identityName'].toString();
    //   connect();
    // });
    String AppointmentID = widget.AppointmentID;
    TwilioFunctionsService().createToken(AppointmentID).then((tokenDetails) {
      token = tokenDetails; // Twillio Token
      name = AppointmentID; // RoomName
      identity = AllURL.IdentityName; // Identity
      connect();
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        // appBar: Utility.TopToolBar(
        //     'Video Consultation',
        //     Utility.getDeviceType() == "1" ? 20 : 30,
        //     Utility.PutDarkBlueColor(),
        //     context),
        appBar: AppBar(
          toolbarHeight: Utility.getDeviceType() == "1" ? 50 : 80,
          titleSpacing: Utility.getDeviceType() == "1" ? -5 : 15,
          backgroundColor: Colors.white,
          leading: IconButton(
              padding: new EdgeInsets.fromLTRB(
                  Utility.getDeviceType() == "1" ? 10 : 20, 0, 0, 0),
              icon: Icon(
                Icons.arrow_back_ios,
                color: Utility.PutDarkBlueColor(),
                size: Utility.getDeviceType() == "1" ? 30 : 45,
              ),
              onPressed: () {
                if (ConnectionStatus == true) {
                } else {
                  Navigator.pop(context);
                }
              }),
          actions: [
            Container(
              padding: Utility.getDeviceType() == '1'
                  ? EdgeInsets.fromLTRB(0, 0, 10, 0)
                  : EdgeInsets.fromLTRB(0, 5, 5, 0),
              height: Utility.getDeviceType() == '1' ? 50 : 70,
              width: Utility.getDeviceType() == '1' ? 50 : 70,
            )
          ],
          title: Text("Video Consultation",
              style: TextStyle(
                  fontSize: Utility.getDeviceType() == "1" ? 20 : 30,
                  fontFamily: 'Poppins_SemiBold',
                  color: Utility.PutDarkBlueColor())),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: _participants.length == 0
                  ? showProgress()
                  : _buildOverlayLayout(),
            ),
          ],
        ),
      ),
    );
  }

  Widget showProgress() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Center(
            child: ConnectionStatus == true
                ? CircularProgressIndicator()
                : Container()),
        SizedBox(
          height: 10,
        ),
        ConnectionStatus == true
            ? Text(
                'Connecting to the room...',
                style: TextStyle(color: Colors.black),
              )
            : Container(
                height: 50,
                width: 170,
                child: TextButton(
                  style: TextButton.styleFrom(
                    shape: StadiumBorder(),
                    shadowColor: Colors.black,
                    textStyle: TextStyle(fontSize: 20),
                    primary: Colors.white,
                    backgroundColor: Utility.PutDarkBlueColor(),
                  ),
                  onPressed: () async {
                    setState(() {
                      enabled = true;
                      videotrackenabled = true;
                      ConnectionStatus = true;
                      String AppointmentID = widget.AppointmentID;
                      TwilioFunctionsService()
                          .createToken(AppointmentID)
                          .then((tokenDetails) {
                        token = tokenDetails; // Twillio Token
                        name = AppointmentID; // RoomName
                        identity = AllURL.IdentityName; // Identity
                        connect();
                      });
                    });
                  },
                  child: Text(
                    "Re-Connect",
                    style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Poppins_Regular',
                        fontSize: 18),
                  ),
                ),
              ),
      ],
    );
  }

  Widget _buildOverlayLayout() {
    return Stack(
      children: [
        _participants.length == 1 ? _participants[0] : _participants[0],
        Positioned.fill(
          child: Align(
              alignment: Alignment.topLeft,
              child: Row(
                children: [
                  Container(
                    width: _participants.length == 2 ? 120 : 0,
                    height: _participants.length == 2 ? 120 : 0,
                    child: _participants.length == 2 ? _participants[1] : null,
                  ),
                ],
              )),
        ),
        Container(
            padding: EdgeInsets.fromLTRB(0, 0, 10, 70),
            alignment: Alignment.bottomRight,
            child: Text(
              "",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontFamily: 'Poppins_Medium'),
            )),
        Positioned.fill(
          child: Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.black,
                    ),
                    child: IconButton(
                      color: Colors.red,
                      icon: Image.asset(
                        enabled == true
                            ? 'Images/unmute.png'
                            : 'Images/Mute.png',
                        width: 50,
                        height: 50,
                        color: Colors.white,
                      ),
                      onPressed: () async {
                        // final RemoteParticipant remoteParticipant = _room.remoteParticipants[0];
                        // toggleMute(remoteParticipant);
                        print("mute Click");
                        setState(() {
                          if (enabled == false) {
                            enabled = true;
                          } else {
                            enabled = false;
                          }
                          _room.localParticipant?.localAudioTracks
                              .forEach((remoteAudioTrackPublication) async {
                            final remoteAudioTrack =
                                remoteAudioTrackPublication.localAudioTrack;
                            if (remoteAudioTrack != null && enabled != null) {
                              await remoteAudioTrack.enable(enabled);
                            }
                          });
                        });
                      },
                    ),
                  ),
                  Container(
                    width: 70,
                    height: 70,
                    child: IconButton(
                      icon: Image.asset(
                        'Images/CallEnd.png',
                        width: 80,
                        height: 80,
                      ),
                      onPressed: () async {
                        disconnect();
                      },
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.black,
                    ),
                    child: IconButton(
                      color: Colors.red,
                      icon: Image.asset(
                        videotrackenabled == true
                            ? 'Images/Video.png'
                            : 'Images/video_close.png',
                        width: 50,
                        height: 50,
                        color: Colors.white,
                      ),
                      onPressed: () async {
                        setState(() {
                          if (videotrackenabled == false) {
                            videotrackenabled = true;
                          } else {
                            videotrackenabled = false;
                          }
                        });
                        _room.localParticipant?.localVideoTracks
                            .forEach((remoteAudioTrackPublication) async {
                          final remoteAudioTrack =
                              remoteAudioTrackPublication.localVideoTrack;
                          if (remoteAudioTrack != null &&
                              videotrackenabled != null) {
                            await remoteAudioTrack.enable(videotrackenabled);
                          }
                        });
                      },
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.black,
                    ),
                    child: IconButton(
                      color: Colors.red,
                      icon: Image.asset(
                        'Images/flipcamera.png',
                        width: 50,
                        height: 50,
                        color: Colors.white,
                      ),
                      onPressed: () async {
                        final sources = await CameraSource.getSources();
                        final source = sources.firstWhere((source) {
                          if (cameraCapturer.source!.isFrontFacing) {
                            return source.isBackFacing;
                          }
                          return source.isFrontFacing;
                        });
                        await cameraCapturer.switchCamera(source);
                      },
                    ),
                  ),
                ],
              )),
        ),
      ],
    );
  }

  connect() async {
    print('[ APPDEBUG ] ConferenceRoom.connect()');
    try {
      var cameraSources = await CameraSource.getSources();
      cameraCapturer = CameraCapturer(
        cameraSources.firstWhere((source) => source.isFrontFacing),
      );
      var localAudioTrack = LocalAudioTrack(true, 'default');
      print(localAudioTrack);
      bool isIOS = Theme.of(context).platform == TargetPlatform.iOS;
      if (isIOS) {
        await TwilioProgrammableVideo.disableAudioSettings();
      }

      var connectOptions = ConnectOptions(
        token,
        roomName: name,
        // Optional name for the room// Optional region.
        preferredAudioCodecs: [OpusCodec()],
        // Optional list of preferred AudioCodecs
        preferredVideoCodecs: [H264Codec()],
        audioTracks: [localAudioTrack],
        // Optional list of preferred VideoCodecs.// Optional list of audio tracks.
        dataTracks: [
          LocalDataTrack(
            DataTrackOptions(
                // Optional, Maximum number of retransmitted messages. Default is [DataTrackOptions.defaultMaxRetransmits]
                name: 'Demo' // Optional
                ), // Optional
          ),
        ],
        // Optional list of data tracks
        videoTracks: [
          LocalVideoTrack(true, cameraCapturer)
        ], // Optional list of video tracks.
      );
      _room = await TwilioProgrammableVideo.connect(connectOptions);
      _streamSubscriptions.add(_room.onConnected.listen(_onConnected));
      _streamSubscriptions.add(_room.onDisconnected.listen(_onDisconnected));
      _streamSubscriptions.add(_room.onReconnecting.listen(_onReconnecting));
      _streamSubscriptions
          .add(_room.onConnectFailure.listen(_onConnectFailure));
      _streamSubscriptions
          .add(cameraCapturer.onCameraSwitched!.listen(_onCameraSwitched));
    } catch (err) {
      print('[ APPDEBUG ] $err');
      rethrow;
    }
  }

  Future _onCameraSwitched(CameraSwitchedEvent event) async {}

  Future<void> toggleMute(RemoteParticipant remoteParticipant) async {
    final enabled = await remoteParticipant
        .remoteAudioTracks.first.remoteAudioTrack!
        .isPlaybackEnabled();
    remoteParticipant.remoteAudioTracks
        .forEach((remoteAudioTrackPublication) async {
      final remoteAudioTrack = remoteAudioTrackPublication.remoteAudioTrack;
      if (remoteAudioTrack != null && enabled != null) {
        await remoteAudioTrack.enablePlayback(!enabled);
      }
    });
    var index = _participants.indexWhere((ParticipantWidget participant) =>
        participant.id == remoteParticipant.sid);
    if (index < 0) {
      return;
    }
  }

  Future<void> disconnect() async {
    print('[ APPDEBUG ] ConferenceRoom.disconnect()');
    await _room.disconnect();
    setState(() {
      _participants = [];
      ConnectionStatus = false;
    });
    //Navigator.of(context).pop();
  }

  void _onDisconnected(RoomDisconnectedEvent event) {
    print('[ APPDEBUG ] ConferenceRoom._onDisconnected');
  }

  void _onReconnecting(RoomReconnectingEvent room) {
    print('[ APPDEBUG ] ConferenceRoom._onReconnecting');
  }

  void _onConnected(Room room) {
    print('[ APPDEBUG ] ConferenceRoom._onConnected => state: ${room.state}');

    // When connected for the first time, add remote participant listeners
    _streamSubscriptions
        .add(_room.onParticipantConnected.listen(_onParticipantConnected));
    _streamSubscriptions.add(
        _room.onParticipantDisconnected.listen(_onParticipantDisconnected));
    final localParticipant = room.localParticipant;
    if (localParticipant == null) {
      print(
          '[ APPDEBUG ] ConferenceRoom._onConnected => localParticipant is null');
      return;
    }

    // Only add ourselves when connected for the first time too.
    _participants.add(_buildParticipant(
        child: localParticipant.localVideoTracks[0].localVideoTrack.widget(),
        id: identity));

    for (final remoteParticipant in room.remoteParticipants) {
      var participant = _participants.firstWhereOrNull(
          (participant) => participant.id == remoteParticipant.sid);
      if (participant == null) {
        print(
            '[ APPDEBUG ] Adding participant that was already present in the room ${remoteParticipant.sid}, before I connected');
        _addRemoteParticipantListeners(remoteParticipant);
      }
    }
    reload();
  }

  void _onConnectFailure(RoomConnectFailureEvent event) {
    print('[ APPDEBUG ] ConferenceRoom._onConnectFailure: ${event.exception}');
  }

  void _onParticipantConnected(RoomParticipantConnectedEvent event) {
    print(
        '[ APPDEBUG ] ConferenceRoom._onParticipantConnected, ${event.remoteParticipant.sid}');
    _addRemoteParticipantListeners(event.remoteParticipant);
    reload();
  }

  void _onParticipantDisconnected(RoomParticipantDisconnectedEvent event) {
    print(
        '[ APPDEBUG ] ConferenceRoom._onParticipantDisconnected: ${event.remoteParticipant.sid}');
    _participants.removeWhere(
        (ParticipantWidget p) => p.id == event.remoteParticipant.sid);
    disconnect();
    reload();
  }

  void _addRemoteParticipantListeners(RemoteParticipant remoteParticipant) {
    _streamSubscriptions.add(remoteParticipant.onVideoTrackSubscribed
        .listen(_addOrUpdateParticipant));
    _streamSubscriptions.add(remoteParticipant.onAudioTrackSubscribed
        .listen(_addOrUpdateParticipant));
  }

  void _addOrUpdateParticipant(RemoteParticipantEvent event) {
    print(
        '[ APPDEBUG ] ConferenceRoom._addOrUpdateParticipant(), ${event.remoteParticipant.sid}');
    final participant = _participants.firstWhereOrNull(
      (ParticipantWidget participant) =>
          participant.id == event.remoteParticipant.sid,
    );

    if (participant != null) {
      print(
          '[ APPDEBUG ] Participant found: ${participant.id}, updating A/V enabled values');
    } else {
      if (event is RemoteVideoTrackSubscriptionEvent) {
        print(
            '[ APPDEBUG ] New participant, adding: ${event.remoteParticipant.sid}');
        _participants.insert(
          0,
          _buildParticipant(
            child: event.remoteVideoTrack.widget(),
            id: event.remoteParticipant.sid,
          ),
        );
        reload();
      }
    }
  }

  reload() {
    setState(() {
      print('Total Participants : ${_participants.length}');
    });
  }
}
