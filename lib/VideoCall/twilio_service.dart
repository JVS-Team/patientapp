import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../Helper/AllURL.dart';
import '../Helper/utils.dart';

class TwilioFunctionsService {
  final http.Client client = http.Client();
  // final accessTokenUrl =
  //     'https://acesstoken-8940.twil.io/videograntToken';
  final accessTokenUrl = AllURL.MainURL + AllURL.TwillioVideoCalling;

  Future<String> createToken(String Roomname) async {
    // String identityName = AllURL.MainURL + AllURL.IdentityName;
    try {
      String AccessToken = await Utility.readAccessToken('Token');
      Map<String, String> header = {
        "Accept": "application/json",
        "Authorization": "Bearer $AccessToken",
      };
      print(accessTokenUrl);
      print(Roomname);
      var url = Uri.parse(accessTokenUrl);
      // final response = await client.post(url,
      //     body: {
      //       'identityname': AllURL.IdentityName,
      //       'appointmentid': Roomname,
      //     },
      //     headers: header);
      final response = await client.post(url,
          body: {
            'identityname': AllURL.IdentityName,
            'appointmentid': Roomname,
          },
          headers: header);
      // print(response.statusCode);
      String TwillioToken = jsonDecode(response.body);
      // Map<String, dynamic> responseMap = jsonDecode(response.body);
      print("Main AccessToken $TwillioToken");
      return TwillioToken;
    } catch (error) {
      throw Exception([error.toString()]);
    }
  }
}
