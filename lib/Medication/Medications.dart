import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:softcliniccare/Helper/utils.dart';
import 'package:softcliniccare/Model/Medication/Medication_MainList.dart';

import '../Model/Medication/Freq_DetailsMaster.dart';
import '../Model/Medication/Medi_Drug_Listing.dart';
import '../Model/Medication/Medication_API.dart';
import '../Model/Medication/Medication_DrugName.dart';
import '../Model/Medication/ROA_DetailsMaster.dart';

class Medications extends StatefulWidget {
  @override
  MedicationState createState() => MedicationState();
}

class MedicationState extends State<Medications> {
  // Default Drop Down Item.

  String checkdevice = '';
  List<MedicationGetListing> medicationGetAllList = [];
  List<DrugName> lst_DrugMainName = [];

  ScrollController _myController = ScrollController();

  List<MainListData> lst_MainList_DrugDetails = [];

  // List Frequency Details
  List<Freq_Details> listing_Freq_Details = [];

  // List ROA Details
  List<ROA_Details> listing_ROA_Details = [];

  // Drug Listing Details
  List<String> medicationNameID = [];

  GlobalKey<State> _MedicationList = new GlobalKey<State>();

  // List<String> medicationDrugName = [];

  @override
  void initState() {
    checkdevice = Utility.getDeviceType();
    super.initState();
    Medication_AllFrequency_Master();
    Medication_ROA_Master();
    MedicationListAPI();
  }

  void MedicationListAPI() async {
    String AccessToken = await Utility.readAccessToken('Token');
    String PatientID = await Utility.readAccessToken('PatientID');
    Utility.showLoadingDialog(context, _MedicationList, true);
    API_Medication()
        .Medication_List_DrugName(PatientID, AccessToken)
        .then((Response) async {
      // Map FinalResponse = json.decode(Response);

      if (Response == "Error") {
        Navigator.pop(context);
      } else {
        var tempdata = Response;

        for (int i = 0; i < tempdata.length; i++) {
          medicationGetAllList.add(MedicationGetListing.fromJson(tempdata[i]));
        }
        medicationGetAllList.sort((a, b) => b.orderDate.compareTo(a.orderDate));

        for (int k = 0; k < medicationGetAllList.length; k++) {
          for (int l = 0;
              l < medicationGetAllList[k].drug_Order_Details.length;
              l++) {
            if (medicationGetAllList[k].drug_Order_Details[l].moduleName ==
                "Drugs") {
              medicationNameID
                  .add(medicationGetAllList[k].drug_Order_Details[l].moduleId);
            }
          }
        }

        if (medicationNameID.length <= 0) {
          Navigator.pop(context);
        } else {
          String allIDDisplay = medicationNameID.join(',');
          Medications_AllDrugName(allIDDisplay);
        }
      }
    });
  }

  void Medication_AllFrequency_Master() async {
    String AccessToken = await Utility.readAccessToken('Token');

    API_Medication()
        .Medication_Freq_MasterList(AccessToken)
        .then((Response) async {
      setState(() {
        var tempdata = Response['value'];

        for (int i = 0; i < tempdata.length; i++) {
          listing_Freq_Details.add(Freq_Details.fromJson(tempdata[i]));
        }
      });
    });
  }

  void Medication_ROA_Master() async {
    String AccessToken = await Utility.readAccessToken('Token');

    API_Medication()
        .Medication_ROA_MasterList(AccessToken)
        .then((Response) async {
      setState(() {
        var tempdata = Response['value'];
        for (int i = 0; i < tempdata.length; i++) {
          listing_ROA_Details.add(ROA_Details.fromJson(tempdata[i]));
        }
      });
    });
  }

  void Medications_AllDrugName(String AllID_DrugName) async {
    String AccessToken = await Utility.readAccessToken('Token');
    API_Medication()
        .Medication_DrugName(AccessToken, AllID_DrugName)
        .then((Response) async {
      setState(() {
        MedicationDrugName sectionDetails =
            new MedicationDrugName.fromJson(Response['value']);
        lst_DrugMainName = [];
        lst_DrugMainName = sectionDetails.designAll;
        Navigator.pop(context);
        // For Drug Name
        for (int k = 0; k < medicationGetAllList.length; k++) {
          for (int l = 0;
              l < medicationGetAllList[k].drug_Order_Details.length;
              l++) {
            for (int m = 0; m < lst_DrugMainName.length; m++) {
              if (lst_DrugMainName[m].id ==
                  medicationGetAllList[k].drug_Order_Details[l].moduleId) {
                medicationGetAllList[k].drug_Order_Details[l].DrugName =
                    lst_DrugMainName[m].displayName;

                DateTime mediStartDate = DateFormat('yyyy-MM-dd')
                    .parse(medicationGetAllList[k].orderDate);

                String FrequencyID = medicationGetAllList[k]
                    .drug_Order_Details[l]
                    .orderDetails
                    .frequency;
                String FrequencyName = "";

                for (int i = 0; i < listing_Freq_Details.length; i++) {
                  if (listing_Freq_Details[i].id == FrequencyID) {
                    FrequencyName = listing_Freq_Details[i].displayName;
                  }
                }

                String ROA_ID = medicationGetAllList[k]
                    .drug_Order_Details[l]
                    .orderDetails
                    .routeOfAdministration;
                String str_ROACodeName = "";

                for (int i = 0; i < listing_ROA_Details.length; i++) {
                  if (listing_ROA_Details[i].id == ROA_ID) {
                    str_ROACodeName = listing_ROA_Details[i].displayName;
                  }
                }

                String FinalDate =
                    DateFormat('dd MMM yyyy').format(mediStartDate);

                lst_MainList_DrugDetails.add(MainListData.fromJson({
                  "drugName":
                      medicationGetAllList[k].drug_Order_Details[l].DrugName,
                  "date": FinalDate,
                  "days": medicationGetAllList[k]
                      .drug_Order_Details[l]
                      .orderDetails
                      .durationDays,
                  "Frequency": FrequencyName,
                  "ROAName": str_ROACodeName,
                  "specialInstructions": medicationGetAllList[k]
                      .drug_Order_Details[l]
                      .orderDetails
                      .specialInstructions
                }));
              }
            }
          }
        }

        // lst_MainList_DrugDetails = [];
        // lst_MainList_DrugDetails = mainListDetails.designAll;
      });
    });
  }

  void CloseDialogBox() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => Utility.Willpopscope,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: Utility.TopToolBar(Utility.translate('medications'),
            checkdevice == "1" ? 20 : 30, Utility.PutDarkBlueColor(), context),
        body: Column(
          children: [
            Expanded(
              child: Container(
                margin: const EdgeInsets.all(10.0),
                // decoration: BoxDecoration(
                //     borderRadius: BorderRadius.all(Radius.circular(15.0)),
                //     border: Border.all(color: Colors.black26)),
                child: lst_MainList_DrugDetails.length == 0
                    ? Container(
                        child: Center(
                          child: Text('No Medication Found'),
                        ),
                      )
                    : ListView.builder(
                        controller: _myController,
                        itemCount: lst_MainList_DrugDetails.length,
                        itemBuilder: (context, pos) {
                          return Container(
                            padding: EdgeInsets.all(5),
                            child: Card(
                              color: Colors.white,
                              elevation: 5,
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: checkdevice == "1" ? 10 : 20,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      SizedBox(
                                        width: checkdevice == "1" ? 8 : 18,
                                      ),
                                      Flexible(
                                          flex: 10,
                                          child: Column(
                                            children: [
                                              Column(
                                                children: [
                                                  Align(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    child: Text(
                                                      lst_MainList_DrugDetails[
                                                              pos]
                                                          .drugName,
                                                      maxLines: 2,
                                                      style: TextStyle(
                                                          fontSize:
                                                              checkdevice == "1"
                                                                  ? 16
                                                                  : 26,
                                                          fontFamily:
                                                              'Poppins_SemiBold',
                                                          color: Utility
                                                              .PutDarkBlueColor()),
                                                    ),
                                                  ),
                                                  // SizedBox(
                                                  //   width: checkdevice == "1"
                                                  //       ? 5
                                                  //       : 15,
                                                  // ),
                                                  // Text(
                                                  //   '',
                                                  //   maxLines: 1,
                                                  //   style: TextStyle(
                                                  //       fontSize:
                                                  //           checkdevice == "1"
                                                  //               ? 12
                                                  //               : 18,
                                                  //       fontFamily:
                                                  //           'Poppins_Regular',
                                                  //       color: Utility
                                                  //           .PutDarkBlueColor()),
                                                  // ),
                                                ],
                                              ),
                                              SizedBox(
                                                height:
                                                    checkdevice == "1" ? 5 : 15,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  Expanded(
                                                    child: Row(
                                                      children: [
                                                        Container(
                                                          child: Image.asset(
                                                            'Images/Mcalendar.png',
                                                            height:
                                                                checkdevice ==
                                                                        "1"
                                                                    ? 30
                                                                    : 40,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width:
                                                              checkdevice == "1"
                                                                  ? 8
                                                                  : 18,
                                                        ),
                                                        Text(
                                                          lst_MainList_DrugDetails[
                                                                  pos]
                                                              .date,
                                                          textAlign:
                                                              TextAlign.left,
                                                          style: TextStyle(
                                                            fontSize:
                                                                checkdevice ==
                                                                        "1"
                                                                    ? 14
                                                                    : 24,
                                                            fontFamily:
                                                                'Poppins_Regular',
                                                            color: Utility
                                                                .PutDarkBlueColor(),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: Row(
                                                      children: [
                                                        Container(
                                                          child: Image.asset(
                                                            'Images/Mcounter.png',
                                                            height:
                                                                checkdevice ==
                                                                        "1"
                                                                    ? 30
                                                                    : 40,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width:
                                                              checkdevice == "1"
                                                                  ? 8
                                                                  : 18,
                                                        ),
                                                        Text(
                                                          lst_MainList_DrugDetails[
                                                                      pos]
                                                                  .days +
                                                              ' Days',
                                                          textAlign:
                                                              TextAlign.left,
                                                          style: TextStyle(
                                                            fontSize:
                                                                checkdevice ==
                                                                        "1"
                                                                    ? 14
                                                                    : 24,
                                                            fontFamily:
                                                                'Poppins_Regular',
                                                            color: Utility
                                                                .PutDarkBlueColor(),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height:
                                                    checkdevice == "1" ? 5 : 15,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  Expanded(
                                                    child: Row(
                                                      children: [
                                                        Container(
                                                          child: Image.asset(
                                                            'Images/Mlearning.png',
                                                            height:
                                                                checkdevice ==
                                                                        "1"
                                                                    ? 30
                                                                    : 40,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width:
                                                              checkdevice == "1"
                                                                  ? 8
                                                                  : 18,
                                                        ),
                                                        Text(
                                                          lst_MainList_DrugDetails[
                                                                  pos]
                                                              .Frequency,
                                                          textAlign:
                                                              TextAlign.left,
                                                          style: TextStyle(
                                                            fontSize:
                                                                checkdevice ==
                                                                        "1"
                                                                    ? 14
                                                                    : 24,
                                                            fontFamily:
                                                                'Poppins_Regular',
                                                            color: Utility
                                                                .PutDarkBlueColor(),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: Row(
                                                      children: [
                                                        Container(
                                                          child: Image.asset(
                                                            'Images/Mvaccine.png',
                                                            height:
                                                                checkdevice ==
                                                                        "1"
                                                                    ? 30
                                                                    : 40,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width:
                                                              checkdevice == "1"
                                                                  ? 8
                                                                  : 18,
                                                        ),
                                                        Text(
                                                          lst_MainList_DrugDetails[
                                                                  pos]
                                                              .ROAName,
                                                          textAlign:
                                                              TextAlign.left,
                                                          style: TextStyle(
                                                            fontSize:
                                                                checkdevice ==
                                                                        "1"
                                                                    ? 14
                                                                    : 24,
                                                            fontFamily:
                                                                'Poppins_Regular',
                                                            color: Utility
                                                                .PutDarkBlueColor(),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height:
                                                    checkdevice == "1" ? 5 : 15,
                                              ),
                                              if (lst_MainList_DrugDetails[pos]
                                                      .specialInstructions !=
                                                  "")
                                                Row(
                                                  children: [
                                                    Container(
                                                      child: Image.asset(
                                                        'Images/Messay.png',
                                                        height:
                                                            checkdevice == "1"
                                                                ? 30
                                                                : 40,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: checkdevice == "1"
                                                          ? 8
                                                          : 18,
                                                    ),
                                                    Flexible(
                                                      child: Text(
                                                        lst_MainList_DrugDetails[
                                                                pos]
                                                            .specialInstructions,
                                                        textAlign:
                                                            TextAlign.left,
                                                        style: TextStyle(
                                                          fontSize:
                                                              checkdevice == "1"
                                                                  ? 14
                                                                  : 24,
                                                          fontFamily:
                                                              'Poppins_Regular',
                                                          color: Utility
                                                              .PutDarkBlueColor(),
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: checkdevice == "1"
                                                          ? 8
                                                          : 18,
                                                    ),
                                                  ],
                                                ),
                                              SizedBox(
                                                width:
                                                    checkdevice == "1" ? 5 : 50,
                                              ),
                                            ],
                                          )),
                                    ],
                                  ),
                                  SizedBox(
                                    height: checkdevice == "1" ? 5 : 15,
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
