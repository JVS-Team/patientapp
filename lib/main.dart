import 'dart:developer';
import 'dart:io';
import 'package:firebase_core/firebase_core.dart';

// import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

// import 'package:notification_permissions/notification_permissions.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:softcliniccare/Home/Home_screen.dart';
import 'dart:async';
import 'Helper/Constant_Data.dart';
import 'Helper/Database/sqflite_helper.dart';
import 'Helper/utils.dart';
import 'Home/main_screen.dart';
import 'Login/Login_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_messageHandler);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Splash Screen'),
      routes: {
        'home': (context) => homescreen(),
        //'anotherPage': (context) => AnotherPage(),
      },
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  var permGranted = "granted";
  var permDenied = "denied";
  var permUnknown = "unknown";
  var permProvisional = "provisional";

  @override
  _MyHomePageState createState() => _MyHomePageState();

// Future<String> getCheckNotificationPermStatus() {
//   return NotificationPermissions.getNotificationPermissionStatus()
//       .then((status) {
//     switch (status) {
//       case PermissionStatus.denied:
//         return permDenied;
//       case PermissionStatus.granted:
//         return permGranted;
//       case PermissionStatus.unknown:
//         return permUnknown;
//       case PermissionStatus.provisional:
//         return permProvisional;
//       default:
//         return permDenied;
//     }
//   });
// }
}

Future<void> _messageHandler(RemoteMessage message) async {
  print('background message ${message.notification!.body}');
}

class _MyHomePageState extends State<MyHomePage> {
  int _start = 1;
  String _message = '';
  late FirebaseMessaging _firebaseMessaging;
  late Future<String> permissionStatusFuture;

  // FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;
  List<Map<String, dynamic>> allDateDetails = [];

  @override
  void initState() {
    super.initState();
    Utility.load();
    DetailsOfAppInfo();
    Firebase.initializeApp();
    // initDynamicLinks();
    _firebaseMessaging = FirebaseMessaging.instance;
    // if (Platform.isIOS) {
    //   _firebaseMessaging.requestPermission();
    // }
    //
    _firebaseMessaging.getToken().then((value) {
      print("FirebaseMessaging token: $value");
      // Utility.SaveAllLangData('Token',value!);
    });
    //
    // permissionStatusFuture = widget.getCheckNotificationPermStatus();
    // print('notification status ${permissionStatusFuture}');
    //
    // FirebaseMessaging.onMessage.listen((RemoteMessage event) {
    //   print("message recieved");
    //   print(event.notification!.body);
    // });
    //
    // FirebaseMessaging.onMessageOpenedApp.listen((message) {
    //   print('Message clicked!');
    // });

    const oneSec = const Duration(seconds: 1);
    Timer _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
            Navigator.pop(context);
            getAccessTokenData();
          });
        } else {
          setState(() {
            _start--;
            print(_start);
          });
        }
      },
    );
  }

  void DetailsOfAppInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    ConstantDetails.vAppName = packageInfo.appName;
    ConstantDetails.vAppPackageName = packageInfo.packageName;
    ConstantDetails.vAppVersionNo = packageInfo.version;
    ConstantDetails.vAppBuilderNo = packageInfo.buildNumber;

    print("Version No ${packageInfo.version}");
  }

  void getMessage() {
    // _firebaseMessaging.configure(
    //     onMessage: (Map<String, dynamic> message) async {
    //       print('on message $message');
    //       setState(() => _message = message["notification"]["title"]);
    //     }, onResume: (Map<String, dynamic> message) async {
    //   print('on resume $message');
    //   setState(() => _message = message["notification"]["title"]);
    // }, onLaunch: (Map<String, dynamic> message) async {
    //   print('on launch $message');
    //   setState(() => _message = message["notification"]["title"]);
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image(
        image: AssetImage('Images/splash_screen.png'),
        fit: BoxFit.fill,
      ),
    );
  }

  // Future<void> initDynamicLinks() async {
  //   dynamicLinks.onLink.listen((dynamicLinkData) {
  //     // Navigator.pushNamed(context, dynamicLinkData.link.path);
  //     print('on Link');
  //     print(dynamicLinkData.link);
  //     print(dynamicLinkData.link.queryParameters["appointmentid"]);
  //     print(dynamicLinkData.link.queryParameters["videostart"]);
  //   }).onError((error) {
  //     print('onLink error');
  //     print(error.message);
  //   });
  // }

  void getAccessTokenData() async {
    String AccessToken = await Utility.readAccessToken('Token');
    if (AccessToken != 'null') {
      log('Access if - $AccessToken');

      Map<String, dynamic> decodedToken = JwtDecoder.decode(AccessToken);
      String employeeID = decodedToken['employeeId'];
      print('Access Decode - $employeeID');

      String yourToken = AccessToken;
      bool hasExpired = JwtDecoder.isExpired(yourToken);

      DateTime expirationDate = JwtDecoder.getExpirationDate(yourToken);
      DateTime currentDateTime = DateTime.now();

      // DateTime currentDate = DateTime.now();
      // int value = daysBetween(currentDate,expirationDate);
      //
      print('Access Expired Date - $expirationDate');
      print('Access Current Date - $currentDateTime');
      print('Access Expired - $hasExpired');

      if (hasExpired) {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => LoginPage(isLogoutClick: false)),
        );
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => MainScreen()),
        );
        // Navigator.push(
        //   context,
        //   MaterialPageRoute(builder: (context) => LoginPage()),
        // );
      }
    } else {
      print('Access else - $AccessToken');
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => LoginPage(isLogoutClick: false)),
      );
    }
    // Date and Time Data will be check here it is or not
    _FetchMainDateDetails();
  }

  // This function is used to fetch all data from the database
  void _FetchMainDateDetails() async {
    final data = await SQLHelper.getDateTimeDetails();
    allDateDetails = data;
    print("Fetch Date ${allDateDetails}");

    if (allDateDetails.length > 0) {
      print("Date Length Greater");
      print("Date ${allDateDetails.elementAt(0)['Date']}");

      DateTime dbDate = DateTime.parse(allDateDetails.elementAt(0)['Date']);
      DateTime currentDate = DateTime.now();

      int daysDetails = daysBetween(dbDate, currentDate);
      print("Days Diff ${daysDetails}");

      if (daysDetails > 0) {
        SQLHelper.deleteAll();

        String cdate = DateFormat("yyyy-MM-dd").format(DateTime.now());
        String tTime = DateFormat("HH:mm").format(DateTime.now());
        VoidDateSave(cdate, tTime, "DateTime");

        List<Map<String, dynamic>> allDateDetails = [];

        final data = await SQLHelper.getDateTimeDetails();
        allDateDetails = data;
        print("Fetch Date ${allDateDetails}");
      } else {}
    } else {
      print("Date Length Less");

      String cdate = DateFormat("yyyy-MM-dd").format(DateTime.now());
      String tTime = DateFormat("HH:mm").format(DateTime.now());
      VoidDateSave(cdate, tTime, "DateTime");
    }
  }

  void VoidDateSave(String Date, String Time, String DateDetails) async {
    await SQLHelper.insertMainDateTime(Date, Time, DateDetails);
  }

  int daysBetween(DateTime from, DateTime to) {
    from = DateTime(from.year, from.month, from.day);
    to = DateTime(to.year, to.month, to.day);
    return (to.difference(from).inHours / 24).round();
  }
}
